-module(exercise3_5).
-export([filter/2,reverse/1,concatenate/1]).

filter(List,Int) ->
	[X || X<-List, X=<Int].

reverse([]) -> [];
reverse([Head | Tail]) ->
	reverse(Tail) ++ [Head].

concatenate(ListOfLists) ->
	lists:foldr(fun(A,B) -> A++B end, [], ListOfLists).

% do flatten of nested lists