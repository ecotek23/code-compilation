% Testing ground for erlang experimentation
-module(test).
-compile([export_all]).

% Factorial functions
factorial(0) -> 1;
factorial(Int) ->
  Int*factorial(Int-1).
  
factorial2(Int) ->
	product(range(1,Int)).

% List of integers from A to B
range(A,B) when B<A -> [];
range(A,B) when A<B ->
	[A]++range(A+1,B);
range(A,A) -> [A].

% Multiply every number in a list by a given number
multiplication([],_) -> done;
multiplication([X|XS],Int) -> io:format("Number:~p~n",[X*Int]), multiplication(XS,Int).

% Add one to all numbers in a list
addOne([]) -> [];
addOne([H|T]) -> [H+1|addOne(T)].

addOne2(List) ->
	lists:map(fun(A) -> A+1 end,List).

%Filter all odd numbers out of a list
evenFilter([]) -> [];
evenFilter([Head|Tail]) -> case Head rem 2 of
				0 -> [Head | evenFilter(Tail)];
				1 -> evenFilter(Tail)
                           end.

evenFilter2(List) ->
	lists:filter(fun(A) -> A rem 2 == 0 end,List).

% Average of a list using tail recursion
tailAverageHelp([],Sum,Length) -> Sum/Length;
tailAverageHelp([Head|Tail],Sum,Length) -> tailAverageHelp(Tail,Sum+Head,Length+1).
tailAverage(List) -> tailAverageHelp(List,0,0).

% Sum of a list in several different ways
sum([]) -> 0;
sum([Head|Tail]) -> Head + sum(Tail).

sum2([]) -> 0;
sum2(List) -> hd(List) + sum(tl(List)).

sum3(List) ->
	lists:foldr(fun(A,B) -> A+B end, 0, List).

% Product of a list
product([]) -> 1;
product([Head|Tail]) ->
	Head*product(Tail).

product2(List) ->
	lists:foldr(fun(A,B)->A*B end, 1, List).

% Length of a list
len([]) -> 0;
len([_|Tail]) -> 1+len(Tail).

% Sums of squares
sumsq(List) ->
	lists:foldr(fun(A,B) -> math:pow(A,2)+B end,0,List).

% Dot product
dotProduct([],[]) -> 0;
dotProduct([Head1|Tail1],[Head2|Tail2]) when length(Tail1)==length(Tail2) ->
	Head1*Head2+dotProduct(Tail1,Tail2).

% Vector magnitude
norm(Vec) ->
	math:sqrt(dotProduct(Vec,Vec)).

% Angle between vectors
angle(Vec1,Vec2) ->
	math:acos(dotProduct(Vec1,Vec2)/(norm(Vec1)*norm(Vec2))).

% Perpendicular vectors
perp(Vec1,Vec2) ->
	case dotProduct(Vec1,Vec2) of
		0 -> true;
		_otherwise -> false
	end.

% Binomial coefficients
choose(N,K) ->
	factorial(N) div (factorial(N-K)*factorial(K)).

% Check if a list is a permutation of another list
isPermutation([],[]) -> true;
isPermutation(Base,[PermutationHead|PermutationTail]) ->
	case lists:any(fun(A) -> A==PermutationHead end,Base) of
		true -> isPermutation(lists:delete(PermutationHead,Base),PermutationTail);
		false -> false
	end.

% Reverse a list
reverse([])->[];
reverse([Head|Tail]) -> reverse(Tail) ++ [Head].

% Index address a list
elementAt([],_) -> {error,insuficcient_length};
elementAt([Head|_],1) -> Head;
elementAt([_|Tail],K) when K>0 -> elementAt(Tail,K-1).
