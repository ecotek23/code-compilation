-module(exercise3_3).
-export([rangePrinter/1,evenRangePrinter/1]).

rangePrinter(N) ->
	helpRangePrinter(1,N).
helpRangePrinter(A,B) when A<B ->
	io:format("Number:~p~n",[A]), helpRangePrinter(A+1,B);
helpRangePrinter(A,A) ->
	io:format("Number:~p~n",[A]).
	
helpEvenPrinter(A,B) when A<B ->
	case A rem 2 of
		0 -> io:format("Number:~p~n",[A]), helpEvenPrinter(A+1,B);
		1 -> helpEvenPrinter(A+1,B)
	end;
helpEvenPrinter(A,A) ->
	case A rem 2 of
		0 -> io:format("Number:~p~n",[A]);
		1 -> done
	end.

evenRangePrinter(N) ->
	helpEvenPrinter(1,N).