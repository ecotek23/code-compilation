-module(exercise3_6).
-export([quicksort/1]).

quicksort([]) -> [];
quicksort([H | T]) ->
	quicksort([X || X<-T, X<H]) ++ [H] ++ quicksort([X || X<-T, X>H]).

% Do merge sort