-module(temp).
-export([f2c/1,c2f/1,convert/1]).

% Temperature conversion between centigrade and fahrenheit

f2c(F) ->
	(5/9)*(F-32).
c2f(C) ->
	(9/5)*C+32.
convert({c,C}) ->
	{f,c2f(C)};
convert({f,F}) ->
	{c,f2c(F)}.
