-module(lists1).
-export([min/1,max/1,max_min/1]).

% Return minimum of a list
min([]) -> {error,empty_list};
min([Head|Tail]) ->
	minHelp([Head|Tail],Head).

minHelp([],Current) -> Current;
minHelp([Head|Tail],Current) ->
	case Head<Current of
		true -> minHelp(Tail,Head);
		false -> minHelp(Tail,Current)
	end.

max([]) -> {error, empty_list};
max([Head|Tail]) ->
	maxHelp([Head|Tail],Head).
	
maxHelp([],Current) -> Current;
maxHelp([Head|Tail],Current) ->
	case Head>Current of
		true -> maxHelp(Tail,Head);
		false -> maxHelp(Tail,Current)
	end.

max_min(List) ->	% Rewrite this with a more effective approach since this is O(2n)
	{min(List),max(List)}.
