-module(exercise3_2).
-export([create/1,reverse_create/1]).

reverse_create(0) -> [];
reverse_create(N) when N>=1 ->
	[N|reverse_create(N-1)].

create(N) -> lists:reverse(reverse_create(N)).