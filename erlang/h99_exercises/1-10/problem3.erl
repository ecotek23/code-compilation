-module(problem3).
-export([elementAt/2]).

% Find the K'th element of a list
elementAt([],_) -> {error,empty_list};
elementAt([Head|_],1) -> Head;
elementAt([_|Tail],K) -> elementAt(Tail,K-1).
