-module(problem5).
-export([myReverse/1]).

% Reverse a list
myReverse([]) -> [];
myReverse([Head|Tail]) ->
	myReverse(Tail) ++ [Head].
