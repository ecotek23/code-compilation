-module(problem8).
-export([compress/1]).

% Eliminate consecutive duplicates of list elements
compress([]) -> [];
compress([A]) -> [A];
compress([A,B|Tail]) ->
	case A==B of
		true -> compress([A|Tail]);
		false -> [A|compress([B|Tail])]
	end.
