-module(problem1).
-export([myLast/1]).

% Find last element of a list
myLast([]) -> {error,empty_list};
myLast([X]) -> X;
myLast([_|Tail]) -> myLast(Tail).
