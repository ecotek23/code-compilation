-module(problem4).
-export([myLength/1,myLength2/1]).

% Find the number of elements of a list
myLength([]) -> 0;
myLength([_|Tail]) -> 1+myLength(Tail).

myLength2(List) ->
	lists:foldr(fun(A,_) -> A+1 end, -1, List). % Returns -1 for empty list
