-module(problem6).
-export([isPalindrome/1]).

% Find out whether a list is a palindrome
isPalindrome(List) ->
	lists:reverse(List)==List.
