-module(problem2).
-export([myButLast/1]).

% Find the last but one element of a list
myButLast([]) -> {error,empty_list};
myButLast([X,_]) -> X;
myButLast([_|Tail]) -> myButLast(Tail).
