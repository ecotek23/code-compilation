-module(problem22).
-export([range/2]).

% Create a list containing all integers within a given range
range(A,B) when A>B -> [];
range(A,B) when A<B ->
	[A|range(A+1,B)];
range(A,A) ->
	[A].
