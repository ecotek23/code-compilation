-module(problem28).
-export([lquicksort/1]).

% Sorting a list according to length of sublists
% I just used a version of quicksort where the sorting key is the list lengths
lquicksort([]) -> [];
lquicksort([Head|Tail]) ->
	lquicksort([X || X<-Tail,length(X)<length(Head)]) ++ [Head] ++ lquicksort([X || X<-Tail,length(X)>length(Head)]).
