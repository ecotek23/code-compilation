-module(problem21).
-export([insertAt/3]).

% Insert an element at a given position into a list
insertAt(Elem,[Head|Tail],1) ->
	[Elem,Head|Tail];
insertAt(Elem,[Head|Tail],K) when K>1 ->
	[Head|insertAt(Elem,Tail,K-1)].
