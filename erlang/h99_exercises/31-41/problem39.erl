-module(problem39).
-export([primesR/2]).
-import(problem31,[isPrime/1]).

% A list of prime numbers within a lower and upper limit
primesR(A,B) when B<A -> [];
primesR(A,B) ->
	case problem31:isPrime(A) of
		true -> [A|primesR(A+1,B)];
		false -> primesR(A+1,B)
	end.
	
