-module(problem31).
-export([isPrime/1]).

% Determine whether a given integer number is prime
isPrime(1) -> false;
isPrime(K) when K>1 ->
	isPrimeHelp(K,2).

isPrimeHelp(C,C) -> true;
isPrimeHelp(Candidate,Current) ->
	case math:sqrt(Candidate)<Current of
		true -> true;
		false ->
			case (Candidate rem Current)==0 of
				true -> false;
				false -> isPrimeHelp(Candidate,Current+1)
			end
	end.
