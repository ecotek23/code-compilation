-module(problem12).
-export([decodeModified/1]).

% Decode a run-length encoded list
decodeSingle([]) -> [];
decodeSingle([0,_]) -> [];
decodeSingle([Multiple,Character]) when Multiple > 0 ->
	Character ++ decodeSingle([Multiple-1,Character]).

decodeModified([]) -> [];
decodeModified([Head|Tail]) ->
	decodeSingle(Head)++decodeModified(Tail).
