-module(problem14).
-export([dupli/1]).

% Duplicate the elements of a list
dupli([]) -> [];
dupli([Head|Tail]) ->
	[Head,Head|dupli(Tail)].
