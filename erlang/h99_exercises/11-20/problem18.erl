-module(problem18).
-export([slice/3]).

% Extract a slice from a list
slice(List,LowerIndex,UpperIndex) ->
	sliceHelp(List,LowerIndex,UpperIndex,1).

% Keeps a running track of current index and ends if on upper side of range
sliceHelp([],_,_,_) -> [];
sliceHelp([Head|Tail],LowerIndex,UpperIndex,CurrentIndex) ->
	case isBetween(CurrentIndex,LowerIndex,UpperIndex) of
		lower -> sliceHelp(Tail,LowerIndex,UpperIndex,CurrentIndex+1);
		inside -> [Head|sliceHelp(Tail,LowerIndex,UpperIndex,CurrentIndex+1)];
		upper -> []
	end.

% Easier to read check for range membership
isBetween(A,B,C) ->
	if
		A<B -> lower;
		A>C -> upper;
		true -> inside
	end.
