-module(problem20).
-export([removeAt/2]).

% Remove the K'th element from a list
removeAt([],_) -> [];
removeAt([_|Tail],1) -> Tail;
removeAt([Head|Tail],K) when K>1 ->
	[Head|removeAt(Tail,K-1)].

