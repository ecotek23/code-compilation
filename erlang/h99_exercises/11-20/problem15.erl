-module(problem15).
-export([repli/2]).

% Replicate the elements of a list a given number of times
individualReplication(_,0) -> [];
individualReplication(Term,K) when K>0 -> 
	[Term|individualReplication(Term,K-1)].

repli([],_) -> [];
repli([Head|Tail],Count) ->
	individualReplication(Head,Count)++repli(Tail,Count).
