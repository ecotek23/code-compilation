-module(problem16).
-export([drop/2]).

% Drop every N'th element from a list
drop(List,Count) ->
	dropHelp(List,Count,Count).

% dropHelp keeps the original count in a holding parameter for
% when the call needs resetting after a value drop
dropHelp([],_,_) -> [];
dropHelp([_|Tail],Count,1) -> dropHelp(Tail,Count,Count);
dropHelp([Head|Tail],Count,K) when K>1 ->
	[Head|dropHelp(Tail,Count,K-1)].
