-- Collatz conjecture function
collatzBool :: Integral a => a -> Bool
collatzBool x
 | x==1 = True
 | even x = collatzBool (x `div` 2)
 | otherwise  = collatzBool (3*x+1)

collatzResult :: Integral a => a -> [a]
collatzResult x
 | x==1 = [1]
 | even x = x:collatzResult(x `div` 2)
 | otherwise = x:collatzResult(3*x+1)