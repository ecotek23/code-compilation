-- Find a triangle where:
-- - All sides are integers
-- - The length of each side is less than or equal to 10
-- - The triangle's perimeter is equal to 24
results = [(x,y,z) | x<-[1..10],y<-[1..10],z<-[1..10],x+y+z==24,x^2+y^2==z^2,x>y]