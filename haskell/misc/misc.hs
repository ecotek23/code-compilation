-- List summation
sum' [] = 0
sum' (x:xs) = x+sum xs

-- Fibonacci sequence in standard recursive form and linear algebra derived formula
fibonacci x = case x of 1 -> 1
			2 -> 1
			otherwise -> fibonacci(x-1)+fibonacci(x-2)

fibonacciAlt x = (1/sqrt 5)*(phi^x-phiHat^x)
	where phi = ((1+sqrt 5)/2)
	      phiHat = ((1-sqrt 5)/2)