-- Insert an element at a given position into a list
insertAt insert [] _ = [insert]
insertAt insert (x:xs) index 
 | index > 0 = x:(insertAt insert xs (index-1))
 | index == 0 = insert:x:xs
 