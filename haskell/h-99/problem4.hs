-- Find the number of elements of a list
myLength xs = sum [1 | x<-xs]