-- Create a list containing all integers within a given range
rangeStep step a b 
 | a > b = []
 | a==b = [b]
 | otherwise = a:rangeStep step (a+step) b

range = rangeStep 1