-- Find out whether a list is a palindrome
isPalindrome xs = if (xs == reverse xs) then True else False