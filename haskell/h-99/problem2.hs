-- Find the last but one element of a list
myButLast [] = error "No elements in list"
myButLast [_] = error "Insufficient elements in list"
myButLast [x,_] = x
myButLast (_:xs) = myButLast xs