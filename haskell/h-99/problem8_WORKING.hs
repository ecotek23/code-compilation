-- Eliminate consecutive duplicates of list elements
compress [] = []
compress (x:y:xs) = if (x==y) then compress (x:xs) else x:compress(y:xs)