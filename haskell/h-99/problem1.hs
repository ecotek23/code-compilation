-- Find the last element of a list
myLast [] = error "No elements in the list"
myLast [x] = x
myLast (_:xs) = myLast xs