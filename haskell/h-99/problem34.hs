-- Calculate Euler's totient function phi(m)
-- USE gcd FROM PROBLEM 32 (STILL TO BE WRITTEN)
totientPhi x = sum [if (gcd(x,i)==1) then 1 else 0 | i<-[1..x]]
