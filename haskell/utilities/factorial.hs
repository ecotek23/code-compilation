-- Different factorial implementations
factorialOne x = product [1..x]
factorialTwo x = if (x==0) then 1 else x*factorialTwo(x-1)