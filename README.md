# README #

### What is this repository for? ###

* This repo holds the scraps of code I've made along the way for side projects and for university tasks
* v0.1

The largest directory by a long way is python, since I use python most for when I need quick scripts or a page scraped.
Several of the other folders do not contain a great deal since I have not yet committed the code I worked on/am working on.