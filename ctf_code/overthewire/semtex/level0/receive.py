# password for the level is B#4j%XXc

import socket
import struct

address = 'semtex.labs.overthewire.org'
port = 24001

def recv_all(sock):
	'''
	Read in all data from a socket connection until no more to get
	'''
        data = ''
        incoming = sock.recv(1024) 
        while incoming:
                data+=incoming
                incoming = sock.recv(1024)
        sock.close()
        return data

# Set up socket connection and read in all data
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM);
sock.connect((address,port))
data = recv_all(sock)

# Write out to file
executableData = bytes(data)[::2]
executable = open('pass','w')
executable.write(executableData)
executable.close()

# Now change the permissions on the 'pass' file so it's executable 
# and it gives you the password for semtex1@semtex.labs.overthewire.org