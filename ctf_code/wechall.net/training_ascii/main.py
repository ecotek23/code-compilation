#!/usr/bin/python3

data = "84, 104, 101, 32, 115, 111, 108, 117, 116, \
105, 111, 110, 32, 105, 115, 58, 32, 111, 109, 99, \
97, 103, 100, 102, 108, 111, 108, 112, 109"

answer = ''.join(chr(int(i)) for i in data.split(', '))
print(answer)