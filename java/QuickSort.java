import java.util.Random;

public class QuickSort {
	public static void main(String[] args) {
		double[] x = {5,4,3,2,1};
		randomizedQuicksort(x,0,x.length-1);
		for (double val : x) {
			System.out.println(val);
		}
	}
	static void quicksort(double[] A,int p,int r) {
		if (p<r) {
			int q = partition(A,p,r);
			quicksort(A,p,q-1);
			quicksort(A,q+1,r);
		}
	}
	static int partition(double[] A,int p,int r) {
		double x = A[r];
		int i = p-1;
		for (int j=p;j<r;j++) {
			if (A[j]<=x) {
				i++;
				exchangeValues(A,i,j);
			}
		}
		exchangeValues(A,i+1,r);
		return i+1;
	}
	static void randomizedQuicksort(double[] A,int p,int r) {
		if (p<r) {
			int q = randomizedPartition(A,p,r);
			randomizedQuicksort(A,p,q-1);
			randomizedQuicksort(A,q+1,r);
		}
	}
	static int randomizedPartition(double[] A,int p,int r) {
		Random rand = new Random();
		int i = rand.nextInt((r-p)+1)+p;
		exchangeValues(A,i,r);
		return partition(A,p,r);
	}
	static void exchangeValues(double[] A,int p,int r) {
		double tmp = A[p];
		A[p] = A[r];
		A[r] = tmp;
	}
}