/*
Code adapted from pseudo-code in "Introduction to Algorithms, 3rd edition"
*/
public class InsertionSort {
	public static void main(String[] args) {
		int[] x = {5,2,4,6,1,3};
		insertionSort(x);
		for (int value : x) {
			System.out.println(value);
		}
	}
	static void insertionSort(int[] A) {
		for (int j=1;j<A.length;j++) {
			int key = A[j];
			int i=j-1;
			while (i>=0 && A[i]>key) {
				A[i+1] = A[i];
				i -= 1;
			}
			A[i+1] = key;
		}
	}
}
