public class PalindromeChecker {
	public static void main(String[] args) {
		String rev = new StringBuilder(args[0]).reverse().toString();
		if (args[0].equals(rev)) {
			System.out.println("It's a palindrome");
		} else System.out.println("It's not a palindrome");
	}
}