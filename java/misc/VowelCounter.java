import java.util.*;

public class VowelCounter {
	public static void main(String[] args) {
		int vowelCounter = 0;
		HashMap<String,Integer> vowelCount = new HashMap<>();
		// Implement individual vowel counting
		String vowels = "aeiou";
		for (char vowel : vowels.toCharArray()) {
			vowelCount.put(String.valueOf(vowel),0);
		}
		for (char value : args[0].toCharArray()) {
			if (vowels.contains(String.valueOf(value).toLowerCase())) {
				vowelCounter++;
				vowelCount.put(String.valueOf(value),vowelCount.get(value)+1);
			}
		}
		System.out.printf("%d vowels counted\n",vowelCounter);
		Set set = vowelCount.entrySet();
		// Get an iterator
	      Iterator i = set.iterator();
	      // Display elements
	      while(i.hasNext()) {
		 Map.Entry me = (Map.Entry)i.next();
		 System.out.print(me.getKey() + ": ");
		 System.out.println(me.getValue());
	      }
	}
}