def collatz(n):
    if n==1:
        return '1'
    elif n%2==0:
        return '{}->'.format(n)+collatz(n/2)
    else:
        return '{}->'.format(n)+collatz(3*n+1)