def group_in_10s(*x):
    if x:
        temp = [None]*((max(x)/10)+1)
        for val in x:
            if type(temp[val/10])==type([]):
                temp[val/10].append(val)
            else:
                temp[val/10] = [val]
        for sub_list in temp:
            if sub_list!=None:
                sub_list.sort()
        return temp
    else:
        return []