def chmod_calculator(perm):
    vals = {'r':4,'w':2,'x':1}; temp = ''
    for kind in ['user','group','other']:
        if kind in perm:
            temp += str(sum(map(lambda x : vals.get(x,0),list(perm[kind]))))
        else:
            temp+='0'
    return temp