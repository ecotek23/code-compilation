function area(d,l){
  if (d<=l) {
    return "Not a rectangle";
  } else {
    var num = Math.sqrt(d*d-l*l)*l;
    if (num==parseInt(num)) {
      return num;
    } else {
      return parseFloat(num.toFixed(2));
    }
  }  
}

