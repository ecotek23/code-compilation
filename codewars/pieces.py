# Some of these are for python2, some for python3
# Require some reformatting on spacing and minor syntax tweaks between version

def accum(s):
    pieces = []
    for index,value in enumerate(s):
        pieces.append(s[index].upper()+s[index].lower()*index)
    return '-'.join(pieces)

def parse_float(string):
    try:
        val = float(string)
    except:
        return None
    return val

import math

def calculate_tip(amount, rating):
    rating = rating.lower()
    if rating == "terrible":
        return 0;
    elif rating == "poor":
        return int(math.ceil(amount*0.05))
    elif rating == "good":
        return int(math.ceil(amount*0.1))
    elif rating == "great":
        return int(math.ceil(amount*0.15))
    elif rating == "excellent":
        return int(math.ceil(amount*0.2))
    else:
        return "Rating not recognised"

def calc(x):
    total1 = ''.join(map(str,map(ord,list(x))))
    total2 = total1.replace('7','1')
    
    total1_sum = sum(map(int,list(total1)))
    total2_sum = sum(map(int,list(total2)))
    
    return total1_sum-total2_sum

def find_even_index(arr):
    for index in range(len(arr)):
        if sum(arr[:index])==sum(arr[index+1:]):
            return index
    return -1

def to_alternating_case(string):
    return string.swapcase()

import re

def get_number_from_string(val):
    return int(re.sub('\D','',val))

def grow(arr):
    return reduce(lambda x,y:x*y,arr)

def positive_sum(arr):
    return sum(filter(lambda x : x>0,arr))

def is_sorted_and_how(arr):
    temp = arr[:]
    temp.sort()
    if arr==temp:
        return "yes, ascending"
    elif arr==temp[::-1]:
        return "yes, descending"
    else:
        return 'no'

def monkey_count(n):
    return list(range(1,n+1))

def dont_give_me_five(start,end):
	count = 0
	for i in range(start,end+1):
		if '5' not in str(i):
			count +=1
	return count   # amount of numbers

def switch_it_up(number):
    numbers = {0:"Zero",1:"One",2:"Two",3:"Three",4:"Four",5:"Five",6:"Six",7:"Seven",8:"Eight",9:"Nine"}
    return numbers[number]

def solution(number):
  sum = 0
  for i in range(1,number):
      if i%3==0 or i%5==0:
          sum+=i
  return sum

def Descending_Order(num):
	x = list(str(num)); x.sort();
	return int(''.join(x[::-1]))

def get_middle(s):
	if len(s)%2==0:
		return s[len(s)/2-1:len(s)/2+1]
	else:
		return s[len(s)/2]

def high_and_low(numbers):
    numbers = map(int,numbers.split(' '))
    h = max(numbers)
    l = min(numbers)
    return "{} {}".format(h,l)

import math

def find_next_square(sq):
    # Return the next square if sq is a square, -1 otherwise
    val = math.sqrt(sq)
    if val==int(val):
        return int((val+1)**2)
    return -1

def get_average(marks):
    return sum(marks)//len(marks)

def average(array):
    return round(sum(array)/len(array))

def is_triangle(l1,l2,l3):
        if (l1>=l2+l3) or (l2>=l1+l3) or (l3>=l1+l2):
                return False
        else:
                return True

def is_triangle(l1,l2,l3):
        if (l1>=l2+l3) or (l2>=l1+l3) or (l3>=l1+l2):
                return False
        #elif (l1==l2+l3) or (l2==l1+l3) or (l3==l1+l2):
        #        return True
        else:
                return True

class Person:
  def __init__(self, name):
    self.name = name
  
  def greet(self, other_name):
    return "Hi {0}, my name is {1}".format(other_name, self.name)

def multiply(a, b):
  return a * b

def find_needle(haystack):
    return 'found the needle at position {}'.format(haystack.index('needle'))

def binary_array_to_number(arr):
  sum = 0
  for i in range(len(arr)):
      sum+= (arr[i]*(2**(len(arr)-i-1)))
  return sum

def basic_op(operator, value1, value2):
    return eval('{} {} {}'.format(value1,operator,value2))

def distinct(seq):
    temp = []
    for i in seq:
        if i not in temp:
            temp.append(i)
    return temp

import string
def position(alphabet):
    return 'Position of alphabet: {}'.format((string.lowercase.index(alphabet))+1)

def find_longest(string):
    spl = string.split(" ")
    longest = 0
    i=0
    while i < len(spl):
        if (len(spl[i]) > longest): 
            longest = len(spl[i])
        i+=1
    return longest

import string
t1 = string.maketrans('01234','00000')
t2 = string.maketrans('56789','11111')
def fake_bin(x):
    return string.translate(string.translate(x,t1),t2)

def fix_the_meerkat(arr):
    return arr[::-1]

def repeat_str(repeat, string):
    return string*repeat

def stringy(size):
    temp = '10'*(size/2)
    if divmod(size,2)[1]:
        temp+='1'
    return temp

def how_much_i_love_you(nb_petals):
    vals = ["I love you","a little","a lot","passionately","madly","not at all"]
    return vals[(nb_petals-1)%6]

