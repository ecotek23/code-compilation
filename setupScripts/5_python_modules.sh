# SET UP PYTHON ENVIRONMENT
cd ~/Desktop
virtualenv pythonEnv
source pythonEnv/bin/activate # ACTIVE ENVIRONMENT
sudo pip install jupyter
sudo pip install requests
sudo pip install BeautifulSoup
sudo pip install openpyxl
sudo pip install scrapy
sudo pip install Pillow
sudo pip install nltk
sudo pip install scapy
sudo pip install numpy
sudo pip install scipy
sudo pip install matplotlib
sudo pip install twisted
sudo pip install nose
sudo pip install tqdm
sudo pip install joblib
sudo pip install dill
sudo pip install tinydb
sudo pip install simplejson
sudo pip install setuptools
sudo pip install mock
sudo pip install pymongo
sudo pip install selenium
sudo pip install Django
sudo pip install gitpython
sudo pip install pycurl
sudo pip install whisper
sudo pip install carbon
sudo pip install graphite-web
deactivate # DEACTIVATE ENVIRONMENT