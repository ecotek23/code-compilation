
# INSTALL SOFTWARE
sudo apt-get update;
sudo apt-get upgrade; 
sudo apt-get dist-upgrade;
# sudo apt-get install  git
sudo apt-get install  mplayer
sudo apt-get install  ipython3
sudo apt-get install  python3-pip
sudo apt-get install  libgnome2-bin
sudo apt-get install  terminator
#sudo apt-get install  wine1.8
#sudo apt-get install  winetricks
sudo apt-get install  virtualenv
sudo apt-get install  deluge
sudo apt-get install  bleachbit
sudo apt-get install  spyder spyder3
sudo apt-get install  idle idle3
sudo apt-get install  vlc 
sudo apt-get install  gnome-disk-utility
sudo apt-get install  synapse
# sudo apt-get install  unity-tweak-tool
sudo apt-get install  filezilla
sudo apt-get install  unrar
sudo apt-get install  p7zip
#sudo apt-get install  conky
#sudo apt-get install  kate
sudo apt-get install  geany
#sudo apt-get install  bluefish
sudo apt-get install  okular
sudo apt-get install  le
sudo apt-get install  texmaker
sudo apt-get install  scala
sudo apt-get install  golang
sudo apt-get install  ghc
sudo apt-get install  python-pip
# sudo apt-get install  spotify-client
#sudo apt-get install guake 
#sudo apt-get install  bpython
sudo apt-get install  openssh-server
sudo apt-get install  vsftpd
#sudo apt-get install lamp-server^
#sudo apt-get install  phpmyadmin
#sudo apt-get install  variety
#sudo apt-get install  variety-slideshow
sudo apt-get install  gimp
sudo apt-get install  gimp-data
sudo apt-get install  gimp-plugin-registry
sudo apt-get install  gimp-data-extras
sudo apt-get install  gxine
sudo apt-get install  libdvdread4
sudo apt-get install  icedax
sudo apt-get install  tagtool
sudo apt-get install  easytag
sudo apt-get install  id3tool
sudo apt-get install  lame
sudo apt-get install  nautilus-script-audio-convert
sudo apt-get install  libmad0
sudo apt-get install  mpg321
sudo apt-get install  gstreamer1.0-libav
#sudo apt-get install  calibre
#sudo apt-get install  banshee
sudo apt-get install  r-base-core
sudo apt-get install  ack-grep
sudo apt-get install  quicksynergy
sudo apt-get install  texlive-full

#sudo apt-get install composer; \
#composer global require "laravel/installer"; \
#echo "export PATH=$PATH:$HOME/.composer/vendor/bin" >> ~/.bashrc;
