# SET FAVOURITE ALIASES
echo "alias e='exit'" >> ~/.bash_aliases
echo "alias ins='sudo apt-get install'" >> ~/.bash_aliases
echo "alias up='sudo apt-get update -y'" >> ~/.bash_aliases
echo "alias upg='sudo apt-get upgrade -y; sudo apt-get dist-upgrade -y'" >> ~/.bash_aliases
echo "alias nano='le'" >> ~/.bash_aliases
echo "alias lash='ls -lash'" >> ~/.bash_aliases
echo "alias t='variety -t'" >> ~/.bash_aliases
echo "alias f='variety -f'" >> ~/.bash_aliases
echo "alias n='variety -n'" >> ~/.bash_aliases
echo "alias p='variety -p'" >> ~/.bash_aliases
echo "alias bleach='sudo bleachbit --clean --preset;up;upg'" >> ~/.bash_aliases
echo "alias pins='pip install'" >> ~/.bash_aliases
echo "alias inv='xcalib -invert -alter'" >> ~/.bash_aliases 

echo "alias python='python3'" >> ~/.bash_aliases
echo "alias ipython='ipython3'" >> ~/.bash_aliases
echo "alias pip='pip3'" >> ~/.bash_aliases

echo "alias dev='cd ~/Dropbox/my_working_code/'" >> ~/.bash_aliases
echo "alias mp3download='youtube-dl --extract-audio --audio-format mp3'" >> ~/.bash_aliases

echo "alias zeros='find . -size 0 -print 2>/dev/null'" >> ~/.bash_aliases