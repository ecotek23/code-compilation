from __future__ import division

def reverse_factorial(number):
	current_divisor = 2
	while number!=1:
		number /= current_divisor
		if int(number)==number: # Divided directly
			current_divisor += 1
		else:
			return None
	return current_divisor-1

if __name__=='__main__':
	test_number = 120
	test_number_2 = 6
	test_number_3 = 7
	print "{} is a reverse factorial? {}".format(test_number,reverse_factorial(test_number))
	print "{} is a reverse factorial? {}".format(test_number_2,reverse_factorial(test_number_2))
	print "{} is a reverse factorial? {}".format(test_number_3,reverse_factorial(test_number_3))