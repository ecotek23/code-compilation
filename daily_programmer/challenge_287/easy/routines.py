def largest_digit(number):
	if len(number)>4 or int(number)<0: # Check for correct number range
		raise ValueError("The digit needs to be a 4 digit number")
	number_list = list(number)
	return int(max(number_list,key=int))

def pad_number(number,length=4):
	"""
	Prepend zeros onto the number to create the appropriate length
	"""
	number_list = list(number)
	if len(number_list)!=4:
		padding_required = 4-len(number)
		for i in range(padding_required):
			number_list.insert(0,'0')
	return ''.join(number_list)

def digit_sort(number,descending=True):
	"""
	Pad the number and sort it in either descending or ascending order
	"""
	number = pad_number(number) # guarantee required length
	number_list = list(number)
	number_list.sort(key=int,reverse=descending) # sort in place
	return ''.join(number_list)

def distinct_digits(number):
	"""
	Use a set to find the number of distinct elements in the padded number
	"""
	number = pad_number(number)
	return len(set(list(number)))

def kaprekar_routine(number):
	"""
	Implement kaprekars routine
	"""
	number = pad_number(number)
	count = 0
	if distinct_digits(number)>1: # Continue, the number meets requirements
		while number!='6174':
			ascending = digit_sort(number,descending=False)
			descending = digit_sort(number,descending=True)
			number = int(descending)-int(ascending)
			number = pad_number(str(number))
			count +=1
		return count
	else: # Number not meeting requirements for at least two distinct digits
		raise ValueError("The number has to have at least two distinct digits")

if __name__=='__main__':
	max_iterations = 0
	for test_number in range(10000):
		try: # Make sure the number is valid
			max_iterations = max(max_iterations,kaprekar_routine(str(test_number)))
		except ValueError: # The number did not meet requirements
			pass
	print "The maximum number of iterations required is {}".format(max_iterations)