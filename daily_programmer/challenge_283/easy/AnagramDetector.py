import re

def anagram_detector(input_file):
	lines = open(input_file,'r').readlines()
	for line in lines:
		line_pieces = line.split('?')
		check_pieces = [converter(i) for i in line_pieces]
		if check_pieces[0]==check_pieces[1]:
			print('{}is an anagram of{}'.format(line_pieces[0],line_pieces[1]),end="")
		else:
			print('{}is NOT an anagram of{}'.format(line_pieces[0],line_pieces[1]),end="")

def converter(piece):
	return sorted(re.sub('[^0-9a-zA-Z]+', '', piece).lower());
	
if __name__=='__main__':
	input_file = './input'
	anagram_detector(input_file)