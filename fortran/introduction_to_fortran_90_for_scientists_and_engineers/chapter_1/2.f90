PROGRAM condenser_energy
IMPLICIT NONE
 REAL capacitance, potential
 WRITE(*,"(A)",ADVANCE="NO") "Input your capacitance and then your potential: "
 READ*, capacitance, potential
 PRINT*, "The energy is:", energy_stored(capacitance,potential)

 CONTAINS
  FUNCTION energy_stored(c,v) RESULT (cap)
   REAL c,v,cap
   cap = c*v**2/2
  END FUNCTION energy_stored
END PROGRAM condenser_energy