PROGRAM ops
IMPLICIT NONE
 REAL first, second	! Numbers to operate upon
 WRITE(*,"(A)",ADVANCE="NO") "Input your first and second number, on the same line separated by a space: "
 READ*, first, second
 PRINT*, "Sum:", first+second
 PRINT*, "Difference:", first-second
 PRINT*, "Product:", first*second
 PRINT*, "Quotient:", first/second
END PROGRAM ops