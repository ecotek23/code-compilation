PROGRAM exponential
USE utils
IMPLICIT NONE
 REAL exponent

 WRITE(*,"(A)",ADVANCE='NO') "Please input your exponent value: "
 READ*, exponent

 PRINT*, e(exponent)

 CONTAINS
  FUNCTION e(exponent) RESULT (sum)
   REAL exponent, sum
   REAL :: current_term = 1
   INTEGER counter
   sum = 1; counter = 1;
   DO WHILE (current_term .GT. 1e-6)
    current_term = (exponent**counter)/REAL(Factorial((counter)))
    counter = counter+1
    sum = sum+current_term
    PRINT*, counter, sum
   END DO
  END FUNCTION e

END PROGRAM exponential