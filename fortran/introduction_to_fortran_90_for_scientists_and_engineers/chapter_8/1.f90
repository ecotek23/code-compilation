IMPLICIT NONE
REAL :: h = 1
REAL :: x = 1
DO WHILE (h .GT. 1e-5)
 PRINT*, x, h, (F(x+h)-F(x))/h
 h = h/10
END DO

CONTAINS
 FUNCTION F(x) RESULT (result)
  REAL result, x
  result = x**3
 END FUNCTION
END