MODULE utils
 IMPLICIT NONE
 CONTAINS

  FUNCTION Even(value)
   INTEGER value
   LOGICAL Even
   IF (MOD(value,2) .EQ. 0) THEN
    Even = .True.
   ELSE
    Even = .False.
   END IF
  END FUNCTION Even

  RECURSIVE FUNCTION Factorial(value) RESULT (fact)
   INTEGER value, fact
   fact = 1
   DO WHILE (value > 0)
    fact = fact * value
    value = value-1
   END DO
  END FUNCTION Factorial

  INTEGER FUNCTION Bin(N, R) RESULT (res)
   INTEGER N, R
   res = Factorial(N)/(Factorial(R)*Factorial(N-R))
  END FUNCTION Bin
END MODULE utils