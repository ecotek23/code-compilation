PROGRAM hailstone
USE utils ! include my even function for added clarity
IMPLICIT NONE
 INTEGER hailstone_number	! although an INTEGER really limits the permissible values range, a real would be more difficult to handle
 
 WRITE(*,"(A)",ADVANCE='NO') "Input the number you want to sequence: "
 READ*, hailstone_number

 DO WHILE (hailstone_number /= 1)
  PRINT*, hailstone_number
  IF (Even(hailstone_number)) THEN
   hailstone_number = hailstone_number/2
  ELSE
   hailstone_number = 3*hailstone_number+1
  END IF
 END DO

 PRINT*, hailstone_number
END PROGRAM hailstone