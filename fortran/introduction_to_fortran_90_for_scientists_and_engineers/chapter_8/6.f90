PROGRAM fibonacci_calculator
IMPLICIT NONE
 INTEGER current

 DO current = 0, 20
  PRINT*, current, Fibonacci(current)
 END DO

 CONTAINS
  INTEGER RECURSIVE FUNCTION Fibonacci(index) RESULT (val)
   INTEGER index
   IF ((index .EQ. 0) .or. (index .EQ. 1)) THEN
    val = 1
   ELSE
    val = Fibonacci(index-1)+Fibonacci(index-2)
   END IF
 END

END PROGRAM fibonacci_calculator