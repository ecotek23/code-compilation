PROGRAM array_testing
IMPLICIT NONE
 INTEGER, PARAMETER :: array_size = 100
 INTEGER, DIMENSION(array_size) :: array
 LOGICAL, DIMENSION(array_size) :: array_evens
 INTEGER index

 DO index=1,array_size
  array(index) = index
 END DO

 WHERE (MOD(array,2) .EQ. 0)
  array_evens = .TRUE.
 ELSEWHERE
  array_evens = .FALSE.
 END WHERE

 DO index=1,array_size
  PRINT*, array_evens(index)
 END DO
 
END PROGRAM array_testing