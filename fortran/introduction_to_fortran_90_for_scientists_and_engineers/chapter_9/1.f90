PROGRAM exercise_one
IMPLICIT NONE
 INTEGER, PARAMETER :: array_size = 100
 INTEGER, DIMENSION(array_size) :: Num
 INTEGER index

 DO index=1,array_size
  Num(index) = index	! Place first 100 integers into Num(1),Num(2) etc.
 END DO

 DO index=2,100,2
  Num(index/2) = index	! Place first 50 even integers into Num(1),Num(2) etc.
 END DO

 DO index=1,array_size
  Num(index) = 101-index	! Place first 100 integers in reverse order into Num
 END DO

END PROGRAM exercise_one