PROGRAM exercise_two
IMPLICIT NONE
 INTEGER, PARAMETER :: array_size = 100
 INTEGER, DIMENSION(array_size) :: F
 INTEGER index

 DO index=1,100
  F(index) = Fib(index)		! Horribly slow after the first 40 or so
  PRINT*, index, F(index)
 END DO

 CONTAINS
  RECURSIVE FUNCTION Fib(x) RESULT (val)
   INTEGER x, val
   IF ((x .EQ. 1) .OR. (x .EQ. 2)) THEN
    val = 1
   ELSE
    val = Fib(x-1)+Fib(x-2)
   END IF
  END FUNCTION Fib
 
END PROGRAM exercise_two