PROGRAM factorial
IMPLICIT NONE
 INTEGER fac
 INTEGER :: result = 1

 WRITE(*,"(A)",ADVANCE='NO') "Please input a number to calculate it's factorial: "
 READ*, fac
 DO WHILE (fac .GT. 1)
  result = result*fac
  fac = fac-1
 END DO

 PRINT*,"The factorial of that number is",result

END PROGRAM factorial