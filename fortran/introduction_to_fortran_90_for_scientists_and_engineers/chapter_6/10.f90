PROGRAM up_to_100
IMPLICIT NONE
 INTEGER :: term_count = 0
 INTEGER :: sum = 0
 INTEGER :: current_value = 1

 DO
  IF (sum+current_value .GT. 100) THEN
   PRINT*, "The number of terms required is",term_count
   EXIT
  ELSE
   sum = sum+current_value
   term_count = term_count+1
   current_value = current_value+1
  END IF
 END DO
END PROGRAM up_to_100