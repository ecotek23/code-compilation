PROGRAM sum_of_even_to_200
IMPLICIT NONE
 INTEGER current_value
 INTEGER :: sum = 0

 DO current_value = 2, 200, 2
  sum = sum+current_value
 END DO

 PRINT*,"The sum is:",sum

END PROGRAM sum_of_even_to_200