PROGRAM maximum_or_equal
IMPLICIT NONE
 REAL first, second

 WRITE(*,"(A)",ADVANCE='NO') "Please enter two numbers on the same line, separated by a single space: "
 READ*, first, second
 
 IF (first .EQ. second) THEN
  PRINT*,"Those two numbers are equal"
 ELSE
  PRINT*,"The larger of those two numbers is",MAX(first,second)
 END IF

END PROGRAM maximum_or_equal