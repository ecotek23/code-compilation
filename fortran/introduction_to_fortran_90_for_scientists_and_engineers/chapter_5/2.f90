PROGRAM ten_signs
IMPLICIT NONE
 INTEGER :: positive = 0
 INTEGER :: negative = 0
 INTEGER :: zero = 0
 INTEGER current_index
 REAL current_value

 DO current_index = 1, 10
  WRITE(*,"(A)",ADVANCE='NO') "Please input a number: "
  READ*, current_value
  IF (current_value .GT. 0) THEN
   positive = positive+1
  ELSE IF (current_value .LT. 0) THEN
   negative = negative+1
  ELSE
   zero = zero+1
  END IF
 END DO

 PRINT*, "The number of positive numbers was:",positive
 PRINT*, "The number of negative numbers was:",negative
 PRINT*, "The number of zero numbers was:",zero

END PROGRAM ten_signs