PROGRAM read_and_write
IMPLICIT NONE
 CHARACTER(LEN=20) file_path ! File where variables are stored
 REAL x, y
 PRINT*, "Enter your file name where I can read the two variables from"
 READ*, file_path
 OPEN(1, FILE=file_path)
 READ(1,*), x, y
 OPEN(2, FILE="output")
 WRITE(2,*), x*y
END PROGRAM read_and_write