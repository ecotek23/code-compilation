PROGRAM pass_fail
IMPLICIT NONE
 INTEGER mark, current
 INTEGER :: count = 0 ! count of people who passed
 OPEN(1,FILE='6_input')

 DO current = 1, 10
  READ(1,*), mark
  IF (mark .GE. 5) THEN
   count = count +1
  ENDIF
 END DO

 PRINT*,"The number of students who passed is:",count

END PROGRAM pass_fail