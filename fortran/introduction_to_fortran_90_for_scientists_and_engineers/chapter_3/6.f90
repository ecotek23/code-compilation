PROGRAM marks_average
IMPLICIT NONE
 INTEGER mark, count
 REAL average, sum
 OPEN(1,FILE='6_input')

 DO count = 1, 10
  READ(1,*), mark
  PRINT*,"Current mark is",mark
  sum = sum+mark
 END DO

 average = REAL(sum)/10
 PRINT*,"The average mark is:",average

END PROGRAM marks_average