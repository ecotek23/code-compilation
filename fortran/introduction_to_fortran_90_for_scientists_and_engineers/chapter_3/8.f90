PROGRAM heads_or_tails
IMPLICIT NONE
 INTEGER current
 INTEGER :: less = 0
 INTEGER :: greater = 0
 REAL random

 DO current = 1, 100
  CALL RANDOM_NUMBER( random )
  IF (random .GT. 0.5) THEN
   greater = greater+1
  ELSE
   less = less+1
  ENDIF
 END DO

 PRINT*,"Less than 0.5:",less
 PRINT*,"Greater than 0.5:",greater
END PROGRAM heads_or_tails