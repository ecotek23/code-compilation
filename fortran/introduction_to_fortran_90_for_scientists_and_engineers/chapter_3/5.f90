PROGRAM even_sum_to_200
IMPLICIT NONE
 INTEGER current
 INTEGER :: sum = 0

 DO current = 2, 200, 2
  sum = sum+current
 END DO

 PRINT*, "The sum of the even numbers from 2 to 200 is",sum
END PROGRAM even_sum_to_200