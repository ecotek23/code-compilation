! Rewrite to break it down into the combined increments

PROGRAM length_conversion
IMPLICIT NONE
 REAL metres, yards, feet, inches

 WRITE(*,"(A)",ADVANCE='NO') "Enter the length in metres that you want converted: "
 READ*, metres

 inches = metres*39.37
 feet = inches/12
 yards = feet/3
 
 PRINT*, "In yards that is:",yards
 PRINT*, "In feet that is:",feet
 PRINT*, "In inches that is:",inches
 
END PROGRAM length_conversion