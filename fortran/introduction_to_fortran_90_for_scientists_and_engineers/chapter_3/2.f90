PROGRAM temp_conversion
IMPLICIT NONE
 REAL celsius_temp, fahrenheit_temp

 WRITE(*,'(A)',ADVANCE='NO') "Please input the temperature in celsius: "
 READ*, celsius_temp

 fahrenheit_temp = 9*celsius_temp/5+32
 PRINT*, "The Fahrenheit temperature is:", fahrenheit_temp

END PROGRAM temp_conversion