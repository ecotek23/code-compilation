PROGRAM sum_to_100
IMPLICIT NONE
 INTEGER current
 INTEGER :: sum = 0

 DO current = 1, 100
  sum = sum + current
 END DO
 
 PRINT*, "The sum from 1 to 100 is", sum
END PROGRAM sum_to_100