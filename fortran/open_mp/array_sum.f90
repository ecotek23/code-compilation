PROGRAM array_sum
IMPLICIT NONE
 INTEGER, PARAMETER :: MAX_DIM = 100
 REAL, DIMENSION(MAX_DIM) :: vector
 REAL :: result = 0.0
 REAL :: seq_result = 0.0
 INTEGER index

 CALL RANDOM_NUMBER(vector)	! initialize vector

 !$OMP PARALLEL DO REDUCTION(+:result)
  DO index=1,MAX_DIM
   PRINT*, vector(index)
   result = result + vector(index)
  END DO
 !$OMP END PARALLEL DO

 DO index=1,MAX_DIM
  seq_result = seq_result+vector(index)
 END DO
 
 PRINT*, "The parallel result is:", result
 PRINT*, "The sequential result is:", seq_result
 
END PROGRAM array_sum