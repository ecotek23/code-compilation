PROGRAM hello_world
IMPLICIT NONE
 INTEGER thread_id, OMP_GET_THREAD_NUM
 
 !$OMP PARALLEL PRIVATE(thread_id)
  thread_id = OMP_GET_THREAD_NUM()
  PRINT*, "Hello, thread number: ", thread_id
 !$OMP END PARALLEL
 
END PROGRAM hello_world