PROGRAM array_max
IMPLICIT NONE
 INTEGER, PARAMETER :: MAX_DIM = 100
 REAL, DIMENSION(MAX_DIM) :: vector
 REAL :: max_value = 0.0
 INTEGER index
 CALL RANDOM_NUMBER(vector)	! initialize the vector

 !$OMP PARALLEL DO REDUCTION(MAX:max_value)
  DO index=1,MAX_DIM
   max_value = MAX(max_value,vector(index))
  END DO
 !$OMP END PARALLEL DO

 PRINT*, "The maximum value of the array is",max_value

END PROGRAM array_max