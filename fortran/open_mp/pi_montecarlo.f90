PROGRAM pi_montecarlo
IMPLICIT NONE
 INTEGER, PARAMETER :: MAX_ITERATIONS=10000000
 INTEGER current
 REAL x,y,in_circle
 
 !$OMP PARALLEL DO PRIVATE(x,y), SHARED(in_circle)
  DO current=1,MAX_ITERATIONS 
   CALL RANDOM_NUMBER(x)
   CALL RANDOM_NUMBER(y)
   IF (x**2+y**2 .LT. 1) THEN
    !$OMP ATOMIC
     in_circle = in_circle+1
    !$OMP END ATOMIC
   END IF
  END DO
 !$OMP END PARALLEL DO
 
 PRINT*, "The estimated value is:",4*(in_circle/MAX_ITERATIONS)

END PROGRAM pi_montecarlo
