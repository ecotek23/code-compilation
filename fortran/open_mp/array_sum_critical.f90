PROGRAM array_sum_critical
IMPLICIT NONE
 INTEGER, PARAMETER :: MAX_DIM = 100
 REAL, DIMENSION(MAX_DIM) :: vector
 INTEGER index
 REAL :: sum = 0.0
 
 CALL RANDOM_NUMBER(vector)	! initialize array
 
 !$OMP PARALLEL DO
  DO index=1,MAX_DIM
   !$OMP CRITICAL
    sum = sum+vector(index)
   !$OMP END CRITICAL
  END DO
 !$OMP END PARALLEL DO

 PRINT*, "The result is:",sum

END PROGRAM array_sum_critical
