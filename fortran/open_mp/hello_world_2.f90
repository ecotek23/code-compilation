PROGRAM hello_world_2
IMPLICIT NONE
 INTEGER OMP_GET_THREAD_NUM, OMP_GET_NUM_THREADS
 INTEGER threads, id

 !$OMP PARALLEL PRIVATE(id,threads)
  threads = OMP_GET_NUM_THREADS()
  id = OMP_GET_THREAD_NUM()
  PRINT*,"Num threads:",threads
  PRINT*,"Hello from threads:",id
 !$OMP END PARALLEL

END PROGRAM hello_world_2