PROGRAM matrix_multiplication
IMPLICIT NONE
 INTEGER, PARAMETER :: MAX_DIM = 2
 REAL, DIMENSION(MAX_DIM,MAX_DIM) :: a,b,c
 INTEGER row,col
 
 ! initialize a and b
 DO row=1,MAX_DIM
  DO col=1,MAX_DIM
   a(row,col) = row+col
   b(row,col) = row+col		!	(2,3;3,4) for MAX_DIM is 2
  END DO
 END DO
 
 !$OMP DO
  DO row=1,MAX_DIM
   DO col=1,MAX_DIM
     c(row,col) = DOT_PRODUCT(a(row,:),b(:,col))
   END DO
  END DO
 !$OMP END DO
 
 ! print resulting matrix
 DO row=1,MAX_DIM
  PRINT*, "new row"
  DO col=1,MAX_DIM
	PRINT*, c(row,col)		!	(13,18;18,25) for MAX_DIM is 2
  END DO
 END DO

END PROGRAM matrix_multiplication
