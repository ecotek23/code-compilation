PROGRAM matrix_vector
IMPLICIT NONE
 INTEGER, PARAMETER :: MAX_DIM = 1000
 INTEGER OMP_GET_THREAD_NUM, OMP_GET_NUM_THREADS, row, column
 DOUBLE PRECISION, DIMENSION(MAX_DIM,MAX_DIM) :: matrix
 DOUBLE PRECISION, DIMENSION(MAX_DIM) :: vector, result
 DOUBLE PRECISION sum

 DO row=1,MAX_DIM
  DO column=1,MAX_DIM
   matrix(row,column) = row*column	! (1,2,3;2,4,6;3,6,9)
  END DO
  vector(row) = row			! (1;2;3)
 END DO
 
 !$OMP PARALLEL	DO PRIVATE(sum)
  DO row=1,MAX_DIM
   sum = 0
   DO column=1,MAX_DIM
    sum = sum+matrix(row,column)*vector(row)
   END DO
   result(row) = sum
  END DO
 !$OMP END PARALLEL DO

 DO row=1,MAX_DIM
  PRINT*, row, ":", result(row)
 END DO

END PROGRAM matrix_vector