PROGRAM compound_trapezium_rule
USE test_functions
 ! COMPLETES RESULT BUT SEGFAULT UPON DEALLOCATION? 
 ! FORUMS ASSIGN THIS AS A COMPILER ERROR IN PREVIOUS VERSION BUT NOT SURE FOR VERSION 6.1.1
 DOUBLE PRECISION x_value, step_size
 DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: sum_pieces
 DOUBLE PRECISION :: sum = 0
 DOUBLE PRECISION :: a = 0
 DOUBLE PRECISION :: b = 1
 INTEGER step_count, current_step
 
 WRITE(*,"(A)",ADVANCE="NO") "Please enter the number of steps you want to take: "
 READ*, step_count
 
 ALLOCATE( sum_pieces(step_count) )
 step_size = (b-a)/step_count
 
 !$OMP PARALLEL DO
  DO current_step = 0,step_count-1
   sum_pieces(current_step) = X_SQUARED(a+current_step*step_size)+X_SQUARED(a+(current_step+1)*step_size)
  END DO
 !$OMP END PARALLEL DO
 
 !$OMP PARALLEL DO REDUCTION(+:sum)
  DO current_step = 0,step_count
   sum = sum+sum_pieces(current_step)
  END DO
 !$OMP END PARALLEL DO
 
 PRINT*, "The result is:",(step_size/2)*sum
 DEALLOCATE( sum_pieces )
 
END PROGRAM compound_trapezium_rule
