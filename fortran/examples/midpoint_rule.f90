PROGRAM midpoint_rule
USE test_functions
IMPLICIT NONE
 DOUBLE PRECISION x_value, step_size
 DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: sum_pieces
 DOUBLE PRECISION :: lower_limit = 0
 DOUBLE PRECISION :: upper_limit = 1
 DOUBLE PRECISION :: sum = 0
 INTEGER step_count, current_step

 WRITE(*,"(A)",ADVANCE="NO") "Please input the number of steps you want to use:"
 READ*, step_count

 ALLOCATE( sum_pieces(step_count) )
 step_size = (upper_limit-lower_limit)/step_count

 !$OMP PARALLEL DO
  DO current_step=1,step_count
   sum_pieces(current_step) = X_SQUARED(lower_limit+(REAL(current_step)-1/2)*step_size)
  END DO
 !$OMP END PARALLEL DO

 !$OMP PARALLEL DO REDUCTION(+:sum)
  DO current_step = 1,step_count
   sum = sum+sum_pieces(current_step)
  END DO
 !$OMP END PARALLEL DO

 PRINT*, "The result is: ", step_size*sum			! result should be ~1/3
 DEALLOCATE( sum_pieces )
 
END PROGRAM midpoint_rule