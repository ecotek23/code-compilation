PROGRAM simpsons_rule
USE test_functions
 DOUBLE PRECISION x_value, step_size
 DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: sum_pieces
 DOUBLE PRECISION :: lower_limit = 0
 DOUBLE PRECISION :: upper_limit = 1
 INTEGER step_count, current_step, i

 WRITE(*,"(A)",ADVANCE="NO") "Input the number of steps you want to use: "
 READ*, step_count

 ALLOCATE( sum_pieces(step_count) )
 step_size = (upper_limit-lower_limit)/step_count

 !$OMP PARALLEL DO
  DO current_step=1,step_count
   i = current_step-1
   sum_pieces(current_step) = X_SQUARED(lower_limit+2*i*step_size)+&
   4*X_SQUARED(lower_limit+(2*i+1)*step_size)+&
   X_SQUARED(lower_limit+(2*i+2)*step_size)
  END DO
 !$OMP END PARALLEL DO

 !$OMP PARALLEL DO REDUCTION(+:sum)
  DO current_step=1,step_count
   sum = sum+sum_pieces(current_step)
  END DO
 !$OMP END PARALLEL DO

 PRINT*, "The result is: ",(step_size/3)*sum
 DEALLOCATE( sum_pieces )

END PROGRAM simpsons_rule