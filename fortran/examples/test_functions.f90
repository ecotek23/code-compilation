MODULE test_functions
 
 CONTAINS
  FUNCTION X_SQUARED(X) RESULT (RES)
   DOUBLE PRECISION X, RES
   RES = X**2
  END FUNCTION X_SQUARED
  
END MODULE test_functions