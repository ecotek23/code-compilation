PROGRAM runge_kutta_4
!!!!!!!!!!!!!!!!!
!!! ALTER FOR PARALLELIZATION
!!!!!!!!!!!!!!!!!

! adapted from https://tinyurl.com/z9ervow
!  quite easy to generalize this with function pointers to user defined functions, 
!  and reading in upper and lower limits for the iteration values of x
IMPLICIT NONE ! FOR DEBUGGING PURPOSES
 DOUBLE PRECISION x_value, step_size
 DOUBLE PRECISION k_1, k_2, k_3, k_4
 DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: x,y
 INTEGER :: current_iteration = 1
 
 WRITE(*,"(A)",ADVANCE="NO") "Please input your step size: " ! READ IN STEP SIZE FOR LOOP
 READ*, step_size
 
 ALLOCATE( x(CEILING(3/step_size)) )
 ALLOCATE( y(CEILING(3/step_size)) )
 y(current_iteration) = 5
 
 DO x_value=0,3,step_size
  x(current_iteration) = x_value
  k_1 = F_xy(x_value,y(current_iteration))
  k_2 = F_xy(x_value+0.5*step_size,y(current_iteration)+0.5*step_size*k_1);
  k_3 = F_xy((x_value+0.5*step_size),(y(current_iteration)+0.5*step_size*k_2));
  k_4 = F_xy((x_value+step_size),(y(current_iteration)+k_3*step_size));
  
  y(current_iteration+1) = y(current_iteration) + (1/6.0)*(k_1+2*k_2+2*k_3+k_4)*step_size;
  current_iteration = current_iteration+1
  PRINT*, x_value, y(current_iteration)
 END DO

 CONTAINS
  FUNCTION F_xy(t,r) RESULT (k)
   DOUBLE PRECISION t,r,k
   k = 3*EXP(-t)-0.4*r
  END FUNCTION F_xy

END PROGRAM runge_kutta_4
