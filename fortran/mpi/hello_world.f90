PROGRAM hello_world
USE MPI
IMPLICIT NONE
 INTEGER ierr

 CALL MPI_INIT( ierr )
 PRINT*, "Hello, world!"
 CALL MPI_FINALIZE( ierr )

END PROGRAM hello_world