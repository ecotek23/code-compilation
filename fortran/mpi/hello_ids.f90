PROGRAM hello_ids
USE MPI
IMPLICIT NONE
 INTEGER ierr, num_procs, proc_id

 CALL MPI_INIT( ierr )
  CALL MPI_COMM_RANK(MPI_COMM_WORLD, proc_id, ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)

  PRINT*, "Hello, this is process number",proc_id,"out of",num_procs
 CALL MPI_FINALIZE( ierr )

END PROGRAM hello_ids