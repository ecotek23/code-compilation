PROGRAM broadcast_n
USE MPI
 INTEGER broadcast_count, ierr, process_id, current
 INTEGER :: master = 0
 CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,process_id,ierr)
  
  IF (process_id .EQ. master) THEN
   WRITE(*,"(A)",ADVANCE="NO") "Enter the number of iterations you want to use:  "
   READ(*,*) broadcast_count
  END IF

  CALL MPI_BCAST(broadcast_count,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  DO current=1,broadcast_count
   PRINT*,process_id,":",current
  END DO
  PRINT*, "FINISHED THREAD",process_id
 CALL MPI_FINALIZE(ierr)

END PROGRAM broadcast_n
