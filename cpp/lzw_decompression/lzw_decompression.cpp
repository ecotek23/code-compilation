/*
	Refactor utility functions out
*/
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <map>
using namespace std;

int binary_to_integer(vector<int> input) {
	/*
	Read a vector that represents a binary number and
	* output it's decimal representation
	*/
	int result = 0;
	for (int i=0; i<input.size(); i++) {
		result += input[i] * pow(2,input.size()-1-i);
	}
	return result;
}

vector<int> read_out_bits(char input_1, char input_2) {
	/*
	Form a vector of the binary representation of the 
	* chars under examination for a 12-width
	*/
	vector<int> output;
	for(int i = 0; i < 8; i++) {
		output.push_back((input_2 >> i) & 1);
	}
	for (int i = 0; i < 4; i++) {
		output.push_back((input_1 >> i) & 1);
	}
	std::reverse(output.begin(), output.end());
	return output;
}

map<int,char> init_dict() {
	map<int,char> output;
	for (int i=0; i<256; i++) {
		output[i] = (char)i;
	}
	return output;
}

string decompress(string filename) {
	/*
	Decompress the file provided, 
	* using the width indicated
	*/
	ifstream is(filename); // Need to deal with reading a char at a time
	char current;

	while (is.get(current)) {
		cout << current;
	}

	is.close();
	return string("placeholder"); // Change for real output
}

int main(int argc, char* argv[]) {
/*
	// Test of binary conversion, answer is 8
	vector<int> testing = {1,0,0,0};
	cout << binary_to_integer(testing) << endl; 
*/
	// Test of char bit representation, answer is 011000010110
	///////////////
	// NOT CORRECT YET, LOGICAL ERROR
	///////////////
	char a = 'a';
	char b = 'b';
	vector<int> bit_mask = read_out_bits(a,b);

	for (int i : bit_mask) {
		cout << i;
	}
	cout << endl;
/*
	// Test of dictionary creation function
	map<int,char> dict = init_dict();
	for (auto const &item : dict) {
		cout << item.first << " : " << item.second << endl;
	}
*/	
	for (int file_number=1; file_number<argc; file_number++) {
		// Convert each filename to a string for easier handling
		string filename(argv[file_number]);
		cout << filename << endl;

		// Decompress the file under examination
		string output = decompress(filename);
	}
	return 0;
}
