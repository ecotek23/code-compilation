/*
	Implement overloading the I/O operators
*/

#include <iostream>
using namespace std;

int gcd(int a,int b) {
	/*
	Find the greatest common divisor using Euclid's algorithm
	*/
	if (b==0) {
		return a;
	} else {
		return gcd(b,a % b);
	}
}

class Fraction {
	private:
		int a,b;
	public:
		Fraction(int a_,int b_): a(a_), b(b_) {}
		
		friend Fraction operator+(const Fraction &lhs, const Fraction &rhs);
		friend Fraction operator-(const Fraction &lhs, const Fraction &rhs);
		friend Fraction operator*(const Fraction &lhs, const Fraction &rhs);
		friend Fraction operator/(const Fraction &lhs, const Fraction &rhs);

		Fraction const inversion() { // Look at const'ness
			Fraction output(b,a);
			return output;
		}

		void display() {
			cout << a << "/" << b << endl;
		}

		void reduce() { 
			// Reduce the fraction to it's lowest terms
			// using repeated applications of euclids algorithm
			int f = gcd(max(a,b),min(a,b));
			while (f!=1) {
				a /= f;
				b /= f;
				f = gcd(max(a,b),min(a,b));
			}
		}
};

Fraction operator+(const Fraction &lhs, const Fraction &rhs) {
	return Fraction(lhs.a*rhs.b+rhs.a*lhs.b,lhs.b*rhs.b);
}

Fraction operator-(const Fraction &lhs, const Fraction &rhs) {
	return Fraction(lhs.a*rhs.b-rhs.a*lhs.b,lhs.b*rhs.b);
}
Fraction operator*(const Fraction &lhs, const Fraction &rhs) {
	return Fraction(lhs.a*rhs.a,lhs.b*rhs.b);
}

Fraction operator/(const Fraction &lhs, const Fraction &rhs) {
	return lhs*rhs.inversion();
}

int main() {
	//Initialize fractions to use for tests
	Fraction lhs(1,3);
	cout << "LHS is: "; lhs.display();
	Fraction rhs(1,2);
	cout << "RHS is: "; rhs.display();

	//Test fraction addition
	Fraction add = lhs + rhs;
	cout << "LHS+RHS = "; add.display();

	//Test fraction subtraction
	Fraction sub = lhs - rhs;
	cout << "LHS-RHS = "; sub.display();

	//Test fraction multiplication
	Fraction mult = lhs * rhs;
	cout << "LHS*RHS = "; mult.display();

	//Test fraction division
	Fraction div = lhs/rhs;
	cout << "LHS/RHS = "; div.display();

	//Test fraction reduction
	Fraction reduce_check(320,56);
	reduce_check.reduce();
	cout << "320/56 reduced is: "; reduce_check.display();

	return 0;
}