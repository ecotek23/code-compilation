#include <iostream>
using namespace std;

int pointer_summation(int *array, int array_size) {
	int output = 0;
	for (int i=0; i<array_size; i++) {
		output += *(array+i);
	}
	return output;
}

int main() {
	int array_size = 5;
	int x[array_size] = {1,2,3,4,5};

	// Expecting the answer to be 15
	cout << pointer_summation(x,array_size) << endl;
	return 0;
}