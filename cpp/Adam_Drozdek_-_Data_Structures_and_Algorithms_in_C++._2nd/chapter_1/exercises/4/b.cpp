#include <iostream>
#include <vector>
using namespace std;

vector<int> odd_clearance(int *a, int a_len) {
	// Used a vector to avoid having to resize the arrays 
	// since the total output size is unknown at the beginning
	vector<int> output;
	for (int i=0;i<a_len;i++) {
		if (*(a+i) % 2 == 0) {
			output.push_back(*(a+i));
		}
	}
	return output;
}

int main() {
	int testing[10] = {1,2,3,4,5,6,7,8,9,10};
	vector<int> cleared = odd_clearance(testing,10);
	for (int i=0;i<cleared.size();i++) {
		cout << cleared[i] << endl;
	}
	return 0;
}