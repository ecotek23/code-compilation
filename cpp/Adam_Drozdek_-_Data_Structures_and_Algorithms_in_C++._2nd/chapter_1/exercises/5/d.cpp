#include <iostream>
using namespace std;

int strchr(char *array, char find) {
	int current = 0, output = -1;
	while (*(array+current)!=find || *(array+current)=='\0') {
		current++;
	}
	if (*(array+current) == find) {
		output = current;
	}
	return output;
}

int main() { 
	char x[5] = "test";
	cout << "Character found at index: " << strchr(x,'s') << endl;
	return 0;
}