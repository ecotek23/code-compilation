#include <iostream>
using namespace std;

int strcmp(char *a, char *b) {
	int current = 0;
	while (*(a+(current))!='\0' || *(b+(current))!='\0') {
		cout << a[current] << " " << b[current] << endl;
		if (*(a+current)=='\0' && *(b+current)=='\0') {
			return 0;
		} else if (*(a+current) < *(b+current)) {
			return -1;
		} else if (*(a+current) > *(b+current)) {
			return 1;
		} else {
			current++;
		}
	}
}

int main() {
	char a[10] = "unlimited";
	char b[9] = "unlikely";

	cout << strcmp(a,b) << endl;
	return 0;
}