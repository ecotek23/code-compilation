// Not the same signature as the original strcat, need to rewrite to fit that
// At least it works though

#include <iostream>
using namespace std;

int strlen(char *x) {
	int length=0;
	while (*(x+length) != '\0') {
		length++;
	}
	return length;
}

char* strcat(char *a,char *b, int a_len, int b_len, char *out) {
	for (int i=0;i<a_len;i++) {
		*(out+i) = *(a+i);
	}
	for (int i=a_len;i<a_len+b_len;i++) {
		*(out+i) = *(b+(i-a_len));
	}
}

int main() {
	char a[7] = "TOBEOR";
	char b[8] = "NOTTOBE";
	char out[strlen(a)+strlen(b)+1];
	strcat(a,b,strlen(a),strlen(b),out);
	cout << out << endl;
	return 0;
}