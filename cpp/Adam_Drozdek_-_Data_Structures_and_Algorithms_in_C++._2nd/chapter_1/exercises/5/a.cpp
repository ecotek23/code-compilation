#include <iostream>
using namespace std;

int strlen(char *array) {
	int len=0;
	while (*(array+len) != '\0') {
		len++;
	}
	return len;
}

int main() {
	char x[5] = {'t','e','s','t','\0'};
	// Answer should be 4
	cout << strlen(x) << endl;
	return 0;
}