#include <iostream>
#include <memory>
#include <string>
using namespace std;

auto alternative_function(int i) -> int {
	return i+2;
}

int main() {
	unique_ptr<string> a = make_unique<string>();
	*a = "testing";
	cout << "Address: " << a.get() << endl << "Value: " << *a << endl;
	return 0;
}