#define ARRAY_SIZE 10

#include <iostream>
#include <vector>
using namespace std;

template <class T>
int partition(T *A, int lo, int hi) {
	T tmp;
	T pivot = A[hi];
	int i = lo-1;
	for (int j=lo;j<hi;j++) {
		if (A[j] <= pivot) {
			i += 1;
			// Swap array values
			tmp = A[i];
			A[i] = A[j];
			A[j] = tmp;
		}
	}
	tmp = A[i+1];
	A[i+1] = A[hi];
	A[hi] = tmp;
	return i+1;
}

template <class T>
void quicksort(T *A, int lo, int hi) {
	if (lo<hi) {
		int p = partition(A, lo, hi);
		quicksort(A,lo,p-1);
		quicksort(A,p+1,hi);
	}
}

template <class T>
void display(T *array, int size) {
	for (int i=0;i<size;i++) {
		cout << array[i] << " ";
	}
	cout << endl;
}

template <class T>
void initialize_backwards(T *array, int size) {
	for (int i=0;i<size;i++) {
		array[i] = size-i;
	}
}

int main() {
	// Initialize and display
	int test[ARRAY_SIZE];
	initialize_backwards(test,ARRAY_SIZE);
	
	cout << "Original:\n\t";
	display(test,ARRAY_SIZE);
	
	// Sort and display
	quicksort(test,0,ARRAY_SIZE-1);
	
	cout << "Sorted:\n\t";
	display(test,ARRAY_SIZE);
	
	return 0;
}