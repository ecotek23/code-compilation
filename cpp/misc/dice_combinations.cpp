#include <iostream>
using namespace std;

int max_sum(int max_value) {
	int count = 0;
	for (int first=1;first<7;first++) {
                for (int second=1;second<7;second++) {
                        if (first+second <= max_value) {
                                count++;
                        }
                }
        }
	return count;
}

int sum_and_different(int value) {
	int count = 0;
	for (int first=1;first<7;first++) {
		for (int second=1;second<7;second++) {
			if (first+second==value && first!=second) {
				count++;
			}
		}
	}
	return count;
}

int main( void ) {
	int max_value, count = 0;
	cin >> max_value;
	cout << "The number of combinations is " << sum_and_different(max_value);
	return 0;
}