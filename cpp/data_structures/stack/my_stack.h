template <typename T>
class stack {
	private:
		int size;
		int items_length;
		T* items;
	public:
		// Constructor and destructors
		stack(int new_size);
		stack(T *intialization_array,int init_length);

		// Class functions
		int get_length();
		void push(T item);
		T pop();

		// Boolean functions
		bool is_empty();
};
