#include <iostream>

#include "my_stack.cpp"
using namespace std;

int main( void ) {
	stack<float> x(5);
	
	if (x.is_empty()) {
		cout << "The stack is empty" << endl;
	}
	
	cout << "Pushing a few onto the stack" << endl;
	for (int i=0;i<10;i++) {
		x.push(i);
	}
	
	cout << "Popping them all off the stack" << endl;
	for (int i=0;i<10;i++) {
		cout << x.pop() << endl;
	}
	
	cout << "Stack length currently is: " << x.get_length() << endl;
	x.pop(); // Stack should be empty, so will error out
	
	return 0;
}
