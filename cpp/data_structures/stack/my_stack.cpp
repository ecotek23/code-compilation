#include <iostream>

#include "my_stack.h"
using namespace std;

// Constructors and destructors
template <typename T>
stack<T>::stack(int new_size) {
	size = new_size;
	items = new T[size];
	items_length = 0; // No items added yet
}

template <typename T>
stack<T>::stack(T* initialization_array,int init_length) {
	size = 2*init_length; // Ensure sufficient size for initialization
	items = new T[size];
	for (int i=0;i<init_length;i++) {
		items[i] = initialization_array[i];
	}
}

// Class functions
template <typename T>
int stack<T>::get_length() {
	return items_length;
}

template <typename T>
void stack<T>::push(T item) {
	if (items_length==size) { // Stack is full, need to reallocate before pushing
		T *temp = items;
		items = new T[size*2];
		for (int i=0;i<items_length;i++) {
			items[i] = temp[i];
		}
		free(temp);
		size *= 2;
	}
	items_length += 1;
	items[items_length] = item;
}

template <typename T>
T stack<T>::pop() {
	if (not is_empty()) { // Ensure stack isn't empty
		items_length -= 1;
		return items[items_length+1];
	} else {
		cout << "THE STACK IS EMPTY!\nILLEGAL REQUEST MADE!\nEXITING...\n";
		exit(1);
	}
}

// Boolean functions
template <typename T>
bool stack<T>::is_empty() {
	if (items_length==0) {
		return true;
	}
	return false;
}
