#include <iostream>
#include <cstddef>

#include "doubly_linked_list.h"
using namespace std;

int main( void ) {
	dll<float> a(1);
	dll_n<float> *temp;
	for (int i=2;i<10;i++) {
		temp = new dll_n<float>;
		temp->set_value(i);
		a.append(a.get_head_ref(),temp); // SOMETHING WRONG WITH APPENDING A VALUE
	}
	
	temp = a.get_head_ref();
	while (temp->has_next()) {
		cout << temp << " -> ";
		cout << temp->get_value() << endl;
		temp = temp->get_next();
	}
	return 0;
}
