#include <iostream>
using namespace std;

template <typename T>
class dll_n {
	private:
		dll_n *previous = nullptr;
		dll_n *next = nullptr;
		T value;
	
	public:
		// Constructor and destructor prototypes
		dll_n(dll_n<T> *new_previous,dll_n<T> *new_next, T new_value);
		dll_n(dll_n<T> *new_next, T new_value);
		dll_n(T new_value);
		dll_n();
	
		// Class functions
		dll_n<T>* get_next();
		dll_n<T>* get_previous();
		T get_value();
		void set_next(dll_n<T> *new_next);
		void set_previous(dll_n<T> *new_previous);
		void set_value(T new_value);
		void append(dll_n<T> *new_last);
		
		// Boolean functions
		bool has_next();
		bool has_previous();
};

// Constructors
template <typename T>
dll_n<T>::dll_n(dll_n<T> *new_previous,dll_n<T> *new_next, T new_value) {
	previous = new_previous;
	next = new_next;
	value = new_value;
}

template <typename T>
dll_n<T>::dll_n(dll_n<T> *new_next, T new_value) {
	next = new_next;
	value = new_value;
}

template <typename T>
dll_n<T>::dll_n(T new_value) {
	value = new_value;
}

template <typename T>
dll_n<T>::dll_n() {
	value = 0;
}

// Class functions
template <typename T>
dll_n<T>* dll_n<T>::get_next() {
	return next;
}

template <typename T>
dll_n<T>* dll_n<T>::get_previous() {
	return previous;
}

template <typename T>
T dll_n<T>::get_value() {
	return value;
}
		
template <typename T>
void dll_n<T>::set_next(dll_n<T> *new_next) {
	next = new_next;
}
		
template <typename T>
void dll_n<T>::set_previous(dll_n<T> *new_previous) {
	previous = new_previous;
}
		
template <typename T>
void dll_n<T>::set_value(T new_value) {
	value = new_value;
}

template <typename T> // Test this
void dll_n<T>::append(dll_n<T> *new_last) {
	if (has_next()) {
		get_next()->append(new_last);
	} else {
		next = new_last;
	}
}

// Boolean functions
template <typename T>
bool dll_n<T>::has_next() {
	if (next != nullptr) {
		return true;
	}
	return false;
}

template <typename T>
bool dll_n<T>::has_previous() {
	if (previous != nullptr) {
		return true;
	}
	return false;
}
