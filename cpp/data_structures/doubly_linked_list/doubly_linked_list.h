#include "doubly_linked_list_node.h"

template <typename T>
class dll {
	private:
		dll_n<T> head;
		dll_n<T> *head_ref;
	public:
		// Constructor and destructor prototypes
		dll(dll_n<T> new_head); // Implement
		dll(T head_value); // Implement
		dll(); // Just use a default value of 0 for the head
	
		// Class functions
		dll_n<T> *get_head_ref();
		
		dll_n<T> get_head();
		void set_head(dll_n<T> new_head); // Set new head, should require a reset first else memory would suck (although if not using 'new'?)

		void reset(); // Run through the list destroying everything and reset the head node
		void append(dll_n<T>* current, dll_n<T> *new_last); // Add to the end of the list
		void insert(dll_n<T> *insertion, int N); // Implement insertion in the middle
		void replace(dll_n<T> *new_node, int N); // Implement replacement of a node
		
		// Boolean functions
		bool is_empty(); // Check if list is empty
		bool contains(T value_to_find); // Implement value finding
};

// Constructor and destructor prototypes
template <typename T>
dll<T>::dll(dll_n<T> new_head) {
	head = new_head;
	head_ref = &head;
}

template <typename T>
dll<T>::dll(T head_value) : head(head_value) {
	head_ref = &head;
}

template <typename T>
dll<T>::dll() : head(0) {
	head_ref = &head;
}

// Class functions
template <typename T>
dll_n<T>* dll<T>::get_head_ref() {
	return head_ref;
}

template <typename T>
dll_n<T> dll<T>::get_head() {
	return head;
}

template <typename T>
void dll<T>::set_head(dll_n<T> new_head) {
	head = new_head;
}

template <typename T>
void dll<T>::append(dll_n<T> *current, dll_n<T> *new_last) {
	current->append(new_last); // Cascade it through //////////
}
// Boolean functions
