'''
Read a keepnote file structure, extracting the data from the html files it generates into a
singular, larger file

Doesn't obey most of the formatting in the original keepnote documents due to the way that 
HTML reads whitespace. Just an easy way to access the data when keepnote isn't playing ball.
'''

from ConfigParser import SafeConfigParser
from pprint import pprint as p
from BeautifulSoup import BeautifulSoup
import os

def extract_from_file(file_path):
	'''
	Take necessary data from the html file and put it into a dictionary
	'''
	data = {'title':'','body':''}
	soup = BeautifulSoup(open(file_path,'r').read())
	data['title'] = soup.title.string
	data['body'] = soup.body.prettify()
	return data

def write_output(data,output_file):
	'''
	Write each page into the file with basic formatting for style
	'''
	output_file.write('<h2>{}</h2>'.format(data['title']))
	output_file.write('<p>{}</p>'.format(data['body']))

if __name__ == '__main__':
	# Set up the parser and get the variable I need
	parser = SafeConfigParser()
	parser.read('data.cfg')
	path = parser.get('paths','keepnote_path')

	# Walk through the provided path to find the matching filenames and store their locations
	files_to_extract = []
	for root, dirs, files in os.walk(path,topdown=True):
		for filename in files:
			if filename == 'page.html':
				files_to_extract.append(os.path.join(root,filename))

	# Open and initialize the output file
	output = open('concat.html','w')
	output.write('<html><head><title>Keepnote combinations</title></head><body>')

	# Extract and store the data for each page
	for filename in files_to_extract:
		data = extract_from_file(filename)
		write_output(data,output)

	# Finished of the output page and close it
	output.write('</body></html>')
	output.close()