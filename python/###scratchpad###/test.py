import re

comp = re.compile(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)")

my_email = """<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"248be19a84","applicationID":"25187941","transactionName":"elcKEBYLDg9XShkQAVhKBww7CAMNVlFYBDtJWQMBSxcKDEU=","queueTime":3,"applicationTime":17,"agent":""}</script>
<script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var o=t[n]={exports:{}};e[n][0].call(o.exports,function(t){var o=e[n][1][t];return r(o||t)},o,o.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(e,t,n){function r(){}function o(e,t,n){return function(){return i(e,[(new Date).getTime()].concat(u(arguments)),t?null:this,n),t?void 0:this}}var i=e("handle"),a=e(2),u=e(3),c=e("ee").get("tracer"),f=NREUM;"undefined"==typeof window.newrelic&&(newrelic=f);var s=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit"],l="api-",p=l+"ixn-";a(s,function(e,t){f[t]=o(l+t,!0,"api")}),f.addPageAction=o(l+"addPageAction",!0),f.setCurrentRouteName=o(l+"routeName",!0),t.exports=newrelic,f.interaction=function(){return(new r).get()};var d=r.prototype={createTracer:function(e,t){var n={},r=this,o="function"==typeof t;return i(p+"tracer",[Date.now(),e,n],r),function(){if(c.emit((o?"":"no-")+"fn-start",[Date.now(),r,o],n),o)try{return t.apply(this,arguments)}finally{c.emit("fn-end",[Date.now()],n)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){d[t]=o(p+t)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,(new Date).getTime()])}},{}],2:[function(e,t,n){function r(e,t){var n=[],r="",i=0;for(r in e)o.call(e,r)&&(n[i]=t(r,e[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],3:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,o=n-t||0,i=Array(o<0?0:o);++r<o;)i[r]=e[t+r];return i}t.exports=r},{}],ee:[function(e,t,n){function r(){}function o(e){function t(e){return e&&e instanceof r?e:e?c(e,u,i):i()}function n(n,r,o){if(!p.aborted){e&&e(n,r,o);for(var i=t(o),a=v(n),u=a.length,c=0;c<u;c++)a[c].apply(i,r);var f=s[w[n]];return f&&f.push([y,n,r,i]),i}}function d(e,t){b[e]=v(e).concat(t)}function v(e){return b[e]||[]}function g(e){return l[e]=l[e]||o(n)}function m(e,t){f(e,function(e,n){t=t||"feature",w[n]=t,t in s||(s[t]=[])})}var b={},w={},y={on:d,emit:n,get:g,listeners:v,context:t,buffer:m,abort:a,aborted:!1};return y}function i(){return new r}function a(){(s.api||s.feature)&&(p.aborted=!0,s=p.backlog={})}var u="nr@context",c=e("gos"),f=e(2),s={},l={},p=t.exports=o();p.backlog=s},{}],gos:[function(e,t,n){function r(e,t,n){if(o.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[t]=r,r}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){o.buffer([e],r),o.emit(e,t,n)}var o=e("ee").get("handle");t.exports=r,r.ee=o},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!h++){var e=y.info=NREUM.info,t=l.getElementsByTagName("script")[0];if(setTimeout(f.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return f.abort();c(b,function(t,n){e[t]||(e[t]=n)}),u("mark",["onload",a()],null,"api");var n=l.createElement("script");n.src="https://"+e.agent,t.parentNode.insertBefore(n,t)}}function o(){"complete"===l.readyState&&i()}function i(){u("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var u=e("handle"),c=e(2),f=e("ee"),s=window,l=s.document,p="addEventListener",d="attachEvent",v=s.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:v,REQ:s.Request,EV:s.Event,PR:s.Promise,MO:s.MutationObserver},e(1);var m=""+location,b={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-998.min.js"},w=v&&g&&g[p]&&!/CriOS/.test(navigator.userAgent),y=t.exports={offset:a(),origin:m,features:{},xhrWrappable:w};l[p]?(l[p]("DOMContentLoaded",i,!1),s[p]("load",r,!1)):(l[d]("onreadystatechange",o),s[d]("onload",r)),u("mark",["firstbyte",a()],null,"api");var h=0},{}]},{},["loader"]);</script>
<!--
Want to join the crew?

At Hunter, we want to have impact on how professionals get connected. We
spend 80% of our time to work on our product to deliver the perfect experience.
We are looking for talented developers (Ruby on Rails and Go) to join the
adventure.

=======================================================

Learn more about us:    https://hunter.io/about
Current job openings:   https://emailhunter.workable.com/
Contact us:             contact@hunter.io

=======================================================
-->

<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<title>linkedin.com • Hunter
</title>
<meta content='noindex, nofollow' name='robots'>
<meta content='All the email addresses found for the domain name linkedin.com. Hunter is the easiest way to find email addresses.' name='description'>
<script>
  window.rails_env = "production"
</script>
<!-- Optimizely -->
<script src='//cdn.optimizely.com/js/3695763703.js'></script>
<!-- StatusPage -->
<script defer src='https://cdn.statuspage.io/se-v2.js'></script>
<!-- Stripe -->
<script defer src='https://js.stripe.com/v2/' type='text/javascript'></script>
<!-- Algolia -->
<link rel="stylesheet" media="all" href="https://hunter.io/assets/application-246cd6aab5866f0b0777cc677d5927f53f7a0fe9b2fe3d3eb64c5cb06e6c5c92.css" turbolinks-track="turbolinks-track" />
<script src="https://hunter.io/assets/application-27e823d1735d6be5771e20b0bc13367abe738c4fd96fd271792a97a8436e2197.js" turbolinks-track="turbolinks-track" defer="defer"></script>
<meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="XRU1rh1S4QRCQmpGsS79Fg8NRx5xFBG1Zsao8nqtjUp4ikMl71HoGCmiwJtiXsBsa8KXYNC2FzTfz2bGPMNc9Q==" />
<!-- Open Graph -->
<meta content='en_US' property='og:locale'>
<meta content='website' property='og:type'>
<meta content='All the email addresses found for the domain name linkedin.com. Hunter is the easiest way to find email addresses.' property='og:description'>
<meta content='https://hunter.io/assets/head/social_media-67856acb2507ffd3fe6ccd3c73ef037fd1f997a69e3cd372e80a036d5b8a1c6d.png' property='og:image'>
<meta content='https://hunter.io/try/search/linkedin.com' property='og:url'>
<meta content='Hunter' property='og:site_name'>
<meta content='linkedin.com • Hunter' property='og:title'>
<!-- Twitter Card -->
<meta content='summary_large_image' name='twitter:card'>
<meta content='https://hunter.io/assets/head/social_media-67856acb2507ffd3fe6ccd3c73ef037fd1f997a69e3cd372e80a036d5b8a1c6d.png' name='twitter:image:src'>
<meta content='linkedin.com • Hunter' name='twitter:title'>
<meta content='All the email addresses found for the domain name linkedin.com. Hunter is the easiest way to find email addresses.' name='twitter:description'>
<meta content='Email_Hunter' name='twitter:creator'>
<meta content='https://hunter.io' name='twitter:domain'>
<!-- Chrome extension -->
<link href='https://chrome.google.com/webstore/detail/hgmhmanijnjhaffoampdlllchpolkdnj' rel='chrome-webstore-item'>
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="https://hunter.io/assets/head/icon_32-d5796c45076e78aa5cf22dd53c5a4a54155062224bac758a412f3a849f38690b.ico" />
<!-- Color theme (used in Android) -->
<meta content='#333' name='theme-color'>
<!-- Intercom is disabled for some users -->
</head>
<body class='application-front search_landing_page show '>
<script>
//<![CDATA[
window.gon={};gon.environment="production";gon.algolia_app_id="3K7VM5WTA3";gon.algolia_search_api_key="d82321c19d3dd7de8ed897eaf36b9bf7";gon.plan_price={"0":0,"1":49,"2":99,"3":199,"4":399};gon.plan_price_yearly={"0":0,"1":470,"2":950,"3":1920,"4":3830};gon.plan_name={"0":"Free","1":"Starter","2":"Growth","3":"Pro","4":"Enterprise"};gon.plan_requests={"0":150,"1":1000,"2":5000,"3":20000,"4":50000};gon.priority_support=true;
//]]>
</script>

<div id='domain-name'>linkedin.com</div>
<div class='container'>
<nav class='navbar'>
<div class='container'>
<div class='navbar-header'>
<div class='navbar-brand'>
<a href='/'>
<svg height='25' space='preserve' version='1.1' viewbox='0 0 653.08002 116.24' width='138' xmlns:cc='http://creativecommons.org/ns#' xmlns:dc='http://purl.org/dc/elements/1.1/' xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' xmlns:svg='http://www.w3.org/2000/svg' xmlns='http://www.w3.org/2000/svg'>
<metadata id='metadata8'>
<rdf>
<work rdf:about=''>
<format>image/svg+xml</format>
<type rdf:resource='http://purl.org/dc/dcmitype/StillImage'></type>
</work>
</rdf>
</metadata>
<defs id='defs6'></defs>
<g id='g10' transform='matrix(1.3333333,0,0,-1.3333333,0,116.24)'>
<g id='g12' transform='scale(0.1)'>
<path d='m 1790.94,21.7969 c -6.47,-6.4727 -14.13,-9.6875 -23.01,-9.6875 h -92.03 c -8.89,0 -16.55,3.2148 -23,9.6875 -6.48,6.4531 -9.69,14.1133 -9.69,23.0078 V 352.371 c 0,50.043 -11.71,89.192 -35.12,117.457 -23.42,28.25 -57.72,42.383 -102.92,42.383 -41.99,0 -75.5,-14.531 -100.51,-43.594 -25.03,-29.062 -37.54,-67.808 -37.54,-116.246 V 44.8047 c 0,-8.8945 -3.23,-16.5547 -9.68,-23.0078 -6.47,-6.4727 -14.14,-9.6875 -23.01,-9.6875 h -92.03 c -8.89,0 -16.56,3.2148 -23.01,9.6875 -6.47,6.4531 -9.69,14.1133 -9.69,23.0078 V 839.152 c 0,8.875 3.22,16.539 9.69,23.008 6.45,6.453 14.12,9.692 23.01,9.692 h 92.03 c 8.87,0 16.54,-3.239 23.01,-9.692 6.45,-6.469 9.68,-14.133 9.68,-23.008 V 581.23 c 48.44,48.438 106.56,72.657 174.37,72.657 91.22,0 157.21,-26.246 197.99,-78.711 40.75,-52.481 61.15,-120.692 61.15,-204.645 V 44.8047 c 0,-8.8945 -3.24,-16.5547 -9.69,-23.0078' id='path14' style='fill:#ff5722;fill-opacity:1;fill-rule:nonzero;stroke:none'></path>
<path d='m 1928.96,632.094 c 6.45,6.445 14.12,9.683 23.01,9.683 H 2044 c 8.87,0 16.54,-3.238 23.01,-9.683 6.45,-6.473 9.68,-14.137 9.68,-23.008 V 301.512 c 0,-50.856 10.69,-90.211 32.09,-118.059 21.38,-27.855 54.68,-41.781 99.9,-41.781 41.97,0 74.47,14.324 97.48,42.992 23.01,28.645 34.51,67.602 34.51,116.848 v 307.574 c 0,8.871 3.22,16.535 9.69,23.008 6.45,6.445 14.11,9.683 23,9.683 h 92.03 c 8.88,0 16.54,-3.238 23.01,-9.683 6.45,-6.473 9.69,-14.137 9.69,-23.008 V 44.8047 c 0,-8.8945 -3.24,-16.5547 -9.69,-23.0078 -6.47,-6.4727 -14.13,-9.6875 -23.01,-9.6875 h -92.03 c -8.89,0 -16.55,3.2148 -23,9.6875 -6.47,6.4531 -9.69,14.1133 -9.69,23.0078 V 72.6523 C 2293.84,24.2188 2237.74,0 2172.35,0 c -91.23,0 -156.2,26.0352 -194.95,78.1016 -38.75,52.0664 -58.13,120.4844 -58.13,205.2504 v 325.734 c 0,8.871 3.22,16.535 9.69,23.008' id='path16' style='fill:#ff5722;fill-opacity:1;fill-rule:nonzero;stroke:none'></path>
<path d='m 3210.09,21.7969 c -6.47,-6.4727 -14.14,-9.6875 -23.01,-9.6875 h -92.03 c -8.89,0 -16.55,3.2148 -23.01,9.6875 -6.47,6.4531 -9.68,14.1133 -9.68,23.0078 V 352.371 c 0,50.043 -11.71,89.192 -35.12,117.457 -23.42,28.25 -57.73,42.383 -102.92,42.383 -41.99,0 -75.5,-14.531 -100.51,-43.594 -25.03,-29.062 -37.54,-67.808 -37.54,-116.246 V 44.8047 c 0,-8.8945 -3.23,-16.5547 -9.68,-23.0078 -6.48,-6.4727 -14.14,-9.6875 -23.01,-9.6875 h -92.03 c -8.89,0 -16.56,3.2148 -23.01,9.6875 -6.47,6.4531 -9.69,14.1133 -9.69,23.0078 V 609.086 c 0,8.871 3.22,16.535 9.69,23.008 6.45,6.445 14.12,9.683 23.01,9.683 h 92.03 c 8.87,0 16.53,-3.238 23.01,-9.683 6.45,-6.473 9.68,-14.137 9.68,-23.008 V 581.23 c 48.44,48.438 106.56,72.657 174.37,72.657 91.22,0 157.21,-26.246 197.98,-78.711 40.76,-52.481 61.15,-120.692 61.15,-204.645 V 44.8047 c 0,-8.8945 -3.23,-16.5547 -9.68,-23.0078' id='path18' style='fill:#ff5722;fill-opacity:1;fill-rule:nonzero;stroke:none'></path>
<path d='m 3571.54,178.605 c 14.11,-14.128 38.54,-21.187 73.26,-21.187 h 73.86 c 8.88,0 16.54,-3.234 23.01,-9.688 6.45,-6.472 9.69,-14.136 9.69,-23.007 V 44.8047 c 0,-8.8945 -3.24,-16.5547 -9.69,-23.0078 -6.47,-6.4727 -14.13,-9.6875 -23.01,-9.6875 h -89.6 c -76.71,0 -135.23,18.7695 -175.59,56.3086 -40.37,37.535 -60.54,93.426 -60.54,167.707 v 260.344 h -79.92 c -8.89,0 -16.56,3.219 -23.01,9.687 -6.47,6.449 -9.68,14.114 -9.68,23.008 v 79.922 c 0,8.871 3.21,16.535 9.68,23.008 6.45,6.445 14.12,9.683 23.01,9.683 h 79.92 v 197.375 c 0,8.875 3.21,16.539 9.69,23.008 6.45,6.453 14.11,9.692 23,9.692 h 92.03 c 8.88,0 16.54,-3.239 23.01,-9.692 6.45,-6.469 9.69,-14.133 9.69,-23.008 V 641.777 h 156.2 c 8.88,0 16.54,-3.238 23.01,-9.683 6.45,-6.473 9.69,-14.137 9.69,-23.008 v -79.922 c 0,-8.894 -3.24,-16.559 -9.69,-23.008 -6.47,-6.468 -14.13,-9.687 -23.01,-9.687 h -156.2 V 239.758 c 0,-26.641 7.05,-47.035 21.19,-61.153' id='path20' style='fill:#ff5722;fill-opacity:1;fill-rule:nonzero;stroke:none'></path>
<path d='m 4233.29,397.176 c -8.89,74.262 -53.7,111.398 -134.41,111.398 -41.98,0 -74.89,-11.308 -98.69,-33.902 -23.82,-22.606 -37.35,-48.434 -40.57,-77.496 z m 21.8,214.933 c 45.2,-27.847 80.1,-65.199 104.74,-112.007 24.61,-46.832 36.93,-98.082 36.93,-153.782 v -54.496 c 0,-8.886 -3.23,-16.554 -9.69,-23.008 -6.47,-6.468 -14.13,-9.683 -23,-9.683 h -404.45 c 0,-34.723 13.72,-62.36 41.18,-82.949 27.43,-20.586 60.13,-30.875 98.08,-30.875 44.39,0 78.71,9.269 102.93,27.851 8.87,6.449 16.14,10.899 21.79,13.317 5.64,2.421 13.32,3.632 23.01,3.632 h 96.87 c 8.87,0 16.54,-3.027 23.01,-9.082 6.45,-6.05 9.68,-13.129 9.68,-21.187 0,-16.957 -11.1,-38.356 -33.29,-64.1837 C 4320.66,69.8125 4288.57,47.4141 4246.61,28.4531 4204.62,9.49609 4155.37,0 4098.88,0 c -59.75,0 -112.01,13.7188 -156.81,41.168 -44.81,27.4336 -79.32,65.789 -103.53,115.035 -24.22,49.231 -36.33,106.145 -36.33,170.738 0,60.547 12.11,115.641 36.33,165.289 24.21,49.645 58.91,89 104.13,118.063 45.2,29.062 97.27,43.594 156.21,43.594 58.92,0 110.99,-13.926 156.21,-41.778' id='path22' style='fill:#ff5722;fill-opacity:1;fill-rule:nonzero;stroke:none'></path>
<path d='m 4681.92,464.379 c -21.4,-21.399 -32.09,-56.309 -32.09,-104.742 V 44.8047 c 0,-8.8945 -3.24,-16.5547 -9.69,-23.0078 -6.47,-6.4727 -14.13,-9.6875 -23.01,-9.6875 h -92.02 c -8.9,0 -16.56,3.2148 -23.01,9.6875 -6.47,6.4531 -9.69,14.1133 -9.69,23.0078 V 609.086 c 0,8.871 3.22,16.535 9.69,23.008 6.45,6.445 14.11,9.683 23.01,9.683 h 92.02 c 8.88,0 16.54,-3.238 23.01,-9.683 6.45,-6.473 9.69,-14.137 9.69,-23.008 V 581.23 c 21.79,20.981 44.8,36.329 69.02,46.016 24.22,9.688 54.89,14.531 92.03,14.531 h 54.49 c 8.87,0 16.53,-3.238 23.01,-9.683 6.45,-6.473 9.68,-14.137 9.68,-23.008 v -79.922 c 0,-8.894 -3.23,-16.559 -9.68,-23.008 -6.48,-6.468 -14.14,-9.687 -23.01,-9.687 h -78.71 c -48.44,0 -83.36,-10.707 -104.74,-32.09' id='path24' style='fill:#ff5722;fill-opacity:1;fill-rule:nonzero;stroke:none'></path>
<path d='m 537.324,496.277 c -18.015,-5.183 -37.879,-5.269 -56.453,-4.484 -21.605,0.914 -44.484,3.645 -64.433,12.512 -16.415,7.293 -29.583,20.453 -22.692,39.461 4.859,13.406 15.629,25.382 28.078,32.132 13.996,7.594 29.375,6.36 43.379,-0.503 15.723,-7.707 28.801,-20.243 40.656,-32.891 6.215,-6.633 12.145,-13.52 17.996,-20.473 2.7,-3.199 21.008,-21.484 13.469,-25.754 z m 299.461,-6.535 c -27.707,6.446 -60.445,4.137 -88.222,8.578 -43.754,7 -108.926,58.016 -144.254,109.871 -24.547,36.032 -36.262,72.731 -55.028,117.45 -13.097,31.214 -41.656,129.941 -81.593,83.73 -16.446,-19.031 -34.856,-81.68 -48.305,-80.797 -9.961,-2.316 -11.586,17.098 -14.738,25.395 -5.723,15.066 -8.758,32.496 -16.426,46.703 -6.129,11.355 -14.176,24.957 -26.5,30.476 -21.957,11.29 -48.43,-22.738 -61.567,-63.089 C 294.535,746.422 256.773,704.48 240.328,694.336 165.504,648.164 15.3281,599.52 0.0117188,437.543 c -0.0625001,-0.672 0,-2.715 2.5429712,-0.875 C 20.4648,449.637 223.613,595.953 93.2383,313.129 13.3672,139.855 399,29.5117 418.824,29.0625 c 2.672,-0.0586 2.477,1.1563 2.567,1.957 30.773,276.5545 334.781,247.4415 405.578,313.6755 70.16,65.629 71.59,129.157 9.816,145.047' id='path26' style='fill:#ff5722;fill-opacity:1;fill-rule:nonzero;stroke:none'></path>
</g>
</g>

</svg>
</a>
</div>
</div>
<div class='collapse navbar-collapse'>
<div class='pull-right'>
<ul class='grey'>
<li class='nav-cta-link'>
<a class='orange-btn' href='/users/sign_up'>Sign up</a>
</li>
<li class='nav-right-link'>
<a href='/users/sign_in'>Log in</a>
</li>
</ul>
</div>
</div>
</div>
</nav>
</div>
<div class='container'>
<div class='search-container'>
<h1>
linkedin.com
<div class='fa fa-spin fa-spinner loader light-grey'></div>
</h1>
<div class='subtitle'>All the email addresses for linkedin.com</div>
<hr>
<div class='grey'>
<div class='search-message'></div>
<div class='search-pattern'></div>
</div>
<div class='search-results'></div>
<div class='search-cta'></div>
</div>
</div>
<div class='landing-footer light-grey'>
<div class='container'>
<a href='/'>Visit homepage</a>
•
<a href='/chrome'>Chrome extension</a>
•
<a href='/api'>API</a>
</div>
</div>

<script id="IntercomSettingsScriptTag">window.intercomSettings = {"app_id":"lmpce2dm"};(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/lmpce2dm';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}};})()</script>
</body>
</html>
"""

dets = re.findall(comp,my_email)

from validate_email import validate_email

valids = {}
for email in dets:
	valids[email] = validate_email(email)

from pprint import pprint as p
p(valids)