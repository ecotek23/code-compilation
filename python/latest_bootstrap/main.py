#!/usr/bin/python3
from bs4 import BeautifulSoup
import requests
import os

def save_url_data(href):
	'''
	Save the zip-file from the provided URL
	'''
	dest_name = os.path.basename(href)
	with open(dest_name,'wb') as dest:
		data = requests.get(href)
		dest.write(data.content)
		
def parse_out_href(page_url):
	'''
	Parse page html to find URL of latest version
	'''
	soup = BeautifulSoup(requests.get(page).content,'lxml')
	row = soup.find('div',{'class':'row bs-downloads'})
	href = row.find_all('a',{'class':'btn btn-lg btn-outline'})[0]['href']
	
	return href


if __name__=='__main__':
	page = "https://getbootstrap.com/getting-started/#download"
	href = parse_out_href(page)
	save_url_data(href)
