je regarde ...		I watch ...
les dessins animés		cartoons
les documentaires		documentaries
les émissions de sport		sports programmes
les émissions de télé-réalité		reality TV shows
les émissions musicales		music shows
les infos		the news
les jeux télévisés		game shows
la météo		the weather
les séries		series
les séries policières		police series
les séries américaines		American series
Mon émission préférée, c'est ...		My favourite programme is ...
j'adore		I love
j'aime bien		I like
je n'aime pas		I don't like
je ne regarde jamais		I never watch
je ne rate jamais		I never miss
j'aime		I like
je suis fan de		I'm a fan of
je ne suis pas fan de		I'm not a fan of
j'ai une passion pour les		I have a passion for
j'ai horreur des		I really dislike
je déteste		I hate
les comédies		comedies
les films d'action		action films
les films d'amour		romantic films
les films d'arts martiaux		martial-arts films
les films d'aventure		adventure films
les films fantastiques		fantasy films
les films d'horreur		horror films
les films de science-fiction		science-fiction films
mon acteur préféré, c'est		my favourite actor is
mon film préféré, c'est		my favourite film is
je lis		I'm reading
une BD		a comic book
un livre sur les animaux		a book on animals
un livre d'épouvante		a horror story
un magazine sur les célébrités		a magazine about celebrities
un manga		a manga
un roman fantastique		a fantasy novel
un roman policier		a thriller
un roman d'amour		a love story
à mon avis, c'est		in my opinion, it's
je pense que c'est		I think it's
je trouve ça		I find it
amusant		funny
assez bien		quite good
barbant		boring
chouette		excellent
effrayant		frightening
émouvant		moving
ennuyeux		boring
génial		great
intéressant		interesting
nul		rubbish
passionnant		exciting
pratique		practical
stupide		stupid
formidable		great
idiot		stupid
J'envoie des e-mails		I send emails
Je fais beaucoup de choses		I do lots of things
Je fais des recherches pour mes devoirs		I do research for my homework
Je fais des achats		I buy things
Je fais des quiz		I do quizzes
Je joue à des jeux en ligne		I play games online
Je mets à jour ma page perso		I update my homepage
Je vais sur mes sites préférés		I go onto my favourite sites
Je vais sur des blogs		I go onto blogs
Je vais sur des forums		I go onto forums
J'ai discuté		I discussed/chatted
J'ai écouté la radio		I listened to the radio
J'ai envoyé des SMS		I sent text messages
J'ai joué à des jeux en ligne		I played games online
J'ai posté des photos		I posted photos
J'ai regardé la télé		I watched TV
J'ai regardé des clips vidéo		I watched video clips
J'ai surfé sur Internet		I surfed the net
J'ai tchatté sur MSN		I chatted on MSN
J'ai téléchargé des chansons		I downloaded songs
assez		quite
aussi		also
car		because
comme		as
et		and
mais		but
très		very
un peu		a bit
parce que		because
par exemple		for example
surtout		above all
d'habitude		usually
de temps en temps		from time to time
en ce moment		at the moment
quelquefois		sometimes
souvent		often
tous les jours		every day
une ou deux fois par mois		once or twice a month
après (le dîner)		after (dinner)
avant (de me coucher)		before (I go to bed)
d'abord		first
ensuite		next
puis		then
un peu plus tard		a bit later
J'ai gagné un concours		I won a competition.
J'ai passé une semaine à Paris		I spent a week in Paris.
J'ai visité la tour Eiffel		I visited the Eiffel Tower.
J'ai mangé au restaurant		I ate in a restaurant.
J'ai admiré la Pyramide du Louvre		I admired the Louvre Pyramid.
J'ai regardé le feu d'artifice		I watched the fireworks.
J'ai acheté des souvenirs		I bought some souvenirs.
J'ai rencontré un beau garçon		I met a good-looking boy
J'ai rencontré une jolie fille		I met a pretty girl
J'ai envoyé des cartes postales		I sent some postcards
J'ai pris des photos		I took some photos
J'ai vu la Joconde		I saw the Mona Lisa
J'ai attendu le bus		I waited for the bus
J'ai très bien dormi		I slept very well
Je n'ai pas visité Notre-Dame		I didn't visit Notre-Dame
On a fait les magasins		We went shopping
On a bu un coca		We drank a cola
On a fait un tour de la ville en segway		We did a tour of the town by segway
On a fait une balade en bateau-mouche		We went on a boat trip
aujourd'hui		today
hier		yesterday
avant-hier		the day before yesterday
(mardi) dernier		last (Tuesday)
C'était		It was
J'ai trouvé ça		I found it
bien		good
bizarre		weird
cool		cool
cher		expensive
effrayant		scary
ennuyeux		boring
fabuleux		wonderful
génial		great
horrible		horrible
intéressant		interesting
marrant		funny
nul		rubbish
Ce n'était pas mal		It wasn't bad
en avion		by plane
en bus		by bus
en car		by coach
en métro		by underground
en train		by train
en voiture		by car
à vélo		by bicycle
à pied		on foot
Je suis allé(e) à Paris		I went to Paris
Je suis parti(e) à dix heures		I left at ten o'clock
Je suis arrivé(e) à dix heures		I arrived at ten o'clock
Le train est parti à		The train left at
Le train est arrivé à		The train arrived at
Je suis sorti(e)		I went out
Je suis resté(e) (chez moi)		I stayed (at home)
Je suis rentré(e)		I went/got home
Je suis monté(e)		I went up
à quelle heure?		at what time?
quand?		when?
combien?		how much/how many?
combien de temps?		how long?
comment?		how?
où?		where?
qui?		who?
avec qui?		who with?
alors		so, therefore
donc		so, therefore
car		because
parce que		because
dernier/dernière		last
beaucoup (de)		a lot (of)
d'abord		first of all
ensuite		next
après		afterwards
finalement		finally
Je suis		I am
Je pense que je suis		I think I'm
Je ne suis pas		I'm not
Je ne suis pas du tout		I'm not at all
Mon meilleur ami est		My best friend is
adorable		adorable
arrogant(e)		arrogant
amusant(e)		funny
casse-pieds		annoying
curieux/curieuse		curious
débrouillard(e)		resourceful
drôle		funny
égoïste		selfish
gentil(le)		nice
intelligent(e)		intelligent
optimiste		optimistic
paresseux/paresseuse		lazy
patient(e)		patient
pénible		annoying
pessimiste		pessimistic
rigolo(te)		funny
sociable		sociable
sympa		nice
s'amuser		to have fun
se chamailler		to squabble
se confier des secrets		to tell each other secrets
se dire		to tell each other
se disputer		to argue
s'entendre		to get on
se fâcher		to get angry
Mon chanteur préféré c'est		My favourite male singer is
Ma chanteuse préférée c'est		My favourite female singer is
Mon groupe préféré, c'est		My favourite group is
J'adore la musique de X.		I love X's music
Je déteste la musique de X		I hate X's music
J'adore la chanson		I love the song
Ça me donne envie de		It makes me want to
danser		dance/
chanter		sing
pleurer		cry
dormir		sleep
Ça me rend		It makes me
joyeux/joyeuse		happy
triste		sad
le hard rock		hard rock
le jazz		jazz
la musique classique		classical music
le pop-rock		pop
le rap		rap
le R'n'B		R'n'B
un peu de tout		a bit of everything
les chorégraphies		choreography
les mélodies		tunes
les paroles		words
Normalement, je porte		Normally, I wear
des baskets		trainers
des bottes		boots
des chaussures		shoes
une chemise		a shirt
un chapeau		a hat
un jean		jeans
une jupe		a skirt
un pantalon		trousers
un pull		a jumper
un sweat à capuche		a hoodie
un tee-shirt		a T-shirt
une veste		a jacket
beige		beige
blanc(he)		white
bleu turquoise		turquoise
gris(e)		grey
marron chocolat		chocolate brown
noir(e)		black
orange		orange
vert kaki		khaki
J'ai un style plutôt		My style is rather
classique		classic
décontracté		relaxed
skateur		skater
sportif		sporty
C'est		It's
moche		ugly
horrible		horrible
cool		cool
chic		chic
alors		so
ben		well
euh		huh
ouah!		wow!
voyons		let's see
avec		with
bien		well
comme d'hab		as usual
en général		in general
en plus		in addition
ensemble		together
même		same
normalement		normally
ou		or
par moments		at times
partout		everywhere
plutôt		rather
quand		when
sinon		otherwise
surtout		especially
souvent		often
tout(e)		all, every
tout le temps		all the time
vraiment		really
j'habite		I live
la maison		house
l'appartement		flat
la rue		street/road
à la campagne		in the country
dans un village		in a village
dans une ville		in a town
petit		small
grand		big
beau/belle		beautiful
joli(e)		pretty
vieux/vieille		old
nouveau/nouvelle		new
neuf/neuve		brand new
moderne		modern
confortable		comfortable
gros(se)		big/fat
Chez moi il y a		In my home there is/are
la chambre (de mes parents)		(my parents') bedroom
ma chambre		my bedroom
la cuisine		kitchen
le jardin		garden
la salle à manger		dining room
la salle de bains		bathroom
le salon		living room
les toilettes		toilet
Il n'y a pas de		There isn't a
l'armoire		wardrobe
le bureau		desk
le canapé/la chaise		sofa/chair
la douche		shower
la fenêtre		window
le frigo		fridge
le lavabo		wash basin
le lit		bed
la machine à laver		washing machine
la télé (satellite)		(satellite) TV
dans/devant		in
devant		in front of
derrière		behind
entre		between
sous		under(neath)
sur		on
à côté de		next to
à droite de		on the right of
à gauche de		on the left of
en face de		opposite
Qu'est-ce que tu prends pour le petit déjeuner?		What do you have for breakfast?
Je mange		I eat
Je prends		I have
du beurre / du pain		butter
du pain		bread
de la confiture		jam
des céréales		cereals
un croissant		a croissant
un pain au chocolat		a pain au chocolat
une baguette		a baguette
une brioche		a brioche
une tartine		a slice of bread and butter
Je bois		I drink
du café		coffee
du lait		milk
du thé		tea
du chocolat chaud		hot chocolate
du jus d'orange		orange juice
du fromage		cheese
du poisson		fish
du poulet		chicken
du riz		rice
de la soupe		soup
de la viande		meat
des crêpes		pancakes
des crudités		crudités
des escargots		snails
des légumes		vegetables
des pâtes		pasta
des plats à emporter		takeaway food
des pommes de terre		potatoes
des tomates		tomatoes
un fruit		a piece of fruit
un steak-frites		steak and chips
un yaourt		a yoghurt
une mousse au chocolat		a chocolate mousse
Je suis végétarien(ne)		I'm a vegetarian
Il faut acheter		You must buy
du chocolat		chocolate
du fromage / du jambon		cheese
du jambon		ham
de la crème Chantilly		whipped cream
de la farine		flour
des bananes		bananas
des champignons		mushrooms
des fraises		strawberries
des œufs		eggs
des pommes		apples
un litre de		a litre of
un paquet de		a packet of
une tranche de		a slice of
cinq cents grammes de		500 grams of
un kilo de		a kilo of
une tablette de		a bar of
une bombe de		a spray can of
chez		at someone's home
ici		here
là		there
là-bas		over there
voici		here is/here are
plus		more
moins		less
il y a		there is/there are
pour		f