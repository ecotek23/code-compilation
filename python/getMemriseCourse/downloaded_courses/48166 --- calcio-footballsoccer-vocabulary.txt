il cartellino giallo		yellow card
il cartellino rosso		red card
il colpo di testa		header
il fallo		foul
il fuorigioco		offsides
il gol		goal
il palo		goalpost 
il pallone 		the football/ Soccer ball
l'ammonizione		sending off
l'ostruzione		obstruction
la partita		match
la traversa		crossbar 
il rete		goal
la porta		goal (the net)
i minuti di recupero		injury time
il fallo di mano		handball
la parata 		save
il pareggio		draw
lo stadio		stadium
il campo di calcio		field
l'area di rigore		penalty area
la bandierina di calcio d'angolo		cornerflag 
la linea di fondo		goal line 
la linea di metà campo		half-way line
la linea laterale		touch line
lo spogliatoio		locker room 
la curva		seats behing the goals
la tribuna		seats along the touchline 
il cerchietto del calcio di rigore		penalty spot
il centrocampista		midfielder
il libero		sweeper
il guardalinee		linesman
l'ala		winger
l'allenatore		coach/trainer
il mister		coach
l'arbitro		referee
la mezz'ala		striker (inside forward) 
la riserva		substitute
la squadra		team
l'attacco		attack
la difesa		defence
l'attaccante		attacker
la panchina		bench
il portiere		goalkeeper 
lo stopper		inside defender 
i tifosi		fans
i campioni		champions 
il capitano 		captain
il calciatore		footballer
Regista		Deep-lying playmaker
Trequartista		Attacking playmaker
i calzoncini		shorts
i calzini		socks
il parastinchi		shin guard
la maglia		shirt (jersey)
la scarpa da calcio		boots
I guanti del portiere 		goalkeeper's gloves
Il calcio d'angolo		corner kick
il corner		corner kick
il calcio di punizione		free kick
il calcio di rigore		penalty kick
il rigore		penalty kick
il calcio di rinvio		goal kick
il calcio d'inizio		kickoff 
l'arresto (della palla)		receiving the ball
il passaggio diretto		pass
il passaggio corto		short pass
la respinta di pugno		save with the fists
la rimessa laterale		throw in
la rovesciata		bicycle kick
il dribbling		dribble
l'intervento		tackle
segnare un gol		to score a goal
Tirare un calcio		to kick
vincere		to win
perdere		to lose
andare espulso		to send off
meritare		to deserve
pareggiare		to equalize/ to draw
lanciare		to throw
Giocare a calcio 		to play football
Guardare a calcio		to pay attention, to watch out, to be careful
la coppa del mondo		world cup
i cori		songs/chants
la sciarpa		scarf
la bandiera		flag
che parata!		what a save!
Mandalo a casa!		send him off!
Che gol! 		What a gol!
Forza...		Let's go (team name)
Gli Ultra		Hooligans
è buttato		he dove!
i rossoneri		AC Milan
I Nerazzurri 		Inter MIlan
I bianconeri		Juventus
I giallorossi 		A.S. Roma
I biancazzurri 		S.S. Lazio
I Rossoblu 		Genoa C.F.C
La viola		ACF Fiorentina 
Partenopei		S.S.C. Napoli
Il Granata		Torino FC
gli azzurri		Italian National Team
Le Zebrette 		Udinese 
Felsinei		Bologna F.