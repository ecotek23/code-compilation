macska		cat
kutya		dog (k)
tehén (tehenet, tehenek, tehene)		cow
bika		bull
patkány		rat (p)
disznó		pig (d)
tyúk		hen (ty)
kakas		cock
lúd		goose (l)
méh		bee (m)
darázs		wasp (d)
poszméh		bumblebee
giliszta		earthworm
légy		fly (bug) (l)
szúnyog		mosquito
galamb		pigeon
sólyom		falcon
hal		fish
tonhal		tuna
cápa		shark (c)
bálna		whale (b)
ponty		carp (p)
harcsa		catfish (h)
béka		frog
egér		mouse
varangy		toad
lepke		butterfly (l)
pillangó		butterfly
molylepke		moth (m)
ló (lovat, lovak, lova)		horse
hangya		ant (h)
kacsa		duck (k)
denevér		bat (animal) (d)
bagoly		owl (b)
teknős		turtle (t)
gólya		stork (g)
farkas		wolf (f)
róka		fox (r)
őz		deer
szarvas		red deer (sz)
bárány		lamb (b)
birka		sheep
vaddisznó		wild boar (v...d...)
nyúl		rabbit (ny)
fácán		pheasant
kecske		goat (k)
medve		bear (animal) (m)
gyík		lizard (gy)
kígyó		snake
csótány		roach
sirály		seagull (s)
pingvin		penguin
delfin		dolphin
jegesmedve		polar bear (j..m..)
oroszlán		lion
elefánt		elephant
gepárd		cheetah (g)
párduc		panther (p)
zsiráf		giraffe (zs)
majom		monkey (m)
csimpánz		chimpanzee
teve		camel (t)
egér		mouse (both animal and computer) (e)
pézsmapatkány		muskrat (p...p...)
menyétfélék		mustelids (badgers, weasels, ...) (m...f...)
tengeri vidra		sea otter (t v)
fóka		seal (f)
juh		sheep (j)
földimalac		aardvark (f...m...)
alpaka		alpaca (a)
hangyász		anteater (h)
húsevő		carnivore (h...e...)
marha		cattle (m)
csincsilla		chinchilla (cs)
mókus		chipmunk (m)
szamár		donkey (sz)
vadászgörény		ferret (v...g...)
ember		person
cica		cat (c)
hiúz		lynx (h)
jávorszarvas		moose (j...sz...)
puma		mountain lion (p)
pézsmatulok		musk oxen (p)
narvál		narwhal (n)
nutria		nutria (n)
párducmacska		ocelot (p...m...)
okapi		okapi
onager		onager
oposszum		opossum
orángután		orangutan
gyilkos bálna		orca (gy b)
oryx		oryx
vidra		otter (v)
ökör		ox (ö)
pandamaci		panda (p...m...)
malac		piglet (m)
méhlepényesek		placental mammals (m)
csőrös emlős		platypus (cs e)
sül		porcupine (s)
delfin		porpoise (d)
mosómedve		raccoon (m...m...)
vörös óriáskenguru		red kangaroo (v ó...k..)
vörös farkas		red wolf (v f)
orrszarvú		rhinoceros (o...sz...)
rágcsáló		rodent (r...cs...)
oroszlánfóka		sea lion (o...f...)
cickány		shrew (c)
bűzösborz		skunk (b...b...)
lajhár		sloth (l)
mókus		squirrel (m)
tapir		tapir
tasmán ördög		Tasmanian devil (t ö)
tigris		tiger
patások		ungulates
vámpírdenevér		vampire bat (v...d...)
kis kenguru		wallaby (k k)
rozmár		walrus (r)
szavannai varacskosdisznó		warthog (sz v...d...)
menyét		weasel (m)
torkosborz		wolverine
amerikai mormota		woodchuck (a m)
jak		yak
zebra		zebra
antilop		antelope
majom		monkey (ape)
tatu		armadillo (t)
pávián		baboon (p)
borz		badger (b)
hód		beaver (h)
fehér delfin		beluga whale (f d)
kanadai vadjuh		bighorn sheep (k v...j...)
kék bálna		blue whale (k b)
kis észak-amerikai híuz		bobcat (k é-a h)
bonobó		bonobo
szenegáli fülesmaki		bushbaby (sz f...m...)
afrikai bivaly		cape buffalo (a b )
rénszarvas		caribou (r...sz...)
prérifarkas		coyote (p...f...)
kölyök		cub (k)
dingó		dingo
dromedár		dromedary
hangyász		echidna (h)
jávorantilop		eland (j...a...)
elefántfóka		elephant seal (e...f...)
hermelin		ermine (h)
sivatagi róka		fennec fox (s r)
repülő mókus		flying squirrel (r m)
gyümölcsevő denevér		fruit bat
indiai erszényes borz		bandicoot (i e b)
amerikai jávorszarvas		elk
gazella		gazelle
mongol futóegér		gerbil
gibbon		gibbon
gnú		gnu
gorilla		gorilla
emberfélék		great apes
szürkemedve		grizzly bear
erdei mormota		groundhog
tengerimalac		guinea pig
hörcsög		hamster
sündisznó		hedgehog
növényevő állat		herbivore
víziló		hippopotamus
hiéna		hyena
szirtiborz		hyrax
kőszáli kecske		ibex
impala		impala
rovarevő		insectivore
jaguár		jaguar
kenguru		kangaroo
kengurupatkány		kangaroo rat
koala		koala
lemmingpocok		lemming
makimajom		lemur
leopárd		leopard
láma		llama
szürke szarvasmarha		longhorn
lajhármaki		loris
makákó		macaque
emlős		mammal
mamut		mammoth
lamantin		manatee
drill		mandrill
selyemmajom		marmoset
mormota		marmot
erszényes		marsupial
szurikáta		meerkat
nyérc		mink
vakond		mole
mongúz		mongoose
kloákások		monotrem