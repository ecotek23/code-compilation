jag		I
det		it, that (neuter)
år		year(s)
du		you
inte		not
att		to, that (as in "are you sure that he will be coming")
en		a / an (common)
och		and
här		here
vi		we
på		on, at
i		in
för		for
han		he
vad		what
med		with
mig		me (accusative), my(self)
som		like (similar to), who (men who hate women), as
här		here
om		if, about
dig		you (accusative), your(self)
var		where
den		it (common)
så		so
till		to
kan		can
de		they
ni		you (plural)
ska		will, shall
ett		a, one (neuter)
men		but
av		of, by
vill		want
nu		now
ja		yes
vet		know
nej		no
bara		just, only
hon		she
kommer		come (present tense)
hur		how
min		my (common)
där		there
honom		him (accusative)
gör		do (present tense)
måste		must, have to
kom		came (past tense)
din		your, yours (common)
skulle		would, might, ought to
då		then (in that case)
bra		good
när		when
att ha		to have
er		you (plural, accusative), your (singular, common)
en tå		a toe
ut		out (showing movement)
att vara		to be
oss		us
att göra		to do
dem		them
eller		or
varför		why
alla		all (plural)
från		from
upp		up (showing movement)
tror		believe (present tense)
okej		okay
igen		again
så		so
tack		thank you
att gå		to go
hade		had
sig		oneself
in		in
allt		all (neuter), everything
att se		to see
ingen		nothing, none, nobody, no one (common)
att få		to get, to receive
henne		her
lite		some, a little
mycket		very, much, a lot of
vem		who
går		go, walk (present tense)
ser		see (present tense)
mitt		my, mine (neuter)
hej		hey, hi
aldrig		never
kanske		maybe, perhaps
något		something, anything (neuter)
behöver		need, require, want, have (to), need (to) (present tense)
finns		is (there is/are)
blir		become(s), get(s) (present tense)
än		than (comparison), still, yet (time)
inget		nothing, none, nobody, no one (neuter)
efter		after
att bli		to become, get, be
ju		of course
två		two
tar		take (present tense)
tillbaka		back
hans		his
över		over, across
sen		late, then
säger		say (present tense)
att ge		to give
mer		more
ditt		your, yours (neuter)
gjorde		did
mina		my, mine (plural)
ner		down (showing movement)
åt		towards, for
fan		damn!, fuck! (mild)
väl		well
någon		something, someone (common)
vänta		wait (imperative)
också		also, too
fick		received
att säga		to say
kunde		could, might, would
nog		enough, probably, surely
en låt		a song
varit		(has) been
hit		here (when movement is involved)
en pappa		a dad
en mamma		a mum
mot		towards, to, against
andra		other, second (plural)
hela		whole, entire (plural)
detta		this, that (neuter)
känner		know (a person), feel (an emotion) (present tense)
vid		by (position)
några		some (plural)
att komma		to come
borde		should
rått		raw
menar		mean (to mean something) (present tense)
själv		(my)self, (your)self
alltid		always
ett hem		a home
gjort		(have) done
dina		your, yours (plural)
ville		wanted
såg		saw, saw (tool)
att titta		to watch, see
dag		day
verkligen		really
fel		wrong, error
utan		without
visst		certainly
tre		three
att prata		to talk
liv		life
sätt		way, put (moving an item)
förstår		understand (present tense)
älskar		love (present tense)
blev		became
tog		took
en hall		a hall
dom		very informal spelling of "dem" (them), a judgment
att döda		to kill
tid		time
tycker		think (present tense)
att sluta		to stop (a behaviour)
trodde		believed
gillar		like (as in, to like a person) (present tense)
precis		precisely, exactly
såg		saw, saw (tool)
bort		away (when movement is involved)
sin		her, his, their (reflexive)
gång		time (singular), aisle, walking (the movement)
inga		none, nothing, nobody, no one (plural)
tänker		imagine, think (present tense)
hör		hear (present tense)
att veta		to know (a fact)
en gud		a god
bättre		better
just		just
att berätta		to tell
många		many, a lot
ett barn		a child, children
gick		went
god		good, tasty
en hjälp		a help
händer		hands
under		under, below, miracle (singular)
sett		(have) seen
visste		knew
innan		before (before an event occurs)
förlåt		I'm sorry
helt		completely (neuter)
ett folk		a people
pengar		money
död		dead
första		first
att hjälpa		to help
ursäkta		excuse me
snälla		please, kind (adjective plural)
länge		long (time)
heter		be called (present tense)
jävla		fucking (emphatic) (mild)
ledsen		sad
att stanna		to stay, stop
helvete		damn, hell (lit.)
vilken		which (as in, which one) (common)
hennes		hers
står		stand, stop, be (in the sense of location) (present tense)
säker		safe, sure, certain, surely (common)
far		father
hallå		hello (implies that you don't know if anyone is there)
håller		hold, keep (present tense)
fortfarande		still (continuing to do something)
kör		drive (present tense)
ur		out, of, from ( to come forth)
hand		hand
problem		problem
ger		give (present tense)
sak		thing, article, object, question, matter, affair
morgon		morning
ens		even (not even a whisper)
en väg		a road, way
vän		friend
våra		our (plural)
mår		feel (health or well being) (present)
enda		only
att hitta		to find, identify
kvar		left, remaining
samma		same
kväll		evening
försöker		try, attempt (present tense)
ute		out (location)
namn		name
dit		there (when movement is involved)
ingenting		nothing
snart		soon
fått		(had) got, (had) recieved
annan		other, second (common)
väldigt		immensely, awfully
en fru		a wife
längre		longer
sitt		his, her, theirs, one's (neuter)
hände		took place, occured, happened
herregud!		Oh my god!
redan		already
bästa		best (plural)
pratar		talk (present tense)
att visa		to show
att kolla		to look at, watch, check
tiden		the time
slut		over, done
sedan		since
skit		crap, crud, shit
härifrån		from here
låter		allow (present tense)
en tro		a belief, faith
fram		forward, along, up, out (to go forth)
en fråga		a question
era		your (subject and audience is plural)
jo		yes (in answer to a negative question)
stor		large, big
att träffa		to meet, hit (an object is hit by another object)
varje		every
först		first
säkert		safe, sure, certain, surely (neuter)
deras		their, belonging to them
börjar		begin (present tense)
att åka		to go (in a vehicle)
rädd		afraid, scared
därför		therefore
jobb		job
att lämna		to leave
hos		at the house of
fem		five
tänkt		(have) imagined, thought
hoppas		hope (present tense)
liten		little, small
genom		through, by
att kunna		to be able to
ses		see (as in, meeting someone)
hörde		heard (past tense)
nästa		next
sex		six, sex
livet		the life
att lyssna		to listen
son		son
verkar		seems
dollar		dollar
bilen		the car
sista		last, back, final
vänner		friends
tåg		train
annat		other, second (neuter)
riktigt		real, proper, very (neuter)
att tala		to speak (less commonly used)
att dö		to die
klarar		take care of, manage, cope with (I can manage to run that far) (present tense)
man		one, you, man
sina		his, her, their (reflexive) (plural)
ihop		together (position)
gav		gave
dra		pull (movement), leave (away from someplace)
nya		new (plural)
väntar		wait, waits (present tense)
vilket		which (neuter)
minuter		minutes
ligger		is situated, laying down (present tense)
att höra		to hear
världen		the world
tills		until
lägg		put (something) (imperative)
älskling		honey (term of affection)
denna		this, this one (common)
fyra		four
åker		go (in a vehicle) (present tense)
fast		although, firm (physical feature)
människor		people
hemma		at home
klart		clear (weather or liquids) (neuter)
blivit		(had) become
idag		today
minns		remember (present tense)
betyder		means (present tense)
att hämta		to pick up (an item)
lugnt		calm (neuter)
runt		around, round (adjective)
helst		rather
del		part
sagt		(has) said
vart		where (to)
rör		touch (present tense), a pipe (neuter)
att störa		to disturb
tänk		imagine, think (imperative)
annars		otherwise, or else
inne		indoor, inside
lilla		little (plural)
att öppna		to open
polisen		the police
tur		luck, turn (someones turn in a queue)
även		even, too, also
spelar		playing (present tense)
lika		alike, same
haft		(have) had
illa		badly, poorly, not well
hittade		found
kvinna		woman
ensam		alone, lonely
släpp		release, let go of (imperative)
att komma ihåg		to remember something
sådant		stuff, things like, like that, "or whatever"
hört		(have) heard
faktiskt		actually (neuter) (gives the impression that you are proving someone else wrong)
fall		case, fall
nästan		almost
gången		the time, the walk (gait)
pengarna		the money
ny		new
att slå		to beat, hit
kul		fun
natt		night
att spela		to play
ifrån		from (a specific destination)
hund		dog
långt		long (neuter)
senare		later
bäst		best
fort		quickly
emot		against, towards
att be		to ask someone for a favour, to pray
gott		good, tasty (neuter)
kille		boy, guy
tillsammans		together (shared activity)
trevlig		pleasant
plats		place
ett hus		a house
ringer		ring, call (present tense)
hittar		find (present tense)
familj		family
bör		should (formal) (present tense)
sitter		sit, sitting (present tense)
tio		ten
hänt		(has) happened
vilka		who, which (plural)
att klara		to take care of, get done
redo		ready, prepared
att vilja		to want
stan		town
fattar		get, understand (present tense)
gammal		old
att äta		to eat
en dörr		a door
glad		happy
dödade		killed
ert		your, yours (neuter)
tyst		quiet
chans		chance
försökte		tried
rum		room (neuter)
känns		feels (present tense)
båda		both
vore		would (subjuncive)
gamla		old (plural)
att tänka		to imagine, think
nära		near
en klocka		a clock, a watch
svart		black
gått		(has) went, gone
ber		ask (for a favour), pray (present tense)
par		couple
lever		living (present tense), liver (organ and food) (common)
gärna		gladly
dessa		these (plural)
all		all
dagar		days
att bry sig		to care
ord		word
ibland		sometimes
klar		clear (weather or liquids) (common)
fint		pretty (neuter)
iväg		away (from here)
bror		brother (common)
gånger		times (plural)
kommit		(had) came
ännu		yet
galen		mad, insane, crazy, incorrect
att låta		to let, to sound (you sound happy)
att rädda		to save (a life)
dags		(it is) time (to)
dör		dying
mor		mother
bakom		behind
mellan		between
att oroa		to worry
sån, sådan		such
att ringa		to call
timmar		hours
fler		several (countable things)
att skynda		to hurry
kapten		captain
killar		guys
mat		food
stannar		stopping (decreasing speed to stop a vehicle)
alls		at all
ett jack		a gash
att stå		to stand
försök		attempt
att känna		to feel
att leva		to live
jobbet		the place of work, the job
vapen		weapon
dotter		daughter
kände		felt
ben		leg, bone
började		started
varandra		each other
glöm		forget (imperative)
egen		own
uppe		up (location)
flicka		girl
ring		call
räcker		pass, hold out, reach
skull		sake
ett ställ		a rack, stand
kvinnor		women
herr		mister
syster		sister
skolan		the school
talar		speak, speaking (present tense) (less common)
lätt		easy, lightweight
fin		fine, pretty (common gender)
fanns		there was, there were
nere		down (location)
ikväll		tonight
att sätta		to put (item)
idé		idea
vatten		water
handlar		shop (for groceries, present tense), about
att följa		to follow
rolig		fun, funny (common)
att reda		to sort out (to handle a problem)
följer		following (present tense)
ganska		sort of, a little
vacker		beautiful
lovar		promise, promising
att flytta		to move
kallar		call, calling, calls
bad		asked, prayed (past tense)
fara		go somewhere (old fashioned)
fortsätt		continue (action) (imperative)
att försöka		to try
tagit		(have) taken
någonting		something, anything
eftersom		since, because
hatar		hates (present tense)
lång		long (common)
skall		shall; ought to
att lära		to learn
dödar		kill, killing
att köra		to drive
grabben		the kid (a boy)
förut		before, previously
hjälper		help, helping
ringde		called, rang
antar		suppose(s) (present tense)
lugn		calm
jaså		Oh (I see)
perfekt		perfect (neuter)
undan		away
skicka		send (imperative)
utanför		outside
letar		look, looking (for something)
ont		hurt (it hurts)
kompis		friend (common)
att stoppa		to stop, put
en plan		a plan
att betala		to pay
imorgon		tomorrow
önskar		wish (present tense)
stämmer		is right/correct
ett nummer		a number
drar		deduct
snäll		good, nice, kind (describing a person)
nytt		new (neuter)
huvudet		the head
gifta		married (plural)
tom		empty
källa		source (common)
självklart		of course
mest		most
natten		the night
full		full
sidan		the page
herre		lord (common)
kärlek		love (common)
följ		follow (imperative)
månader		months (plural)
lår		thigh (neuter)
att jobba		to work
käften		the jaw (common), offensive way of saying "shut up"
att hoppa		to jump
sätter		put, putting
slår		hit (present tense)
ställe		place (usually a building)
snabbt		quickly, fast (neuter)
vadå?		what?
berättade		told
pojke		boy (common)
lugna		calm (plural), (to) calm someone down
att skriva		to write
saken		the thing
direkt		directly
köpa		to buy
före		by, before (before the end of the month)
tittar		watch, see (present tense)
att sova		to sleep
någonsin		ever
kort		short
att äter		to eat
tjej		girl (common)
sköt		shot
hittat		(have) found
vecka		week (common)
sticker		sticking
gäller		is about, applies to
sanningen		the truth
nå		reach
roll		role (common)
viktig		important
ögon		eyes (plural)
föräldrar		parents
tyckte		(had) thought (expressing a opinion)
välkommen		Welcome
herrar		lords (plural)
att lita		to trust (on)
lämnade		left
att skjuta		to shoot
resten		the rest
hjärta		heart (neuter)
lycka		happiness
vidare		broader, wider
att hända		to happen
lämnar		leave, leaving (present tense)
att sitta		to sit
veckor		weeks
ett sätt		a way (neuter)
dum		stupid
sju		seven
hårt		hard
att använda		to use
undrar		wonder (present tense)
sida		side, page (common)
flera		several
att svara		to answer, reply
att fixa		to fix
egentligen		actually
trött		tired
medan		while
träffas		meet (present tense)
allihop		everyone
blod		blood
alltså		therefore, accordingly, ergo, thus, consequently
att lägga		to put
ursäkt		apology (common)
första		first
pratade		talked (past tense)
små		small (plural)
älskade		loved
väl		well
kära		in love (plural)
inom		within
vissa		some (plural)
miljoner		millions
barnen		the children
tv		tv (common)
tyvärr		unfortunately
land		country (neuter)
igår		yesterday
tidigare		earlier, before
visar		shows (present tense)
skyldig		guilty / in debt, owing
typ		a type, sort of
jävel		fucker (mild)
arg		angry
massa		lots of
order		order
gift		married
konstigt		strange
varsågod		you're welcome
kaffe		coffee (common)
hämtar		get (an item) present tnse
skjut		shoot (imperative)
spring		run (imperative)
läget		situation
jaha		oh well
förklara		to explain
slog		hit (past tense)
otroligt		incredible
tillräcklig		enough, sufficiently
mindre		smaller
själva		ourselves
synd		pity, sin
åtta		eight
toppen		super
hälsa		health, to to greet someone
riktig		real, proper
misstag		mistake (neuter)
ögonblick		moment (neuter)
att läsa		to read
slåss		fight
sjuk		sick
goda		tasty (plural)
igång		started, in motion, running
betalar		paying (present tense)
tjänst		service (common)
strax		soon
att dricka		to drink, a drink (common)
polis		police officer (common)
frågor		questions
historia		history, a story (common)
sägs		(it is) said
middag		dinner (common)
fart		speed
absolut		absolutely
en stund		a moment
att bo		to live
behövde		needed
förbi		past, over
la		put (an item)
krig		war (neuter)
möjligt		possible
framför		in front of
såna		such (plural)
flickan		the girl
att fortsätta		to continue
skiten		the shit (common)
skjuter		shooting (present tense)
slutar		finishes (present tense)
jorden		the earth (common)
händerna		the hands
resa		a trip (common)
på		on, at
omkring		around
ung		young
hemskt		horribly
fungerar		works
enkelt		simple
frågar		ask (present tense)
förra		previous
exakt		exact
tja		sup, "well"
dåligt		bad
snackar		talking (present tense) (everyday slang)
berättar		tells (a story) (present tense)
hund		a dog (common)
skrev		wrote
doktorn		the doctor (common)
ansikte		face (neuter)
låt		tune, song (common)
arbete		work, a job (occupation) (neuter)
träffade		met
Förrän		until
huvud		head
landet		the country (neuter)
allting		everything, all things
någonsin		ever
använder		use (present tense)
guds		god's
överallt		everywhere
förr		before (describing the past)
jobbat		worked
värre		worse
skriver		write (present tense)
saknar		miss (present tense)
inför		before
åkte		went
större		bigger
vänster		left (direction)
höll		held
kriget		the war
en familj		a family
ögonen		the eyes
rakt		straight (shape, direction)
egna		own (plural)
dess		its
att behöva		to need
bör		should (formal)
att vinna		to win
skaffa		acquire, obtain
brukar		usually
person		person (common)
glömde		forgot
frågade		asked
låg		lied (down)
senaste		last
stod		stood
att leta		seach (for something)
annorlunda		different, unusual
damer		ladies (common)
javisst		certainly, of course
tvungen		forced
grund		shallow
söker		search (for) (present tense)
eget		own (singular neuter)
grabbar		boys (slang)
brukade		used to
aning		idea, clue (I had no idea)
smart		smart
att älska		to love
Enligt		according to
eld		fire (common)
underbar		wonderful
största		largest
sover		sleep (present tense)
början		the beginning
vinner		win (present tense)
menade		meant
chefen		the boss
veckan		the week
fred		peace (common)
råd		(pieces of) advice
generell		general
sekunder		seconds
fantastiskt		fantastic
ofta		often
pojken		the boy
att må		feel (general physical and mental state)
att vakna		to wake up
lyssnar		listening (present tense)
uppfattat		understood
nånting		something
dålig		bad
kläder		clothes
fortsätter		continue (present tense)
rummet		the room
höger		right (direction)
kropp		body
andas		breathe (present tense)
timme		hour (common)
öl		beer (common)
skada		hurt (someone/something), a damage/sore (common)
ned		down (direction)
människa		human (common)
vita		white (plural)
telefon		a telephone (common)
snygg		pretty, attractive (about a person)
kung		king (common)
passar		suits (somthing)
värld		world (common)
vattnet		the water
farbror		uncle (common)
svart		black
Här		here
orolig		anxious, worried
Kallas		is called
att röra		touch
att snacka		to talk (everyday slang)
arbetar		work (present tense)
lycklig		happy, glad (common)
bär		a berry (neuter)
fixar		fix (present tense)
ers		your (majesty)
oj		oj
ställa		to put (an item)
att dansa		to dance
tjejen		the girl
försiktig		careful
skickade		sent
allvar		seriousness (neuter)
Adjö		goodbye (formal)
planet		the plane
pojkar		boys
levande		alive
naturligtvis		of course, naturally
alldeles		entirely, just
beklagar		being sorry for something
att akta		to watch out (for something)
tjejer		girls
svår		difficult
hel		whole (common)
finnas		there is
staden		the city (common)
fri		free
träffat		met
djur		animal (neuter)
vackert		beautiful (neuter)
särskilt		especially, particularly
körde		drove
förlorade		lost
gett		gave
läkare		doctor (common)
ljuger		lie (to tell a falsehood) (present tense)
döden		the death
försvinn		fuck off, disappear
kvällen		the evening
nånstans		somewhere, someplace
skojar		kidding (present tense)
fängelse		prison (neuter)
ändå		however, anyway
stark		strong, spicy (food)
raring		sweetie (gay)
att förlora		to lose
litet		little (neuter)
affärer		shops (common), affairs
stopp		stop, a stop (neuter)
att skydda		to protect
bok		book (common)
Sticka		to go away (everyday slang), knit
skickar		send (present tense)
funkar		works (present tense)
advokat		lawyer (common)
meter		metre (common)
duktig		skilled, good at stuff
olika		different
sälja		to sell
jösses		heavens (middle aged women say this)
flickor		girls
fäder		fathers
problemet		the problem
bevis		evidence (neuter)
rätta		correct, right (plural)
berättat		(had) told (a story)
att kasta		to throw
tredje		third
stolt		proud (neuter)
stäng		close
meddelande		message (neuter)
söt		cute
mord		murder (neuter)
behövs		needs (present tense)
spel		game (neuter)
gatan		the street (common)
satte		sat (down)
underbar		wonderful
djävlar		goodamn!
köpte		bought
frun		the woman / the wife
ett sjukhus		a hospital
gillade		liked
glömma		forget
tacka		thank (someone)
hungrig		hungry
att ordna		to arrange
äntligen		at last, finally
Fina		pretty (plural)
dödat		(have) killed
räddade		saved
snyggt		neat, nice (neuter)
riktiga		real, decent (plural)
sov		slept
intressant		interesting
slutet		the end
Musik		music (common)
högt		loudly
samtal		conversation
försiktigt		carefully
istället		instead (of)
speciellt		especially, specially
nyheter		news
barnet		the child
förutom		in addition to
sättet		the way (behaviour, situation)
skitsnack		bullshit
chef		boss (common)
ungefär		approximately, roughly
svarta		black (plural)
åtminstone		at least
klarade		to take care of, manage, cope with (I can manage to run that far) (past tense)
fröken		miss, old fashion word for teacher (female)
glömt		forgotten
frågan		the question
Trots		despite
fest		party (common)
att ha		to have
tå		toe (common)
att göra		make, do
att gå		to go
att se		to s