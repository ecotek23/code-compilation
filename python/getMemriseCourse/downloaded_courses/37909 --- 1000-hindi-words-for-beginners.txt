अँग्रेज़		English person
अच्छा		good, nice; really?, o I see!
अमरीकन		American
अलमारी		cupboard
आदमी		man, person
आप		you (formal)
आशा		hope
और		and
एक		one, a
कमरा		Room
काफ़ी		quite
कुरसी		chair
क्या		what?; (question-marker)
ख़ाली		empty, vacant
खिड़की		Window
गाड़ी		car
गुजराती		Gujarati
चाचा		paternal uncle
चिड़िया		bird
छोटा		small
जनवरी		January
ज़रूर		of course, certainly
जर्मन		German
जापानी		Japanese
जी		(respect-marker)
जी नहीं		no
जी हाँ		yes
ठीक		all right, OK
तीन		three
तुम		you (familiar)
तू		you (intimate)
दादी		paternal grandmother
दूसरा		other, second
दो		two
दोनों		both
नमस्ते		hello; goodbye
नहीं		not
पंखा		fan
पंजाबी		Panjabi
पत्थर		stone
पलंग		bed
पाकिस्तानी		Pakistani
पिता		father
पुराना		old (of things)
प्रति		copy
बग़ीचा		garden
बच्चा		child
बड़ा		big
बहुत		very
बहू		daughter-in-law
बूढ़ा		elderly
भारतीय		Indian
भाषा		language
मकान		house
माता		mother
मानव		human being
मारुति		Maruti (car make)
मूर्ति		statue, image
मेज़		Table
मैं		I
यह		this, he, she, it
यहाँ		here
ये		these, they; he, she (formal)
रविवार		Sunday
राजा		king
रूसी		Russian
लड़का		boy
लड़की		girl
लाल		red
लोग		people
वह		that, he, she, it
वे		those, they; he, she (formal)
शुक्रिया		thank you (Persian derived)
सफ़ेद		white
साइकिल		bicycle
साफ़		clean
सिर्फ़		only
सुन्दर		beautiful
हम		we
हवादार		airy
हाँ		yes
हिन्दुस्तानी		Indian
हिन्दू		Hindu
हूँ		am
है		is
हैं		are (used with formal and intimate forms)
हो		are (used with informal form)
अख़बार		newspaper
अध्यापक		teacher
अभी		at the moment, right now
आज		today
और		more, else, other
काग़ज़		paper; a piece of paper
कितना		how much/many?
किताब		Book
कृपया		please
कैसा		of what kind?, what like?, how?
कौन		who?
क्या		what?
क्यों		why?
क्योंकि		because
ख़राब		bad
ख़ुश		happy
गंदा		dirty
चप्पल		sandal
चिट्ठी		letter, note, chit
छात्र		student
छात्रा		female student
जवाब		answer
जूता		shoe; pair of shoes
ज़्यादातर		mostly, most
दिल्ली		Delhi
दुबला		thin
दुबला-पतला		thin, slight of build
धन्यवाद		thank you (Sanskrit derived)
न		not; is it not so?
नमस्कार		hello; goodbye
नया		new
नए		new
नैइ		new
नाराज़		angry, displeased
नौ		nine
पतला		thin
पर		but
परेशान		troubled, upset
पाँच		five
पागल		mad, crazy
प्रदूषण		pollution
बहिन		sister
बात		matter, thing
बिलकुल		completely
बिल्कुल नहीं		not at all
बीमार		ill
बुरा		bad
बेटा		son
बेटी		daughter
भाई		brother
भी		also, too
महँगा		expensive
मालूम नहीं		[I] don't know
मेहरबानी		kindness
मैडम		madam
मोटा		fat, thick
मौसम		weather
या		or
रामायण		Ramayana (epic poem)
लंबा		tall, high
विद्यार्थी		student
शब्दकोश		dictionary
श्री		Mr; Lord (with Hindu deity)
श्रीमती		Mrs.
सख़्त		strict, severe
सब		everything, all
सर		sir (English derived)
सवाल		question
सस्ता		cheap
साहब		sir (Arabic derived)
सोमवार		Monday
सोलह		sixteen
हलो		hello
हेलो		hello
हवा		air
हाल		condition, state
हिन्दी		Hindi
अब		now
आपका		your, yours
आराम		rest, comfort
आराम से		comfortably, easily
इस		(oblique of यह)
इन		(oblique of ये)
उस		(oblique of वह
उन		(oblique of वे
ऐनक		glasses
ओ		o!
कपड़ा		cloth; garment
क़लम		pen
काला		black
किस		(oblique singular of कौन and of क्या)
किन		(oblique plural of कौन and of क्या)
को		to, on, at
ख़ैर		well, anyway
गाँव		village
ग़ुसलख़ाना		bathroom
घर		house, home
चश्मा		glasses
चार		four
चीज़		thing
छः		six
तक		up to, as far as
तबियत		health
तस्वीर		picture
तुझ		(oblique of तू)
तुम्हारा		your, yours
दरवाज़ा		Door
दराज़		drawer
दरी		floor rug, mat
दस		ten
दीवार		wall
पति		husband
पत्नी		wife
पर		on
परदा		curtain
परिवार		family
फ़र्श		floor
बस		bus
बहुत-सारा		lots of
बाथरूम		bathroom
भारत		India
मुझ		(oblique of मैं)
में		in
में से		from among, out of
मेरा		my, mine
रसोईघर		kitchen
रात		night
रे		eh, hey
लंदन		London
वग़ैरह		etc., and so on
वहाँ		there
शहर		town, city
शायद		Perhaps
समय		time
सामान		furniture, things
सारा		all, whole
सुबह		morning
से		by, since, from, with
अकेला		alone
अफ़्सोस		regret
अरे		oh! What!
उम्र		age
उमर		age
एकदम		completely
और		more
कम		less; little
कल		yesterday; tomorrow
काम		work
कालेज		college
कि		that
कुछ नहीं		Nothing
क्लास		class
खाना		food
ख़ुशी		happiness, pleasure
ख़ूबसूरत		beautiful, handsome
गरम		warm, hot
गोली		tablet, pill
घर पर		at home
चाँदी		silver
चाचा		paternal uncle
चाय		tea
ज़ुकाम		a cold
ज़्यादा		more, very
ठंडा		cold
तो		so, etc.
था		was, were
थी		was, were
थे		was, were
थीं		was, were
दफ़्तर		office
दाल		daal, lentils
दुकान		shop
दोस्त		friend
पड़ोसी		neighbor
पसंद		liked, pleasing (verb)
पहले		previously
पहाड़		hill, mountain
प्यारा		dear, lovely
प्रिय		dear (in letter writing)
बंद		closed, shut
बाज़ार		market
बाल-बच्चे		children, family
बाहर		out; outside
बुख़ार		fever
मज़बूत		strong
मतलब		Meaning
महल		palace
माँ		Ma, Mother
मालूम		known
मित्र		friend
मेहनती		hardworking
रिश्तेदार		relative
वाराणसी		Varanasi, Banaras
विदेश		foreign country; abroad
समोसा		samosa
साल		year
सोना		gold
सौ		hundred
होशियार		clever, intelligent
आना		to come
इधर		here, over here
उधर		there, over there
ऋषि		sage, seer
कंबल		blanket
करना		to do
(से) कहना		to say (to)
का		s (indicating possession)
की		s (indicating possession)
के		s (indicating possession)
काफ़ी		coffee
कुत्ता		dog
कुरता		kurta, Indian shirt
कौन्सा		which?
कौन्सी		which?
कौन्से		which?
ख़त		letter
ख़याल		opinion
ख़ाँ मर्केट		Khan Market (in Delhi)
खाना		to eat
खिलाना		to serve, to give to eat
खिलौना		toy
घोड़ा		horse
चलना		to move
चाबी		key
छूना		to touch
जगाना		to arouse from sleep
ज़रा		just, a little
जल्दी		quickly, soon, early
जाना		to go
झगड़ा		quarrel, row
झगड़ा करना		to quarrel
ठहरना		to stop, to wait
ढूँढ़ना		to look for, to find
तैयार		ready
थैला		bag, cloth bag
दूध		milk
देखना		to see, look
देना		to give
देवर		husband's younger brother
धोना		to wash
धोबी		dhobi, washerman
ध्यान		attention
ध्यान से		attentively
नारा		slogan
नौकर		servant
पड़ा		lying
पढ़ना		to read, to study
पान		paan
पानी		water
पापा		Papa, Father
पीना		to drink; to smoke
(से) पूछना		to ask (of)
पेड़		tree
पैसा		money; paisa (hundredth of a rupee)
फल		fruit
बंद करना		to close, to shut
बताना		to tell
न​		don't (with commands; informal)
बुलाना		to call, to invite
बैठना		to sit
बोलना		to speak
भैया		brother
मारना		to beat, to hit
रखना		to put, to keep
लाना		to bring
लिखना		to write
लेना		to take, to receive
शैतान		devil
सचमुच		really
सरल		simple
साफ़ करना		to clean
सिग्रेट		cigarette
मत​		don't (with commands; formal)
सुनना		to listen, to hear
होना		to be, to become
अकेलापन		loneliness
अक्सर		often; usually
अच्छा		well, proficiently
अपना		(one's) own
इक्कीस		twenty-one
उच्चारण		pronunciation
उठना		to get up, to rise
उन्नीस		nineteen
कंपनी		company, firm
कभी कभी		sometimes
कभी नहीं		never
की तरफ़		Towards
की तरह		Like, as (postposition)
के अंदर		Inside
के आगे		in front of, ahead of
के ऊपर		on top of
के नीचे		beneath, below
के/से पहले		before
के पास		near
के पीछे		behind
के बाद		After
के बारे में		about, concerning
के बाहर		Outside (postposition)
के यहाँ		at the place of
के लिए		For
के साथ		with, in company of
के सामने		facing, opposite
(का) ख़याल रखना		to take care of, mind
ख़ूब		a lot, well
गपशप		gossip
गोश्त		meat
चाँद		moon
चुप		silent
चौदह		fourteen
छोड़ना		to leave
जानना		to know
टुकड़ा		piece, bit
(से) डरना		to fear
डाकघर		post-office
तेरा		your (from तू)
थोड़ा		a little
दिन		day
दिन भर		all day
देवनागरी		Devanagari script
(का) ध्यान रखना		to take care of, to mind
नफ़रत		hate, dislike
नापसंद करना		to dislike
प्यार		love, affection
(को/से) प्यार करना		to love
फिर		then; again
बचपन		childhood
बजे		o' clock
बधाई		congratulation
(से) बात/बातें करना		to talk, to converse
बाप		father
बारह		twelve
मन		mind
महसूस करना		to feel, to experience
मालिक		boss
मुन्ना		Munna (nickname for little boy)
मुश्किल		difficult
मेहमान		guest
लंबाई		height, length
लखनऊ		Lucknow
लिपि		script
वाह!		(expresses admiration or scorn) wonderful!
विदेशी		foreigner
वैसे		actually
व्यंग्य		sarcasm
शराब		alcohol, liquor
शरारत		mischief
शुक्रवार		Friday
शुद्ध		pure, unmixed
संस्कृत		Sanskrit
सहेली		girl's female friend
साड़ी		sari
साल		year
सीखना		to learn
से पहले		before
सोचना		to think
सोना		to sleep
स्टेशन		station
हँसना		to laugh
हँसाना		to make laugh
हमारा		our, ours (informal)
हमेशा		always
होनहार		promising
अलग		separate, different
आठ		eight
उर्दू		Urdu
और कोई		any other
औरत		woman
करोड़		ten million
कानपुर		Kanpur
किसी		(oblique of कोई)
कुछ		some; somewhat, something
कुछ और		something else, some more
कुछ न कुछ		something or other
के नज़दीक		Near
के समान		like, equal to
कोई		anyone, someone; some, any, about
कोई और		someone else
कोई न कोई		someone or other
कोई नहीं		no one
कौन कौन		which various people
खड़ा		standing, waiting
ख़त्म		finished
ख़त्म करना		to finish
गरमी		heat; summer
चतुर्थ		fourth
चावल		rice
चाहिए		(is) needed
चौथा		fourth
छ्ठा		sixth
छुट्टी		holiday; free time
ज़मीन		ground; land
ज़रूरत		need
ज़रूरी		urgent, necessary
जवाब देना		to reply
जी		yes
जेब		pocket
ठीक से		properly; exactly
डाक्टर		doctor
डिब्बा		box; railway compartment
तरफ़		side, direction
तीसरा		third
तृतीय		third
दर्जन		a dozen
दूर		distant, far, away
दूसरा		second
देर		delay, a while
द्वितीय		second
नवाँ		ninth
पंचम		fifth
पढ़ाना		to teach
पहला		first
पाँचवाँ		fifth
प्रथम		first
फ़र्क		difference, separation
बाद में		later on
बाबू		clerk
बिजली		electricity
बेकार		useless
बैठा		seated, sitting
महायुद्ध		great war
मार्च		March
मुसलमान		Muslim
मोती		pearl
रंग		color
रोटी		bread; food
लाख		hundred thousand
वजह		reason, cause
शनिवार		Saturday
शाम		evening
शुरू करना		to begin
होना		to begin
सपना		dream
सपना देखना		to dream
सब कुछ		everything
सिनेमा		cinema
सैकड़ा		a hundred
सैर		walk, trip
सैर करना		to go for a walk, trip
स्वर्ग		heaven
हज़ार		thousand
हर		each, every
हर कोई		everyone
होटल		hotel, café
ख़ास		special, particular
अंदर		inside, within
अगला		next
अठारह		eighteen
अलग		separately
अस्पताल		hospital
आँख		eye
आगरा		Agra
आजकल		nowadays
आम		ordinary
आम तौर पर		usually
ऊपर		above; up; upstairs
ऐश		luxury
ऐश करना		to live a life of pleasure
कमाना		to earn
कलकत्ता		Calcutta
कहानी		story
काठमांडु		Kathmandu
से पास		in (our) possession
ग़लत		wrong, incorrect
गली		lane
घंटा		hour
चलाना		to drive
जन्मदिन		birthday
जून		June
ज़ोर		force, strength
डाल		branch (of tree)
तंग		narrow
तंग करना		to harass
तकलीफ़		trouble, distress
तरह		way, manner
तारीख़		date
ताली		clapping
तू-तू मैं-मैं करना		to call names
तैयार करना		to prepare
थकान		tiredness
थाली		platter
थोड़ा-सा		a little
दल		political party
दाहिना		right (opposite of left)
दिख्ना		to appear, to seem
दिली		intimate; "of the heart"
देर		delay; a while
नंबर		number
नज़दीक		near, nearby
निकलना		to come/go out, to emerge
निन्यानवे		ninety-nine
नीचे		below, down, downstairs
नेपाल		Nepal
पल		moment
पहर		part of the day
पिछ्ला		last, previous
पुस्तक		book
पूरा		full, complete
प्रोग्राम		program, plan
फ़रवरी		February
बनाना		to make
बाद (में)		later
बायाँ		left (opposite of right)
भगवान		God
भेजना		to send
मई		May
महीना		month
मिलाना		to dial, to phone
मोड़		to turn, to bend
मोर		peacock
विलंब		delay
सन्		year (of calendar or era)
सब्ज़ी		vegetable(s)
सवेरा		early morning
साथ		along, with, in company
सावधानी		care
सिर		head
सिर खाना		to pester
सीधा		straight, straightforward
सीधे		straight; to the right
सैंतालीस		forty-seven
हाथ		hand
हे		Oh!
अकेले		alone
अगर		if
अच्छा-ख़ासा		really good
अधिक		much, many, more
अधिक से अधिक		at the most
अमरीका		America
असली		real
आसान		easy
इंतज़ाम		arrangement(s), organization
इतिहास		History
इरादा		intention
ईव-टीज़िंग		sexual harassment of girls and women; "Eve-teasing"
उषा		dawn
कठिन		hard, difficult
कभी		sometime
कम से कम		at least
कमाना		to earn
काल		time, period of day
के आस-पास		around, in vicinity of
ख़रीदना		to purchase, to buy
गरमियाँ		summer
ग़लत		wrong
गाना		song
गाना		to sing
गुंडा		lout, hooligan
गुंडागर्दी		hooliganism
ग्यारह		eleven
चिल्लाना		to shout
छपना		to be printed
जनता		public, the people
ज़्यादा से ज़्यादा		at the most
ठीक करना		to put right, to fix
डाक		mail, post
तबला		tabla (drum)
तैरना		to swim
थोड़ा-बहुत		a certain amount
थोड़े		scarcely, by no means
दर्शन		vision (of), audience or auspicious meeting (with)
दावत		(invitation to) a dinner
ध्यान देना		to pay attention
नमक		salt
नुक़सान		harm
नौजवान		youth, young man
पत्र		letter
परंतु		but
पसंद करना		to like, to approve
पहचानना		to recognize
पहुँचना		to arrive
प्राध्यापक		lecturer
फ़ायदा		profit, advantage, point
फ़ोन करना		to phone
बजाना		to play (music)
बनारस		Banaras, Varanasi
बिकना		to be sold
बी॰एच॰यू॰		BHU (Benaras Hindu University)
बोर करना		to bore
भतीजा		nephew
भविष्य		future
भोला		innocent, guileless, simple
मँगवाना		to order, to send for
मचाना		to create (noise etc.)
मना		prohibited
मसूरी		Mussoorie (an Indian state)
महाराजा		maharaja
(से) मिलना		to meet
मिस्तरी		mechanic, artisan
यहीं		right here, in this very place
रवानी		fluency
रुकना		to stop, to stay on
रेलगाड़ी		train
रोशनी		light
लायक़		worth (doing)
लौटना		to return
वहीं		right there, in that very place
विश्वविद्यालय		university
व्यवस्था		arrangement(s)
समझ		understanding
समझ में आना		to enter the understanding, to be understood
सही		true, correct
हवाई जहाज़		airplane
हाथ आना		to come to hand
हिलना		to move, to stir
ही		only (emphatic)
अब की बार		this time
अवश्य		certainly
असंभव		impossible
आख़िर		after all
आटो		auto-rickshaw
आधा		half
इतना		so (as in "so big"), so much
इलाज		treatment, cure
उचित		proper, appropriate
उतारना		to take down, to take off
उम्मीद		hope
ऊँचा		high
कंजूस		miserly, mean
काँटा		fork
कामचोर		work-shy, lazy
कि		or
ख़रीदारी		buying, shopping
ख़ाली करना		to vacate
खींचना		to draw; to take (photo)
ख़ुद		oneself
गर्व		pride
गाय		cow
गुस्से		angry
चपरासी		peon, orderly
चम्मच		spoon
चाहना		to want, to wish, to be fond of
छुरी		knife
ज़िम्मेदारी		responsibility
झूठ		lie
टेढ़ा		twisted, complex
डेढ़		one and a half
तुरंत		immediately
दाम		price
नहीं तो		otherwise
निवेदन		request
नोट		note
पकड़ना		to catch, to grab, to hold
पड़ना		to fall
पता		address
पता नहीं		don't know, no idea
पता होना		to know, to be aware
पानी पड़ना		to rain
पुलिस		(used in singular) police
बार		time, occasion
बार बार		time and again
बारे में		about, concerning
बोरियत		boredom
भारी		heavy
भूल		mistake
भूल करना		to make a mistake
भूलना		to forget, to err
मंदिर		temple
मक्खी		fly
मक्खियाँ मारना		to laze about; to "kill flies"
मगर		but
मदद		help
मदद देना		to help
मामला		matter, affair
माहिर		expert, skilled
यक़ीन		confidence, faith
यदि		if
रिक्शा		rickshaw
लिस्ट		list
वक़्त		time
वापस		back
वापस आना		to come back
विश्वास		belief, confidence
संभव		possible
सफ़ाई		cleaning
सलाह		advice
सीढ़ी		stair, staircase
हफ़्ता		week
हाथ बँटाना		to lend a hand
आख़िर में		in the end, after all
आनंद		joy, enjoyment
आनंद आना		to feel enjoyment
आवश्यकता		necessity
उँगली		finger
उठाना		to pick up
उधार		loan
उधार लेना		to borrow
उपहार		present, gift
कब		when?
क़ीमत		price, value, cost
कैमरा		camera
खोलना		to open
गंगा		Ganges
गिलास		tumbler (glass or metal)
गीत		song
गुज़ारना		to spend (time)
घाट		(steps at) riverbank
घूमना		to tour, to roam
चोर		thief
छूटना		to be left, fall back
जारी		current, continuing
जारी रखना		to maintain
ज़ेवर		(piece of) jewelry
ट्रेकिंग		trekking
तमाशा		spectacle, show
तिब्बती		Tibetan
तोड़ना		to break
तोहफ़ा		a present
दवा		medicine
दादा		paternal grandfather
दुकानदार		shopkeeper
दुर्घटना		accident
देर तक		until late
दोपहर		noon, afternoon
नाक		nose
नींद		sleep
नींद आना		to feel sleepy
पच्चीस		twenty-five
पढ़ाई		studies, studying
परसों		the day before yesterday; the day after tomorrow
पहनना		to put on, to wear
पाठ		lesson, chapter
पाना		to find, to obtain
पीछे		behind
पेट		stomach
प्याला		cup
प्रसिद्ध		famous
फ़िल्म		film
फ़िल्मी		film-related, film-style
फ़ैक्स		fax
फ़ोटो		photograph
बजना		to resound, to chime
बाँह		(upper) arm
बीस		twenty
बेचना		to sell
भूत		ghost
भेंट		gift, presentation
मजबूरी		compulsion
मज़ा		fun, pleasure, enjoyment
मज़ा आना		to enjoy, to have fun
मानना		to accept, to believe
माफ़ करना		to forgive, to excuse
माफ़ी		forgiveness
माफ़ी माँगना		to apologize
माली		gardener
मीठा		sweet
मुमकिन		possible
रोना		to weep, to cry
लाठी		lathi, stick
वापस लेना		to take back
शब्द		word
शिकायत		complaint
शिकायत करना		to complain
साथ साथ		together
सुनाना		to relate, to tell, to recite
अंत		end
अद्भुत		remarkable
अवकाश		leisure, free time
आठों पहर		all day long
आम		mango
आराम करना		to rest
आवाज़		voice, sound
आवश्यकता		necessity
कविता		poem; poetry
कि		or
कृपा		kindness, grace
कृपा करके		kindly, please
खेद		regret
खोना		to lose
अजीब		strange, odd
अनबन		discord
अभ्यास		practice
आग		fire
आगे		later, ahead
आगे चलकर		in future, from now on
इधर		recently, latterly
इलाहाबाद		Allahabad
कहलाना		to be called, named
क़िस्मत		fate
की वजह से		because of
कोना		corner
गुरु		teacher, guru
गोली		bullet
घंटी		bell
चल देना		to set off
चाहिए		(with infinitive) should
चोट		hurt, injury
चोट लगना		to get hurt
चोरी		theft, robbery
छाता		umbrella
जगह		place
जब...तब		when...then
जब तक...तब तक		for as long as...for that long
जब तक नहीं...तब तक		until...until then
जब से...तब से		since the time when...since then
जवान		young, youthful
जवान		soldier
जैसे ही...वैसे ही		as soon as...then
जो		who/which, the one who/which
जो कुछ		whatever
जो कोई		whoever
जो भी		whoever, whatever
टिकट		ticket; stamp
ठंड		cold
डर		fear
डाकू		dacoit, bandit
डालना		to throw
तलाक़		divorce
तलाक़ होना		to become divorced
ताला		lock
दिल लगना		to feel at home
देन		contribution, gift
नज़र		glance, look, gaze
पक्का		ripe
प्यास		thirst
प्यास लगना		to feel thirsty; thirst to strike
फ़ुरसत		leisure, free time
बँगला		Bengali (language)
बत्ती		light, lamp
बदसूरत		ugly
बरबाद करना		to ruin
बारिश		rain
बारिश होना		to rain
बियर		beer
बेचारा		poor, wretched, helpless; poor fellow
भूख		hunger
भूख लगना		to feel hungry; hunger to strike
भूखा		hungry
भौंकना		to bark
मन लगना		to feel at home
मरना		to die
मिलना		to be available; to resemble
मिलना-जुलना		to resemble
मिल-जुलकर		together, jointly
मूर्ख		fool
मेहनत		hard work
रोज़		every day, daily
लगना		to strike; to appeal; to seem; to catch (of fire, illness); to be related, attached; to be expended (of time, money)
लड़ना		to fight, to quarrel
लिफ़ाफ़ा		envelope
वाक्य		sentence
विचार		thought, idea, opinion
विषय		subject
शादी		marriage, wedding
X की शादी Y से होना		X to marry Y
शिष्य		disciple
सो		he, she, it (archaic)
हीरो		male film-star; "hero"
अनुपस्थित		absent
अमीर		rich, wealthy
अर्थ		meaning
आँखें दिखाना		to look angrily
आबादी		population, settlement
आर्थिक		financial
इलाक़ा		area
ऊल-जलूल		silly, pointless
क़रीब		about, roughly
काँटा		thorn; fork
कारण		reason, cause
कि		when suddenly
किधर		where? Whither?
की बग़ल में		next to
के कारण		because of
(के) द्वारा		by, by means of
के बीच में		in the middle of
ख़बर		news; information
गुस्ताख़ी		rudeness, impertinence
घटिया		(inv.) inferior, low-grade
चटनी		chutney
चेहरा		face, features
चोरी करना		to steal
जब कि		while
ज़बान		language, tongue
जहाँ...वहाँ		where...there
जितना...उतना		as much as...
जिधर...उधर		whither...thither
जैसा...वैसा		as...so
जोड़ना		to add
डाँटना		to scold
तमिल		Tamil
ताज़ा		fresh
तालाब		pond
तालियाँ बजाना		to clap
दक्षिण		South
दबना		to yield, to cower
दिलचस्प		interesting
दुबारा		again, a second time
दूरदर्शन		television; Doordarshan (national TV network)
देना		(with obl. inf.) to allow to, to let
धुलना		to be washed
नदी		river
निराशा		disappointment, despair
परिवर्तन		alteration
पाकिस्तान		Pakistan
पुल		bridge
प्रकार		manner, way, type, kind
प्रदेश		state, province
प्रयोग		use, usage
प्राचीन		ancient
प्राप्त करना		to obtain, to attain
प्रेम		love
फ़ारसी		Persian
फूल		flower
बचा		saved, left
बढ़िया		(inv.) nice, of good quality
बस		enough! That's all!
बिठाना		to make sit
बिल्ली		cat
बोली		speech, dialect
भीगा		wet
भीगी बिल्ली		timid creature; "wet cat"
भ्रष		corrupted
भ्रष्ट करना		to corrupt
मक्खन		butter
मच्छर		mosquito
मज़ाक़		joke
महसूस होना		to be felt, experienced
महिला		lady
महीन		fine, delicate
महोदय		gentleman, sir
मुलाक़ात		meeting, encounter
मौक़ा		chance, opportunity
राजस्थान		Rajasthan
लगना		(with obl. inf.) to begin to
लादना		to load
लेख		article (written)
लेखक		writer
व्यक्ति		person, individual
शादी-शुदा		(inv.) married
शानदार		magnificent
सफल		successful
सफलता		success
साहित्य		Literature
स्वादिष्ट		tasty
गृह मंत्री		Home Minister
ग्वालियर		Gwalior
चला आना		to come back
चला जाना		to go one's way
चिढ़ाना		to irritate, to tease
चुकना		(after verb stem) to have already done
(को) छोड़कर		apart from, except for
जान-बूझकर		deliberately
जी-हुज़ूरी		sycophancy, flattery
टैक्सी		taxi
डिगरी		degree
डिनर		dinner
ढाई		two and a half
तीसरे पहर		in the afternoon
दौड़ना		to run
धीमा		low, faint
नहाना		to bathe, wash
नाश्ता		breakfast
पछताना		to regret, to repent
परीक्षा		examination
परीक्षा देना		to take (sit) an exam
पाना		(after verb stem) to manage to
पार करना		to cross
पास करना		to pass (exam etc.)
पौना		three-quarters
पौन		three-quarters
प्रकाशक		publisher
बदमाश		villain, rogue
बदलना		to change
बर्थ		berth
बाँटना		to divide
बोतल		bottle
भाभी		elder brother's wife
भूलकर भी		even by mistake
भेंट		meeting, encounter
भोजन		food
भोजन करना		to dine
मगर		crocodile
मिठाई		sweet, sweet dish
मिनट		minute
मुड़ना		to turn
मेहरबानी करके		kindly, please
रेडियो		radio
ले आना		to bring
ले जाना		to take away
लेना-देना		dealings, connection
विवाह		marriage
विशेष		particular
वैर		hostility
सकना		(after stem verb) to be able
समाचार		news
सवा		one and a quarter, plus a quarter
साढ़े		plus a half
सात		seven
साथी		companion, friend
साबुन		soap
सेब		apple
(से) होकर		via
होली		Holi, springtime festival of colors
होल्ड करना		to hold (phone line)
हवाई डाक		airmail
अँधेरा		darkness
अगस्त		August
अचानक		suddenly, unexpectedly
अनौपचारिक रूप से		informally
अपने आप		of one's own accord, oneself
असल में		really, in reality
आधीरात		midnight
आपस में		between/among themselves
आसानी से		easily
(से) इनकार करना		to refuse (to)
उड़ना		to fly
उदाहरण		example
उपन्यास		novel
औपचारिक रूप से		formally
कब से		since when
कभी कभी		sometimes
काँपना		to shiver, to tremble
किनारा		edge, shore
किराया		rent, fare
के बजाय		instead of
के बिना		without
के मारे		on account of, through
के सिवा		apart from
सिवाय		apart from
ग़रीब		poor
ग़लती		mistake
गाली		abuse, swearing
गाली देना		to abuse, to swear at
गोरा		pale, fair
गोरा		white person
घना		dense, thick
घमंड		pride, arrogance
चपाती		chapatti
चौकीदार		watchman
जंगल		jungle, scrub
जँभाई		yawn
जँभाई लेना		to yawn
ज़माना		period, age, time
जीते रहो		bless you ("stay living")
जीना		to live, to be alive
जीवन		life
जैसा		like, such as
जैसे कि		as if, as though
झेंपना		to be embarrassed
ट्रेन		train
डरपोक		timid
ड्राइवर		driver
तरक़्क़ी		progress, advancement
थकना		to be tired
दिखाई देना		to be visible, to appear
देर से		late
देश		country
धोखा		trick, deceit
धोखा देना		to trick, to deceive
नल		pipe, tap
नेता		leader, politician
नौकरी		job, employment, service
पंडित		pandit
परवाह करना		to care
पहिया		wheel
पूँछ		tail
पैदल		on foot, walking
फुसफुसाना		to whisper
बंदर		monkey
बढ़ना		to increase, to grow
बत्तीसी		set of 32 - the teeth
बत्तीसी दिखाना		to grin broadly
बर्फ़		snow, ice
बादल		cloud
ब्राह्मण		Brahmin
भिखारी		beggar
मशहूर		famous
यात्री		traveler, passenger
यू॰पी॰		UP (Uttar Pradesh)
राजधानी		"the capital" - name of some Delhi express trains
रात भर		all night
व्यक्तिगत रूप से		personally
शक्ति		power
शराबी		drunkard
शुरू		beginning
सच		true
समस्या		problem
समुद्र		sea, ocean
सरकार		government
सरकारी		governmental
-सा		-like, -ish
सितार		sitar
सूरज		sun
स्नान करना		to bathe (especially ritually)
स्वयं		oneself
हालत		state, condition
अनुभव		experience
अहम		important
आलसी		lazy
इंतज़ाम		arrangement
का इंतज़ाम करना		to arrange
इंतज़ार		wait, waiting
का इंतज़ार करना		to wait for
इनसान		human being, person
इमारत		building
इस किए		so
इसी लिए		that's why
इस्तेमाल		use
का इस्तेमाल करना		to use
ई-पत्र		e-mail
ई-मेल		e-mail
उदाहरण		example
उद्घाटन		inauguration
उल्लू		stupid person; "owl"
कंप्यूटर		computer
कम करना		to reduce
कसूर		fault, error
कहीं		somewhere; somehow
कहीं और		somewhere else
कहीं का		downright, utter
कहीं ज़्यादा		particularly
कहीं न कहीं		somewhere or other
कहीं नहीं		nowhere
कहीं भी		anywhere at all
कार्यक्रम		program
कुली		porter
कोशिश		attempt
की कोशिश करना		to try
खाँसना		to cough
गँवाना		to waste, to squander
गप		gossip
गपशप		gossip
गुज़ारा		livelihood, subsistence
चाहे...चाहे		whether...or
चिंता		anxiety
जंगली		wild
जन्म		birth
का जन्म होना		to be born
जानवर		animal
ज़िंदगी		life
डूबना		to be immersed, to drown
ढंग		way, manner
तरह तरह का		of various kinds
तलाश		search
की तलाश करना		to search for
तलाशना		to search for
तहज़ीब		refinement, culture
तारीफ़		praise
की तारीफ़ करना		to praise
दुनिया		world
देखभाल		supervision, care
की देखभाल करना		to take care of
देहान्त		death, demise
का देहान्त होना		to die
न...न		neither...nor
निंदा		blame, speaking ill
की निंदा करना		to blame
निकालना		to extract, to bring out
निबंध		essay
नौकर-चाकर		servants etc.
पति-देव		"respected husband"
(का) पीछा करना		to follow
पेंसिल		pencil
पेशा		profession
प्याज़		onion
प्रयोग		use, usage
का प्रयोग करना		to use
बेवक़ूफ़		stupid
मदद		help
की मदद करना		to help
मर्द		man, male
माला		garland
मालूम करना		to ascertain
मीटिंग		meeting
या तो...या		either...or
याद		memory
याद आना		to be recalled, to come to mind, to be missed
याद करना		to remember, to learn by memory; to think of, to summon (e.g. an employee)
याद दिलाना		to remind
याद रखना		to keep in mind, not to forget
याद रहना		to be remembered
याद होना		to be remembered
लहसुन		garlic
सदा		always, ever
सवारी		rider, passenger
साधन		means
सुनाई देना		to be heard, to be audible
सुनाई पड़ना		to be heard, to be audible
हरेक		each, every
हल		solution
हल होना		to be solved
अंतर		difference
अजनबी		stranger
असर		effect, impact
असर पड़ना		to have an effect
आख़िर		end
आरंभ		beginning
उतरना		to get down, to alight
उत्तर		reply
उबलना		to boil, to rage
ओहो		oho! oh no!
कटना		to be cut
कवि		poet
कष्ट		trouble, distress
काटना		to cut
किंतु		but
क़ीमती		costly, valuable
ख़बर		news
खाट		bedstead, "cot"
गंभीर		serious, profound
गुस्सा		anger; angry
घड़ी		wristwatch
घुसना		to enter (forcibly, or uninvited)
छात्रवृत्ति		scholarship
जलना		to burn
ज़िद्दी		obstinate, stubborn
ठग		swindler, robber
ढीला		loose
ढेर		pile, heap
तथापि		nevertheless, even so
दंगा		riot
दफ़ा		time, occasion
दिमाग़		mind, brain
नाचना		to dance
(का) नाम लेना		to mention
नामुमकिन		impossible
निहायत		extremely
प्रकाशित		published
प्रश्न		question
फँसना		to be stuck, to be caught, to be snared
फाड़ना		to tear
फिर भी		even so
बरदाश्त करना		to tolerate, to endure
बाँसुरी		bamboo flute
बाक़ी		remaining, left
बात बनना		to go well; an aim to be achieved
बातचीत		conversation, negotiation
बिगड़ना		to go wrong, to get angry
भागना		to run away, to flee
भेद		difference
मचना		to break out, to be caused
मज़दूर		worker, laborer
महाभारत		India's mythical epic war
माँस		meat
मुँह		mouth, face
मुसाफ़िर		traveler
मेरा सिर		"...my foot!"
यद्यपि		although
यात्रा		journey, travel
योजना		plan, scheme
रस्सी		string, cord
राज़ी		agreeable, content
रूबरू		face to face
रोकना		to stop
लकड़ी		wood; stick
लाभ		profit, advantage
लूटना		to loot, to steal, to pillage
लोकप्रिय		popular
वर्ष		year
वापस मिलना		to be got back
शीघ्र		soon, quickly
शूटिंग		filming; "shooting"
संतुष्ट		satisfied
संतोष		satisfaction
सप्ताह		week
सफ़र		journey, travel
समाचार-पत्र		newspaper
समाप्त		finished, concluded
सहमत		in agreement
साधारण		ordinary
सुझाव		suggestion
सुझाव देना		to make a suggestion
सुधरना		to improve, to be put right
हड़ताल		strike, lockout
हड़ताल करना		to strike
हालाँकि		althou