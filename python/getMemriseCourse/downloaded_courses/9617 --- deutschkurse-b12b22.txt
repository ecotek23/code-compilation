die Kneipe		pub, bar
der Verein		club
verwenden		to use
bescheinigen jdm etw		to certify sth for sb
das Entgelt		payment
der Feierabend		end of the work day
erleichtert		relieved
sich lustig machen über		to joke about sth
eifersüchtig		jealous
die Vermutung		assumption
der Spielstein		gaming piece
die Spielfigur		pawn in a game
würfeln		to play dice
der Würfel		dice, cube
eine Runde aussetzen		miss a turn
umschreiben		to paraphrase
die Eierschale		eggshell
erraten		work sth out by guessing
der Gegner		enemy
der Fortschritt		progress
der Pechvogel		unlucky person
das Reihenhaus		terraced house
sich beschweren		to complain
sich erholen		to recover
schummeln		to cheat
duzen		to use "du"
siezen		to use "Sie"
drum		that's why
das Springseil		skipping rope
der Gummitwist		chinese jump rope
falls		if
gleichaltrig		of the same age
der Kumpel		mate
Geld abbuchen/abheben		to withdraw money
auffrischen		to brush up
vertrauenswürdig		trustworthy
menschenscheu sein		be shy with people
mollig		chubby
vollschlank		plump
mager		thin, gaunt, scraggy, skinny
treu		loyal
gesellig		convivial
das Wunschkonzert		musical request programmme
unbedingt		absolute
selbstständig		independent
lebendig		vivid
unabhängig		independent
zärtlich		tender (affectionate)
rücksichtsvoll		considerate
unbesorgt		unconcerned
jdm Bescheid sagen		to give sb a shout
ehrlich		honest
anspruchsvoll		demanding
der Kerzenschein		candlelight
der Witz		joke
die Mitteilung		notification
weiterhin		furthermore
der Vorschlag		proposal
übrigens		by the way
dran (sein)		be for it
der Abschnitt		part
nachschauen		to look after
schleudern		to spin (washing machine)
spülen		to flush the toilet
trocken		dry
empfindlich		sensitive
der Farbstoff		artificial colouring
enthalten		to contain
reichhaltig		extensive
der Wirkstoff		active substance
abstimmen		to coordinate
samtweich		velvety
die geschmeidige Haut		soft skin
mindern		to reduce
die Spannkraft		tone, resilience
das Ehepaar		married couple
die Alleinerziehende		single parent (female)
üblich		usual
ausführlich		detailed
deutlich		clearly
der Herbst		autumn
sich verwöhnen		to treat oneself
verpflichten		to oblige
der Kummer		sorrow, grief
Kummer haben		to have worries
sich verabreden mit jdm		to go out with sb
kühlen Kopf bewahren		to keep a cool head
herausfinden		to find out
die Sicht		view
der Umzug		move
der Herd		stove
überhaupt nicht		not at all
ausgefallen		unusual
der Schneebesen		whisk
der Multizerkleinerer		multiple chopper
der Behälter		container
der Edelstahl		stainless steel
das Gerstenmalz		barley malt
die Spur		trace; track; trail
genießen		to enjoy
feurig		fiery
die Abholung		collection
sich melden unter etw		to answer on/with
der Kratzer		scratch
drauf		on it
die Tafel		board
einwandfrei		flawless, perfect, irrefutably
aufräumen		to tidy
die Besprechung		meeting
das Regal		shelves
die Steckdose		electronical outlet
der Stecker		plug
der Stift		pen
der Bildschirm		screen
den Tisch decken		to set the table
die Gebrauchsanweisung		(operating) instructions
erfolgreich		successful
selbstbewusst		self-confident
schwach		weak
das Waschmittel		detergent
gleichberechtigt		equal, having equal rights
verführerisch		seductive
das Opfer		victim
auffallen		to stand out
riesig		enormous
grell		dazzling
der Stau		traffic jam
vertauschen		to switch
die Bestellung		order
der Unfall		accident
verfahren		drive wrong
sich verfahren		lose one's way
merkwürdig		strange, odd
der Taschenrechner		pocket calculator
der Rasenmäher		lawnmower
gesund		healthy
überprüfen		to control
vorsichtig		careful
der Verlag		publishing house
schaffen		to accomplish
umdrehen		to turn over
ausdrücken		to express
ein Unglück kommt selten allein		it never rains but it pours
das Missgeschick		adversity, mishap
Aller guten Dinge sind drei		All good things come in threes.
so ein Pech aber auch		bad luck
scheinbar		apparently
die Kontaktlinse		contact lens
der Anspitzer		pencil sharpener
Kommt nicht in die Tüte		No way!
weder...noch		neither...nor
sanft		gentle, easy, mild
mild		mild
komisch		funny
lebendig		alive, lively
mutig		courageous, brave
künstlich		artificial, fake, man-made, synthetic
der Umzug		relocation
die Spur		track
vorschriftswidrig		against the regulations
sich weigern		to refuse, decline
die Geschwindigkeitskontrolle		speed trap
die Eile		haste
drohen mit		to threaten with
unangenehm		unpleasant
vollkommen		flawless, perfect
infrage kommen		be possible
überzeugen		to convince
abspülen		to wash up
die Ernährung		diet (as in daily diet, not to lose weight)
kleinkariert		narrow-minded
wahnsinnig		insane
anschnallen		to fasten, to strap
der Blitzer		speed camera
das Bußgeld		fine
rechtlich		legal
die Beitrittserklärung		membership confirmation
die Streitigkeit		dispute
die Aufnahmegebühr		membership fee
kauen		to chew
schätzen		to value
ehrenamtlich		honorary
wohltätig		charitable
versprechen		to promise
der Rollstuhl		wheelchair
die Behinderung		disability
das Vorbild		role model
abliefern		to hand over, to deliver
unterstützen		to support
retten		to save
der Obdachlose		homeless person
der Finderlohn		reward
vermutlich		probably
die Pflicht		duty
die Überlegung		thought
schönreden		to play down
die Gewissensfrage		question of conscience
einen Termin einhalten		to keep a deadline
vertreten		to represent
zustimmen		to agree upon, to concur, to accept
widersprechen		to contradict
Das kommt darauf an.		It depends.
furchtbar		dreadful
aufheben		to pick up
wegfahren		to leave
die Absicht		intention
sich freimachen		to take time off, to undress
benachbart		neighboring
der Tropfen		drop
sich weigern		to refuse
voraussagen		to predict
fordern		to demand
verabschieden		to say goodbye
der Vorsatz		resolution
die Vorhersage		prediction
die Aufforderung		prompt
das Versprechen		promise
verteilen		to distribute
der Strafzettel		parking/speeding ticket
den Schlüssel stecken lassen		let the key in the lock
die Tüte		bag
hupen		to honk
die Reihe		row, line
die Genehmigung		approval
die Rücksicht		consideration
eigentlich		actually
hindern		to hamper
ausgehen		to go out
im Allgemeinen		generally speaking
sich widersetzen		to resist
die Gemeinschaft		community
bewegen		to move
selbstherrlich		high-handed
selbstgefällig		complacent
begeistert		enthusiastic
schadenfroh		malicious
wütend		furious
ängstlich		worried
mitfühlend		sympathetic
schnippisch		saucy
der Räuber		robber
der Werkzeugkasten		toolbox
reißen		to tear
der Umgang		handling
vermuten		to suppose
einschalten		to turn on
die Anschnallpflicht		obligatory wearing of seat belts
der Kindersitz		child safety seat
die Höchstgeschwindigkeit		top speed
die Abtreibung		abortion
abdecken		to cover
aufzeichnen		to record
ausweisen		to identify
befragen		to question
behaupten		to claim
bekannt geben		to announce
berichten		to tell
bestätigen		to confirm
betonen		to emphasize
beurteilen		to assess
bezweifeln, dass		to doubt that
in die Materie eindringen		to deal with the matter in-depth ways
feststellen		to assess
gelten als		to be regarded as
hervorgehen aus		to emerge from
kommentieren		to comment
lauten		to sound
meinen		to think, have an opinion
mitteilen		to inform
referieren		to report
resümieren		to summarize
sich äußern zu		to say something about
sich ergeben aus		to result from
verbreiten		to spread
vereinbaren		to agree
versichern		to ensure
wiedergeben		to quote
zusammenfassen		to summarize
der Abschluss		end
die Agentur		agency
die Aufmachung (von Buch)		presentation
die Ausrottung		extermination
die Aussage		assertion, statement; deposition
der Bericht		report
die Besprechung		review
die Bestätigung		confirmation
die Erklärung		explanation
die Filiale		branch
die Frist		deadline, period of time, time extension
die Garantie		guarantee
die Glosse		ironic comment
das Interview		interview
die Katastrophe		catastrophe
die Kolumne		column
der Kommentar		statement
die Kritik		criticism
die Meldung		notification
die Metereologie		metereology
die Nachricht		news
der Nervenkitzel		thrill
die Publikation		publication
die Quellenangabe		reference
die Redewiedergabe		style (linguistic)
die Reportage		reportage
die Schlagzeile		headline
die Stellungnahme		statement
das Tagesgeschehen		daily events
die Tagung		conference
die Textsorte		type of text
der Veranstalter		organizer
die Versuchsreihe		series of experiments
das Werk		work (eg of art)
die Wirtschaft		industry
die Hochschule		college
die Wissenschaft		science
das Zitat		quote
abgestumpft		numb
altgedient		long-serving
ausgestorben		extinct
bedauerlich		unfortunate
belletristisch		fiction and poetry
entsetzt		horrified
fassungslos		staggered
grotesk		grotesque
ironisch		ironic
katastrophal		catastrophic
knapp		scarce
notariell		notarial
objektiv		objective
pausenlos		nonstop
polemisch		polemical
renommiert		renowned
sachlich		objective
unsachlich		unobjective
subjektiv		subjective
unverhofft		unexpected
verheerend		devastating
zutreffend		correct
fern		far
gemäß		according to
laut		according to
nach		after, to
samt		along with
zufolge		according to
zuliebe		for the sake of
außer Kontrolle geraten		to get out of control
den Blick öffnen für		to open ones eyes to
eine Frage aufwerfen		to raise an issue
einen Vertrag schließen		to enter into an agreement
laufen gut/schlecht		to run well/badly
im Einsatz		to be on duty
Konsequenzen ziehen aus		to take the necessary action as result of
nach Angaben von		by  (sb's) account
nach Aussage von		according to
unter Berufung auf		on the authority of
vor Augen führen		to make aware
Widerstand leisten		to resist
feststehen		to be certain
verantwortlich		responsible
schieflaufen		to go wrong
die Menge		quantity
sich befassen mit		to deal with
hinaus		out
die Beförderung		promotion
der Kontoauszug		bank statement
der Gerichtsvollzieher		bailiff
furchtbar		terrible, awful, dreadful
leiden		to suffer
fies		nasty
aufeinander treffen		to hit each other
es fliegen die Fetzen		es gibt Streit (sparks are flying)
klopfen (an +a)		to knock (on)
der Scheinwerfer		floodlight
das Reh		deer
der Steinbock		capricorn
das Sternzeichen		zodiac sign
vernaschen		to lay sb
das Vergnügen		pleasure
dazwischenfunken		to butt in
kräftig		strong
verraten (verriet, verraten)		to tell; betray; reveal
sich überzeugen		to satisfy oneself
randvoll		brimful, full to the brim
wegwerfen		to throw away
sich schnappen		to nobble
beibringen		to teach
die Sau rauslassen		to let it all hang out
aussprechen mit		to talk things out with
die Windpocken		chickenpox
die Selbstbefriedigung		masturbation
erwischen		to catch (a criminal)
wie am Schnürchen klappen		to go swimmingly
das/der Rambazamba		fuss
eine Tischdecke auflegen		to put on a tablecloth
die Leidenschaft		passion
jdn antörnen		to give sb a kick
die Schaumstoffmatte		foam mat
ähnlich		similar
reizvoll		attractive
die Örtlichkeit		locality
die Badewanne		bathtub
der Säureschutzmantel		protective layer of the skin
der Fahrstuhl		elevator
irgendwie		somehow
bescheuert		daft, stupid
Mist bauen		to screw up
die Schuld auf jdn schieben		to lay the blame on sb
mittelmäßig		mediocre; indifferent
mittelgroß		of average height
schlau		sly, cunning
mittellos		destitute
mittelschwer		medium-weight
ständig		constant(ly)
die Waage		Libra
die Ermahnung		admonition
duzen		to use &quot;du&quot;
siezen		to use &quot;Sie&quot;
sich entspannen		to relax
der Bauchnabel		bellybutton
kleben an		to stick (to)
der Bauch		belly
die Aufschrift		inscription
passen		to fit
sich niederlassen		to settle down
hingehören		to belong
trauen		to trust
schummrig		weak, dim
sich an jdn kuscheln (kuschelte, hat gekuschelt)		snuggle up to sb
gedankenverloren		absent-mindedly
schmücken		to decorate
die Spitze		lace
blöderweise		stupidly
die Beurteilung		assessment
ausgerichtet		oriented
ansässig		resident
das Umfeld		setting, environment
sich einarbeiten		to get used to
projektbezogen		project-related
das Geschäftsfeld		business area
im Rahmen + gen.		in line with
die Einarbeitungszeit		training period
im Herzstück		in the core
wundervoll		wonderful
nebenbei		on the side
ausländerveindlich		xenophobic
der Großteil		majority
nachdenklich		thoughtful
feinfühlig		sensitive
unverfroren		barefaced
stets		always
boshaft		malicious
die Ausstrahlung		charisma
streitsüchtig		quarrelso