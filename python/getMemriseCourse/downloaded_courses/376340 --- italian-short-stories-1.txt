Lo sgombero		The Removal
buonuscita		compensation, key money
crepacuore		broken heart
fossa		pithole, cesspool
caparbio		stubborn
sfratto		eviction
scortecciato		scratched, flayed
zoccolo		hoof; boot, clog; plinth, base
in combuta		in league, in cahoots; herded
all'occorrenza		if need be, if necessary
L'indomani		The next day
puzzo		stink, stench, foul smell
orinatoio		urinal
caparra		deposit, down payment
giocoforza		inevitably, willy-nilly, in any case, necessarily
proroga		delay, deferment, postponement, extension
barroccino		barrow
spartire		to share out, separate; to have something in common with sb. (fig.)
suppergiù		roughly, more or less, just about
rasente		close to, very near, hugging close
stanga		bar, shaft
incolume		unhurt, safe and sound
stordito		stunned, dazed
cianfrusaglie		junk, bits and pieces, knick-knacks
inquilino		tenant
stabile		stable (adj.); building (n.)
tramezzo		partition, dividing wall
fornello		stove
intercalare		to insert (into) (v.); stock phrase (n.)
stonatura		false note, eyesore
usciere (m.)		usher, bailiff
gengive (f.pl.)		gums
masserizie (f.pl.)		(household) furnishings
già		already; formerly; agreed, naturally, of course
spicchio		segment, piece, clove (garlic), sliver (fig.)
intirizzire		to numb
slittare		to slip, slide, skid
sbucciarsi		to graze (oneself)
vinsanto		(inferior) sweet wine
interpellare		to consult, question
rintocco		toll, chime
intimazione di sfratto		eviction notice, order
venale		venal, grasping, greedy
viuzza		alley, lane
gobbo		hunchback
slacciare		to untie, undo, loosen
borbottare		to mutter
smorfia		grimace, funny face
assonnato		sleepy
sbalordire		to stun, amaze, astound
soprapensiero		absent-minded, lost in thought
sussultante		jerky, fidgety
fazzoletto		handkerchief, tissue, Kleenex
asciugare		to dry, wipe
andare a spasso		to go for a walk
matto		mad, crazy (2)
penoso		painful, distressing, difficult
irto di		bristling with
abbaino		attic room, skylight
comignolo		chimney (top), gable
dedalo 		maze, labryinth
lussato		dislocated, broken
intonaco		plastered (lit.)
davanzale (m.)		(window)sill
butterato		pock-marked, pitted
sussidio		subsidy, aid, welfare
tozzo		piece, morsel (n.); stocky, squat, thickset (adj.)
incutere paura a		to strike fear into
disgraziato		poor, wretched, unlucky, unfortunate, good-for-nothing; rascal, rogue, scoundrel
acquaio		(kitchen) sink
piagnucolare		to whimper, whine
socchiuso		ajar, half-open
tastoni		groping
acre		acrid, pungent, bitter, harsh, biting
sudore		sweat, perspiration
orina		urine
federa		pillow-case
avvilirsi		to lose heart, become discouraged
biascicare		to mumble
zingaro		Gypsy, gypsy
osteria		tavern, inn
congedo		leave, time off
rigovernare		to wash up (dishes); to tend to, groom (animals)
adoperarsi per (fare)		to make every effort, do one's best, strive
scapolo		bachelor
avventizio		casual labourer, casual laborer, temp
bestemmia		curse, blasphemy
calzone		trousers (2)
rimboccato		turned-up, rolled-up
patella		limpet
staccare		to prise, remove
tutti e due		together, both
dapprima		at first
granchio		crab (n.); blunder (n.), stubborn (adj.)
polpo		octopus
medusa		jellyfish
tracagnotto		dumpy, stocky (person)
venire su		to grow (up), come up
finne		flippers, fins
limpido		clear, limpid, lucid
muso		muzzle, snout; face, mug, chops
antenna per respirare		breathing tube
arnese (m.)		tool, implement, gadget, thing, weapon
alghe		seaweed
tremulo		trembling, quivering, flickering, twinkling
bracciata		armful, stroke (swimming)
branco		flock, pack, shoal, gang
squame		(fish) scales
figurati		of course, not at all, just imagine
palpebra		eyelid
traboccare		to overflow
struggimento		yearning, longing, urge
dondolare		to dangle, sway, swing, rock
ondeggiare		to ripple, flutter, wave
infiggere		to thrust, impale
conca		valley, basin, rock pool
accoccolarsi		to crouch (down)
pigliare		to take, catch (2 - informal)
infelice		unhappy, unfortunate, bad, poor (person)
branchie		gills
ficcare		to plunge, thrust
gabbiano		seagull
lazzeretto		(lepers') hospital
ventosa		sucker (zoo.), suction pad
frusta		whip
stanare		to dislodge, drive out
svenne		(he/she) passed out
afflosciarsi		to sag, droop, go flabby
probo		upright, serious
commendatore		OBE, award of merit
ariano		Aryan
ugrofinnico		Finno-Ugric
dacché		since, for in
bontà		goodness, kindness, quality
ripetizione (f.)		repetition, revision (Br.), review (US), private tutoring, coaching
fautore		supporter
asportato		removed (3), taken away
abiatici		descendents
cicatrice		scar
cicatrizzare; cicatrizzarsi		to heal, form a scar
ceppo		stump, stock, block, log
chicchirichì		cock-a-doodle-doo, cheerful crowing
inamidato		starched
santamente		devoutly
amido		starch
ritto		upright, erect
affatto		completely (2)
encomio		commendation, praise
innaffiatoio		watering-can
dilettissimo		beloved, dear to sb. heart
ginnasio		the first and second year of liceo classico (secondary school specializing in classics)
quisquilia		trifle, unimportant matter
uggiolare		to whine, whimper, howl (dog)
bizza		tantrum, outburst, (bad) mood
inopinatamente		unexpectedly
tentennamento		hesitation, wavering, diffident
tergiversante		evasive, prevaricating
mago		magician, conjurer, wizard
toccasana (m.)		panacea, miracle cure
rovinio		rumbling, crash (noise)
folgore (f.)		thunder, thunder-bolt
dirimpetto		opposite (3.)
verecondo (lett.)		modest, shy
bretelle		(trouser) braces
condiscepola		school-friend, co-disciple
more solito		usual way
nicchiato		hesitated
sprangato		locked, bolted
atterrito		terrified
gabbare		to deceive, delude, dupe, trick
Ho le ossa rotte		I'm knackered, done for, worn out (fig.)
sempiterno		eternal, everlasting
arditamente		daringly, boldly
gilè (m.)		waistcoat, sleeveless cardigan
constatazione (f.)		observation
caparbietà		stubbornness, obstinacy
scemo		dimwit
fatica sprecata		wasted effort, waste of time
catenaccio		bolt (of a door)
pitagorici		Pythagoreans
imbizzarrirsi		to get agitated, excited, frisky
porca		bitch, harlot, whore
fregata		rub, polish (action)
incipriarsi		to powder one's face, nose
cipria		face powder, hair powder
trapunta		quilt
paralume		lampshade
cencio		rag, cloth, duster
fruscio		rustle, rustling
castagno 		chestnut tree, chestnut (wood)
svanire		to fade, to vanish
tisico		tubercular, consumptive
marmo		marble
svolazzare		to flap (about), flutter (about)
aceto		vinegar
piccione (m.)		pigeon
sottana		skirt, petticoat, cassock
cesso		bog, loo (Br.), john (US), can, shitter (n.); shit, crap (adj., fig.)
cagna		she dog, bitch (zoo.), bitch (vulg. fig.)
singhiozzo		hiccup, jolt, sob
rannicchiare		to huddle up, tuck up, curl up
guanciale (m.)		pillow
capezzolo		nipple
capezzale (m.)		bolster, bedside
ascella		armpit
catinella		basin (2)
piovere a catinelle		to rain cats and dogs
folto		thick, dense, coarse
zitella		spinster, old maid
ventre (m.)		belly, stomach, womb
allattare		to (breast-)feed, suckle
bavero		lapel, collar
dondolo		rocking chair, rocking horse, swing
bussare		to knock (at the door), rap
apposta		special, deliberately, purposely, on purpose
cruciverba (m.)		crossword (puzzle)
gota		cheek (phys.)
andito		corridor, passage
protervia		(outwards) arrogance
infingardaggine		laziness, idleness, sloth
manco		left-handed (adj.); imperfection (n.); not even (as much - adv.)
corrotto		corrupt, depraved
tumido		tumid, blubbery, swollen
sdraiato		lounging
ancheggiare		to wiggle (one's hips), to walk with a wiggle, to sway one's hips, to slink
indugiare		to delay (trans.), to dither, take one's time (intrans.)
cavarsela		to pull through, cope, get away with
piantala di fare la stupida		to play the fool
calvo		bald
fodera		lining, (furniture) cover
esiguità		scarcity, meagreness, slenderness
congedare		to dismiss, send away, say goodbye
invalicabile		impassable, insurmountable
villeggiatura		holiday, vacation (3), holiday season
arroventare		to make red hot, scorch, inflame
fitta		sharp pain, pang, spasm, cramp
neghittoso		lazy, listless, indolent
la goccia che fa traboccare il vaso		the last straw (fig.), the straw that broke the camel's back (fig.)
modanatura		moulding, trim (of a car)
fatalmente		inevitably, with fatal consequences
abbozzo		sketch, outline, draft
schizzare		to squirt, splash
fastidioso		annoying, tiresome
tenerissimo		most tender
troncare		to break off, cut off
confondersi		to merge, mingle, mix, confuse
empio		impious, cruel
molo		jetty, pier
'rebrousser chemin'		retrace one's steps, go back (French)
marciapiede (m.)		pavement (Br.), sidewalk (US), (railway) platform
belva		wild beast (lit.), bitch (fig.)
cascinale (m.)		farmhouse
a riscatto		mortgaged, under mortgage
falansteri		communal houses
cappelletta		shrine
risata		laugh
spessore (m.)		thickness, significance, gauge
ovattare		to pad, muffle
dileguare		to dispel, disperse, die out
sfrigolare		to sizzle, crackle, splutter
frastuono		noise, din, racket
scrostato		peeling, peel, scrape
breccia		gap, breach
inerpicarsi		to clamber (up), struggle (up)
smottare		to slide
afferrandosi		grasping, grabbing
sgretolato		flaking, chipped, crumbling
a quando a quando		occasionally, from time to time
a poco a poco		gradually, little by little
aiuola		bed, border, flower-bed
varco		passage (2)
siepe (f.)		hedge, hurdle
sfollato		evacuated
intarsiato		veneered, inlayed
schianto		crash, crack (of noise)
fiocco		bow, ribbon; (snow)flake, (corn)flake