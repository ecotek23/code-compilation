verleiten		to tempt
die Gewalt		violence
atmen		to breathe
das Ziel		target
fortsetzen		to continue
der Atem		breath
gewalttätig		violent
aufnehmen		to record
abnehmen		to decrease
zunehmen		to increase
lästig		bothersome
anbringen		to attach
leer		empty
begegnen		to encounter
der Wettbewerb		competition
verdeckt		concealed
der Anblick		sight
vermuten		to suppose
anlegen		to invest
erfahren		to experience
die Hypothek		mortgage
auffallen		to stand out
herstellen		to manufacture
verzichten auf		to do without
die Behörde		authority
vollständig		complete
anregen		to motivate
vergleichbar		comparable
die Tatsache		fact
im Freien		in the open
stärken		to strengthen
unvermeidlich		unavoidable
der Zettel		piece of paper, note
darstellen		to depict
abrufen		to obtain
versorgen		to provide
aufrufen		to call
der Empfänger		recipient
von etwas abhängig		depending on something
abwesend		absent
dienen als		to serve as
unverzichtbar		indispensable
bestrafen		to punish
bedrohen		to threaten
bedrohlich		threatening
ausdrücken		to express
behaupten		to claim
abbrechen		to abandon
erscheinen		to appear, to turn up
lässig		casual
ablehnen		to refuse
der Darsteller		actor
der Zufall		accident
verpassen		to miss (a scheduled thing)
die Schuld		debt
schuldig		guilty
schulden		to owe
drehen		to turn
die Reithose		jodhpurs
zuverlässig		reliable
die Quelle		source
statt		instead of
enttäuschen		to disappoint
die Macht		power
die Wirtschaft		economy
stören		to disturb
zerstören		to destroy
mieten		to rent
vermieten		to rent out
aufführen		to perform (on a stage)
das Muster		pattern
der Geschmack		taste
der Zweifel		doubt
der Eindruck		impression
der Druck		pressure
üben		to practise
der Klang		sound
herunterladen		to download
hochladen		to upload
die Sammlung		collection
der Fragebogen		questionnaire
passen		to fit
erst jetzt		only now
die Haltung		attitude
straff		taut
der Einklang		harmony
überfordern		to overstrain
durchführen		to conduct
zurzeit		currently
das Opfer		victim
der Krankenwagen		ambulance
der Notfall		emergency
unbedingt		absolutely
den Vorteil nutzen		to take advantage
entsetzlich		terrible, horrid, abysmal
zusammenfassend		in summary
grundsätzlich		basically
ehemalig		former
der Täter		perpetrator
verherrlichen		to glorify
der Auslöser		trigger
beitragen		to contribute
in dieser Hinsicht		in this respect
das Vorstellungsgespräch		job interview
begutachten		to examine
beobachten		to observe
bewerten		to assess
der Einfluss		influence
unscheinbar		inconspicuous
schlampig		sloppy
das Gespür		intuition
zwicken		to pinch
der Klimawandel		climate change
retten		to save
der Lohn		wage
sich ärgern		to get angry
der Bericht		report
verfolgen		to pursue
die Freude		joy
entdecken		to discover
die Entdeckung		discovery
die Entwicklung		development
gelten		to pertain/be valid
verbinden		to connect
das Gesetz		law
erlassen		to issue (a law)
aufheben		to repeal
das Gehaben		demeanour
entwerfen		to design
vorbildlich		exemplary
das Verhalten		behaviour
sich benehmen		to behave
gerecht		righteous
zwingen		to force; to oblige
erhalten		to receive
der Erfolg		success
mager		thin, gaunt, scraggy, skinny
üblich		common
hübsch		pretty
ansprechen		to address; to respond
der Verband		association
stattfinden		to take place
der Torwart		goalkeeper
erkennen		to recognize
vorführen		to perform
gering		small (amount)
die Ausrede		excuse
die Mühe		effort
auf etwas zukommen		to approach something
der Verlust		loss
hauptsächlich		mainly
die Kraft		strength
die Ausdauer		endurance
der Botschafter		ambassador
zu den Besten zählen		to rank among the best
krönen		to crown
zurücktreten		to retire
belebend		invigorating
der Rausch		intoxication
der Wertgegenstand		object of value
enthalten		to contain
sich enthalten		to abstain from
der Betrag		amount
in Maßen		in moderation
übermäßig		excessive
die Menge		quantity
vernachlässigen		to neglect
die Erschöpfung		exhaustion
das Abenteuer		adventure
die Welle		wave
der Wolkenkratzer		skyscraper
berühren		to touch
der Bach		stream
die See		sea
der Berg		mountain
schweben		to float
die Wüste		desert
die Gewohnheit		habit
tatsächlich		really
die Pflicht		duty
einladen		to invite
durchfallen		to fail (an exam)
der Misserfolg		failure
bestehen		to pass (exam)
behalten		to keep
das Schlittenfahren		tobogganing
auf etwas setzen		to bet on something
über etwas verfügen		to have something at one's disposal
fehlen		to lack
verlassen		to leave; deserted/desolate
der Dreck		dirt
erobern		to conquer
übermitteln		to convey
die Folge		consequence
einfühlsam		sensitive
großzügig		generous
unsichtbar		invisible
der Streit		argument
geborgen		secure
was mich betrifft		as far as I am concerned
locker		loose; casual
egoistisch		selfish
zuversichtlich		confident
ehrlich		honest
die Verantwortung		responsibility
misstrauisch		distrustful
pedantisch		meticulous
peinlich		embarrassing
der Kulturkreis		culture goup
überwiegend		predominant
die Gesellschaft		society
die Einbeziehung		inclusion
die Krabbelgruppe		playgroup
die Scheidung		divorce
trennen		to separate, to detach
der Wandel		change, transformation
die Besprechung		meeting
verbreitern		to broaden
beibringen		to teach
der Gegensatz		opposite; in contrast
das Grinsen		grin
die Vorstellung		idea; picture, imagination; performance (theater), showing (film); introduction (to people)
der Umzug		relocation
der Zauberer		wizard
die Geisel		hostage
wiederholen		to repeat
dringend		urgent
der Streitpunkt		issue
anschauen		to view
umschalten		to change channel
kürzlich		recently
spenden		to donate
einführen		to introduce
das Verbot		ban
das Spielzeug		toy
finanzieren		to fund
die Gegenwart		the present
die Forschung		research
vorhanden		available
umstritten		controversial
deswegen		that is why
festnehmen		to arrest
gehören zu		to belong to
der Fortschritt		progress
vollgestopft		crowded
überall		everywhere
die Kohlehydrate		carbohydrates
die Fettleibigkeit		obesity
es sei denn		unless
ansonsten		otherwise
die Erziehung		upbringing
verheiratet		married
heiraten		to marry
scheiden		to divorce
die Witwe		widow
der Hochzeitstag		wedding anniversary
der Begriff		concept
sich entschließen		to decide on
scheitern		to fail
beantragen		to apply for
Zeit aufbringen		to spend time
die Freiheit		freedom
begrenzen		to limit
zahlen		to pay
betonen		to emphasize
sich erholen		to recover
die Erderwärmung		global warming
die Eilmeldung		breaking news
eindrücksvoll		impressive
das Verhältnis		relationship
traurig		sad
einzigartig		unique
die Zuneigung		affection
das Geheimnis		secret
vertrauen		to trust
anvertrauen		to entrust
außerhalb		outside
bedingungslos		unconditional
aufregen		to excite
locken		to lure
versetzen		to transfer
keine Sorge		don't worry
sich kümmern		to take care of
zeitgemäß		contemporary, appropriate, timely
sich erweisen		to prove oneself
berufstätig		employed
vorschreiben		to dictate
das Übel		evil
das Bedürfnis		need
das Ausmaß		extent
die Ehre		honour
der Geruch		smell
ekelhaft		disgusting
das Anzeichen		evidence
weich		soft
der See		lake
die Katastrophe		disaster
zwangsläufig		inevitably
einfordern		to demand
begabt		gifted
schreien		to scream
Geld aufbringen		to raise money
der Vorschlag		proposal
der Ersatz		replacement
der Rat		advice
böse		evil, vicious
erklären		to explain
bevorzugen		to prefer
ersetzen		to surrogate
die Oper		opera
der Soldat		soldier
die Satellitenschüssel		satellite dish
der Zeichentrickfilm		cartoon
der Wetterbericht		weather report
die Reklame		advertisement
emotional		emotional
spannend		exciting
deprimierend		depressing
abholen		to pick up (from a place)
aushalten		to endure
sich ausruhen		to rest
benötigen		to require
sich mit etwas beschäftigen		to occupy oneself with
sich bewerben		to apply for
deutlich		clearly
eindeutig		clear
das Ereignis		event
fördern		to promote
der Gedanke		thought
gefährden		to endanger
der Hauptgrund		main reason
die Maßnahme		measure
sich melden		to get in touch
der Konsumerismus		consumerism
der Verbraucher		consumer
ähnlich		similar
die Ausnahme		exception
zustimmen		to agree upon, to concur, to accept
berichten		to report
nass		wet
das Vorurteil		prejudice
der Zweck		purpose
schädlich		harmful
ständig		constant(ly)
suchen nach		to look for
überschätzen		to overestimate
übertrieben		exaggerated
übertreiben		to exaggerate
die Untersuchung		investigation
lenken		to steer
die Eigenschaft		feature
die Fälle		cases
ein Drittel		a third
stolz		proud
dünn		thin
eine Hälfte		a half
verwalten		to manage
verbieten		to forbid
die Anforderung		request
putzen		to clean
der Staub		dust
fegen		to sweep
der Anteil		share
begeistern		to inspire
gestalten		to design
zufrieden		satisfied
versichern		to ensure
schätzen		to value
die Ahnung		suspicion
begründen		to justify
das Loch		hole
bereits		already
feststellen		to assess
die Schlacht		battle
umgehen		to deal with
anfangen		to begin
ausrüsten		to equip
geeignet		suitable
die Aussage		assertion, statement; deposition
der Genuss		enjoyment
die Vergiftung		poisoning
die Kasse		checkout
preiswert		inexpensive
die Leistung		performance
das Tschechien		Czech Republic
das Ungarn		Hungary
die Vorschrift		regulation/rule
die Statistik		statistic
knapp		scarce
abschwächen		to attenuate, to mitigate
die Sättigung		saturation
scheuen		to shy away (from)
die Aufsicht		supervision
der Spiegel		mirror
verzeihen		to forgive
erfinden		to invent
zeihen		to accuse
entlang		along
außer		except for
hinter		behind
die Schwäche		weakness
das Betriebsfest		office party
unzertrennlich		inseparable
sich berühren		to converge
die Wahrheit		truth
die Ursache		reason
beeindruckt		impressed
befehlen		to command
betrügen		to deceive
fressen		to eat (for animals)
graben		to dig
messen		to measure
pfeifen		to whistle
der Ausweg		outlet
leicht		light, easy
erlauben		to allow
gültig		valid
gemeinsam		collectively
behandeln		to treat
die Behandlung		treatment
der Anlass		occasion
die Ablenkung		distraction
die Fähigkeit		ability
herrschen		to rule
schenken		to give a present
neugierig		curious
bauen		to build/construct
verschieben		to postpone
lediglich		merely
die Abkürzung		abbreviation
insgesamt		altogether, on the whole
ehrgeizig		ambitious
der Kunde		customer
kostspielig		expensive
eigens		specifically
die Entscheidung		decision
furchtbar		dreadful
die Luft		air
die Hitze		heat
liegen		to lie (down)
werfen		to throw
überflüßig		superfluous
notwendig		necessary
die Aussicht		prospect
die Strahlung		radiation
die Waffe		weapon
der Feinstaub		particulate
der Stoff		fabric
das Gift		poison
die Verbrennung		incineration
die Anlage		facility
der Schwefel		sulphur
der Stickstoff		nitrogen
der Meeresspiegel		sea level
der Sauerstoff		oxygen
das Quecksilber		mercury
das Eisen		iron
das Zinn		tin
das Uran		uranium
sich anreichern in		to accumulate in
entsorgen		to dispose of
der Sondermüll		hazardous waste
das Kraftwerk		power station
sich über etwas Gedanken machen		to worry about something
der Geländewagen		all-terrain vehicle
der Vertrag		agreement/ treaty
der Strom		current
der Grenzwert		limit
überwachen		to monitor
erschöpfbar		exhaustible
die Freisetzung		release
der Ruß		soot
der Schmierstoff		lubricant
bestätigen		to confirm
die Leitung		management, cable, line
schmutzig		dirty
die Verwendung		usage, use, appropriation
lagern		to store
der Vorrat		supply
die Nachfrage		demand
auftauchen		to surface
auftreten		to occur
erwähnen		to mention
sogar		even
wehen		to blow
stetig		constant
verwandeln		to transform
abhalten		to deter
der Sonnenkollektor		solar panel
füttern		to feed
verzögern		to delay
der Hof		courtyard
ernten		to harvest
fast		almost
verschlimmern		to exacerbate
die Abwechslung		change
die Rücksicht		consideration
der Rohstoff		raw material
neidisch		envious, jealous
schonen		to preserve, to conserve, to rest, to take care
der Brunnen		fountain
der Aktenordner		(document) file
die Glühbirne		light bulb
sorgfältig		thorough, careful, meticulous
reichhaltig		comprehensive, substantial
das Holz		wood
das Medikament		medicine
schonungslos ausbeuten		to exploit relentlessly
der Holzeinschlag		logging
die Plantage		plantation
erstaunlich		amazing
der Siedler		settler
der Wilderer		poacher
eindringen		to infiltrate; to penetrate; to break in, invade; to intrude
der Raubbau		depletion
gnadenlos		mercilessly
der Fachmann		expert
der Verfall		decline, decay
zerquetschen		to swat, crush
der Beifang		bycatch
anbauen		to grow, to cultivate
übrigens		by the way
höflich		polite
die Kette		chain
erstaunen		to amaze
das Versehen		mistake, blunder, oversight, lapse, slip-up
der Boden		ground, floor, bottom
pflügen		to plough
die Möbel		furniture
unmittelbar		direct, immediate, straight
der Räuber		robber
anscheinend		apparently
im Beisein von		in the presence of
verschweigen		to conceal
stürzen		to fall, to plunge
ernsthaft		seriously, genuine, urgently, sincere
stocksauer		peeved off
beleidigen		to insult
der Abschied		departure, farewell
sofort		immediately
dementsprechend		accordingly, correspondingly
die Sehnsucht		desire; longing; craving
der Mut		courage
der Abstand		distance, gap
der Umschlag		envelope
die Ausreise		departure, emigration
verhaften		to arrest, to detain
eintauchen		to immerse
entweder		either
inwiefern		to what extent, in what way
überzeugen		to convince
das Schwellenland		newly industrialized country
trocken		dry
mutig		courageous, brave
lindern		to alleviate
plötzlich		suddenly
das Vieh		cattle
überschwemmungsgefährdet		flood-prone
das Getreide		cereal
zurechtkommen		to manage, get by
überleben		to survive
leiten		to conduct; to lead
schmelzen		to melt
gelingen		to succeed
die Versicherung		insurance
verseucht		contaminated
drücken		to push
ziehen		to pull
die Lage		location
sieden		to boil
der Schritt		step
ersetzen		to replace
die Steuer		tax
die Subvention		subsidy
besteuern		to tax
umgekehrt		conversely
bezüglich		regarding
achten auf		to be mindful of, to pay attention
unentbehrlich		indispensable/ essential
die Anerkennung		recognition
gelegentlich		casually, occasionally
der Wohlstand		prosperity
ausüben		to exert
verbreiten		to spread
der Verfolgte		persecutee
die Einwanderung		immigration
der Zustrom		influx
zwar		indeed
beunruhigen		to upset, bother, disturb, worry
dämpfen		to dampen
aufgrund		on the basis of
die Geburtenrate		birth rate
verkraften		to bear, to cope with
erwarten		to expect
begehen		to commit, to perpetrate
beherrschen		to control
ein Mangel an		a lack of
die Bereicherung		enrichment
die Versorgung		supply, care
vorläufig		temporary
die Oberstufe		the 6th form
vorbereiten		to prepare
einschränken		to restrict
beitreten		to join, affiliate
äußern		to utter, to express
das Verbrechen		crime
einheimisch		native
sich kennen lernen		to get to know one another
das Denkmal		monument
der Bewerber		candidate, applicant
angreifen		to attack
beschimpfen		to insult, abuse
verprügeln		to thrash
verstoßen		to violate
vergebens		in vain
die Stimme		voice
aus etwas stammen		to originate from something
bergab		downhill
das Vergnügen		pleasure
das Elend		misery
besetzen		to occupy
der Wecker		alarm
melden		to report; to notify; to bring word; to announce
die Träne		tear, teardrop, waterdrop
die Hemmung		inhibition
erschüttern		to shock
sich abgewöhnen		to give up (something)
der Witz		joke
der Metzger		butcher
köstlich		delicious
die Ohnmacht		unconsciousness
tüchtig		brave, hearty, proficient, strenuous
der Flur		corridor
erwehren		to resist
unverzüglich		immediate, prompt
die Szene		scene
das Kapitel		chapter
spähen		to peer
ahnen		to suspect, forebode
begreifen		to grasp/comprehend
fassen		to grasp
beeilen		to hurry up
reichlich		abundant, abundantly
wegnehmen		to remove, take away
der Hafen		port, harbour
gewähren		to grant
die Verzweiflung		despair
der Ausnahmezustand		state of emergency
erschüttern		to shake/rattle
rasen		to career/race
die Geschwindigkeit		speed
der Zwischenfall		incident
der Umkreis		circle, perimeter
fieberhaft		desperate, frantic
der Schaden		damage
vertreten		to represent, to advocate
der Altersgenosse		contemporary
das Niveau		level
hartnäckig		defiant, stubborn, strong-willed
der Kontext		context
die Beschreibung		description
die Effektivität		effectiveness
analysieren		to analyse
in Abschnitte teilen		to split into sections
der Katalysator		catalyst
letztendlich		ultimately
die Genauigkeit		accuracy
die Denkweise		mindset
verteidigen		to defend
der Ruf		reputation
das Zeichen		sign
bescheiden		modest
erben		to inherit
die Kluft		gap
der Dichter		poet
betteln		to beg
donnern		to thunder
friedlich		peaceful
das Schlaraffenland		paradise
der Bereich		realm/range
das Einkommen		revenue/income
die Ausgrenzung		exclusion
hinrichten		to execute
das Vermögen		fortune
rausschmeißen		to sack/throw out
der Zusammenhalt		cohesion
anpassen		to adapt
prägen		to shape; to coin
der Pendler		commuter
der Ursprung		origin
der Quadratmeter		square metre
der Kubikmeter		cubic metre
folglich		thus, consequently
der Atommüll		nuclear waste
das Gefängnis		prison
die Todesstrafe		capital punishment
vernünftig		sensible
das Delikt		criminal offence
randalieren		to riot
das asoziale Benehmen		antisocial behaviour
die Siedlung		estate, settlement
die Gier		greed
die Vorbeugung		prevention, prophylaxis
zerkratzen		to scratch
das Messer		knife
die Absicht		purpose/ intention
verdienen		to earn
die Gegend		region/ district
erfüllen		to fulfil
die Bedingung		condition, requirement
der Asylbewerber		asylum seeker
die Entschlossenheit		determination
die Selbstgefälligkeit		complacency
die gemeinnützige Arbeit		community service
ermitteln		to ascertain, explore, investigate
die Genehmigung		approval, permission
das Potential		capability, potential
der Anspruch		claim, aspiration
prahlen		to boast
der Niedergang		demise
veranlassen		to prompt, induce
umarmen		to hug, embrace
grob		rude, rough, crude
das Soll		debit
trügerisch		elusive; deceptive; fallacious
die Führung		conduct; leadership/governance
der Übergang		transition
verfassungsmäßig		constitutional
verwickeln		to embroil
die Frist		deadline
der Kompromiss		compromise
ertrinken		to drown
das Unglück		misfortune, bad luck
die Leiche		corpse
die Offenheit		candour; openness
der Vorrang		precedence
loben		to praise, extol
ausdrücklich		particularly, explicitly
einflussreich		influential
verschärfen		to exacerbate/aggrevate
das Visier		sight
ansässig		resident
verurteilen		to condemn
zutiefst		profoundly/deeply
der Beweis		evidence, proof
leugnen		to deny
erstechen		to stab (with a weapon)
die Redefreiheit		free speech
die Abschreckung		deterrent
die Tat		deed, action
die Fertigkeit		skill, proficiency
straffällig		delinquent
die Haft		custody, detention
die Haftstrafe		imprisonment; prison sentence
die Versöhnung		reconciliation
verkünden		to proclaim, declare
vorwerfen		to accuse, reproach
passend		appropriate
der Verein		society, association
die Empörung		indignation, disgust, outrage
die Wut		rage, fury, wrath
der Umstand		circumstance
der Gefangene		prisoner
die Unregelmäßigkeit		anomaly, irregularity
befürchten		to fear
mutmaßlich		alleged, suspected
erheben		to raise
verdächtig		suspicious
die Verurteilung		conviction, condemnation, sentence
verringern		to diminish
die Entführung		abduction
langfristig		long-run/ long-term
zunächst		initially
der Rückfall		relapse, recidivism
der Unmut		discontent
der Eingriff		intervention
abtreten		to resign
explodieren		to explode
die Klage		lawsuit
das Zugeständnis		concession/admission
das Ergebnis		result, outcome
das Schicksal		fate, destiny
die Unverträglichkeit		incompatibility
heilen		to cure
unheilbar		incurable
die Stammzelle		stem cell
der Herzinfarkt		heart attack
der Schlaganfall		stroke
eventuell		perhaps, possibly
die Kenntnisse		skills, knowledge
klonen		to clone
weder ... noch		neither ... nor
die Ausländerfeindlichkeit		xenophobia
beeinträchtigen		to impair
umfassend		comprehensive
sowie		plus/ as well as
der Ausweis		identification
fordern		to demand
bewachen		to guard
befürworten		to support/ endorse
denkwürdig		memorable
annehmbar		passable, reasonable
der Zugang		access
verschwinden		to disappear
die Rache		revenge
ausführen		to implement
rückläufig		declining
die Vernichtung		annihilation
die Erzeugung		generation, creation
das Versuchstier		laboratory animal
die Vergeltung		retaliation
der Spielraum		leeway
verpflichten		to oblige
anmelden		to register
gewährleisten		to ensure / warrant / guarantee
mangelhaft		deficient, flawed
die Trümmer		debris, rubble
die Verabredung		appointment
der Schalter		(shop) counter
der Beamter		official, officer
auf etwas vertrauen		to rely on something
sowohl ... als auch		as well as, both ... and
voraussagen		to predict
die Alltagsgegenstände		everyday things
ausweichen		to avoid, to dodge
einweihen		to inaugurate
die Vergebung		forgiveness
die Buße		atonement, penance
stöhnen		to groan
entmutigen		to discourage
die Sichtweise		perception
gedeihen		to thrive, flourish, prosper
verraten		to betray
vorsichtig		careful
nachahmen		to emulate, imitate
verlangsamen		to decelerate
das Gewürz		spice
der Gipfel		height; peak; summit
entziehen		to deprive; detract; revoke; withdraw
beschäftigen		to employ
die Apotheke		pharmacy
erlangen		to gain
die Umgebung		surroundings, environment
die Schande		shame, disgrace
der Durchfall		diarrhoea
ausgeklügelt		sophisticated, ingenious
aufrechterhalten		to maintain, to sustain
die Artenvielfalt		biodiversity
abtreiben		to abort
entkommen		to escape
der Teufelskreis		vicious circle, vicious cycle
das Beileid		condolence, condolences
aufzeigen		to reveal
benachteiligen		to handicap, disadvantage
veröffentlichen		to publish
der Anhänger		supporter
der Verräter		traitor
anlässlich		on the occasion of
ausrotten		to exterminate
erhältlich		available
überwinden		to overcome
wesentlich		crucial, essential
das Gewissen		conscience
der Restmüll		residual waste
die Deponie		landfill, landfill site
einen Beitrag zu etwas leisten		to make a contribution to something
die Nachhaltigkeit		sustainability
dauerhaft		permanent
der Anwalt		lawyer
die Querschnittslähmung		paraplegia
umleiten		to redirect, divert
die Sünde		sin
der Sündenbock		scapegoat
die Lebensgrundlage		livelihood
die Vernunft		reason, rationality
der Rahmen		frame
voranbringen		to advance, to further
die Konkurrenz		competition, rivalry
sich auf etwas verlassen		to count on something
bedauern		to regret
öde		bleak, dull
die Unvermeidbarkeit		inevitability
beurteilen		to assess
die Gültigkeit		validity
verwundbar		vulnerable
auf Anhieb		instantly, straightaway
die Gewissheit		certainty
die Würde		dignity
der Verbündete		ally
verspotten		to mock
der Nachruf		obituary
waagerecht		horizontal
senkrecht		vertical
beschleunigen		to accelerate
die Waise		orphan
verzagen		to despair
der Köder		bait, lure
konkurrieren		to rival
der Hinweis		evidence, hint, suggestion
beschämen		to embarrass, to shame
aufmerksam		attentive, considerate
reinigen		to purify
beachtlich		considerable
der Dünger		fertilizer
das Erbgut		genotype
die Anziehungskraft		appeal, attraction
anfällig		susceptible, prone
abbringen		to dissuade
die Qual		agony, torture
unerträglich		unbearable
ausschließen		to exclude, to preclude
die Fortpflanzung		reproduction
die Befruchtung		fertilization, insemination
der Bezug		reference
erblich		hereditary
einstufen		to classify, to grade
überqueren		to cross, to traverse
voreilig		premature, rash
der Zusammenstoß		collision
der Vorsprung		advantage, edge
der Vorgänger		predecessor
der Nachfolger		successor
die Beerdigung		funeral
die Unterwerfung		submission, subjugation
flüchtig		brief; (chemically) volatile
es lohnt sich		it's worth it
die Ausstellung		exhibition
der Betrug		fraud
der Hochstapler		impostor
vortäuschen		to pretend
die Demütigung		humiliation
wehmütig		wistful, melancholic
aufbrauchen		to deplete
der Bestand		stock
die Überraschung		surprise
der Einwand		objection
der Ausbruch		outburst
eifrig		eager
die Fahndung		manhunt
die Überwachungskamera		CCTV camera
der Leibwächter		bodyguard
der Umfang		extent, breadth
die Montage		assembly, installation
bewundernswert		admirable
zugeben		to admit, to concede
blass		pale
festhalten		to adhere
demütig		humble, meek
einfangen		to capture
die Hingabe		commitment
die Herzensangelegenheit		matter close to one's heart
würdigen		to appreciate
unternehmerisch		entrepreneurial
zerschlagen		to smash
das Ärgernis		nuisance
der Ökostrom		green energy/ electricity
die Gezeitenenergie		tidal power/ energy
die Rückerstattung		refund
die Mehrwegflasche		refundable bottle
lebhaft		vivid
ausführlich		extensive
verlockend		tempting
begrüßen		to welcome
unschuldig		innocent
der Angeklagte		defendant
der Verurteilte		convict
ausliefern		to deliver, to ship
skrupellos		ruthless, unscrupulous
wettbewerbsunfähig		uncompetitive
bildgewaltig		visually stunning 
der Roman		novel
herablassend		condescending, patronising
andeuten		to imply, to suggest
verpflichtend		mandatory, compulsory
unbestreitbar		undeniably, undeniable
ungeschickt		awkward, clumsy
hegen		to cherish
die Entgiftung		detoxication
buchstäblich 		literal, literally
der Trost		consolation, solace
die Unterkunft		accommodation
die Beförderung		promotion
unterdrückerisch		repressive
der Spion		spy
einheitlich		consistent
der Vorzug		preference, merit
versiert		accomplished, experienced
abscheulich		hideous
zurückhaltend		reserved, reluctant
launisch		erratic, fickle
das Silizium		silicon
das Kalium		potassium
das Chlor		chlorine
das Wolfram		tungsten
das Natrium		sodium
der Drang		urge
unbefriedigend		unsatisfactory
heraufbeschwörend		evocative
der Lehrling		apprentice, trainee
das Jubiläum		anniversary
die Unzulänglichkeit		inadequacy, shortcoming
fließend		fluent
die Schlägerei		brawl
wagen		to dare, to venture
austauschen		to exchange, to swap
anderthalb		one and a half
die Strafverfolgung		criminal prosecution
der Darm		gut, intestine
schälen		to peel
der Schnabel		beak, bill
das Säugetier		mammal
schrumpfen		to shrink
die Währung		currency
das Versäumnis		omission, lapse, failure
würdevoll		dignified
bezaubernd		charming, adorable
die Finsternis		eclipse; gloom
lodern		to flare, to blaze
die Bewährung		probation, parole
anstreben		to strive, to aspire
die Bedrängnis		affliction, distress
entfernen		to remove
vollziehen		to fulfil
der Vorwurf		reproach
die Vertreibung		expulsion, eviction
die Eitelkeit		vanity
die Einschätzung		assessment, estimation
künftig		prospective
abwandern		to migrate
die Feuchtigkeit		moisture
vermehren		to breed, to multiply
die Abholzung		deforestation
der Umgang		contact, dealings
abwägen		to ponder
reifen		to mature
das Rückgrat		backbone
unterliegen		to succumb
verdrängen		to suppress; to supersede
untergraben		to undermine
aufladen		to charge (elec.), to recharge (batteries)
erfassen		to gather
die Freiheitsstrafe		prison term
das Fernziel		distant goal
ausstatten		to equip, to endow
subtil		subtle
der Anstand		decency
erfinderisch		ingenious
oben genannt		aforementioned
die Erweiterung		expansion
unüberwindbar		insurmountable
unabsichtlich		unwittingly; unintentionally
baufällig		dilapidated
das Rätsel		riddle, puzzle
entwirren		to unravel
verwirren		to distract, to confuse
örtlich		local
umstellen		to shift
der Schiedsrichter		referee
schleppen		to drag; to haul
die Wohlfahrtsorganisation		charitable organisation
tätig		active
die Einrichtung		facility, establishment
die Betreuung		care, support
vergnügt		jolly
tendenziell		tend to be
das Raubtier		predator
die Beute		prey
der Schurke		rouge; villain
glatt		smooth
anwerben		to recruit
sagenhaft		fabulous, legendary
das Röntgen		X-ray
vergöttern		to adore, to idolise
heimtückisch		treacherous, malicious, insidio