avion		airplane
migdală		almond
răspuns		answer
supărare		anger
gleznă		ankle
cameră		room
copil		child
spate		back, rear
prost		bad, stupid
sac		bag
brutărie		bakery
sferă		sphere
mal		shore, coast, bank
bară		bar, railing
barbă		beard
frumos		beautiful
pat		bed
dormitor		bedroom
boabă		berry
bicicletă		bicycle
mare		big
pasăre		bird
cuvertură		bedspread
carte		book
șef		boss
vas		bowl, vessel
cutie		box
brățară		bracelet
cosiță		braid
pâine		bread
micul dejun		breakfast
adiere		breeze
pod		bridge
mătură		broom
frate		brother
perie		brush
găleată		bucket
autobuz		bus
zahăr		sugar
mașină		car
aparat		device
morcov		carrot
bani		money
pisică		cat
tavan		ceiling
scaun		chair
ieftin		cheap
brânză		cheese
bucătar		cook, chef
vișină		sour cherry
bărbie		chin
așchie		splinter
ciocolată		chocolate
Crăciun		Christmas
biserică		church
cerc		circle
oraș		city
curat		clean
ceas		clock
dulap		closet
noros		cloudy
coastă		coast
haină		piece of clothing
cafea		coffee
monedă		coin
culoare		color
grăunte		grain, seed
cost		cost
a tuși		to cough
canapea		sofa, couch
colț		corner
corect		correct
sfoară		cord
țară		country
curte		court
vacă		cow
frișcă		cream
crimă		crime
cioară		crow
castravete		cucumber
cultură		culture
creț		curly
valută		currency
perdea		curtain
deștept		smart, clever
tăticu		daddy
lăptărie		dairy, milkshop
margaretă		daisy
dată		date
fiică		daughter
cerb		deer
pustiu		desert
desert		dessert
masă		meal
sufragerie		dining room
murdărie		dirt
dezastru		disaster
discuție		discussion
boală		ilness, disease
farfurie		plate
câine		dog
păpușă		doll
dolar		dollar
măgar		donkey
ușă		door
punct		point
doctor		doctor
aluat		dough
sertar		drawer
rochie		dress
tobă		drum
rață		duck
mut		mute
pulbere		dust, powder
colorant		dye
vultur		eagle
ureche		ear
timpuriu		early
cercel		earring
pământ		soil, land, earth
cutremur		earthquake
simplu		simple
educație		education
ou		egg
opt		eight
optsprezece		eighteen
cot		elbow
elefant		elephant
ascensor		elevator
ambasadă		embassy
eventualitate		eventuality, possibility
capăt		end
toți		everyone
toată lumea		everyone
seară		evening
exemplu		example
schimb		exchange
a ieși		to go out, to exit
scump		expensive
experiență		experience
a explica		to explain
ochi		eye
sprânceană		eyebrow
eșec		failure
șoim		falcon
familie		family
gras		fat
tată		father
favoare		favor
februarie		February
onorariu		fee
gard		fence
festival		festival
febră		fever
puțini		few
ficțiune		fiction
deget		finger
foc		fire
pompier		fireman
pește		fish
pumn		fist
steag		flag
lanternă		flashlight
gust		taste
zbor		flight
podea		floor (flooring, indoor surface)
făină		flour
ceață		fog
hrană		food
frunte		forehead
străin		foreign
furcă		pitchfork
pentru că		because
vulpe		fox
luni		Monday
marți		Tuesday
miercuri		Wednesday
joi		Thursday
vineri		Friday
sâmbătă		Saturday
duminică		Sunday
broască		frog
fruct		fruit
mobilă		furniture
joc		game
garaj		garage
gunoi		garbage
grădină		garden
usturoi		garlic
fată		girl
cadou		gift
clei		glue
zeu		god
gâscă		goose
guvern		government
băcănie		grocery
păr		hair
fericit		happy
tare		hard
pălărie		hat
durere de cap		headache
cap		head
ajutor		help
sughiț		hiccup
deal		hill
istorie		history
pasiune		hobby
cămin		home, fireplace
miere		honey
oribil		horrible
cal		horse
fierbinte		hot
oră		hour
înghețată		ice cream
insectă		insect
muncă		work, labour
jachetă		jacket
glumă		joke
jurnal		journal
junglă		jungle
cheie		key
cuțit		knife
domn		sir, gentleman
doamnă		lady, woman, madam
miel		lamb
spălătorie		laundry
lege		law
avocat		lawyer
frunză		leaf
literă		letter
leu		lion
buză		lip
listă		list
prânz		lunch
loțiune		lotion
noroc		luck
bagaj		luggage
nebun		crazy
furios		furious
poștă		mail
hartă		map
matematică		mathematics
carne		meat
memorie		memory
meniu		menu
lapte		milk
oglindă		mirror
eroare		error
lună		moon
munte		mountain
mamă		mother
mustață		mustache
gură		mouth
muzeu		museum
casă		house
alee		alley
parter		ground floor
etajul întâi		first floor
beci		cellar
perete		wall
fereastră		window
baie		bathroom
scări		stairs
bucătărie		kitchen
hol		hallway
masă de sufragerie		dining room table
ziar		newspaper
revistă		magazine
covoraș		rug
lampă		lamp
chiuvetă		sink
duș		shower
pernă		pillow
pătură		blanket
televizor		TV set
aspirator		vacuum cleaner
trist		sad
nefericit		unhappy
drăguț		nice
blând		kind, gentle
neliniștit		anxious
timid		shy
curajos		courageous
plăcut		pleasant
neastâmpărat		naughty
supărat		upset
vesel		cheerful
irascibil		irascible, ill-tempered
fermecător		charming
binevoitor		kind
este frumos		it is fine
este soare		it is sunny
este cald		it is warm
este răcoare		it is chilly
este frig		it is cold
bate vântul		it is windy
este noros		it is cloudy
este vreme rea		the weather is bad
curcubeu		rainbow
plouă		it is raining
ninge		it is snowing
este ceaţă		it is foggy
soare		sun
vânt		wind
ploaie		rain
zăpadă		snow
tunet		thunder
lapoviță		sleet
fulger		lightning
fulger globular		ball lightning
stânga		left
dreapta		right (side)
pe		on
în		in
de mai jos		below
la partea de		the side of
în spatele		behind
în fața		in front of
lângă		near
departe		far
roșu		red
oranj		orange
roz		pink
mov		purple
albastru		blue
verde		green
alb		white
negru		black
maro		brown
galben		yellow
a încerca		to try
a privi		to look, to watch
a usca		to dry
a aduce		to bring
a ajunge		to arrive
a pune		to put
a dormi		to sleep
a fura		to steal
a găsi		to find
a lua		to take
a lupta		to fight
a porni		to start
a scrie		to write
a citi		to read
a spera		to hope
a vrea		to want
a fi		to be
a avea		to have
a vopsi		to paint, to dye
a vizita		to visit
a alege		to choose
a aminti		to remind
a muri		to die
a oferi		to offer
a părea		to seem
a petrece		to pass (time)
a pleca		to leave
a povesti		to tell
a prefera		to prefer
vai!		oh, my!
dacă		if
atunci		then
aici		here
acolo		there
jos		down
sus		up
ce		what
care		which
de ce		why
unde		where
destul		enough
prea		too (more than enough)
din nou		again
doar		only
mult		much
tot		all
de asemenea		also
apoi		then
decât		than
altul		other (singular, masculine)
a (se) căsători		to marry
a se culca		to go to bed
a se distra		to have fun
a se îmbrăca		to get dressed
a se încălța		to put on shoes
a mișca		to move (something)
a schimba		to change
a se scula		to get up
a se spăla		to wash oneself
a se trezi		to wake up
a acoperi		to cover
a acorda		to grant, to give
a afla		to find out
a ajuta		to help
a alerga		to run
a apărea		to appear
a aprinde		to turn on (to light)
a arăta		to show
a auzi		to hear
a bate		to beat
a bea		to drink
a cădea		to fall
a călări		to ride a horse
a călători		to travel
a cânta		to sing
a căuta		to look for, to search
a cere		to ask for, to demand
a cina		to have dinner
a coace		to bake
a coase		to sew
a coborî		to go down, to descend
a conduce		to drive
a construi		to build
a crea		to create
a crește		to grow
a cumpăra		to buy
a curăța		to clean
a da		to give
a dansa		to dance
a deschide		to open
a desena		to draw
a discuta		to discuss
a duce		to carry
a face		to do, to make
a fierbe		to boil
a fotografia		to photograph
a fugi		to run
a fuma		to smoke
a găti		to cook
a hotărî		to decide
a ieși		to go out
a împleti		to knit
a înțelege		to understand
a întoarce		to return
a intra		to enter
a învăța		to learn
a juca		to play
a locui		to live, to dwell, to inhabit
a lovi		to hit
a lucra		to work, to practise, to function
a mânca		to eat
a mătura		to sweep (floor, with broom)
a menține		to maintain
a merge		to go
a munci		to work
a ninge		to snow
a opri		to stop
a organiza		to organize
a se petrece		to take place, to happen, to occur
a picta		to paint (pictures)
a pierde		to lose
a ploua		to rain
a preda		to teach
a primi		to receive
a purta		to wear
a putea		to be able to
a rade		to shave
a râde		to laugh
a rupe		to tear, to break
a se gândi		to think (about sth)
a servi		to serve
a sparge		to break (into many pieces)
a spune		to say (to tell)
a sta		to stay, to remain, to stand
a șterge		to wipe (clean)
a ști		to know
a stinge		to turn off, to put out
a tăcea		to be silent
a ține		to hold
a trata		to treat
a trebui		to have to (must)
a trece		to pass
a tunde		to cut (hair or grass)
a urî		to hate
a vedea		to see
a veni		to come
a vinde		to sell
a visa		to dream
a vorbi		to talk, to speak
a-i plăcea		to like
a deveni		to become
a întârzia		to be late
a rămâne		to remain, to stay
a completa		to fill out
a participa		to participate
a durea		to ache
a pregăti		to prepare
a traduce		to translate
a cunoaște		to know, to be aware of
a însemna		to mean
a folosi		to use
a reveni		to come back
a include		to include
a conține		to contain
a muta		to move
a răsări		to rise (sun, plants)
a apune		(of a heavenly body) to set
a părăsi		to leave
a freca		to rub
a strecura		to squeeze, to sneak
a se bucura		to be glad
a trimite		to send
tu		you (singular)
eu		I
el		he
noi		we
voi		you (plural)
ei		they (masculine or mixed)
acela		that
cine		who
când		when
cum		how
nu		no, not
mulți		many (masculine)
niște		some
acesta		this (masculine)
puțin		a little
alt		other
unu		one
doi		two
trei		three
patru		four
cinci		five
lung		long
larg		wide, broad
gros		thick
greu		heavy, difficult
mic		small
scurt		short
strâmt		narrow
slab		thin
femeie		woman
bărbat		man (adult male)
om		man, human
soție		wife
soț		husband
animal		animal
păduche		louse
șarpe		snake
vierme		worm
arbore		tree
pădure		forest
băț		stick
sămânță		seed
rădăcină		root
scoarță		bark (of a tree)
floare		flower
iarbă		grass
frânghie		rope
piele		skin
sânge		blood
os		bone
grăsime		fat
corn		horn (of animals)
coadă		tail
pană		feather
nas		nose
dinte		tooth
limbă		tongue (organ), language
unghie		nail
laba piciorului		foot
picior		leg
genunchi		knee
mână		hand
aripă		wing
burtă		belly
măruntaie		guts, bowels, entrails
gât		neck
piept		chest
inimă		heart
ficat		liver
a bea		to drink
a mânca		to eat
a mușca		to bite
a suge		to suck
a scuipa		to spit
a vomita		to vomit
a sufla		to blow
a respira		to breathe
a râde		to laugh
a vedea		to see
a auzi		to hear
a ști		to know
a mirosi		to smell
a se teme		to fear
a dormi		to sleep
a trăi		to live
a muri		to die
a ucide		to kill
a lupta		to fight
a vâna		to hunt
a lovi		to hit
a tăia		to cut
a despica		to split
a înjunghia		to stab
a zgâria		to scratch
a săpa		to dig
a înota		to swim
a zbura		to fly
a merge		to go, to walk
a veni		to come
a se întoarce		to come back
a cădea		to fall
a da		to give
a ține		to hold
a strânge		to tighten
a freca		to rub
a spăla		to wash
a șterge		to wipe, to delete
a împinge		to push
a arunca		to throw
a lega		to tie
a coase		to sew
a număra		to count
a zice		to say
a cânta		to sing
a juca		to play
a pluti		to float
a curge		to flow
a îngheța		to freeze
a se umfla		to swell
stea		star
apă		water
râu		river
lac		lake
mare		sea
sare		salt
piatră		stone
nisip		sand
praf		dust
nor		cloud
cer		sky
sloi		ice
fum		smoke
cenușă		ash
a arde		to burn
drum		road
noapte		night
zi		day
an		year
cald		warm
rece		cold
plin		full
nou		new
vechi		old
bun		good
rău		bad
putred		rotten
murdar		dirty, filthy
drept		straight
rotund		round
ascuțit		sharp
tocit		dull, blunt
neted		smooth
umed		wet
uscat		dry
drept		right
stâng		left
la		at
cu		with
și		and
deoarece		because
nume		name
apus		sunset
răsărit		sunrise
pustiu		deserted, empty
etaj		floor (storey, level)
a costa		to cost
multe		many (feminine)
palmă		palm (of hand)
a gândi		to think
a trage		to pull, to shoot
greu		heavy, difficult
un		a, an, one (masculine)
o		a, an, one (feminine)
a se mișca		to move (to be in motion)
voi		you (plural)
bărbat		man (adult male)
scoarță		bark (of a tree)
a bea		to drink
a mânca		to eat
a mușca		to bite
a suge		to suck
a scuipa		to spit
a vomita		to vomit
a sufla		to blow
a respira		to breathe
a râde		to laugh
a vedea		to see
a auzi		to hear
a ști		to know
a mirosi		to smell
a se teme		to fear
a dormi		to sleep
a trăi		to live
a muri		to die
a ucide		to kill
a lupta		to fight
a vâna		to hunt
a lovi		to hit
a tăia		to cut
a despica		to split
a înjunghia		to stab
a zgâria		to scratch
a săpa		to dig
a înota		to swim
a zbura		to fly
a merge		to go, to walk
a veni		to come
a se întoarce		to come back
a cădea		to fall
a da		to give
a ține		to hold
a strânge		to tighten
a freca		to rub
a spăla		to wash
a șterge		to wipe, to delete
a împinge		to push
a arunca		to throw
a lega		to tie
a coase		to sew
a număra		to count
a zice		to say
a cânta		to sing
a juca		to play
a pluti		to float
a curge		to flow
a îngheța		to freeze
a se umfla		to swell
a arde		to burn
a trage		to pull, to sho