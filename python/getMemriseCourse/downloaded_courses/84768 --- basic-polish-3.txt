chłopiec		boy
dziewczyna		girl
bogaty		rich (nominative m sg)
bogaty chłopiec		rich boy
biedny		poor (nominative m sg)
biedna dziewczyna		poor girl
jest		is
Chłopiec jest biedny.		The boy is poor.
Dziewczyna jest bogata.		The girl is rich.
mężczyzna		man
Mężczyzna jest przystojny.		The man is handsome.
przystojny		handsome (nominative m sg)
piękny		beautiful (nominative m sg)
kobieta		woman
Kobieta jest piękna.		The woman is beautiful.
nie jest		is not
Kobieta nie jest brzydka.		The woman is not ugly.
brzydki		ugly (nominative m sg)
samochód		car
dom		house
stół		table
czarny		black (nominative m sg)
Samochód jest czarny.		The car is black.
biały		white (nominative m sg)
Dom nie jest biały.		The house is not white.
kwiat		flower
czerwony		red (nominative m sg)
Kwiat jest czerwony.		The flower is red.
książka		book
Czarna książka jest nowa.		The black book is new.
nowy		new (nominative m sg)
mały		small, little (nominative m sg)
Mały kwiat nie jest niebieski. 		The small flower is not blue.
niebieski		blue (nominative m sg)
Kobieta nie jest stara.		The woman is not old.
stary		old (nominative m sg)
ciężarówka		truck, lorry
Ciężarówka jest zielona.		The truck is green.
zielony		green (nominative m sg)
kot		cat
pies		dog
koń		horse
brązowy		brown (nominative m sg)q
brązowy koń i żółty kot		a brown horse and a yellow cat
i		and
żółty		yellow (nominative m sg0
wysoki		tall (nominative m sg)
budynek		building
drzewo		tree
Drzewo jest wysokie.		The tree is tall.
Budynek jest wysoki.		The building is tall.
łóżko		bed
Stare łóżko jest duże.		The old bed is big.
duży		big, large (nominative m sg)
młody		young (nominative m sg)
ołówek		pencil
Ołówek nie jest stary.		The pencil is not old.
czasopismo		magazine
Nowe czasopismo jest drogie.		The new magazine is expensive.
drogi		expensive, costly (nominative m sg)
tani		cheap, inexpensive (nominative m sg)
różowy		pink (nominative m sg)
różowy kwiat i fioletowy kwiat		a pink flower and a purple flower
fioletowy		purple (nominative m sg)
rower		bicycle
ten		this (nominative m sg)
ten pies		this dog
Ten srebrny samochód jest bardzo drogi.		This silver car is very expensive.
srebrny		silver (color) (nominative m sg)
bardzo		very
ta		this (nominative f sg)
Ta mała książka nie jest tania.		This small book is not cheap.
Ta kobieta jest niska.		This woman is short.
niski		short (height) (nominative m sg)
to		this (nominative neuter)
To drzewo nie jest martwe.		This tree is not dead.
martwy		dead (nominative m sg)
żywy		alive, living (nominative m sg)
tamten		that (nominative m sg)
tamta		that (nominative f sg)
tamto		that (nominative neuter sg)
Tamten pomarańczowy dom jest brzydki.		That orange house is ugly.
pomarańczowy		orange (color) (nominative m sg)
Tamta stara koszula jest granatowa.		That old shirt is navy blue.
koszula		shirt
granatowy		navy blue (nominative m sg)
szary		gray (nominative m sg)
Tamto piwo nie jest zimne.		That beer is not cold.
piwo		beer
zimny		cold (nominative m sg)
gorący dzień		hot day
noc		night
dzień		day
zimna noc		cold night
ojciec		father
mój ojciec		my father
matka		mother
moja matka		my mother
moje łóżko		my bed
twój, twoja, twoje		your (sg) (nominative m sg, f sg, n sg)
mój, moja, moje		my (nominative m sg, f sg, n sg)
syn		son
córka		daughter
twój syn		your (sg) son
jego		his
jego córka		his daughter
jej		her (possessive adjective)
nasz, nasza, nasze		our (nominative m sg, f sg, n sg)
wasz, wasza, wasze		your (pl) (nominative m sg, f sg, n sg)
ich		their
brat		brother
nasz brat		our brother
siostra		sister
wasza siostra		your (pl) sister
ich pies		their dog
swój, swoja, swoje		one's own (nominative m sg, f sg, n sg)
ja jestem		I am
on jest		he is
ona jest		she is
ono jest		it is
my jesteśmy		we are
wy jesteście		you (pl) are
ty jesteś		you (sg) are
oni są		they (m) are
one są		they (f) are
ja nie jestem		I am not
ty nie jesteś		you (sg) are not
on nie jest		he is not
ona nie jest		she is not
ono nie jest		it is not
my nie jesteśmy		we are not
wy nie jesteście		you (pl) are not
oni nie są		they (m) are not
one nie są		they (f) are not
gorący		hot, heated
