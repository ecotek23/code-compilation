also		أيضاً / كذلك
and		وَ
but		لكن
first of all		أوّلاً وقبل كل شئ
or		أو
so		لذلك
then		ثُم
how many, how much		كم؟ بكم؟
how?		كيف؟
what?		ماذا؟/ما؟
when?		متى؟
who?		مَن؟
why?		لماذا؟
about/around		حَوْلَ
above		فوقَ
according to		بِناءً على
across		عِبرَ
after		بعدَ
against		ضِدَ
ahead of		أمامَ
all over		من جديد
along		على طول
among		بين
like		كـَ / مِثل
aside		بجانب
at		على
away from		بعيدًا عن
because of		بِسبب
before		قبلَ
behind		وراءَ
below		تحتَ
beneath		تحتَ
beside		بجانب
besides		بالإضافَةِ إلى
between		بينَ
beyond		وراءَ
but		لكن
by		على
close by		بالقُربِ مِن
close to		بجانب
concerning		بخُصوص
despite		رَغمَ
down		تحت
due to		ناتج عن / بسَبَب
during		خِلال
except for		بِاستِثناء
excluding		ياستثناء
for, in order to		لأجلِ أن / لـ
from		مِن
in		في
in front of		أمامَ
in place of		مكانَ
in spite of		بالرغم من
inside		بالدّاخِل
less		أقل
like		مِثل
minus		ناقص
near		قريب
near to		قريبٌ من
next to		بِجانب
on		على
on top of		فوق
opposite		عكسَ
out		خارج
outside		بالخارج
over		على
through		خلالَ
to		إلى
towards		باتِّجاه
under		تحت
until		إلى غاية
with		مع
without		بِدون
about/around		حَوْلَ
above		فوقَ
according to		وفقًا لـ / بِناءً على
across		عِبرَ
after		بعدَ
against		ضِدَ
ahead of		أمامَ
all over		من جديد
along		على طول
among		بين
around		حولَ
like, example		كـَ / مِثل
accept		قبِل
advise		نَصَحَ
allow		سمَح
answer		جاوَب
appear		ظهَر
apply		طبَّق
arrive		وصَل
argue		ناقش
attach		ربَط
avoid		تجنَّب
bake		خبز
be		أصبح
able		قدر
call		نادى
beat		ضرَب
begin		بدأ
bind		ربط
bleed		نزَف
blow		نفَخ
borrow		استَعارَ
break		كسَر
burn		حرَق
calculate		حسِب
carry		حمَل
check		راجَع
chew		مضَغ
climb		تسلَّق
close		قفَل
collect		جمَع
come		جاء
copy		نسخ
crawl		زحَف
cry		بَكى
cross		عبر
cut		جرح
dance		رقص
deliver		نقل
describe		وصَف
deserve		استَحَقَّ
detect		كشَف
dig		حفر
dislike		كرَهَ
do		عَمِل
draw		رسم
dream		حلم
dress		لبس
drink		شرب
earn		كسَب
eat		أكل
empty		فَرَّغَ
end		أنهى
enter		دخل
escape		هرب
explain		شرح
fall		سقط
fail		فشل
feel		شعَر
find		وجد
finish		أنهى
follow		تبِع
forge		زيَّف
forgive		سامَح
forsake		هجر
freeze		جمد
get		نال
get angry		غضب
give		أعطى
go		ذهَب
go down		نزل
go for a walk		تنَزَّه
grind		طحن
guard		حرس
hate		أبغض
have to		وجب
hear		سمِع
hit		ضرب
hold		أمسك
hope		أمل
hurry		عجَّل
hurt		جرح
Inform		خبَّ / أخبَرَ
introduce		قدَّم
invite		دعا
joke		مزح
jump		قفز
kill		قتل
kneel		ركع
know		عرف
knock		طرَق
last		دامَ
laugh		ضحِك
lay		رقد
leap		وثب
leave		ترك
lend		أقرض
let		ترك
lie		كذب
listen		سمِع
live		عاش
look		نظَر
look after		حافظ
lose		خسر
make		صنع
manage		أدار
mean		قصد
meet		قابل
murder		قتل
note		سجل
obtain		حصل
open		فتح
organise		نظم
pass by		مر
pay		دفع
permit		أذَن
phone		هاتف / تلفَنَ
place		وضع
please		أرضى
plant		زرع
play		لعِب
prefer		فضَّل
present		عرض
prevent		منع
produce		أنتج
promise		وعد
pull		جذب
push		دفع
put		وضع
reach		وصل
read		قرأ
receive		إستقبل
refuse		رفض
regret		ندِم
remember		ذكر
rent		أجَّر
request		طلب
repair		أصلح
repeat		كرَّر
research		بحث
reserve		حجز
return		عاد
revise		راجع
ride		ركِب
rise		نهض
rule		حكم
run		ركض
save		ادخر
saw		نشر
say		قال
scream		صرخ
search		بحث
see		رأى
seek		بحث
seem		بدأ
sell		باع
send		أرسل
serve		خدم
show		عرض
sign		وقَّع
sink		غرق
sit		جلس
slay		ذبح
sleep		نام
smile		ابتسم
sneeze		عطس
speak		تكلم
spend		أنفق
spill		دلق
split		قسم
spread		نشر
squeeze		عصر
stand		وقف
stay		بقى
steal		سرق
sting		لسع
strike		ضرب
stop		وقف
study		درس
succeed		نجح
swear		حلف
sweep		مسح
take		أخذ
tell		أخبر
thank		شكر
think		فكَّر
thrive		نجح
throw		رمى
touch		لمس
try		جرَّب
understand		فهِم
use		إستعمل
visit		زار
walk		مشى
want		أراد
wash		غسل
wear		لبِس
win		ربح
wish		تمنّى
work		عمِل
worry		قلق
write		كتب
able		قادر
active		نشيط
alone		وحيد
angry		غاضب
awake		مستيقظ
bad		سيء
beautiful		جميل
bent		منحنٍ
best		الأفضل
big		كبير
bitter		مُر
black		أسود
boiling		يغلي
boring		مُمِل
brief		مُختَصَر
bright		لامع
brilliant		متألق
broken		مكسور
charming		فاتن
cheap		رخيص
cheerful		مُبهِج / مُبتَهِج / مَرِح
clean		نظيف
clear		صافي
clever		ذكي
closed		مغلَق
cold		بارد
comfortable		مُريح
common		عادي
complete		كامل
complex		معقَّد
cruel		قاسي
dark		مُظلِم
dead		ميِّت
deep		عميق
different		مختلِف
difficult		صعب
dirty		وسِخ
disgusting		مُقرِف
dry		جاف
early		مُبَكِّر
easy		سهِل
electric		كهربائي
equal		مساوِ
exciting		مُثير
false		زائِف
fast		سريع
fat (person)		بدين / سَمين
favourite		مُفَضَّل
female		أُنثى
first		أوَّل
free		حُر
friendly		لَطِيف
full		مُمتلئ
funny		مُضحِك
general		عام
good		جيد
great		عظيم
happy		فرحان
hard (difficult)		صعُب
hard (stiff)		صلب
healthy		صِحِّي
heavy		ثقيل
high		عال
hot		حار
ill		مريض
important		مهم
kind		لطيف
large		واسع
last		أخير
late		متأخر
lazy		كَسول
least		الأقل
light		خفيف
little		قليل
long		طويل
loving		محب
low		مُنخفِض
male		ذَكَرٌ
married		مُتَزَوَِج
marvellous		مُدهِش
mature		ناضج
medical		طبي
mixed		مُختلِط
narrow		ضيِّق
natural		طبيعي
new		جديد
nice		جميل
noisy		كثير الضجيج
normal		عادي
numerous		عديد
old		قديم
open		مفتوح
perfect		مِثالي
pleased		راضٍ
polite		مهذَّب
poor		فَقِير
pretty		جميل
private		خاص
public		عُمومي
punctual		دقيق
quick		سريع
quiet		هادئ
ready		مستعد
real		حقيقي
regular		عادي
responsible		مسؤول
rich		غني
right		يمين
rough		خشن
round		مستدير
sad		حزين
safe		آمن
same		مماثل
secret		سِرّي
sensitive		حسَّاس
separate		مُنفصل
serious		جِدِّي
sharp		حاد
short		قصير
shy		خجول
silent		صامت
simple		بسيط
slow		بطئ
small		صغير
soft		ناعم
sorry		آسف
sour		حامض
special		خاص
strange		غريب
strict		قاسٍ
strong		قوي
stupid		غبي
sudden		مفاجئ
sweet		حلوٌ
tall		طويل
the best		الأفضل
the greatest		الأعظم
the least		الأقل
the worst		الأسوأ
thick		سميك
thin		رقيق
tired		تعبان
true		صحيح
typical		نموذجي
ugly		قبيح
unbelievable		غير معقول
useful		مُفيد
valid		صالح
valuable		قيِّم
violent		عنيف
warm		دافئ
weak		ضعيف
well		جيد
wet		مبتل
wide		عريض
wise		حكيم
witty		ظريف
wrong		خطأ
young		شاب
already		تَواً / الآن
almost		تقريبا
always/still		دائِما
here		هُنا
however		غير أن / مَهما
immediately		حالاً
long time		وقت طويل
more		أكثر
often		غالِبا (ما)
perhaps		رُبَما
quickly		بِسُرعة
really		حقا
recently		مؤخَّرا
sometimes		بعض الأحيان
straight away		حالاً
there		هُناك
too		أيضا
unfortunately		للأسف
very		جدًا
And you?		وأنت؟
Bless you (when sneezing)		رحمك الله
Congratulations!		مبروك
Could you repeat that, please?		أعد من فضلك
Could you speak slowly, please?		تكلم ببطء من فضلك
Did You Like It Here?		هل استمتعت بوقتك هنا؟
Do You Speak English?		هل تتكلم اللغة الإنجليزية؟
Do You Speak Arabic?		هل تتكلم اللغة العربية؟
Don't worry!		لاتقلق/ لا تقلقي!
Enjoy! (for meals)		شهية طيبة
Good Evening		مساء الخير
Good Luck!		بالتوفيق! إن شاء الله
Good Morning		صباح الخير
Good Night		تصبح/  تصبحين على خير
Good night & sweet dreams!		ليلة سعيدة وأحلام طَيِّبة !
Good/so-so		زين / لا بأس
goodbye		مع السلامة
Happy Birthday		عيد ميلاد سعيد
Happy Eid!		عيد مبارك
Happy New Year		سنة سعيدة
Happy Ramadan		رمضان مبارك
Merry Christmas		أعياد ميلاد سعيد
Hello		مرحباً
Hi friend(brother)		مرحباً يا أخي
Hi sister		مرحباً يا أخي
How are you?		كيف حالك؟
How Old Are You?		كم هو عمرك ؟
I don't know!		لآ أعرف!
I don't understand!		لا أفهم
I have missed you so much		اشتقت إليك كثيرا
I have no idea.		لاأدري
I Have To Go		يجب أن أذهب الآن
I Like Arabic		أحب اللغة العربية
I live in…		أعيش في ......
I need to practise my Arabic		أحتاج أن أمارس اللغة العربية
I Will Be Right Back!		سأرجع حالا
I’m American		أنا أمريكي/أمريكية
I’m fine thanks		أنا بخير .  شكرا
I'm (age)…		عمري .....
I'm From…		أنا مِن ...
I'm Learning Arabic		أتعلَّمُ اللغة العربية
I'm sorry! (if you don't hear)		عفوا !
Just a Little.		قليلا
Mr...		سيد...
Mrs.…		سيدة...
Miss…		آنسة...
My Name Is …		اسمي....
Nice To Meet You		أنا سعيد بِلِقائك
No Problem!		ليست هناك مشكلة
Nothing much		لا شيء جديد
see you later		أشوفك / أراك في مابعد
Sorry		آسف !
Thank you (very much)		شكرا (جزيلا)
Welcome		أهلاً وسهلاً
What Do You Do For A Living?		ما مِهنتُك؟
What is this?		ما هذا ؟
What’s new?		ما هي الأخبار؟
What's that called in Arabic?		ما اسمه بالعربية؟
What's Your Name?		ما اسمك؟
Where Are You From?		مِن أين أنت؟
Where Do You Live?		أين تسكن؟  أين تسكُنين؟
Write it down please!		أكتبها من فضلكَ! / أكتبيها من فضلكِ!
You’re welcome		العفو
You're Very Kind		أنت لطيف! أنتِ لطيفة
Can I Help You?		مُمكِن أن أُساعِدَكَ ؟
Can You Help Me?		هل يُمكن أن تساعدني ؟
Come With Me		تعال معي !
Excuse Me (to pass by)		المعذرة
Excuse Me... (to ask for something)		من فضلك
Go Straight		أمشٍ إلى الأمام
Hold On Please (phone)		انتظِر/ ابقى على الخط رجاءً / مِن فضلك
How Much Is This?		ما ثمنُ هذا / هذه ؟؟
I'm Looking For……		أنا أبحث عن ...
I'm Lost		أضعت طريقي!
Go Straight then Turn Left 		أمشٍ إلى الأمام ثم لِف إلى اليمين
One Moment Please		لحظة من فضلك
Go Straight then Turn Right		أمشٍ إلى الأمام ثم لِف إلى اليسار 
Where is the bathroom?		أين أجد المرحاض؟
black		أسود
blue		أزرق
brown		بِنِّي
chestnut brown		كستنائي
colour		لون
dark		غامق
green		أخضر
grey		رَمادي
light		فاتِح
orange		بُرتُقالي
pink		زَهري
red		أحمر
violet		بنفسجي
white		أبيض
yellow		أصفر
Africa		أفريقيا
Asia		آسيا
Australia		أستراليا
Europe		أوروبا
North America		أمريكا الشمالية
South America		أمريكا الجنوبية
Egypt		مِصر
France		فرنسا
Germany		ألمانيا
Great Britain		بريطانيا
Iraq		العراق
Ireland		إيرلندا
Italy		إيطاليا
Kingdom of Saudi Arabia		المملكة العربية السعودية
Lebanon		لبنان
Russia		روسيا
Spain		إسبانيا
United States		الولايات المتحدة الأمريكية
Yemen		اليمن
African		أفريقي
American		أمريكي
British		بريطاني
Egyptian		مِصري
French		فرنسي
German		ألماني
Iraqi		عراقي
Irish		إيرلندي
Italian		إيطالي
Lebanese		لبناني
Russian		روسي
Saudi		سُعودي
Spanish		إسباني
Yemeni		يمني
1 o’clock		الساعة الواحدة
2 o'clock		الساعة الثانية
3 o'clock		الساعة الثالثة
4 o’clock		الساعة الرابعة
5 o'clock		الساعة الخامسة
6 o'clock		الساعة السادسة
7 o'clock		الساعة السابعة
8 o'clock		الساعة الثامنة
9 o'clock		الساعة التاسعة
10 o'clock		الساعة العاشرة
11 o'clock		الساعة الحادية عشرة
12 o'clock		الساعة الثانية عشرة
Five past 		وخمس دقائق
Ten past 		وعشر دقائق
Half past		والنصف
Quarter past		والربع
Twenty past		والثلث
Five to 		إلا خمس دقائق
Ten to		إلا عشر دقائق
Quarter to		إلا الربع
Twenty to		إلا الثلث
time 		وقت 
time (countable units)		مرة 
time (general concept)		الزمان
century		قرن 
decade		حقبة 
year		سنة 
years		سنوات
week		أسبوع 
weeks		أسابيع
day		يوم 
days		أيّام
an hour		ساعة 
hours		ساعات
a minute		دقيقة 
minutes		دقائق
second		ثانية 
seconds		ثواني
moment		لحظة 
every day		كل يوم
all day		طوال اليوم
next (week)		الأسبوع) المقبل)
last (week)		الأسبوع) الماضي)
tomorrow		غدا
the day after tomorrow		بعد الغد
today		اليوم
last night		ليلة أمس
yesterday		أمس
the day before yesterday		أول أمس
dawn		فجر
sunrise		شروق الشمس
day after day		يوما بعد يوم
morning		صباح
every other day		كل يومين
noon		ظهر
afternoon		بعد الظهر
weekend		عُطلة نهاية الأسبوع
evening		مساء
sunset		غروب الشمس
sunset		مغرب
midnight		منتصف الليل
night		ليلة 
nights		ليالي
before		قبل
after		بعد
then		ثم
until		حتى
now		الآن
early		مبكر
late		متأخر
after a while		بعد قليل
later on, soon		لاحقا
always		دائما
usually		عادةً
sometimes		أحيانا
rarely		نادرا
from		مِن
later		فيما بعد
punctual		حريص على مواعيده
since		منذ
soon		حالاً
Monday		الاثنين
Tuesday		الثلاثاء
Wednesday		الأربعاء
Thursday		الخميس
Friday		الجمعة
Saturday		السبت
Sunday		الأحد
January		يناير
February		فبراير
March		مارس
April		إبريل
May		مايو
June		يونيو
July		يوليو
August		أغسطس
September		سبتمبر
October		اكتوبر
November		نوفمبر
December		ديسمبر
a third		ثُـلث
bottle		زُجاجة / قِنّينة
little		قليل
many		كثير
packet		رزمة
piece		قطعة
several		عِدّة
weather		الجوّ
weather forecast		نشرة جوية
temperature		درجة الحرارة
degree		درجة 
degrees 		درجات
high		درجة الحرارة) العظمى)
low		درجة الحرارة) الصغرى)
climate		مناخ
hot		حار
cold		بارد
warm		دافئ
warmth		دفء
clear		صحو
moderate		معتدل
rainy		ممطر
it's raining		الجو مطير
cloudy		غائم
sunny		مشمس
snowy		مثلّج
humid		رطب
stormy		عاصف
dusty		ترابي
cloud		غيمة 
clouds		غيوم
cloud		سحابة 
clouds		سحب
sun		شمس 
moon		قمر 
star		نجم 
stars		نجوم
rain		مطر 
snow, ice		ثلج
snowflake		ندفة الثلج
frost		صقيع
wind		ريح 
humidity		رطوبة
fog		ضباب
hail		برد 
storm		عاصفة 
thunder		رعد 
lightning		برق 
storms		عواصف
hurricane		اعصار 
hurricanes		أعاصير
tornado		زوبعة 
tornadoes		زوابع
earthquake		زلزال 
earthquakes		زلازل
flood		فيضان 
floods		فيضانات
volcano		بركان 
volcanoes		براكين
rainbow		قوس قزج
season		فصل 
seasons		فصول
spring		ربيع
summer		صيف
fall		خريف
winter		شتاء
animal		حيوان 
animals		حيوانات
cat		قطّة 
cats		قطط
dog		كلب 
dogs		كلاب
cow		بقرة 
cows		بقرات
bull		ثور 
bulls		ثيران
calf		عجل 
calves		عجول
buffalo		جاموس 
buffaloes		جواميس
cattle, livestock		بهيمة 
sheep		غنم 
sheep (plural)		أغنام
lamb		خروف 
lambs		خراف
donkey		حمار 
donkeys		حمير
horse		حصان 
horses		حصن
camel		جمل 
camels		جمال
goat		ماعز 
goats		مواعز
pig		خنزير 
pigs		خنازير
rabbit		أرنب 
rabbits		ارانب
mouse		فأر 
mice		فئران
rat		جرذ 
rats		جرذان
bird		طائر 
birds		طائرات
small bird, sparrow		عصفور 
small birds, sparrows		عصافير
parrot		ببغاء 
parrots		ببغاوات
duck		بطة 
ducks		بط
pigeon		حمامة 
pigeons		حمام
goose		اوزة 
geese		اوز
chicken		دجاجة 
chickens		دجاج
rooster		ديك 
roosters		ديوك
turkey		ديك رومي
owl		بومة 
owls		بوم
peacock		طاووس 
peacocks		طواويس
stork		لقلق 
storks		لقالق
swallow		عصفور الجنة
sparrows		عصفور دوري
ostrich		نعامة 
ostrich (plural)		نعام
crow		غراب 
crows		غربان
nightingale		بلبل 
nightingales		بلابل
falcon, hawk		صقر 
falcons, hawks		صقور
eagle		نسر 
eagles		نسور
swan		بجعة 
swans		بجع
dove		يمامة 
doves		يمام
reptile		زاحف 
reptiles		زواحف
amphibian		برمائية 
amphibians		برمائيات
snake		ثعبان 
snakes		ثعابين
turtle		سلحفاة 
turtles		سلاحف
lizard		سحلية 
lizards		سحال
frog		ضفدع 
frogs		ضفادع
crocodile		تمساح 
crocodiles		تماسيح
lion		أسد 
lions		اسود
hippopotamus		فرس 
hippopotami 		افراس النهر
rhinocerus		وحيد القرن
giraffe		زرافة 
giraffes		زرافات
zebra		حمار وحشي
kangaroo		كنغر
hyena		ضبع 
hyenas		ضباع
cheetah		فهد 
cheetahs		فهود
wolf		ذئب 
wolves		ذئاب
bear		دبّ 
bears		دببة
tiger		نمر 
tigers		نمور
elephant		فيل 
elephants		فيلة
monkey		قرد 
monkeys		قرود
bat		خفاش 
bats		خفافيش
gazelle		غزال 
gazelles		غزلة
fish		سمك 
fishes		أسماك
whale		حوت 
whales		حيتان
shark		قرش
dolphin		دخس
octopus		أخطبوط
jellyfish		قنديل البحر
squid		حبّار
swordfish		أبو سيف
shrimp		روبيان
lobster		سرطان بحري
crab		سرطان
starfish		نجمة البحر
eel		ثعبان البحر
seahorse		حصان البحر
seal		عجل البحر
walrus		فظّ
salmon		سالمون
catfish		قرموط
oysters, clams		محار
insect		حشرة 
insects		حشرات
mosquito		بعوضة 
mosquitoes		بعوض
fly		ذبابة
flies		ذباب
bee		نحلة
bees		نحل
ant		نملة
ants		نمل
cockroach		صرصور
flea		برغوث
louse		قملة
beetle		خنفساء
wasp		دبّور
worm		دودة
worms		دود
butterfly		فراشة
spider		عنكبوت
scorpion		عقربة
snail		حلزون 
tail		ذيل
paw		قدم الحيوان
hoof		حافر
claw		مخلب
fur		فروة
beak		منقار
wing		جناح
feather		ريشة
fin		زعنفة
to bark		نبح
to meow		ماء
to moo		خار
to tweet, chirp		زقزق
to twitter		زقزق
to roar		زأر
to crow		صاح
to bray (donkey)		شهق
to neigh		حمحم
shirt		قميص
shirts		قمصان
blouse		بلوزة 
blouses		بلوزات
coat		معطف 
coats		معاطف
trouser		بنطلون 
trousers		بنطلونات
suit		بدلة 
suits		بدل
tie		ربطة 
ties		رباط
sock		جورب 
socks		جوارب
shoe		أحذية 
shoes		حذاء
button		زرّ 
buttons		ازرار
wristwatch		ساعة يد
belt		حزام 
belts		أحزمة
dress		فستان 
dresses		فساتين
bag		حقيبة 
bags		حقائب
jacket		سترة 
jackets		ستر
skirt		تنّورة 
skirts		تنّورات
nightgown		ثوب النوم
pajamas		بيجامة 
pajamas		بيجامات
underwear		ملابس داخلية
slipper		خفّ 
slippers		خفاف 
slippers		اخف