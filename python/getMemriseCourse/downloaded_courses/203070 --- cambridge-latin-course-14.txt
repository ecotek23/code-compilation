canis		dog
coquus		cook
est		is
filius		son
hortus		garden
in		in
laborat		works, is working
mater		mother
pater		father
sedet		sits, is sitting
servus		slave
via		street
amicus		friend
ancilla		slave-girl, maid
cena		dinner
cibus		food
dominus		master
dormit		sleeps
intrat		enters
laetus		happy
laudat		praises
mercator		merchant
quoque		also
salutat		greets
ad		to
bibit		drinks
circumspectat		looks around
clamat		shouts
ecce!		look!
et		and
exit		goes out
exspectat		waits for
ianua		door
iratus		angry
leo		lion
magnus		big
navis		ship
non		not
portat		carries
respondet		replies
ridet		laughs, smiles
salve!		hello!
surgit		gets up, stands up
taberna		shop, inn
videt		sees
vinum		wine
agit		does
anulus		ring
coquit		cooks
cur?		why?
e		from, out of
ego		i
eheu!		oh dear! oh no!
habet		has
inquit		says
iudex		judge
mendax		liar
pecunia		money
perterritus		terrified
poeta		poet
quaerit		looks for, searches for
quis?		who?
reddit		gives back
satis		enough
sed		but
signum		sign,seal
vocat		calls
tu		you (singular)
adest		is here
adsunt		are here
agricola		farmer
ambulat		walks
audit		hears
clamor		shout, uproar
contendit		hurries
currit		runs
fabula		play, story
femina		woman
hodie		today
iuvenis		young man, young
meus		my, mine
multus		much
multi		many
optimus		very good, excellent
petit		makes for, attacks
plaudit		applauds
puella		girl
senex		old man
spectat		looks at, watches
stat		stands
turba		crowd
ubi?		where?
urbs		city
venit		comes
abest		is out, is absent
aberat		was out, was absent
cubiculum		bedroom
emit		buys
ferociter		fiercely
festinat		hurries
fortis		brave
fur		thief
intente		intently, carefully
libertus		freedman, ex-slave
olim		once, some time ago
parvus		small
per		through
postquam		after
pulsat		hits, thumps
quod		because
res		thing
scribit		writes
subito		suddenly
superat		overcomes, overpowers
tum		then
tuus		your, yours
vendit		sells
vituperat		blames, curses
cenat		dines
conspicit		catches sight of
cum		with
facit		makes, does
heri		yesterday
ingens		huge
intellegit		understands
lacrimat		weeps, cries
mortuus		dead
narrat		tells, relates
necat		kills
nihil		nothing
omnis		all
parat		prepares
prope		near
rogat		asks
tacite		quietly
tamen		however
terret		frightens
valde		very much
agitat		chases, hunts
consumit		eats
ducit		leads, takes
eum		him
facile		easily
ferox		fierce
gladius		sword
hic		this
ignavus		cowardly
nuntius		messenger
pes		foot
porta		gate
postulat		demands
puer		boy
pugnat		fights
saepe		often
sanguis		blood
silva		wood
spectaculum		show, spectacle
statim		at once
totus		whole
agnoscit		recognises
celeriter		quickly
cupit		wants
dat		gives
dies		day
emittit		throws, sends out
fert		brings, carries
homo		human being, man
hospes		guest, host
ille		that
inspicit		looks at, examines
iterum		again
manet		remains, stays
medius		middle
mox		soon
offert		offers
ostendit		shows
post		after
procedit		proceeds, advances
pulcher		beautiful
revenit		comes back, returns
tradit		hands over
abit		goes away
accipit		accepts
callidus		clever, cunning
contentus		satisfied
exclamat		exclaims
frater		brother
habitat		lives
imperium		empire
invenit		finds
liber		book
nos		we
nuntiat		announces
pax		peace
portus		harbour
quam		than
semper		always
servat		saves, looks after
solus		alone
suus		his, her, their
tacet		is silent, is quiet
uxor		wife
vehementer		violently, loudly
vos		you (plural)
capit		takes
civis		citizen
convenit		gathers, meets
credit		trusts, believes
de		about
favet		supports
in		in
it		goes
legit		reads
liberalis		generous
minime!		no!
nos		We
noster		our
nunc		now
placet		it pleases
primus		first
promittit		promises
pugna		fight
senator		senator
sollicitus		worried, anxious
stultus		stupid
vale!		goodbye
verberat		strikes, beats
vir		man
amittit		loses
complet		fills
custodit		guards
epistula		letter
flamma		flame
fortiter		bravely
frustra		in vain
fugit		runs away, flees
fundus		farm
iacet		lies
iam		now
igitur		therefore
mirabilis		strange, extraordinary
mittit		sends
mons		mountain
optime		very well
paene		almost, nearly
sentit		feels
tandem		at last
templum		temple
terra		ground, land
timet		is afraid, fears
unus		one
duo		two
tres		three
a		1DSN
am		1DSA
ae (D)		1DSD
ae (N)		1DPN
as		1DPA
is (1)		1DPD
us		2DSN
um		2DSA
o		2DSD
i (N)		2DPN
os		2DPA
is (2)		2DPD
?		3DSN
em		3DSA
i (D)		3DSD
es (N)		3DPN
es (A)		3DPA
ibus		3DPD
tu		You Nom.
te		You Acc.
tibi		You Dat.
ego		Me Nom.
me		Me Acc.
mihi		Me Dat.
sum		I am Pres.
es		You are (s) Pres.
est		He/she is Pres.
sumus		We are Pres.
estis		You are (pl) Pres.
sunt		They are Pres.
eram		I was Imperf.
eras		You were (s) Imperf.
erat		He/she was  Imperf.
eramus		We were Imperf.
eratis		You were (pl) Imperf.
erant		They were Imperf
o		I
s		you (s)
t		He/she/ it
nos		We
tis		You (pl)
nt		They
bam		I was carrying
bas		You (s) were carrying
bat		He/she was carrying
bamus		We were carrying
batis		You (pl) were carrying
bant		They were carrying
vi		I carried
visti		You (s) carried
vit		He/she carried
vimus		We carried
vistis		You (pl) carried
verunt		They carrie