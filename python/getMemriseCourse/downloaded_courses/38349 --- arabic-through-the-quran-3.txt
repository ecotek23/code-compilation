اِبْنٌ		son
أَبْنَاءٌ / بَنُونَ		sons (plural)
وَلِيٌّ		friend, protector
أَوْلِيَاءُ		friends, protectors (plural)
خَالِدٌ		remaining in, everlasting
خَالِدُونَ		remaining in, everlasting (plural)
صَالِحٌ		righteous
نِعْمَةٌ		blessing, bounty
أَخٌ		brother
إِخْوَانٌ		brothers (plural)
إِخْوَةٌ		brothers (plural)
رَبٌّ		lord
أَرْبَابٌ		lords (plural)
عَمَلٌ		work
أَعْمَالٌ		work (plural)
حَسْبٌ		sufficiency, reckoning
قَرِيْبٌ		near
سَمِيْعٌ		hearing (adjective)
حَيَاةٌ / حَيَوٰةٌ		life
أَلِيْمٌ		painful
مِنْ		from, of
فِيْ		in, concerning
لِ		to, for
بِ		in, at, by
عَلَی		on, against
إِلَی		to (motion)
مَعَ		with
بَيْنَ		between, among
نَارٌ (f)		fire, Hell
نِیْرَانٌ (f)		fires, Hell (plural)
صَدَقَةٌ		alms
صَدَقَاتٌ		alms (plural)
عُرْفٌ		crest, ridge, height
أَعْرَافٌ		crests, ridges, heights (plural)
سَبِيْلٌ (f)		way, road, path
سُبُلٌ (f)		ways, roads, paths (plural)
سَفَرٌ		journey
أَسْفَارٌ		journeys (plural)
ضَلَالٌ		error
فَسَادٌ		corruption
حَمْدٌ		owner of praise
أَهْلٌ		people, family, folk
أَهْلُونَ		people, families, folk (plural)
شَمْسٌ		sun
رَحِيْمٌ		compassionate
صَابِرٌ		patient, steadfast
أُمَّةٌ		community
أُمَمٌ		communities (plural)
شَيْءٌ		thing
أَشْيَاءُ		things (plural)
دَارٌ (f)		abode (f)
دُورٌ  (f)		abodes (plural, feminine)
دِيَارٌ (f)		abodes (plural, feminine)
حِزْبٌ		party
أَحْزَابٌ		parties (plural)
أَثَرٌ		track; effect
آثَارٌ		tracks; effects (plural)
لِسَانٌ		tongue
أَلْسِنَةٌ		tongues (plural)
نَاقَةٌ		she-camel
إِبِلٌ		she-camels (plural)
نَهْرٌ		river
أَنْهَارٌ		rivers (plural)
فَضْلٌ		bounty
سِحْرٌ		magic
صِرَاطٌ		way, path
قَمِيْصٌ		shirt
مُجْرِمٌ		sinner
قَلِيْلٌ		few
مُسْتَقِيْمٌ		straight
عَجِيْبٌ		amazing, strange, wonderful
حَلَالٌ		permissible
حَرَامٌ		forbidden, sacred
بَعِيْدٌ		far reaching, distant
يَسِيْرٌ		easy
عَسِيْرٌ		difficult
قَبْلَ		before
بَعْدَ		after
إِبْنَةٌ		daughter
بَنَاتٌ		daughters (plural)
أَلرَّحْمٰنُ		the Merciful
أَلْعَالَمِيْنَ (gen.pl.)		created beings (gen.pl)
شَفًا		lip, rim, edge
حُفْرَةٌ		pit
أَبٌ		father
آبَاءٌ		fathers (plural)
صَاحِبٌ		companion, inhabitant
أَصْحَابٌ		companions, inhabitants (plural)
طَاءِفَةٌ		group, number, party
إِمْرَأَةٌ		woman
نِسَاءٌ		women (plural)
نِسْوَةٌ		women (plural)
أَمْرٌ		affair, matter, command
أُمُوْرٌ		affairs, matters, commands (plural)
إِنْسَانٌ		man; people (plural)
أَلنَّاسُ		man; people (plural)
ذُوْ		possessor of
أُولُو		possessors of (plural)
إِسْمٌ		name
أَسْمَاءٌ		names (plural)
أَلْبَابٌ		hearts, intellects (pl)
بَصَرٌ		sight; eye
أَبْصَارٌ		sights; eyes
يَدٌ (f)		hand
أَيْدٍ		hands (plural)
عَدُوٌّ		enemy
أَيْدٍ		enemies (plural)
شَيْطَانٌ		devil
شَيَاطِيْنُ		devils (plural)
سَوْءٌ		evil
عَاقِبَةٌ		end, consequence
مَدِيْنَةٌ		city
مُدُنٌ		cities (plural)
بَيْتٌ		house
بُيُوْتٌ		houses (plural)
أَلِيْمٌ		painful
مُهِيْنٌ		humiliating
سَيِّئَةٌ		evil, ill
سَيِّئَاتٌ		evil, ill (plural)
قَمَرٌ		moon
ضِيَاءٌ		light, illumination
نُوْرٌ		light
أَنْوَارٌ		light (plural)
مَثَلٌ		likeness, parallel
أَمْثَالٌ		likeness, parallel (plural)
مَطْلِعٌ		rising-place
مَغْرِبٌ		setting-place
مَغَارِبُ		setting-places (plural)
بَرٌّ		land
بَحْرٌ		sea
بِحَارٌ		seas (plural)
حَمْلٌ		load
يَوْمُ ٱلْبَعْثِ		Day of Resurrection
اَلزَّبُوْرُ		the Psalms
خَفِيفٌ		light (adj.)
إِذْنٌ		permision
نَهْرٌ		river
أَنْهَارٌ		rivers (plural)
ذَنْبٌ		sin
ذُنُوْبٌ		sins (plural)
لَعْنَةٌ		curse
سَلَامٌ		peace
رِجْلٌ (f)		foot (f)
أَرْجُلٌ (f)		feet (f) plural
نَفْسٌ (f)		soul, self (f)
أَنْفُسٌ (f)		soul, self (f) (plural)
وَلَدٌ		child
أَوْلَادٌ		children (plural)
خِلَالَ		through
عَنْ		away from; concerning
بَعَثَ		يَبْعَثُ
بَلَغَ		يَبْلُغُ
جَعَلَ		يَجْعَلُ
حَمَلَ		يَحْمِلُ
خَلَقَ		يَخْلُقُ
دَخَلَ		يَدْخُلُ
ذَكَرَ		يَذْكُرُ
ذَهَبَ		يَذْهَبُ
سَمِعَ		يَسْمَعُ
شَرِبَ		يَشْرَبُ
شَهِدَ		يَشْهَدُ
ضَرَبَ		يَضْرِبُ
عَمِلَ		يَعْمَلُ
كَتَبَ		يَكْتُبُ
وَجَدَ		يَجِدُ
وَعَدَ		يَعِدُ
رَجَعَ		يَرْجِعُ
ظَهَرَ		يَظْهَرُ
كَرِهَ		يَكْرَهُ
لَبِثَ		يَلْبَثُ
أَخَذَ		يَأْخُذُ
أَمَرَ		يَأْمُرُ
تَبِعَ		يَتْبَعُ
خَرَجَ		يَخْرُجُ
رَفَعَ		يَرْفَعُ
سَأَلَ		يَسْأَلُ
فَعَلَ		يَفْعَلُ
كَذَبَ		يَكْذِبُ
نَصَرَ		يَنْصُرُ
عَلِمَ		يَعْلَ