l'asciugamano		towel, hand towel
l'asciugacapelli		hairdryer
la vasca da bagno		bath tub
la carta igienica		toilet paper
la bilancia		scale
la lampada		lamp
il letto		bed
il letto a castello		bunk bed
il lenzuolo		bedsheet
il rubinetto		tap, faucet
il lavandino		washbasin
il materasso		mattress
il cuscino		pillow, cushion
la coperta		blanket
la credenza		chest of drawers, sideboard (Italian with a z)
il paralume		lampshade
il copriletto		bed cover
il forno, il fornello		oven
lo scendiletto		bed rug
il divano		sofa, couch
la libreria		bookcase
la poltrona		armchair
il tappeto		rug, mat, carpet
il microonde		microwave
il tostapane		toaster
il caminetto		fireplace
il divano-letto		sofa-bed
il vaso		vase
lo sgabello		stool
lo sgabello poggiapiedi		footstool
il posacenere		ashtray
il frigorifero		fridge
il termostato		thermostat
il forno a gas		gas stove
il forno elettrico		electric stove
il lavello		kitchen sink
il climatizzatore		air conditioner
la lavastoviglie		dishwasher
lo scolapiatti		draining rack
le scale		stairs, staircase
l'armadio a muro		closet, built in wardrobe
l'armadio		wardrobe
il water, il gabinetto, la toilette		toilet
il telecomando		remote control
la radio		radio
la radiosveglia		clock radio
la sveglia		alarm clock
il telefono		telephone
il computer oggi e alla portata di tutti		the computer today is within everyone's reach
il computer		computer
il portasapone		soap dish
il portasciugamano		towel rack
lo scaffale		shelf
il frullatore		mixer
la tovaglia		tablecloth
il piatto fondo		soup bowl
il piatto piano, il piatto da cena		dinner plate
il lucchetto		padlock
il tavolinetto		coffee table
il comodino		bedside table
il copriletto		bedspread
il bidet		bidet
il coltello		knife
la forchetta		fork
il cucchiaio		spoon
il mestolo		ladle
la macchina da cucire		sewing machine
la macchina del caffè		coffee machine
la lavatrice		washing machine
la sedia		chair
Il tavolo		table
la credenza		sideboard
la vetrinetta delle porcelane		china cupboard
il mobile da cucina		kitchen cupboard
la catena		chain
il tubo per innaffiare		garden hose
il rastrello		rake
la cassettiera		dresser, chest of drawers
il tavolino da toletta		dressing table
il bicchiere		(drinking) glass
la bottiglia		bottle
la pentola a pressione		pressure cooker
il frullatore		blender
lo spremiagrumi		squeezer
la lampada a stelo		floor lamp
il tavolo da pranzo		dining table
il tavolo della cucina		kitchen table
lo stereo		stereo system
il lettore DVD		DVD player
il portatile		laptop
il cellulare		mobile phone
lo scopo		aim, goal, target
la scopa		broom
lo spazzolone		mop
l'ascensore		elevator, lift
la piscina		swimming pool
il corridoio		hallway, corridor
il giardino		garden
la cucina		kitchen
la sala da pranzo		dining room
la mansarda		attic
il seminterrato		basement
l'aspirapolvere		vacuum cleaner
il salotto		living room
la camera da letto		bedroom
la camera dei bambini		nursery
il televisore		television (set)
il terrazzo		terrace
il balcone		balcony
il dondolo		porch swing
la sedia a sdraio		deckchair
l'entrata coperta		porch
il cortile		yard, courtyard
il recinto		fence
il vialetto d'accesso		driveway
la padella		skillet,frying pan
il garage		garage
il bagno, la sala da bagno		bathroom
la dispensa		pantry, larder
la lavanderia		laundry room
il ripostiglio		storage closet
lo specchio		mirror
la parete, il muro		wall
il pavimento		floor
la piastrella		floor tile
il tetto		roof
il soffitto		ceiling
le tende		curtains
le veneziane		blinds
la finestra		window
la porta		door
il cavatappi		corkscrew
l'apriscatole		can opener
il bidone della spazzatura		dustbin, garbage can
la cuccia		doghouse
il camignolo, il camino		chimney
la grondaia		gutter pipe
la tegola		roofing tile
il giardino d'inverno		conservatory, sunroom
il vestibolo		vestibule
il vestibolo		vestibule
lo studio		home office, study
la tazza		cup
il piattino		saucer
il tazzone		mug
il thermos		thermos flask
il calorifero		radiator
la stufa elettrica		electric heater
il ventilatore		electric fan
il congelatore		freezer
towel, hand towel		l'asciugamano
hairdryer		l'asciugacapelli
bath tub		la vasca da bagno
toilet paper		la carta igienica
scale		la bilancia
lamp		la lampada
bed		il letto
bunk bed		il letto a castello
bedsheet		il lenzuolo
tap, faucet		il rubinetto
washbasin		il lavandino
mattress		il materasso
pillow, cushion		il cuscino
blanket		la coperta
chest of drawers, sideboard (Italian with a z)		la credenza
lampshade		il paralume
bed cover		il copriletto
oven		il forno, il fornello
bed rug		lo scendiletto
sofa, couch		il divano
bookcase		la libreria
armchair		la poltrona
rug, mat, carpet		il tappeto
microwave		il microonde
toaster		il tostapane
fireplace		il caminetto
sofa-bed		il divano-letto
vase		il vaso
stool		lo sgabello
footstool		lo sgabello poggiapiedi
ashtray		il posacenere
fridge		il frigorifero
thermostat		il termostato
gas stove		il forno a gas
electric stove		il forno elettrico
kitchen sink		il lavello
air conditioner		il climatizzatore
dishwasher		la lavastoviglie
draining rack		lo scolapiatti
stairs, staircase		le scale
closet, built in wardrobe		l'armadio a muro
wardrobe		l'armadio
toilet		il water, il gabinetto, la toilette
remote control		il telecomando
radio		la radio
clock radio		la radiosveglia
alarm clock		la sveglia
telephone		il telefono
the computer today is within everyone's reach		il computer oggi e alla portata di tutti
computer		il computer
soap dish		il portasapone
towel rack		il portasciugamano
shelf		lo scaffale
mixer		il frullatore
tablecloth		la tovaglia
soup bowl		il piatto fondo
dinner plate		il piatto piano, il piatto da cena
padlock		il lucchetto
coffee table		il tavolinetto
bedside table		il comodino
bedspread		il copriletto
bidet		il bidet
knife		il coltello
fork		la forchetta
spoon		il cucchiaio
ladle		il mestolo
sewing machine		la macchina da cucire
coffee machine		la macchina del caffè
washing machine		la lavatrice
chair		la sedia
table		Il tavolo
sideboard		la credenza
china cupboard		la vetrinetta delle porcelane
kitchen cupboard		il mobile da cucina
chain		la catena
garden hose		il tubo per innaffiare
rake		il rastrello
dresser, chest of drawers		la cassettiera
dressing table		il tavolino da toletta
(drinking) glass		il bicchiere
bottle		la bottiglia
pressure cooker		la pentola a pressione
blender		il frullatore
squeezer		lo spremiagrumi
floor lamp		la lampada a stelo
dining table		il tavolo da pranzo
kitchen table		il tavolo della cucina
stereo system		lo stereo
DVD player		il lettore DVD
laptop		il portatile
mobile phone		il cellulare
aim, goal, target		lo scopo
broom		la scopa
mop		lo spazzolone
elevator, lift		l'ascensore
swimming pool		la piscina
hallway, corridor		il corridoio
garden		il giardino
kitchen		la cucina
dining room		la sala da pranzo
attic		la mansarda
basement		il seminterrato
vacuum cleaner		l'aspirapolvere
living room		il salotto
bedroom		la camera da letto
nursery		la camera dei bambini
television (set)		il televisore
terrace		il terrazzo
balcony		il balcone
porch swing		il dondolo
deckchair		la sedia a sdraio
porch		l'entrata coperta
yard, courtyard		il cortile
fence		il recinto
driveway		il vialetto d'accesso
skillet,frying pan		la padella
garage		il garage
bathroom		il bagno, la sala da bagno
pantry, larder		la dispensa
laundry room		la lavanderia
storage closet		il ripostiglio
mirror		lo specchio
wall		la parete, il muro
floor		il pavimento
floor tile		la piastrella
roof		il tetto
ceiling		il soffitto
curtains		le tende
blinds		le veneziane
window		la finestra
door		la porta
corkscrew		il cavatappi
can opener		l'apriscatole
dustbin, garbage can		il bidone della spazzatura
doghouse		la cuccia
chimney		il camignolo, il camino
gutter pipe		la grondaia
roofing tile		la tegola
conservatory, sunroom		il giardino d'inverno
vestibule		il vestibolo
vestibule		il vestibolo
home office, study		lo studio
cup		la tazza
saucer		il piattino
mug		il tazzone
thermos flask		il thermos
radiator		il calorifero
electric heater		la stufa elettrica
electric fan		il ventilatore
freezer		il congelator