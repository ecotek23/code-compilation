le bus		the bus
le train		the train
la gare		the (train) station
la ville		the city; the town
le centre		the centre; the middle
le centre-ville		the city centre
le parc		the park
le bar		the bar; the pub
le théâtre		the theatre
la bibliothèque		the library; the bookcase
l'étranger; l'étrangère		the foreigner
la carte		the map; the card
l'adresse		the address
le guide		the guide book
aller		to go
trouver		to find
perdu		lost
perplexe		confused
là-bas		over there
où est la gare?		where is the train station?
le parc est là-bas		the park is over there
nous avons un problème		we have a problem
nous sommes perdus		we're lost
est-ce que tu as une carte ?		do you have a map?
je suis étranger		I'm a foreigner
je suis un peu perplexe		I'm a little confused
où est-ce que tu vas ?		where are you going?
elle va à un bar		she's going to a bar
je veux trouver le théâtre		I want to find the theatre
le liquide		the cash; the liquid
la carte de crédit		the credit card
le pourboire		the tip
le vin rouge		the red wine
le vin blanc		the white wine
le déjeuner		the lunch
le dîner		the dinner
dîner		to have dinner
le petit déjeuner		the breakfast
petit déjeuner		to have breakfast
accepter		to accept
nous aimerions quelque chose à manger s'il vous plaît		we would like something to eat please
j'aimerais un petit déjeuner s'il vous plaît		I would like some breakfast please
est-ce que vous aimeriez plus de bière ?		would you like more beer?
est-ce que je peux avoir l'addition s'il vous plaît ?		can I have the bill please?
je n'ai pas de liquide		I don't have any cash
est-ce que vous acceptez la carte ?		do you accept cards?
l'instant		the moment
regarder		to look; to watch
essayer		to try
cher		expensive
plus cher		more expensive
pas cher		cheap
moins cher		cheaper
vraiment		really
celui-ci		this one
celui-là		that one
j'ai besoin d'un nouveau chapeau		I need a new hat
j'ai besoin d'un instant pour réfléchir		I need a moment to think
regarde ça		look at that
est-ce que c'est possible ... ?		is it possible ... ?
est-ce que c'est possible d'essayer ça ?		is it possible to try this on?
c'est vraiment cher		it's really expensive
celui-ci est moins cher		this one is cheaper
celui-là est trop grand en fait		that one is actually too big
la question		the question
la rue		the street
le coin		the corner
le bâtiment		the building
tourner		to turn
suivre		to follow
arrêter		to stop; to quit; to arrest
poser une question		to ask a question
la droite		the right (direction)
la gauche		the left (direction)
tout droit		straight (direction)
rapide		quick; fast
rapidement		quickly
lent		slow
plus lentement		slower
sur		on; over
est-ce que je peux vous poser une question ?		can I ask you a question?
tournez à gauche		turn left (plural; formal)
c'est dans cette rue		it's on this street
vous parlez trop rapidement		you speak too quickly
est-ce que vous pouvez parler plus lentement s'il vous plaît ?		can you speak slower please?
le monde		the world
le pays		the country
la capitale		the capital (city)
le drapeau		the flag
le Royaume-Uni		the United Kingdom
Allemagne		Germany
Italie		Italy
Espagne		Spain
Japon		Japan
Chine		China
Inde		India
Egypte		Egypt
Canada		Canada
Ecosse		Scotland
Irlande		Ireland
Norvège		Norway
Suède		Sweden
Corée		Korea
le journaliste; la journaliste		the journalist
le docteur; la docteur		the doctor
le psychologue; la psychologue		the psychologist
le dentiste; la dentiste		the dentist
le professeur; la professeure		the professor; the teacher
l'étudiant; l'étudiante		the student
l'écrivain; l'écrivaine		the writer
l'avocat; l'avocate		the lawyer
l'artiste		the artist
le serveur		the waiter
la serveuse		the waitress
M. (Monsieur)		Mr.
Mme (Madame)		Mrs.
Mlle (Mademoiselle)		Miss
mon cœur		my sweetheart
chéri		darling
la cravate		the tie
les lunettes		the glasses
le visage		the face
l'œil		the eye
le nez		the nose
la bouche		the mouth
la dent		the tooth
l'oreille		the ear
les cheveux		the hair
l'histoire		the story; the history
porter		to wear; to carry
écrire		to write
blond		blond
roux		ginger
quelquefois		sometimes
chaque		every
quelquefois elle porte une robe rouge		sometimes she wears a red dress
elle est blonde		she's blond
elle a les yeux marron et les cheveux gris		she has brown eyes and grey hair
je suis journaliste		I'm a journalist
j'écris des histoires chaque jour		I write stories every day
le corps		the body
la tête		the head
le bras		the arm
la jambe		the leg
la main		the hand
le pied		the foot
l'orteil		the toe
le doigt		the finger
le cou		the neck
le genou		the knee
le poignet		the wrist
la cheville		the ankle
le coude		the elbow
l'épaule		the shoulder
la poitrine		the chest; the breast
la côte		the rib; the coast
le ventre		the stomach
le cœur		the heart
l'urgence		the emergency
l'accident		the accident
la douleur		the pain
le mal		the ache; the evil
le mal de ventre		the stomachache
le mal de tête		the headache
l'infection		the infection
le rhume		the cold (disease)
la fièvre		the fever
le médicament		the medicine
le papier		the paper
les toilettes		the toilet
le papier toilette		the toilet paper
je devrais		I should
tu devrais		you should (informal)
se sentir		to feel (oneself)
faire mal		to hurt
vomir		to vomit
saigner		to bleed
c'est une urgence		this is an emergency
qu'est-ce qui ne va pas ?		what's wrong?
je ne me sens pas bien		I don't feel well
ça fait mal		it hurts
j'ai mal au ventre		I have a stomachache
tu devrais voir un docteur		you should see a doctor
le docteur pense qu'il a besoin de médicaments		the doctor thinks he needs medicine
bon rétablissement !		get well soon!
la dame		the lady
le gentleman		the gentleman
mesdames et messieurs ...		ladies and gentlemen ...
une fois		once; one time
il était une fois ...		once upon a time ...
vivre		to live (be alive)
heureux		happy (in life)
jusqu'à		until
la fin		the end
... et ils vécurent heureux jusqu'à la fin		... and they lived happily ever after
le dieu		the god
mon dieu !		oh my god!
la vie		(the) life
la mort		(the) death
jamais de la vie !		never ever!
au fait ...		by the way ...
la façon		the way
d'une façon		in a way
à moitié		sort of
allez !		come on!
mince !		damn!
l'attention		the attention
fais attention !		pay attention! ; watch out!
la pièce		the room; the coin; the play (theatre)
la cuisine		the kitchen
la chambre		the bedroom
la salle de bains		the bathroom
le salon		the living room
la chambre d'ami		the guest room
en haut		upstairs
en bas		downstairs
à côté de		next to
entre		between
où sont les toilettes ?		where are the toilets?
elles sont entre la cuisine et le salon		it's between the kitchen and the living room
le cadeau		the present; the gift
la bague		the ring
la boucle d'oreille		the earring
le collier		the necklace
le bracelet		the bracelet
la montre		the watch
le jouet		the toy
le jeu vidéo		the video game
la fleur		the flower
le bisou		the kiss
le câlin		the hug
le téléphone portable		the mobile phone
le nouveau		the new (one)
donner		to give
déjà		already; yet
lui		him; her
leur		them
pour lui		for him
pour eux (pour elles)		for them
mais		but
un autre		another
je dois acheter un cadeau pour ma mère		I have to buy a present for my mum
je dois lui offrir quelque chose		I have to give him something
ma mère veut aussi une montre		my mum also wants a watch
j'ai déjà un téléphone		I already have a phone
il a déjà un téléphone, mais il en veut un nouveau		he already has a phone, but he wants a new one
mes enfants n'ont pas besoin d'un autre jeu vidéo		my children don't need another video game
fais-moi un bisou		give me a kiss
fais-moi un câlin		give me a hug
un mètre		a metre
un millimètre		a millimetre
un centimètre		a centimetre
un kilomètre		a kilometre
un gramme		a gram
un kilo		a kilo
une tonne		a ton
le point		the point; the full stop
peser		to weigh
gagner		to win; to gain
perdre		to lose
le poids		the weight
perdre du poids		to lose weight
prendre du poids		to gain weight
il y a cent centimètres dans un mètre		there are 100 centimetres to a metre
il y a mille grammes dans un kilo		there are 1000 grams to a kilo
elle pèse soixante kilos		she weighs 60 kilos
je pèse trois kilos de trop		I weigh three kilos too much
il fait un mètre soixante-quinze		he's one metre and seventy-five centimetres tall
ça pèse une tonne !		it weighs a ton!
le musée		the museum
l'église		the church
l'hôtel		the hotel
la salle de sport		the gym
le pont		the bridge
le cinéma		the cinema
répéter		to repeat
conduire		to drive
voler		to steal; to rob; to fly
naviguer		to sail
proche		close
près de ...		near (to) ...; by
loin		far
très loin		far away
juste ici		right here
est-ce qu'il y a un musée près d'ici ?		is there a museum near here?
la salle de sport est juste ici		the gym is right here
l'hôtel est là-bas		the hotel is over there
le pont est très proche		the bridge is very close
le cinéma est très loin		the cinema is far away
la bibliothèque est près du bar		the library is near the bar
est-ce que vous pouvez répéter ça s'il vous plaît ?		can you repeat that please?
est-ce que vous pouvez me conduire à cet hôtel s'il vous plaît ?		can you drive me to this hotel please?
la maternelle		the kindergarten
le collège		(the) secondary school (like GCSE)
le lycée		(the) college (like A-levels)
l'université		the university
étudier		to study
son (sa; ses)		his; her
notre (nos)		our
votre (vos)		your (plural; formal)
leur (leurs)		their
qui est ce garçon ?		who is this boy?
mon frère étudie à l'université		my brother studies at university
ma fille va à la maternelle		my daughter goes to kindergarten
le sujet		the topic; the subject
l'art		(the) art
la musique		(the) music
la géographie		(the) geography
les maths		(the) maths
la physique		(the) physics
la chimie		(the) chemistry
la littérature		(the) literature
la langue		the language; the tongue
les informations		the news; the information
le journal		the newspaper
le magazine		the magazine
le sport		the sport
le film		the film
la télé		the TV
l'émission de télé		the TV show
lire		to read
détester		to hate; to detest
intéressé		interested
être intéressé par ...		to be interested in ...
s'intéresser à ...		to be (oneself) interested in ... (questions)
qu'est-ce qu'il aime faire ?		what does he love to do?
qu'est-ce qui t'intéresse ?		what are you interested in?
il déteste lire le journal		he hates reading the newspaper
est-ce que tu t'intéresses à l'art ?		are you interested in art?
il est intéressé par les langues		he's interested in languages
ma petite amie déteste l'art et la musique		my girlfriend hates art and music
notre mère regarde toujours les informations		our mum always watches the news
l'ambulance		the ambulance
la police		the police
appeler		to call
cassé		broken
à tes souhaits !		bless you! (singular)
appelle une ambulance		call an ambulance
où est-ce que ça fait mal ?		where does it hurt?
mon bras me fait mal		my arm hurts
le docteur pense que ta jambe est cassée		the doctor thinks your leg is broken
trop bien !		awesome!
ça craint !		that sucks!
supposer		to suppose; to guess
je suppose ...		I suppose ... ; I guess ...
attendre		to wait; to expect
attends un peu		wait a minute
le truc		the stuff
des trucs comme ça		stuff like that
importer		to matter
peu importe ...		doesn't matter ...
Dieu merci !		thank God!
Noël		Christmas
joyeux Noël		merry Christmas!
Nouvel An		New Year
bonne année !		happy New Year!
le visa		the visa
la carte d'embarquement		the boarding pass
le passeport		the passport
le billet		the ticket
l'aéroport		the airport
le terminal		the terminal
le bagage		the luggage
la valise		the suitcase
voyager		to travel
visiter		to visit (a place)
rendre visite		to visit (a person)
montrer		to show
lourd		heavy
léger		light
quelle ville est-ce que nous devrions visiter ?		which city should we visit?
tu dois avoir un billet et un visa		you have to have a ticket and a visa
tu dois montrer ton passeport à l'aéroport		you have to show your passport in the airport
nous devons prendre un taxi		we have to take a taxi
mon bagage est trop lourd		my luggage is too heavy
le matin		the morning
la matinée		the (late) morning
l'après-midi		the afternoon
le soir		the evening
la nuit		the night
hier		yesterday
aujourd'hui		today
demain		tomorrow
cet après-midi		this afternoon
ce soir		tonight; this evening
tard		late
tôt		early
danser		to dance
nager		to swim
aller nager		to go for a swim
marcher		to walk; to work
aller se promener		to go (oneself) for a walk
sortir		to go out; to go outside; to hang out; to date
déjeuner à l'extérieur		to go out for lunch
dîner à l'extérieur		to go out for dinner
qu'est-ce que tu veux faire aujourd'hui ?		what do you want to do today?
je veux aller nager cet après-midi		I want to go for a swim this afternoon
dormir		to sleep
rester		to stay; to remain
veiller		to stay up
aller à la maison		to go home
courir		to run
aller courir		to go for a run
est-ce que tu veux aller te promener ?		do you want to go for a walk?
nous allons aller courir demain soir		we're going to go for a run tomorrow night
premier		first; 1st
deuxième		second; 2nd
troisième		third; 3rd
quatrième		fourth; 4th
cinquième		fifth; 5th
sixième		sixth; 6th
septième		seventh; 7th
huitième		eighth; 8th
neuvième		ninth; 9th
dixième		tenth; 10th
janvier		January
février		February
mars		March
avril		April
mai		May
juin		June
juillet		July
août		August
septembre		September
octobre		October
novembre		November
décembre		December
la date		the date (calendar)
nous sommes en juillet		it's July
quelle est la date d'aujourd'hui ?		what's the date today?
nous sommes le deux janvier aujourd'hui		today is January 2nd
l'argent		the money; the silver
l'euro		the euro (€)
la livre		the pound (£)
le dollar		the dollar ($)
la réduction		the sale; the discount
en soldes		on sale
l'affaire		the deal (business); the case (law)
l'offre		the offer
faire une affaire		to make a deal
faire une offre		to make an offer
coûter		to cost
dépenser		to spend
économiser		to save (money)
emprunter		to borrow
prêter		to lend
devoir ... à		to owe
tirer		to pull; to shoot
pousser		to push
laisser		to let; to leave (something)
gratuit		free (of charge)
assez		pretty; quite; enough
la librairie fait une réduction sur ...		the bookshop has ...  on sale
ça coûte combien ?		how much is that?
ça coûte quatre euros		that's four euros
c'est vraiment pas cher parce que c'est en soldes		it's very cheap because it's on sale
je n'ai pas assez d'argent		I don't have enough money
faisons affaire		let's make a deal
laissez-moi vous faire une offre		let me make you an offer
c'est une très bonne offre		it's a very good offer
elle a trop d'argent		she has too much money
nous ne pouvons pas dépenser tout notre argent		we can't spend all our money
c'est gratuit !		it's free!
les meubles		the furniture
la chaise		the chair
le canapé		the sofa
le lit		the bed
le placard		the cupboard
la commode		the chest of drawers
l'armoire		the wardrobe
le réfrigérateur		the fridge
le congélateur		the freezer
la cuisinière		the stove
le four		the oven
la porte		the door
la fenêtre		the window
le sol		the floor; the ground
le plafond		the ceiling
le mur		the wall
le toit		the roof
la lampe		the lamp
la couverture		the blanket
l'oreiller		the pillow
la couette		the duvet
le miroir		the mirror
l'ordinateur		the computer
l'ordinateur portable		the laptop
l'appareil photo		the camera (photos)
la caméra		the camera (videos)
le chargeur		the charger
le stylo		the pen
le crayon		the pencil
la clé		the key
le verrou		the lock
la quantité		the quantity; the amount
combien		how many; how much
sous		under
derrière		behind
devant		in front of; before; ahead of
est-ce que tu sais où sont mes clés ?		do you know where my keys are?
tes clés sont sous le canapé		your keys are under the sofa
combien de chaises est-ce qu'il y a dans le salon ?		how many chairs are there in the living room?
quelle quantité de lait reste-t-il dans le réfrigérateur ?		how much milk is there in the fridge?
le vol		the flight; the robbery
le bateau		the ship; the boat
l'avion		the plane
l'hélicoptère		the helicopter
la montagne		the mountain
la mer		the sea
la plage		the beach
le lac		the lake
retardé		delayed
à l'heure		on time; in time
nous devons prendre l'avion pour le Japon ce soir		we have to take a plane to Japan tonight
le vol est retardé		the flight is delayed
nous allons à la montagne		we're going to the mountain
le plan		the plan
le pop-corn		the popcorn
plus tard		later
ou quoi ?		or what?
est-ce que vous avez des projets plus tard ?		do you have any plans later?
ma mère et moi allons regarder mon émission de télé préférée		my mum and I are going to watch my favourite TV show
est-ce que tu veux venir ?		do you want to come?
est-ce que tu veux venir ou quoi ?		do you want to come or what?
j'irai		I'll go
je ferai		I'll do
jouer		to play; to act
le jeu		the game
jouer à un jeu		to play a game
peut-être		maybe; perhaps
à la place		instead
les autres		the others
les deux		both
tous les deux		both of us
nous tous		all of us
qu'est-ce que les autres feront cet après-midi ?		what will the others do this afternoon?
peut-être qu'ils iront au théâtre		maybe they'll go to the theatre
nous irons au marché et au bar		we'll go to the market and to the bar
est-ce que les autres veulent venir à la plage cet après-midi ?		do the others want to come to the beach this afternoon?
je pense qu'ils iront faire du shopping à la place		I think they'll go shopping instead
mille		1,000
deux mille		2,000
deux mille un		2,001 (number); 2001 (year)
deux mille dix-sept		2,017 (number); 2017 (year)
mille neuf cent quarante-cinq		1,945 (number); 1945 (year)
le quart		the quarter
durer		to last
combien de temps		how long (time)
il est une heure moins le quart		it's a quarter to one
combien de temps dure le film ?		how long is the film?
deux heures, vingt minutes et cinq secondes		two hours, twenty minutes and five seconds
le jardin		the garden
la fête		the party
la boîte de nuit		the club
j'adorerais		I would love
se lever		to get (oneself) up; to stand (oneself) up
quelqu'un		someone
n'importe qui		anyone; anybody
tout le monde		everyone
personne		no one; person
avec moi		with me
avec toi		with you (singular)
est-ce que tu veux venir à la fête avec moi ?		do you want to come to the party with me?
j'adorerais, mais je dois me lever tôt demain		I would love to, but I have to get up early tomorrow
je ne veux rien faire		I don't want to do anything
personne ne veut sortir avec moi		no one wants to hang out with me
pourquoi pas ?		why not?
je serai		I'll be
tu seras		you'll be (singular)
au travail		at work
à l'école		at school
à la maison		at home
l'hiver		the winter
l'été		the summer
le printemps		the spring
l'automne		the autumn
prochain		next
de retour		back
bientôt		soon
avant		before
après		after
est-ce que tu seras de retour avant ou après dîner ?		will you be back before or after dinner?
je serai à la maison à quatre heures		I'll be at home at four o'clock
je ne serai pas à l'école demain		I won't be at school tomorrow
nous allons aller en France cet été		we're going to go to France in the summer
avec qui est-ce que tu vas y aller ?		who are you going to go with?
elle ira en Angleterre l'hiver prochain		she'll go to England next winter
la rédaction		the essay
la lettre		the letter
le colis		the parcel
envoyer		to send
recevoir		to receive
faire la vaisselle		to do the dishes
faire la lessive		to do the laundry
sale		dirty
propre		clean
faire le ménage		to clean (the house)
aider		to help
seul		alone
il veut être seul		he wants to be alone
il est fatigué donc il veut rester à la maison		he's tired so he wants to stay at home
est-ce que tu veux m'aider ?		do you want to help me?
je dois écrire une rédaction		I have to write an essay
je déteste faire la lessive		I hate doing the laundry
le voyage		the trip; the journey
la marche		the step (stairs)
partir		to leave (somewhere)
arriver		to arrive
sans		without
la surveillance		the surveillance
sans surveillance		unattended; without surveillance
c'est un long trajet		it's a long journey
bon vol		have a good flight
attention à la marche		mind the gap
ne laissez pas vos sacs sans surveillance		don't leave your bags unattended
lundi		Monday
mardi		Tuesday
mercredi		Wednesday
jeudi		Thursday
vendredi		Friday
samedi		Saturday
dimanche		Sunday
l'anniversaire		the birthday; the anniversary
quel jour de la semaine sommes-nous aujourd'hui ?		which day of the week is today?
aujourd'hui nous sommes lundi donc demain nous serons mardi		today is Monday so tomorrow will be Tuesday
son anniversaire est le trois février		her birthday is the third of February
bon anniversaire !		happy birthda