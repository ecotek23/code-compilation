dare dell'idiota a qualcuno		to call somebody an idiot
fare un figlio		to have a child
fare silenzio		to keep silent
fare uno sforzo		to make an effort
crollare (cedere) sotto le torture		to break under torture
fare una sosta		to break a journey, to take a break
fare lo stupido		to play the fool
scoppiare a ridere (a piangere, in singhiozzi)		to burst out laughing (crying, sobbing)
scoppio dal caldo		I'm boiling
scoppiare (uno scandalo)		to break (a scandal)
spezzare il cuore		to break the heart
tenerci		to value (to hold dear)
domare un cavallo		to break in a horse
fare attenzione		to watch out, to be careful
fare il bucato		to do the laundry
fare casino		to make a noise
fare un complimento		to pay a compliment
fare la doccia		to shower
fare un favore		to do a favour
fare un giro		to take a ride, to take a spin
fare una passeggiata		to go for a walk (a stroll)
fare le pulizie		to do the cleaning
fare uno sbaglio		to make a mistake
fare un viaggio		to take a trip
scoppiare una bolla (un palloncino)		to burst a bubble (a balloon)
scoppiare uno pneumatico		to blow a tyre
comunicare una notizia		to break the news
entrarci		to matter, to be relevant
fare un passo		to take a step
fare un pisolino		to take a nap
fare il proprio dovere		to do one's duty
prendere parte		to take part
rendersi conto		to realise
rompere la gamba (il cellulare, un rapporto)		to break a leg (the phone, a relationship)
scoppiare una bomba		to explode a bomb, to blow up a bomb
sono pieno da scoppiare		I'm fit to burst (I'm bursting at the seams)
venire meno a una promessa		to break a promise
correre un rischio		to run the risk (to take a risk)
fare affari		to do business
fare amicizia		to make friends
fare un discorso		to make a speech
fare un esame		to take an exam
fare una foto		to take a photo
fare una gita		to make a trip
fare un incidente		to have an accident
fare un'offerta		to make an offer
fare i piatti		to do the dishes
fare il finto tonto		to play dumb
liberarsi da un'abitudine		to break a habit
mi scoppia la testa		my head is pounding
rimetterci		to lose (out)
rompere le palle a qualcuno		to irritate somebody (to annoy somebody)
avercela con qualcuno		to have it in for somebody
cedere il posto		to give up one's seat
farcela		to manage, to cope
fare il bagno		to take a bath
fare una chiacchierata		to have a chat
fare una festa		to have a party
fare un intervallo		to take a break
fare una partita		to have a game
fare un sogno		to have a dream
mancare di parola		to break a promise
scoppiare in lacrime		to burst into tears
scoppiare una guerra (un'epidemia, un incendio, una lite)		to break out (a war, an epidemic, a fire, a quarrel)
accorgersene		to realize, to notice, to become aware of
approfittarsene		to take advantage of, to exploit
cavarsela		to get by, to manage, to succeed, to hack it, to muddle through, to make ends meet
lavarsene le mani		to wash one's hands of something
legarsela al dito		to bear a grudge against somebody
mettercela tutta		to do everything you can possibly do
metterci		to take time
passarsela bene		to have a good time in life
prendersela con qualcuno per qualcosa		to get offended with someone over something
sbrigarsela		to finish something
sentirsela		to feel capable of doing something
smetterla		pack it in, knock it off (imperative)
spassarsela		to have a ball, to have a great time
vedersela brutta		to be saved by a miracle
volerci		to be needed, to take time
volercene		to take a lot of effort to do something
a chi tocca?		whose turn is it?
avere fiducia in qualcuno		to trust somebody, to have faith in somebody
essere fuori di sé		to be beside oneself, to be furious
essere a secco		to be out of money
fare due passi		to take a walk
farsi le ossa		to gain experience
farsi in quattro		to work very hard
fare quattro chiacchiere		to have a chat
prima di		before
tenere duro		to hold on, to not give up
un tipo in  gamba		a smart person, a capable person
una buona forchetta		a foodie, a lover of food, a hearty eater
un buontempone		a good time person, a fun loving person
un campanilista		a parochial person
un casinista, un pasticcione		a scatterbrain
un fanfarone, uno spaccone		a boaster
imbroccarla giusta		to hit the nail on the head
un menagramo		a jinx
un musone		a sulker, a moper
un ruffiano		a pimp, a panderer
sono contento di		I am happy to
sono felice di		I am happy to
un attaccabottoni		a bore, a drone
a bizzeffe		in abundance
a bruciapelo		point-blank
un burlone, un mattacchione		a practical joker
un cialtrone		a scoundrel, a rascal, a slob
un dormiglione		a sleepyhead
un golosone		gourmand, someone who has a sweet tooth
un impiccione, un ficcanaso		a busybody, a snooper, a nosy parker
un piantagrane		a troublemaker
un pivello		a newcomer, a greenhorn
un rompiscatole		a pain in the neck, a pest
uno scansafatiche		a lazybones, a shirker, a slacker
un voltagabbana		a turncoat
a casa del diavolo		at the back of beyond, in the middle of nowhere
a casaccio		haphazardly, at random
a cuor leggero		light heartedly
cogliere (prendere) qualcuno alla sprovvista		to catch somebody off guard (unawares, off balance)
correre (scappare) a gambe levate		to make a dash for it, to beat a hasty retreat
a macchia d'olio		like wildfire
a malincuore		reluctantly
a mano a mano		hand in hand
a occhio e croce		more or less
a pennello		perfectly, to perfection
ridere a crepapelle		to laugh until one's sides ache
a rotoli		downhill, to the dogs
a sbafo, a scrocco, a ufo		scrounging
a squarciagola		at the top of one's voice
a tu per tu		face to face
avere la faccia tosta		to be cheeky
avere fegato		to have guts
avere le mani bucate		to be a spendthrift
avere gli occhi foderati di prosciutto		to be blinkered
cercare un ago in un pagliaio		to look for a needle in a haystack
cogliere in flagranza		to catch red handed (in the act)
a denti stretti		through gritted teeth
la disponibilità		the means, the resources, the assets, the supplies, the availability, the accessibility
essere al verde		to be broke
un fancazzista, un fannullone		a lazybones
fare il filo		to have the hots for, to fancy
mangiare la foglia		to get wise to, to catch on to, to get the hang of
un maschilista		a male chauvinist
un menefreghista		a self-centred person, someone who couldn't care less
mi sono cascate le braccia		I could have wept
mordersi la lingua		to bite one's tongue
prendere due piccioni con una fava		to kill two birds with one stone
prendere sotto gamba		to underestimate
ripercuotersi		to effect, to have repercussions on
uno sciovinista		a jingoistic person
uno stacanovista		an eager beaver
tenere il piede in due scarpe		to be indecisive
verificarsi		to happen, to occur, to take place, to prove to be
accorgersi di		to notice
ammettere di		to admit to
avere bisogno di		to need to
avere il diritto (il dovere) di		to have the right (the duty) to
avere intenzione di		to intend to
avere paura di		to be afraid to (of)
avere il tempo di		to have the time to
avere voglia di		to want to
cercare di		to try to
chiedere di		to ask to
confessare di		to confess to
credere di		to think, believe
decidere di		to decide to
dimenticarsi di		to forget to
dire di		to say to
dubitare di		to doubt
evitare di		to avoid
fingere di		to pretend to
finire di		to finish
immaginare di		to imagine
impedire di		to prevent
innamorarsi di qualcuno		to fall in love with somebody
lamentarsi di		to complain about
negare di		to deny
ordinare di		to order
parlare di		to talk about
pensare di		to have an opinion about
permettere di		to allow, to permit
piantarla di		to stop doing something, to pack it in
pregare di		to ask, to request to (also: to pray)
preoccuparsi di		to worry about
pretendere di		to claim to
proibire di		to prohibit
promettere di		to promise
ricordarsi di		to remember to
rifiutarsi di		to refuse to
scusarsi di		to excuse
sforzarsi di		to make an effort to
smettere di		to stop (doing something)
sognare di		to dream about (of)
sperare di		to hope to
suggerire di		to suggest
temere di		to fear
tentare di		to attempt to
terminare di		to cease, to stop
vergognarsi di		to be ashamed of
vietare di		to forbid
abituarsi a		to get used to
accompagnare a		to accompany
aiutare a		to help
andare a		to go to
arrivare a		to manage to
cominciare a		to begin to
continuare a		to continue
convincere a		to convince
correre a		to rush, to hurry
costringere a		to compel to, to force to
credere a		to believe in
decidersi a		to make up one's mind to, to resolve to
divertirsi a fare qualcosa		to enjoy doing something, to have fun doing something
entrare a		to enter
fare in tempo a		to make it, to have time to
fermarsi a		to stop
giocare a		to play sport
imparare a		to learn to
incoraggiare a		to encourage to
iniziare a		to start
insegnare a		to teach to
interessarsi a		to be interested in
invitare a		to invite
mandare a		to send to
mettersi a		to start to
partecipare a		to participate in
pensare a		to think about
preparare a		to prepare to
prepararsi a fare qualcosa		to get ready to do something
provare a		to endeavour to
restare a		to stay to
rinunciare a		to give up, to quit
riuscire a		to succeed to
salire a		to go, to come up
sbrigarsi a		to hurry to, to get a move on
sorridere a		to smile at
telefonare a		to telephone
tornare a		to return to
uscire a		to go out to
venire a		to come to
amare		to love, to like
desiderare		to wish, to want, to desire
dovere		to have to
fare		to do, to make
lasciare		to leave, to let, to allow
piacere (mi piace, mi piacciono)		to please, to like (I like)
potere		to be able
preferire		to prefer
sapere		to know (a fact)
volere		to wa