les dessins animés		cartoons
une émission		a programme
un jeu télévisé		a games show
une série		a series
ça m'est égal		I don't mind
ça ne me dit rien		I'm not very keen
je n'ai pas tellement envie		I don't really want to
une séance		a showing (of a film)
un spectacle		a show
un pièce		a play
drôle		funny
èmouvant		moving
lent		slow
agréable		pleasant
formidable		tremendous
marrant		funny
passionnant		exciting
l'histoire		story, tale
l'ambiance		atmosphere
le but		goal
le concours		contest/competition
la course		race
l'équipe		team
la vainqueur		winner
contre		against
tchater		to chat (online)
télécharger		to download
la patinoire		ice-skating rink
l'église		church
à gauche		left
à droite 		right (direction)
trouver		to find
le syndicat d'initiative		tourrist information
arrêter		to stop
feux rouges		traffic lights
carrefour		crossroads
derrière		behind
entre		between
devant		in front of
à côté de		next to
au coin		on the corner
au bout de		at the end of
immeuble		a block of flats
la banlieue		suburbs, inner city
un lotissement		an estate
pittoresque		picturesque
la circulation		traffic
la pollution		pollution
les gaz d'échappement		exhaust fumes
le poids lourd		lorry
le périphérique		ring road
épouvantable		appaling, dreadful
inondations		floods
des commerces		shops
souterrain		underground
le piéton		pedestrian
le lait		milk
le jambon		ham
la pomme de terre		potato
la tranche		slice
la bouteille		bottle
le paquet		packet
le pot		jar, pot, tub
le saucisson		salami
la moutarde		mustard
les framboises		raspberries
le haut		top
le jogging		tacksuit
le pantalon		trousers
le pull		jumper, sweater
la chemise		shirt
la robe		dress
les chaussures		shoes
les baskets		trainers
acheter		to buy
dormir		to sleep
porter		to wear/carry
court		short
large		baggy/wide
démodé		old fashioned
serré		tight
le chapeau		hat
le imper(méable)		raincoat
le maillot de bain		swimming costume
la casquette		cap
le cuir		leather
la laine		wool
échanger		to exchange
la mode		fashion
les vêtements de marque		designer clothes
cher		expensive
être à l'aise		to be at ease
la matière		subject (at school)
allemand		German
la chimie		chemistry
la physique		physics
la géographie		geography
la leçon		lesson
difficile		difficult
fort		strong
faible		weak, frail
la chemise		shirt
la cravate		tie
la jupe		skirt
les collants		tights
la veste		jacket
repassage		ironing
se réveiller		to wake up, to awaken
se doucher		shower
se coucher		to go to bed
se habiller		to dress
l'avertissement		warning
la convocation		meeting
la retenue		detention (punishment)
la licence		degree
le apprentissage		apprenticeship 
bénévole		voluntary
la entreprise		business
l'étude		study
recevoir		to receive
donner		to give
par		per
faire la vaiselle		to do the washing up
garder		to look after
mettre la table		to lay the table
passer l'aspirateur		to vacuum
promener		to take something for a walk
ranger		to tidy
elle m'enerve		she gets on my nerves
l'argent		money
l'argent de poche		pocket money
généreux		generous
injuste		unfair
économiser		to save (up for)
livrer		to deliver
le journal		newspaper
gagner		to earn
la caisse		till
le policier		police officer
le boulanger		baker
le caissier		cashier
le facteur		postman, mailman
le patron		boss
le salaire		salary, wages, wage
les gens		people
monotone		monotonous
gratifiant		rewarding
sale		dirty
propre		clean
libre		free (time not money)
le pourboire		tip, gratuity
maîtrise		fluency
étranger		foreign
la publicité		advertising
la blague		joke
le stage		work experience
l'usine		factory
apprendre		to learn
une perte de temps		waste of time
mécanique		mechanical
l'outil		tool
le fichier		file
classer		to classify
les rendez-vous		appointments
le client		customer
la surveillance		surveillance
le camping		campsite
le gîte		holiday home
la tente		tent
l'auberge de jeunesse		youth hostel
l'hôtel		hotel
brouillard		foggy
nuages		cloudy
l'orage		storm, thunderstorm
le vent		wind
la pluie		rain
la neige		snow
le printemps		spring (the season)
l'hiver		winter
planche à voile		windsurfing
plaindre		to pity, to complain
le bruit		noise
le remboursement		refund
la station balnéaire		seaside resort
revendiquer		to claim
s'amuser		to enjoy oneself
le sentier pédestres		foot path
la piste cyclables		cycle track
la allée cavaliéres		bridle path
le repas		meal
la viande		meat
des oignons		onions
le gâteau		cake, biscuit
le bras		arm
le cou		neck
le dos		back
le ventre		stomach
la gorge		throat
la langue		tongue
la main		hand
les jambes		legs
casser		to break
piquer		to sting
une grippe		flu
la fièvre		fever
tousser		to cough
le besoin		need
prendre		to take
des comprimés		pills
un pansement		bandage
ciseaux		sissors
reposer		to rest
l'ordonnance		prescription
le rhume		cold
les rendez-vous		appointments
sain		healthy
également		equally
lentement		slowly
la cigarette		cigarette
dégoûtant		disgusting
la détente		relaxation
gaspillage		waste
la dépendance		addiction
le réchauffement de la terre		global warming
la faim		hunger
la guerre		war
la pauvreté		poverty
L'Inde		India
les pays en voie de développement		Developing counties 
les produits		Products
Les produits issus du commerce équitable		Fair trade products
la famine		famine
La paix		Peace
combattre		to fight
l'événement		event
parrainer		to sponsor 
respirer		to breathe
la poubelle		bin, trash can
jeter		to throw
lancer		to throw
Déchetes		rubbish, trash
le chômage		unemployment
le chômeur		unemployed person
Camion		Lorry
feux rouges		traffic lights
l'embouteillage		traffic jam
le bouchon		traffic jam
éteindre		to put out (of a fire)
baisser		to lower
Chauffage central		Central heating
gaspiller		to waste
le robinet		the tap 
emballages		packaging
détruire		to destroy
empoisonner		to poison
le carton		cardboard box, box
le frigo		Fridge
toile		Cloth
le verre		glass
la boîte		box
se baigner		to have a bath
partager		to share
le feu		fire
l'ouragan		hurricane 
la fuite		Leak
Sécheresse		Drought
l'arbre		tree
Je me présente		let me introduce myself
nous n'avons pas d'animal		we don't have a pet
faire du vélo		to ride a bike
jouer aux éches		to play chess
jouer de la batterie		to play the drums
j'en fais une fois par semaine		I do it once per week
souvent		often
une soeur cadette		a younger sister 
une soeur aînée		an older sister
célibataire		single
elle m'enerve		she annoys me
comptable		accountant
kinésithérapeute		physio therapist
maçon		builder
menuiser		carpenter
nourrice		child minder
sapeur-pompier		fireman
vendeur/euse		sales person
eux		them
lui		him
branchée		switched on/trendy
décontracté(e)		laid-back
insupportable		unbearable
travailleur/euse		hardworking
commencer		to begin
gagner		to win
devenir		to become
un tournoi		a tournament
un emploi		a job
travailler		to work
les grandes vacances 		summer holiday
jamais		never
seul		alone, on one's own
louer		to rent
maintenant		now
la lecture		reading
le chapeau		hat
les baskets		trainers
le savon		soap
en face de		opposite
le mari		husband
toujours		always
avant		before, beforehand
le tarif		cost
la règle		rule
la confiance		confidence
la moitié		half
fiable		secure
la livraison		delivery
avant		before, beforehand
ranger		to tidy
propre		clean
réussir		to succeed
attirer		to attract
sans		without
le roman		novel
interdire		to ban
comprendre		to understa