attaquer		to attack
attraper		to catch
accepter		to accept
battre		to beat
blanchir		to bleach  (not d...)
boire		to drink
cacher		to hide
comparer		to compare
coûter		to cost
cuire		to cook
détester		to hate  (not h...)
développer		to develop
devenir		to become
dormir		to sleep
s'enfuir		to run away
enseigner		to teach  (not i...)
être		to be
exclure		to exclude
faiblir		to weaken
forcer		to force  (not o...)
geler		to freeze
manger		to eat
obéir		to obey
perdre		to lose
sembler		to seem
accuser reception		to acknowledge receipt of
amuser		to amuse
apparaître		to appear  (not p...)
attirer		to attract
augmenter		to increase
blâmer		to blame
briser		to break  (not c, r...)
chanter		to sing
concourir		to compete
consommer		to consume
continuer		to continue
contrevenir		to contravene
corriger		to correct
découvrir		to discover
défricher		to lay groundwork
dépeindre		to depict
détruire		to destroy
douter		to doubt
échapper		to escape
écrire		to write
enclore		to enclose
entendre		to hear
entrouvrir		to half-open
essuyer		to wipe
éviter		to avoid
ajouter		to add
apporter		to bring (something)
arranger		to arrange
accueillir		to accommodate; to meet; to welcome
bâtir		to build  (not c...)
bénir		to bless
blaguer		to joke  (not p...)
brosser		to brush
changer		to change
conjoindre		to unite
coucher		to put to bed
culpabiliser		to make one feel guilty
décourager		to discourage
commettre		to commit
se dédire		to retract
défaire		to undo
démolir		to demolish
diminuer		to diminish
écraser		to crush
effacer		to erase
forclore		to foreclose
fuir		to flee, to escape, to avoid
goûter		to taste
grimper		to climb
guérir		to cure
accompagner		to accompany
s'accuser		to confess
admettre		to admit
agacer		to annoy
arriver		to arrive
assaillir		to assail
bouillir		to boil
casser		to break (not b..., not r....)
chercher		to look for
coller		to stick
considérer		to consider
cueillir		to gather
défendre		to defend
désobéir		to disobey
effrayer		to frighten
énoncer		to state
s'enquérir		to inquire
espérer		to hope
établir		to establish
excuser		to excuse
expliquer		to explain
frémir		to shudder
gêner		to bother
habiter		to live in
indiquer		to indicate  (not s...)
abolir		to abolish
s'abstenir		to abstain
accuser		to accuse
adresser		to address
admirer		to admire
advenir		to happen
aventurer		to venture
cheminer		to walk on
clore		(put an end to) to close (debate, ballot, account)
contrôler		to control
couper		to cut
découdre		to unpick
dénoncer		to denounce
déranger		to disturb
discuter		to discuss
disjoindre		to disconnect
emprunter		to borrow
encourager		to encourage
enregistrer		to record
entreprendre		to undertake
épreindre		to juice
fermer		to close
frire		to fry
hésiter		to hesitate
absoudre		to absolve
acclamer		to cheer, to acclaim
appeler		to call  (not n...)
brûler		to burn
circonvenir		to circumvent
comprendre		to understand
contredire		to contradict
correspondre		to correspond
crier		to shout
déduire		to deduce
défaillir		to faint
dépasser		to exceed
dessiner		to draw
deviner		to guess
dire		to say
discourir		to speak at length
élever		to raise (e...)
élire		to elect
éloigner		to move away
envoler		to take flight
estimer		to estimate
étendre		to stretch
finir		to finish  (not t...)
garder		to keep
habiller		to dress
acquérir		to acquire
agir		to act
adjoindre		to appoint
aider		to help  (not s...)
apprendre par coeur		to memorize
bêler		to bleat
adorer		to adore
bouger		to move
conquérir		to conquer
choisir		to choose
consentir		to consent
confire		to preserve
décevoir		to disappoint
dépendre		to depend
dépenser		to spend
dissoudre		to dissolve
feuilleter		to leaf through
enfreindre		to infringe
enlever		to remove
essayer		to try
étreindre		to embrace
exprimer		to express
fonder		to found
gâter		to spoil
imposer		to impose
abasourdir		to daze, to bewilder
aller		to go
annoncer		to announce
armer		to arm
avancer		to advance
ceindre		to put on, gird, encircle (part of body)
compléter		to complete  (not a...)
célébrer		to celebrate
conclure		to conclude
créer		to create
déconfire		to destroy; annihilate (archaic)
déteindre		to bleach  (not b...)
émoudre		to sharpen
encourir		to incur
enjoindre		to enjoin
enterrer		to bury
étonner		to astonish
envoyer		to send
exister		to exist
féliciter		to congratulate
fumer		to smoke
geindre		to groan
gérer		to manage
inclure		to include
inquiéter		to worry
acheter		to buy
apprendre		to learn
approcher		to approach
assurer		to assure
avoir		to have
balayer		to sweep
circonscrire		to contain, delimit, circumscribe
commander		to order, to command, to commission
convaincre		to convince  (not p...)
corrompre		to corrupt
courir		to run
croire		to believe
départir		to accord
déplaire		to displease
dévêtir		to undress
disparaître		to disappear
donner		to give
éconduire		to dismiss, turn away so.
enduire		to coat
entrer		to enter
épouser		to marry, to espouse, to embrace
étudier		to study
exagérer		to exaggerate
grandir		to grow
haïr		to hate  (not d...)
appartenir		to belong
cesser		to cease
charger		to load
circoncire		to circumcise
concevoir		to conceive
couvrir		to cover
craindre		to fear
danser		to dance
baisser		to lower
décider		to decide
déclore		to reopen (archaic)
décrire		to describe
déménager		to move, to empty of its furniture
devoir		to have to  (not f...)
échouer		to fail
divorcer		to divorce
évaluer		to evaluate
explorer		to explore
fondre		to melt
informer		to inform
insister		to insist
interdire		to forbid
intéresser		to interest
interrompre		to interrupt
intervenir		to intervene
inventer		to invent
inviter		to invite
introduire		to introduce
jaillir		to gush, to squirt
jalonner		to line, to mark
jalouser		to be jealous of
jardiner		to garden
jaser		to gossip
jaunir		to turn yellow
jeter		to throw  (not l...)
se joindre à		to join (a person, a group)
jouer		to play
jouir		to enjoy
juger		to judge
kidnapper		to kidnap
klaxonner		to honk
lécher		to lick
laisser		to leave  (not p...)
lancer		to throw  (not j...)
laver		to wash
lever les sceaux		to declassify
lever un malentendu		to clear up a misunderstanding
lire		to read
louer		to rent
luire		to shine  (not b...)
maintenir		to maintain  (not e...)
manquer		to miss  (not r...)
maudire		to curse
méconnaître		to be unaware of
médire		to malign
mélanger		to mix
menacer		to threaten
mener		to lead  (not d...)
mentir		to lie
méprendre		to mistake
mesurer		to measure
montrer		to show
moquer		to mock
moudre		to mill, to grind
mourir		to die
naître		to be born
naviguer		to navigate
négliger		to neglect
négocier		to negotiate
neiger		to snow
nettoyer		to clean (housework)
neutraliser		to neutralize
nier		to deny
nourrir		to feed
noyer		to drown
epérir		to perish
occlure		to occlude
occuper		to occupy
offrir		to offer
oindre		to anoint
oser		to dare
oublier		to forget
ouvrir		to open
pardonner		to forgive
partager		to share
participer		to participate
partir		to leave  (not l...)
parvenir		to reach (not a...)
passer pour		to pass for, to impersonate
patiner		to skate
payer		to pay
pécher		to sin
pêcher		to fish
peindre		to paint
peler		to peel
pendre		to hang, to dangle
penser		to think
peser		to weigh
percevoir		to perceive
plaindre		to pity
plaire		to please
plaisanter		to joke  (not b...)
planter		to plant
se planter		to screw up, to crash
pleurer		to mourn, to cry, to weep
pleuvoir		to rain
plonger		to dive
poindre		to dawn, to appear
pondre		to lay (an egg)
poursuivre		to pursue
pousser		to push
pouvoir		to be able to
prédire		to predict
préférer		to prefer
prendre		to take
presser		to squeeze
prévaloir		to prevail
prévoir		to foresee
prier		to pray
produire		to produce
promettre		to promise
prononcer		to pronounce
proscrire		to prohibit
protéger		to protect
prouver		to prove
avoir recours à		to resort to
puer		to stink
punir		to punish
quérir		to summon so. [ seul. apres: aller, venir, envoyer, faire]
quitter		to leave (depart)
ralentir		to slow down
rassurer		to reassure
rater		to miss  (not m...)
ravir		to delight
recevoir		to receive
reconduire		to renew, to extend, to see (sb) out/home, to walk/drive (sb), to escort
reconnaître		to recognize
reconstruire		to rebuild
recoudre		to sew back on
récrire		to rewrite
redire		to repeat
réduire		to reduce
réélire		to re-elect
refaire		to redo
refléter		to reflect
refuser		to refuse
regretter		to regret
rejeter		to reject
relire		to reread
remarquer		to notice  (not c...)
remercier		to thank
se remettre		to get well
remettre au lendemain		to procrastinate
remoudre		to regrind
remplacer		to replace
remplir		to fill
renouveler		to renew
reparaître		to reappear
réparer		to repair
repeindre		to repaint
se repentir		to repent
répéter		to repeat
répondre		to answer
reposer		to rest
réprimander		to reprimand
ressentir		to feel  (not e, s...)
restreindre		to restrict
reteindre		to dye again
retenir		to retain
retourner		to return
réussir		to succeed
réveiller		to wake up
révéler		to reveal
revenir		to come back
rêver		to dream
revoir		to see again
riposter		to retort
rire		to laugh
rompre		to break  (not b, c...)
rouvrir		to reopen
saillir		to protrude
sauter		to jump
satisfaire		to satisfy
savoir		to know  (not c...)
secouer		to shake
secourir		to help  (not a...)
séduire		to seduce
séjourner		to sojourn
séparer		to separate
servir		to serve
signer		to sign
signifier		to mean
skier		to ski
songer		to daydream, to think, to dream
sonner		to ring
souffrir		to suffer
soumettre		to submit
sourire		to smile
souscrire		to subscribe
se souvenir		to remember
subir		to undergo
subvenir		to provide for
sucer		to suck
suffire		to suffice
suggérer		to suggest
suivre		to follow
supplier		to beg
supposer		to suppose
surprendre		to surprise
survenir		to occur
survivre		to survive
survoler		to fly over
tapoter		to tap
teindre		to dye
tenir		to hold
tendre la main		to reach out
tirer		to pull
tomber		to fall
tondre		to shear, to mow
tonner		to thunder
toquer		to knock
tordre		to twist
tourner		to turn
tracer		to draw
traduire		to translate
trahir		to betray
transcrire		to transcribe
transparaître		to show through
travailler		to work
traverser		to cross
trouver		to find
tuer		to kill
tutoyer		to use "tu" with
unifier		to unify
unir		to unite, to combine
user		to wear out
utiliser		to use
vaincre		to defeat
valoir		to be worth
vanter		to praise
varier		to vary
vendre		to sell
venger		to avenge
venir		to come
verdir		to turn green
verser		to pour
vêtir		to clothe
viser		to aim at, to aim for
visiter		to visit
vivre		to live
aimer		to like
causer		to cause
équivaloir		to be equivalent
éteindre		to put out
faire		to do
nuire à		to harm, to injure, to be detrimental to
parcourir		to travel all over
paraître		to appear  (not a...)
proposer		to propose
poser		to put
posséder		to own
renvoyer		to send back, to return (a thing)
ranger		to tidy up
rédiger		to write out, to copy out
rentrer		to go back, to be part of, to return
souffler		to blow
souffler fort		to pant
sortir		to go out
voiler		to veil
voir		to see
voter		to vote
vouloir		to want
voyager		to travel
zézayer		to lisp
zoner		to zone
aménager		to do up (the house)
arrêter		to stop
attendre		to wait
conseiller		to advise
détenir		to hold, to detain
employer		to employ
fouiller		to search
induire		to induce
lever		to raise
se méfier		to distrust
parler		to speak
plier		to fold
persuader		to convince  (not c...)
promener		to go for a walk
recueillir		to collect
reproduire		to reproduce
rendre		to return
répandre		to spread, to scatter, to spill
répérer		to spot
ressembler		to look like
rester		to stay
supporter		to support
revendiquer		to claim, to demand
téléphoner		to telephone
voler		to steal
abattre		to slaughter
achever		to complete  (not c...)
amener		to bring (someone)
balancer		to swing
céder		to yield
connaître		to know  (not s...)
contraindre		to constrain, to compel
déchirer		to tear
dégager		to free
écouter		to listen to
frapper		to hit
interroger		to question
loger		to house
mordre		to bite
noter		to note
obliger		to force  (not f...)
obtenir		to get
passer		to pass
placer		to place
prescrire		to prescribe
projeter		to plan
regarder		to look
repasser		to come by again, to iron
résoudre		to solve
saisir		to seize  (not s'...)
s'asseoir		to sit
atteindre		to reach (not p...)
chasser		to hunt
combattre		to fight
confondre		to confuse; to mix up
débattre		to debate
descendre		to go down
emmener		to take along
entraîner		to lead to, to bring about, to entail
entretenir		to maintain (not m...)
gagner		to win
jurer		to swear
mettre		to put
modérer		to restrain
lâcher		to let go
omettre		to leave out
présenter		to present
rejoindre		to meet (up with), to join, to catch up with
renoncer		to give up
revêtir		to assume
serrer		to tighten
terminer		to finish  (not f...)
transmettre		to forward
vérifier		to check
vieillir		to grow old
abaisser		to pull down
accrocher		to hang up
apercevoir		to catch sight of, to notice
approuver		to approve
construire		to build  (not b...)
demeurer		to remain
différer		to differ
désirer		to desire
empêcher		to prevent
exiger		to require
embrasser		to kiss
fixer		to fix
ignorer		to ignore
nouer		to knot
porter		to carry
prétendre		to claim, to pretend
prêter		to lend
recouvrir		to recover
reprendre		to resume
sauver		to rescue
signaler		to indicate  (not i...)
soutenir		to sustain
tenter		to tempt
traiter		to treat
tromper		to deceive, to cheat on
affaiblir		to weaken
animer		to bring life to
blesser		to hurt
comparaître		to appear (in court)
compter		to count
convenir		to agree
contenir		to contain
diriger		to lead  (not m...)
engager		to hire
ennuyer		to bore
éprouver		to feel  (not r, s...)
fournir		to provide
instruire		to teach (not e...)
mépriser		to despise
mériter		to deserve
nommer		to call  (not a...)
pénétrer		to penetrate
permettre		to allow
provenir		to be from
raconter		to tell
réfléchir		to reflect [thoughts]
remettre		to put back, to lay, to put
rouler		to roll
sentir		to feel  (not e, r...)
toucher		to touch
arracher		to pull out, to uproot
assister		to attend
bavarder		to chat
commencer		to begin
conduire		to drive
conserver		to keep  (not g...)
demander		to ask
se dépêcher		to hurry up
détourner		to redirect, to divert
dîner		to dine
doubler		to overtake
faillir		to fail
falloir		to have to  (not d...)
s'ensuivre		to ensue
gaspiller		to waste
gronder		to scold
marcher		to walk
monter		to go up
nager		to swim
prévenir		to prevent
se rappeler		to recall
rencontrer		to meet
souhaiter		to wish
tendre		to extend
tricher		to cheat
accorder		to grant
avoir besoin de		to need
avoir raison		to be right
broyer		to crush
chuchoter		to whisper
conter		to tell
coudre		to sew
déjeuner		to lunch
éclore		hatch, take form, bloom
égayer		to enliven, cheer up, amuse
empreindre		to imprint
épeler		to spell
se fâcher		to get angry
fendre		to split
gratter		to scratch
grêler		to hail
peigner		to comb
pianoter		to tinkle (piano), tap in (number, pin)
susciter		to arouse, to spark off, to give rise to
salir		to soil  (not so...)
siffler		to whistle
souiller		to soil  (not sa...)
se tromper		to make a mistake
tousser		to cough
veiller		to stay up at night
accourir		to run up, arrive on the spot
astreindre		to compel
briller		to shine  (not l...)
constater		to notice  (not r...)
s'emparer		to seize  (not sa...)
s'endormir		to fall asleep
épouser		to marry
étourdir		to stun, to daze
feindre		to feign
se fier à		to trust in
hoqueter		to hiccup
se lever		to get up
maigrir		to lose weight
mijoter		to simmer (cooking)
mouiller		to get something wet
noircir		to blacken
pressentir		to foresee
remporter		to win  (not g...)
rougir		to blush; to turn red
succomber		to succumb
tressaillir		to twitch, start, quiver
vouvoyer		to use "vous" with
zébrer		to stri