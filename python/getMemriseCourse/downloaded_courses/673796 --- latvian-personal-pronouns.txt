nominative 		subject of sentence
genitive		possessor 
dative		indirect object of sentence
accusative		direct object of sentence
locative		in, on
es		I (nominative)
tu		you (nominative singular informal)
viņš		he (nominative)
viņa		she (nominative)
mēs		we (nominative)
jūs		you (nominative singular formal/plural)
viņi		they (nominative masculine)
viņas		they (nominative feminine)
manis 		my
tevis		your (singular informal)
viņa		his
viņas		her (genitive)
viņu		their (gender-neutral)
mūsu		our
jūsu		your (singular formal/plural)
man		I/me (dative)
tev		you ( dative singular informal)
viņam		he (dative)
mums		we (dative)
viņai		she (dative)
jums 		you (dative singular formal/plural)
viņiem		their (dative masculine)
viņām		their (dative feminine)
mani		me
tevi		you (accusative singular informal)
viņu 		him/her (accusative)
mūs		us (accusative)
jūs 		you (accusative singular formal/plural)
viņus		them (accusative masculine)
viņas		them (accusative feminine)
manī		me (locative)
tevī		you (locative singular informal)
viņā		him/her (locative)
mūsos		us (locative)
jūsos		you ( locative singular formal/plural)
viņos		them (locative masculine)
viņās		them (locative feminine)
nominative		Es salstu. (I feel cold)
genitive		Jūsu velosipēds ir pazudis. (Your bike is missing)
dative		Viņa nes tev velosipēdu. (She brought [to] you a bike)
accusative		Aktieris redzēja mūs! (The actor saw us)
locative		Gribu viņās mans velosipēdu. ([I] want in them my bik