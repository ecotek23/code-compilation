les courses		grocery shopping
le shopping		clothes shopping
la couleur		colour
rose		pink
le manteau		coat
indispensable		essential, indispensable, required (not essentiel)
s'habiller		to dress
le cuir		leather
la veste		jacket
la ceinture		belt
dès		from, since the
l'état		state
la santé		health
mourir		to die
la peur		fear, apprehension, fright
le médicament		medicine, drug
la roulée		rolled cigarette
le filtre		filter
le cor		horn (instrument)
la musique classique		classical music
la nature		nature
le jardin		garden
la pluie		rain
la neige		snow
le nuage		cloud
le ciel		sky
la lune		moon
la terre		earth
la feuille		leaf
la fleur		flower
le cheval		horse
le serpent		snake
le singe		monkey
l'éléphant		elephant
la baleine		whale
secourir		to help
agir		to act
accepter		to accept
la cause		cause
offrir		to offer
mieux		better
atteindre		to reach
protéger		to protect
fournir		to provide
la religion		religion
l'égard		consideration
soutenir		to sustain
s'occuper de		to look after
modeste		modest
l'humanité		humanity
la vertu		virtue
idéal		ideal
remercier		to thank
religieux		religious
l'appui		support
l'honneur		honour
mériter		to deserve
contribuer		to contribute
prêter		to lend
la vérité		truth
honorable		honorable
espérer		to hope
sympa		nice
la théologie		theology
le croyant		believer
le dieu		god
l'aveu		admission (as in confession not as in entrance)
l'aveu		confession
la pitié		pity
l'ange		angel
reconnaissant		grateful
la croyance		belief
la peur		the fear
le prêtre		priest
la foi		faith
la vierge		virgin
la morale		ethics
l'église		church
prier		to pray
pur		pure
consacrer		to devote
le saint		saint
moral		moral
assister		to assist
la bonne action		good deed
l'abstinence		abstinence
abandonner		to give up (not renoncer)
le conflit		conflict
maltraiter		to mistreat
négatif		negative
le meurtre		murder
exclure		to exclude
commettre		to commit
attaquer		to attack
condamner		to condemn
la criminalité		crime
pire		worse
l'arme		weapon
la violence		violence
priver		to deprive
tuer		to kill
violer		to rape
le mensonge		lie
le voleur		thief
le délit		offence (as in crime)
méchant		mean
la punition		punishment
insulter		to insult
abuser		to abuse
la culpabilité		guilt
trahir		to betray
infliger		to inflict
le meurtrier		murderer
négliger		to neglect
le vice		fault, vice
le sort		fate
creuser		to dig
le comportement		behavior
tenter		to tempt
le vol		theft
le purgatoire		purgatory
la contraception		contraception
le péché		sin
l'orgueil		pride
la luxure		lust
l'avidité		greed
le désir		desire
la paresse		sloth
la colère		anger
l'art		art
l'artiste		artist
montrer		to show
l'effet		effect
le projet		project
l'idée		idea
le sens		sense
le public		public
essayer		to try
le détail		detail
l'oeuvre		work, piece
créer		to create
la ligne		line
le sujet		subject
le titre		title
valoir		to be worth
exprimer		to express
signifier		to mean
la création		creation
le matériel		material, equipment
quelqu'un		somebody
commercial		commercial
la culture		culture
le modèle		model
la peinture		painting (not tableau)
le dessin		drawing
le tableau		painting (not peinture)
magnifique		magnificent
profond		deep
horrible		horrible
affreux		dreadful
le portrait		portrait
le paysage		scenery
disposer		to arrange (not arranger)
simplement		simply
illustrer		to illustrate
publier		to publish
figurer		to appear (intr) / to represent, to show, to depict (tran) / to be an extra, to do a walk-on part
excellent		excellent
l'ouvrage		work, piece of work
critiquer		to criticise
moderne		modern
décrire		to describe
le symbole		symbol
la représentation		representation
l'impression		impression
culturel		cultural
le maître		master
poser		to pose
personnel		personal
la technique		technique
la critique		criticism
la matière		matter, material
la sculpture		sculpture
le chef d'oeuvre		masterpiece
la nature morte		still life
abstrait		abstract
le nu		nude
lui		him
le jeu		game, play
injuste		unfair
l'âge		age
conter		to tell
malade		ill
pleurer		to mourn, to cry, to weep
l'anniversaire		birthday
innocent		innocent
la croissance		growth
obéir		to obey
dessiner		to draw
hausser		to raise
l'énergie		energy
l'autorité		authority
précoce		precocious
joyeux		joyful
le jouet		toy
inventer		to invent
le conte		tale
chuter		to fall
le gamin		kid
vif		lively
réclamer		to ask for
frapper		to hit
interrompre		to interrupt
le langage		language
traduire		to translate
italien		Italian
le dialogue		dialogue
indien		Indian
espagnol		Spanish
la définition		definition
suggérer		to suggest
encourager		to encourage
informer		to inform
le geste		gesture
l'avis		opinion
l'intention		intention
l'échange		exchange
confier		to entrust
directement		directly
conclure		to conclude
insister		to insist
définir		to define
exposer		to display
confirmer		to confirm
la communication		communication
réagir		to react
prétendre		to claim, to pretend
la discussion		discussion
la promesse		promise
russe		Russian
provoquer		to provoke
sourire		to smile
l'aventure		adventure
le récit		story
vachement		really, bloody
ta bouche bébé		shut your trap
gronder		to scold
il était une fois		once upon a time
et ils vécurent heureux et eûrent beaucoup d'enfants		and they lived happily ever after
le dessin animé		cartoon
mien		mine
le mioche		brat
le morveux		snotty kid
le nounours		teddy bear
voir le jour		to be born
la vie		life
l'existence		existence
le début		beginning
le présent		present
le passé		past
exister		to exist
révéler		to reveal
gâcher		to waste
le monde		world
le cours		course
la situation		situation
humain		human
l'époque		time, era
la réalité		reality
la paix		peace
l'erreur		mistake
la conséquence		consequence
véritable		genuine
la provenance		origin
la scène		scene
le malheur		unhappiness
le bonheur		happiness
général		general
l'allégresse		joy
rêver		to dream
la fin		end
la vie après la mort		afterlife
interminable		endless
l'accent		accent
interpréter		to interpret
convaincre		to convince
l'instruction		instruction
résumer		to summarise
communiquer		to communicate
britannique		British
la conversation		conversation
la confusion		confusion
mentionner		to mention
l'explication		explanation
le contexte		context
percevoir		to perceive
approuver		to approve
transmettre		to forward
la procédure		procedure
remarquer		to remark
conseiller		to advise
la qualité		quality
le défaut		flaw
capable		able
riche		rich
pauvre		poor
le gène		gene
plusieurs		several
compliqué		complicated
simple		simple
considérer		to consider
aboutir		to succeed
la profession		profession
l'héritage		heritage
la fortune		fortune
la beauté		beauty
le doute		doubt
la grossesse		pregnancy
le succès		success
intelligent		intelligent
fou		crazy
la nationalité		nationality
belge		Belgian
canadien		Canadian
allemand		German
l'âme		soul
l'aptitude		ability
le démocrate		democrat
le républicain		republican
le conservateur		conservative
le travailliste		labourite
célèbre		famous
juif		jewish
catholique		Catholic
le réformé		Protestant
musulman		Muslim
chrétien		Christian
bouddhiste		Buddhist
optimiste		optimistic
pessimiste		pessimistic
la passion		passion
la compétence		competence
le bilan		outcome, balance sheet, assessment
alcoolique		alcoholic
soit... soit		either... or
l'embouteillage		traffic jam
la corne		horn
emboutir		to ran into
l'accident		accident
la vitesse		speed
le témoin		witness
la voie		lane
crier		to shout
la frustration		frustration
l'amende		fine (monetary)
la curiosité		curiosity
le chauffeur		driver
agressif		aggressive
freiner		to brake
accélérer		to speed up
le troupeau		herd
signaler		to indicate
le plan		plan; map
déplacer		(voluntarily) to move (object, person.)
le véhicule		vehicle
véhiculer		to transport
la manoeuvre		manoeuvre
reculer		to move back
bouger		to move
la foule		crowd
demeurer		to remain
l'essence		gas, petrol
le moteur		motor
le mètre		meter
le kilomètre		kilometer
éloigner		to move away
la priorité		priority
la circulation		traffic
le feu		traffic light
l'assurance		insurance
le motard		biker
le virage		bend
je vais te casser la gueule		i'm gonna beat you up
le rond-point		roundabout
garer		to park
la contravention		small infraction
la bagarre		brawl
les travaux routiers		road works
le badaud		gawker
le piéton		pedestrian
la place du mort		shotgun seat
la banquette arrière		back seat
le mauvais sens		wrong way
l'heure de pointe		rush hour
le mauvais côté de la route		the wrong side of the road
rebrousser chemin		to go back the way you came from
faire demi-tour		to turn around
la boite aux lettres		postbox
le courrier		mail
la lettre		letter
envoyer		to send
poster		to post
le mot		word
la phrase		sentence
le timbre		stamp
s'adresser à		to address
combler		to fill
le texte		text
charger		to load
noter		to note
la livraison		delivery
l'écriture		writing
l'appel		call
la signature		signature
le trajet		journey
la revue		magazine
la publicité		advertising
la facture		invoice
hebdomadaire		weekly
l'article		article
l'adresse		address
le message		message
la boîte		box
le carton		cardboard
le facteur		postman
emballer		to wrap, to pack
la carte postale		postcard
le télégramme		telegram
le colis		parcel
le tract		leaflet
la brochure		booklet
l'opuscule religieux		religious pamphlet
bosser		to work (hard), to slog (away)
le boulot		work
le boulot		job (casual)
le métier		trade, occupation, job
la besogne		task
la commission		commission
la fonction		function
la carrière		career
professionnel		professional
le chômage		unemployment
le congé		holiday, leave
la pause		break
le fonctionnaire		state employee
le secrétaire		secretary
responsable		responsible
le directeur		director
confrère		colleague
le mois		month
l'an		year
le bureau		office
l'ordinateur		computer
le contrat		contract
la rencontre		meeting
le budget		budget
le client		client
le document		document
la formation		training
engager		to hire
former		to form
employer		to employ
la gestion		management
la conférence		conference
l'association		association
l'entreprise		enterprise
l'organisation		organization
la firme		firm
finir		to finish
le terme		term
l'emploi		employment
le dossier		file
l'entretien		interview
reprendre		to resume
le treizième mois		end-of-year bonus
la sieste		nap
les ragots		gossip
bavarder		to chat
la grève		strike
la politique		politics
la république		republic
la constitution		constitution
la présidence		presidency
le ministre		minister
le chef		leader
le maire		mayor
le gouvernement		government
le parti		party (political)
le débat		debate
le discours		speech
la liaison		liaison
la pute		prostitute
décider		to decide
concerner		to concern
représenter		to represent
proposer		to propose
annoncer		to announce
pressentir		to foresee
diriger		to lead
nier		to deny
local		local
global		global
national		national
international		international
mondial		worldwide
universel		universal
extraterrestre		extraterrestrial
la société		society
la communauté		community
la nation		nation
voter		to vote
élire		to elect
l'élection		election
la décision		decision
l'opposition		opposition
la majorité		majority
le changement		change
la réforme		reform
la consultation		consultation
la crise		crisis
protester		to protest
démontrer		to demonstrate
l'émeute		riot
le syndicat		union
le principe		principle
la liberté		liberty
l'égalité		equality
la fraternité		fraternity
la laïcité		secularism
libéral		liberal
le salaud		bastard
corrompre		to corrupt
la puissance		power
assurer		to assure
la confiance		trust
le conseil		advice
le vendu		bribed person
la justice		justice
précis		precise
la preuve		proof
la date		date
le procès		trial
coupable		guilty
obliger		to force
accorder		to grant
avouer		to admit
affirmer		to affirm
soumettre		to submit
le processus		process
la responsabilité		responsibility
l'importance		importance
exiger		to require
contester		to contest
exécuter		to execute
l'argument		argument
judiciaire		judiciary
cacher		to hide
s'expliquer sur		to justify
l'enquête		investigation
la police		police
appliquer		to apply
résoudre		to solve
interroger		to question
la conscience		conscience
accuser		to accuse
témoigner		to testify
le commissaire		superintendent
juridique		legal
l'hypothèse		hypothesis
le tort		wrong
le prisonnier		prisoner
l'avocat		lawyer
dégager		to free
le juge		judge
l'évidence		evidence
dénoncer		to denounce
la circonstance		circumstance
signer		to sign
la source		source
officiel		official
le tribunal		court (of law)
le motif		motive
l'individu		individual
l'autorisation		authorization
la libération		liberation
identifier		to identify
la reconnaissance		gratitude
limiter		to limit
sévère		severe
la surveillance		surveillance
le gardien		guardian
l'indice		clue
la conviction		conviction
constater		to notice
l'intervention		intervention
le policier		police officer
impliquer		to implicate
le régime		diet
le poids		weight
lourd		heavy
léger		light, not heavy
la balance		scales
des		some
la mesure		measure
le produit		product
autant		as much
le progrès		progress
la taille		size
gras		greasy
réduire		to reduce
augmenter		to increase
la calorie		calorie
large		wide
viser		to aim at, to aim for
demi		half
peser		to weigh
plein		full
contrôler		to control
l'objectif		objective
la laitue		lettuce
le bonbon		sweet
obèse		obese
le maillot de bain		swimsuit
vomir		to throw up
la courgette		courgette, zucchini
l'asperge		asparagus
l'aubergine		aubergine, eggplant
le champignon		mushroom
le chou-fleur		cauliflower
le concombre		cucumber
les épinards		spinach
l'oignon		onion
le petit pois		pea
le radis		radish
la tomate		tomato
le poivron		pepper (eg. bell or sweet)
la pomme de terre		potato
le haricot		bean
le céleri		celery
l'artichaut		artichoke
le brocoli		broccoli
le chou		cabbage
le potiron		pumpkin
l'adolescent		teenager
le silence		silence
déjà		already
paraitre		to seem
le contraire		opposite
discuter		to discuss
l'université		university
triste		sad
expérimenter		to experience
le coup de foudre		love at first sight
l'amitié		friendship
l'influence		influence
l'alcool		alcohol
la drogue		drug
le film		film
l'adulte		adult
reprocher		to reproach
la jeunesse		youth
résister		to resist
évoluer		to evolve
l'émotion		emotion
mineur		minor
majeur		major
la honte		shame
sensible		sensitive
le couple		couple
l'indépendance		independence
désobéir		to disobey
morne		gloomy
renfrogné		sullen
la virginité		virginity
l'acné		acne
le bouton		pimple
l'insécurité		insecurity
le gros mot		swear word
s'enfuir		to run away
le pote		buddy
le sport		sport
le joueur		player
la douleur		pain
l'athlète		athlete
la force		strength
le niveau		level
l'effort		effort
le but		goal
l'équipe		team
le tennis		tennis
le match		game
le téléspectateur		TV viewer
le golf		golf
le relâchement		relaxation
la lutte		struggle
le ski		ski
l'arbitre		referee
l'entraîneur		coach (as in trainer, not the vehicle)
l'exercice		exercise
l'uniforme		uniform
la médaille		medal
la victoire		victory
le formulaire		form
l'activité		activity
le mouvement		movement
le stade		stadium
jeter		to throw
qualifier		to qualify
l'exigence		demand
accomplir		to fulfil, to accomplish
pratiquer		to practice
le défi		challenge
l'or		gold
l'argent		silver
le bronze		bronze
la course		race
le rugby		rugby
la raquette		racket (sport not noise)
la batte		bat
le muscle		muscle
le cyclisme		cycling
le ski de fond		cross-country skiing
l'escrime		fencing
le ping pong		table tennis
le supporteur		supporter
le patin à glace		ice skating
le patin à roulettes		roller skating
le roller		rollerblading
simuler		to fake
le dopage		doping
les stéroïdes		steroids
le football		football
l'expédition		expedition
la survie		survival
terrible		terrible
difficilement		with difficulty
la couverture		blanket
la tente		tent
la provision		stock
geler		to freeze
maigre		skinny
raide		stiff
liquide		liquid
solide		solid
la photo		photo
l'appareil photo		photo camera
la caméra		video camera
alors		then, so
amorcer		to begin
la marche		walk
l'étape		stage (as in step, not theatrical)
le camp		camp
le manque		lack
lutter		to struggle (not se battre)
avancer		to advance
ralentir		to slow down
l'espace		space
finalement		finally
l'espoir		hope
amener		to bring (someone)
le risque		risk
souffrir		to suffer
la direction		direction
le départ		departure
la limite		limit
le bout		end, tip
d'abord		first of all
apporter		to bring (something)
le pôle		pole
la rentrée		return
la prise		grip
disparaître		to disappear
constituer		to constitute
la mission		mission
découvrir		to discover
essentiel		essential
la détermination		determination
le danger		danger
le sommet		summit
franchir		to get over
le courage		courage
l'initiative		initiative
emporter		to take, to carry away
la glace		ice
dépasser		to exceed
le sac à dos		backpack
installer		to install
nécessaire		necessary
la capacité		capacity
bâtir		to build
l'ouvrier		blue-collar worker
l'outil		tool
le travailleur		worker
l'usage		use
la structure		structure
utile		useful
concevoir		to conceive
garantir		to guarantee
durable		durable
baisser		to lower
appuyer		to press
la disposition		arrangement
le fil		thread
dérouler		to unwind
le mécanisme		mechanism
incapable		incompetent
l'équipement		equipment
fonctionner		to function
renforcer		to reinforce
détruire		to destroy
la rupture		break, fracture
suspendre		to suspend
doter		to endow
exploiter		to exploit
supprimer		to eliminate
écarter		to move aside; to separate
l'usine		factory
la bande		band
actif		active
étendre		to stretch
réparer		to repair
pratique		practical
fabriquer		to manufacture
l'échelle		ladder
forger		to forge
bricoler		to do handiwork
le vieillard		old man
l'ancêtre		ancestor
la retraite		retirement
inévitable		unavoidable
le fait		fact
la vue		view
la faiblesse		weakness
l'os		bone
fidèle		faithful
la visite		visit
l'expérience		experience
la croisière		cruise
aimable		pleasant
regretter		to regret
la peau		skin
la dignité		dignity
songer		to daydream, to think, to dream
solitaire		solitary
le veuf		widower
la coutume		custom
l'habitude		habit
les moeurs		morals
le déclin		decline
la technologie		technology
confondre		to confuse; to mix up
actuel		current
ancien		ancient
la possession		possession
fatiguer		to tire
l'aide		help
la mémoire		memory
inexorable		inexorable
le petit-enfant		grandchild
l'arrière-petit-enfant		great-grandchild
vieux jeu		old fashioned
le remords		remorse
incontinent		incontinent
la télévision		television
l'émission		programme
la série		series
l'épisode		episode
suivant		following
précédent		previous
la distribution		distribution
l'annonce		announcement
le personnage		character
les médias		media
l'acteur		actor
la presse		press, papers
le journaliste		journalist
la radio		radio
émettre		to emit
la chaîne		chain; tv channel
how		comment
l'audience		audience
régional		regional
promouvoir		to promote
le scénario		scenario
la vidéo		video
le créateur		creator
le rapport		report
la vedette		star
les prévisions météo		weather forecast
particulier		particular
diffuser		to broadcast
la pub		advert
le documentaire		documentary
particulièrement		particularly
complexe		complex
l'exception		exception
extraordinaire		extraordinary
intéressant		interesting
exceptionnel		exceptional
extrême		extreme
la norme		norm
semblable		similar, alike
spécial		special
ordinaire		ordinary
habituel		customary
la tradition		tradition
généralement		generally
contraire		contrary
distinguer		to distinguish
supérieur		superior
égal		equal
inférieur		inferior
la convention		convention
saisissement		shock
la surprise		surprise
étonnant		surprising
susciter		to arouse, to spark off, to give rise to
fondamental		fundamental
exemplaire		exemplary
banal		banal
hum		um
peut-être		maybe
ouais		yeah
putain		crap
hein		huh
également		equally
afin de		in order to
lors		at the time of
dont		whose
ainsi		thus
en cas de besoin		if need be
tel		such
lorsque		when (not quand)
cependant		however
notamment		notably
pourtant		yet, however, all the same
un propos		a subject, a topic, a remark, a point, an intention
actuellement		at present, currently
probablement		probably
exactement		exactly
absolument		absolutely
certes		indeed, certainly, of course
guère		hardly
dorénavant		from now on
voire		even
prealablement		beforehand
néanmoins		nevertheless
sinon		otherwise
je m'en fous		I don't care
ah bon?		really?
du coup		as a result
ouf		phew
Aïe!		ouch!
le poisson d'avril		april fool
ras le bol		up to there
la bouffe		food, grub, nosh
c'est clair et net		that's absolutely clear
mon oeil		my foot
quant à		as for
le casting		audition
la région		region
le chiffre		number
part		part
revenir		to come back
l'ordre		order
l'exemple		example
le système		system
la présence		presence
nombreux		numerous
produire		to produce
préparer		to prepare
prochain		next
le membre		member
juger		to judge
la plupart		most
le hasard		chance
marquer		to mark
poursuivre		to pursue
l'acte		act
aura		aura
préférer		to prefer
prouver		to prove
la larme		tear
artificiel		artificial
dramatique		dramatic
le rêve		dream
inspirer		to inspire
le talent		talent
le chanteur		singer
la chanson		song
l'interprétation		interpretation
original		original
le vote		vote
faillir		to fail
la tragédie		tragedy
hésiter		to hesitate
la blague		joke
l'audition		hearing, audition
destiner		to intend
faux		out of tune
la reprise		cover (music)
touchant		touching
trancher		to settle, decide (solve a problem)
populaire		popular
persévérer		to persevere
le royaume		kingdom
le roi		king
la reine		queen
le prince		prince
la princesse		princess
le comte		count
le duc		duke
principal		principal
lui-même		himself
le garde		guard
historique		historical
l'institution		institution
la protection		protection
fonder		to found
exercer		to exercise
régner		to reign
la masse		mass
l'exécution		execution
la révolution		revolution
le trône		throne
la cour		courtyard
le statut		status
l'exploitation		exploitation
attribuer		to award, to assign
le règne		reign
abolir		to abolish
royal		royal
doré		golden
traditionnellement		traditionally
le château		castle
la souveraineté		sovereignty
le souverain		sovereign
se révolter		to revolt
l'héritier		heir
le prédécesseur		predecessor
l'échelon		rung
couronner		to crown
la couronne		crown
l'empire		empire
la majesté		majesty
la cérémonie		ceremony
la science		science
indépendant		independent
l'échec		failure
transformer		to transform
modifier		to modify
la donnée		fact, concept
mesurer		to measure
examiner		to examine
exact		exact
la méthode		method
relatif		relative
absolu		absolute
précisément		precisely
l'analyse		analysis
rechercher		to search for
le circuit		circuit
supposer		to suppose
la différence		difference
préciser		to make clear, to specify (not spécifier)
le calcul		calculation
multiplier		to multiply
diviser		to divide
ajouter		to add
soustraire		to subtract
le résultat		result
l'étude		study
le chercheur		researcher
élaborer		to work out, develop
le critère		criterion
l'observation		observation
étonner		to astonish
la théorie		theory
l'efficacité		efficiency
spécialiste		specialist
relativement		relatively
le gaz		gas
l'expert		expert
recueillir		to collect
déterminer		to determine
effectuer		to carry out
la formule		formula
intellectuel		intellectual
la précision		precision
la trace		trace
la dimension		dimension
positif		positive
provenir		to be from
béton		concrete
scientifique		scientific
logique		logical
inconnu		unknown
causer		to cause
l'armée		army
la guerre		war
la défense		defence
l'ennemi		enemy
déployer		to deploy
la troupe		troup
l'alliance		alliance
militaire		military
le combat		fight
la gamme		range
le soldat		soldier
le traité		treaty
la bombe		bomb
armé		armed
le héros		hero
l'attentat		attack, attempt
confronter		to confront
crever		to burst
la stratégie		strategy
combattre		to fight
désigner		to designate
la pression		pressure
l'accès		access
direct		direct
le signal		signal
la vigueur		vigor
prudent		cautious
la coopération		cooperation
le rang		rank
l'arrivée		arrival
l'obstacle		obstacle
le front		forehead
l'allié		ally
volontaire		voluntary
laver		to wash
nettoyer		to clean (housework)
cuire		to cook
taire		to be silent, to keep quiet
le soin		care
le moelleux		softness
la solution		solution
régulièrement		regularly
faciliter		to make easier
la recette		recipe
le fer		iron
le foyer		home
traditionnel		traditional
se débrouiller		to manage
la machine		machine
surveiller		to watch
nourrir		to feed
efficace		efficient
maintenir		to maintain
accueillir		to accommodate; to meet; to welcome
rassasier		to satisfy
content		glad
aspirer		to vacuum
repasser		to iron
supporter		to support
la vaisselle		dishes
le linge		linen
la lessive		laundry
le rôti		roast
se soucier de		to care about
le lifting		facelift
épiler		to epilate (remove hair from legs), to wax, to pluck
s'occuper		to take care
l'éponge		sponge
la serviette		towel
le savon		soap
le shampooing		shampoo
frotter		to rub
étudier		to study
l'enseignement		education
le papier		paper
la note		note
l'examen		exam
le concours		competition
le contrôle		control
l'ennui		boredom
copier		to copy
apprendre		to learn
le primaire		primary
le collège		secondary school
le lycée		high school
la classe		class
enseigner		to teach
l'élève		pupil (student)
répondre		to answer
l'instituteur		schoolteacher
la réponse		answer
intéresser		to interest
occuper		to occupy
organiser		to organize
scolaire		educational
la chimie		chemistry
le latin		Latin
mathématiques		mathematics
l'excuse		excuse
la connaissance		knowledge
l'étudiant		student
la leçon		lesson
souligner		to underline
la division		division
le devoir		homework
la physique		physics
le stylo		pen
le bic		ballpoint pen
le stylo à plume		fountain pen
le crayon		pencil
le carnet		notebook
la colle		glue
la géographie		geography
l'SVT		science and biology
tricher		to cheat
le baccalauréat		baccalaureate
la sonnerie		bell
sécher les cours		to play hooky
le chapitre		chapter
l'auteur		author
lire		to read
la page		page
écrire (écrit)		to write
la lecture		reading
citer		to quote
l'édition		publishing
l'écrivain		writer
le roman		novel
la publication		publication
la marge		margin
baser		to base
le thème		theme
la version		version
la conclusion		conclusion
inquiétant		worrying
s'en faire		to worry
rassurer		to reassure
la tension		tension
l'épreuve		ordeal
l'inquiétude		(state) anxiety, concern
la crainte		fear
renoncer		to give up
extrêmement		extremely
douter		to doubt
la pensée		thought
la perspective		perspective
la résolution		resolution
la tentative		attempt
imaginer		to imagine
envisager		to contemplate
réfléchir		to reflect [thoughts]
la modification		modification
final		final
concentrer		to concentrate
formuler		to formulate
définitif		definitive
la notion		notion
le contenu		contents
l'essai		(experiment) trial
entreprendre		to undertake
reflet		reflection
clore		(put an end to) to close (debate, ballot, account)
rédiger		to write out, to copy out
le livre		book
conséquent		substantial
revoir		to see again
partiel		partial
achever		to complete
inédit		unpublished
la littérature		literature
le casino		casino
le pari		bet
la mise		stake
endettement		debt
la sécurité		security
quasi		almost
profiter		to take advantage
fixer		to fix
le montant		total
la base		base
placer		to place
le taux		rate
panneau		sign
suffire		to suffice
la proportion		proportion
statistique		statistical
investir		to invest
oser		to dare
la totalité		totality
récupérer		to recover
la tendance		tendency
l'offre		offer
l'équilibre		balance
l'établissement		establishment
considérable		considerable
favorable		favorable
la probabilité		probability
la ruine		ruin
l'impulsion		impulse
inciter		to incite
l'enjeu		stake, amount of risk
le gain		gain
persister		to persist
la déconvenue		disappointment
miser		to stake
parier		to bet
l'unité		unit
estimer		to estimate
doubler		to double
tripler		to triple
la tactique		tactic
la bataille		battle
recevoir		to receive
le coup		blow
risquer		to risk
monter au créneau		to intervene
pousser		to push
le poing		fist
la raison		reason
la suite		follow-up
à hauteur de		up to
le sang		blood
violent		violent
la victime		victim
le contact		contact
l'avantage		advantage
la réaction		reaction
menacer		to threaten
tandis que		while
la résistance		resistance
hurler		to scream, to yell, to shout (not 'crier')
la mâchoire		jaw
le retentissement		impact
blesser		to hurt
bloquer		to block
la prudence		caution
l'action		action
opposer		to oppose
participer		to participate
mêler		to mingle
la menace		threat
défendre		to defend
la revanche		revenge
le coup de pied		kick
tendre l'autre joue		to turn the other cheek
tirer		to pull
griffer		to scratch
mordre		to bite
la baffe		slap
le coup de boule		headbutt
l'oeil au beurre noir		black eye
livrer		to deliver
puisque		since, as
le cadeau		present, gift
autoriser		to authorize
le pneu		tire
soulever		to lift up, to arouse, to raise
d'autant		all the (more; less), accordingly, by the same amount
d'ailleurs		moreover, by the way
malin		shrewd, cunning
renvoyer		to send back, to return (a thing)
entraîner		to lead to, to bring about, to entail
bref		brief
la tempête de neige		snow storm
la gourmandise		gluttony
quelconque		any, some
carrément		really, downright
Sauver		To rescue
le cornichon		pickle
la canneberge		cranberry
les fruits de mer		the seafood
le bleuet		blueberry
la barbotte		catfish
le corbeau		raven
le faisan		pheasant
la mûre		blackberry
le melon d'eau		watermelon
le perroquet		parrot
le fou rire		the giggles/laughing fit/crazy laughter
le nourrisson		newborn
le vacarme		noise, racket, din
le râleur		the one who complains a lot, grouch, moaner (m.)
le colibri		hummingbird
l'hirondelle		swallow (n.)
l'ortie (f)		the nettle
saucer		wipe with a piece of bread
l'aléa		hazard
saperlipopette!		oh my!
pleurnicheur/euse		crybaby
godiche		silly
l'ornithorynque (m.)		the platypus
le rabais		discount
se vanter		to boast
le phare		lighthouse
le gaillard		strapping lad
le fût		barrel
costaud		strong, sturdy, hefty
cheminer		to stroll
le rouge-gorge		robin
rugir		to roar
la marmite		pot
l'hibou		owl
les habits		clothes
la fouine		stone marten, also someone who is too nosy
fendre		to split
crisper		to tense up
la niche		kennel
flou		blurry
fangeux		swampy, miry
la sueur		sweat
l'emprunt		loan
le bourbier		quagmire, mud bath
le fourgon		van, goods wagon
blinder		to reinforce, bulletproof
bondir		to bounce
courber		to bend
l'épopée		epic, saga
cantonner		to confine
le sillon		fissure, groove
le févier		honey locust
le frêne		ash tree
le hêtre		beech
le châtaignier		sweet chestnut tree
le saule		willow
étroit		narrow
beugler		to moo, to bellow
les menottes		handcuffs
le môme		kid, brat
gueule de bois		hangover
le gaspillage		waste (of money, talent, resources etc)
le décalage		lag, gap, discrepancy
farouche		wild, untamed
escarmouche		squabble, skirmish, brush
cocasse		comical
bravache		brash; shameless; bold; impulsive
fustiger		flay; remove skin; critize
la bisbille		quarrel
le quatuor		quartet
dérapage		skid, blunder
les écailles		scales (fish)
étendue		expanse
la causerie		chit chat, chatter
la boiserie		paneling
une lueur		a faint light, a glow
s'écouler		to flow, pass, drain away
ombre à paupière		eye shadow
la paupière		eyelid
frémir		to quiver, ripple, trembl