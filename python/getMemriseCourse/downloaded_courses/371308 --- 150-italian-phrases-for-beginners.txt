ciao		hello, hi
Come sta?		How are you? (formal)
Come stai?		How are you? (informal)
Sto bene		I'm fine
grazie		thank you, thanks
prego		you're welcome
E lei?		And you? (formal)
E tu?		And you? (informal)
anch'io		Me too
Piacere		Nice to meet you
Come si chiama?		What's your name? (formal)
Come ti chiami?		What's your name? (informal)
Mi chiamo		My name is
Di dov’è?		Where are you from? (formal)
Di dove sei?		Where are you from? (informal)
Io sono dagli Stati Uniti		I'm from the United States
Buongiorno		Good day/morning
Buon pomeriggio		Good afternoon
Buona sera		Good evening
buonanotte!		goodnight
Arrivederla		Goodbye (formal)
arrivederci		goodbye
A presto		See you soon
Ci vediamo più tardi		See you later
Ci vediamo domani		See you tomorrow
per favore		please
Benvenuto		Welcome (male)
Benvenuta		Welcome (female)
Buona fortuna!		Good luck!
Buona giornata!		Have a nice day!
Capisce?		Do you understand?
Capisco		I understand
Non capisco		I don't understand
scusi!		excuse me
Mi dispiace		I'm sorry
Quanto costa?		How much is this?
Parla italiano?		Do you speak Italian?
Si, parlo italiano		Yes, I speak Italian
un po'		a little
Parla inglese?		Do you speak English?
Si, parlo inglese		Yes, I speak English
Mi può aiutare?		Can you help me?
Ho bisogno di aiuto		I need help
Ti aiuterò		I'll help you
Grazie per il vostro aiuto		Thanks for your help
Posso aiutarla?		May I help you?
Come stà la tua famiglia?		How's your family?
Molto bene		Very well
Molto male		Very bad
oggi		today
ieri		yesterday
domani		tomorrow
Cosa hai fatto oggi?		What did you do today?
Che cosa hai fatto ieri?		What did you did yesterday?
Ho fame		I'm hungry
Ho sete		I'm thirsty
Vuoi mangiare?		Do you want to eat?
Cosa c’è da mangiare?		What's there to eat?
Cosa c’è da bere?		What's there to drink?
E 'delizioso		It's delicious
Voglio di più		I want more
Voglio meno		I want less
Grazie per il cibo		Thanks for the food
Sei libero?		Are you free? (male)
Si, sono libero		Yes. I'm free (male)
Si, sono libera		Yes, I'm free (female)
Sei occupato?		Are you busy?
Si, sono occupato		Yes, I'm busy
Cosa stai facendo?		What are you doing?
Andiamo a mangiare		Let's go eat
Andiamo		Let's go
Andiamo fuori		Let's go out
Sono stanco		I'm tired
Ho sonno		I'm sleepy
Io vado a dormire		I'm going to sleep
Ti piace?		Do you like it?
Mi piace		I like it
Che cosa sta succedendo?		What's happening?
Dove vivi?		Where do you live?
Cosa stai studiando?		What are you studying?
A che università studi?		At what university do you study?
chi		who
dove		where
perché		why, because
quando		when
cosa?		what?
Come?		How?
Chiamami		Call me
Ti chiamerò		I'll call you
Cosa vuoi fare?		What do you want to do?
Come è stata la tua giornata?		How is your day?
La mia giornata è stata buona		My day is good
La mia giornata era male		My day was bad
Va bene		It's fine
Che cosa stai pensando?		What are you thinking about?
giorno		day
mattina		morning
pomeriggio		afternoon
sera		evening
notte		night
Sono in anticipo		I'm early
Sono in ritardo		I'm late
E 'facile		It's easy
E 'difficile		It's difficult
Cosa stai dicendo		What are you saying?
Credo di sì		I think so
Non ci penso		I don't think so
Buona idea		Good idea
Buon lavoro		Good job
Che sorpresa		What a surprise
Voglio andare lì		I want to go there
E 'un piacere		It's a pleasure
Molto tempo che non ci vediamo		Long time no see
Per favore, parli più lentamente		Please speak more slowly
Buon Natale!		Merry Christmas!
Buon anno!		Happy New Year!
Buon compleanno!		Happy Birthday!
Che cosa sta succedendo?		What's happening?
Che cosa è successo?		What happened?
Mi piaci		I like you
Chi ti piace?		Who do you like?
Riesci a crederci?		Can you believe it?
Non riesco a crederci		I can't believe it
E 'una bella giornata		It's a beautiful day
E 'un giorno di pioggia		It's a rainy day
C'è il sole		It's sunny
È nuvoloso		It's cloudy
Sta nevicando		It's snowing
Sta piovendo		It's raining
Siediti per favore		Sit down please
Avanti		Enter
di nuovo		again
Prova di nuovo		Try again
Che tipo di musica ti piace?		What type of music do you like?
Dov'è il bagno?		Where is the restroom?
Laggiù		Over there
qui		here
Vieni qui		Come here
Anche lei		You too (formal)
Anche tu		You too (informal)
Come si dice in italiano...?		How do you say in Italian...?
Ho i compiti		I have homework
Cosa vuoi fare?		What do you want to do?
Che bello!		How beautiful!
Hai fratelli?		Do you have siblings?
Cosa ti piace fare nel tuo tempo libero?		What do you like to do in your free time?
Mi piace guardare il film		I like watching movies
Mi piace ascoltare la musica		I like listening to music
Mi piace leggere		I like reading
Mi piace viaggare		I like traveling
Io sono con i miei amici		I am with my friends
Sono con la mia famiglia		I'm with my family
Salute!		Cheers/Good health!
Guarisci presto!		Get well soon!
Cosa farai dopo?		What will you do later?
Ho un esame più tardi		I have an exam later
Quando è il tuo compleanno?		When is your birthday?
Sarò libero dopo		I'll be free later
Sarò occupato più tardi		I'll be busy later
Ho avuto una giornata intensa		I had a busy day
Ho avuto una giornata di divertimento		I had a fun day
Posso chiamarti più tardi?		Can I call you later?
Ho bisogno di practicare il mio italiano		I need to practice my italian
Hai Skype?		Do you have Skyp