benvenuto		welcome
il vino		wine
l'auto		car
l'aereo		plane
il cellulare		cell phone
il pallone		ball (larger, also football)
il libro		book
il bicchiere		glass
la borsa		purse, handbag
la scarpa		shoe
il calciatore		football player
il bambino		child (boy)
il ragazzo		boy
la ragazza		girl
lo studente		student
buongiorno!		good morning!
buonasera!		good evening
ciao (informal)		hi / bye
arrivederci		good bye
essere		to be
Il Colosseo		The Colosseum
il professore		professor
salve		hello, hi
sì		yes
no		no
bene		well
grazie		thank you
molto bene		very well
così così		so-so (mood/feeling)
abbastanza		enough
male		badly
piacere		pleased to meet you
l'amico		friend
l'attore		actor
la cartolina		postcard
la casa		house
la città		city
il mare		sea
il pomodoro		tomato
la scuola		school
il sole		sun
la studentessa		female student
la tazzina		cup
la torre		tower
il turista		tourist
il caffè		coffee
l'ape		bee
l'erba		grass
l'isola		island
la mela		apple
il miele		honey
l'orso		bear
l'uva		grape
l'amicizia		friendship
indiano		Indian
spagnolo		Spanish
coreano		korean
russo		Russian
italiano		Italian
cinese		Chinese
finlandese		Finnish
canadese		Canadian
sengalese		Sengalese
tedesco		German
portoghese		Portugese
inglese		English
greco		Greek
il museo		museum
il teatro		theater
l'orologio		clock
il fiume		river
la classe		class
il ristorante		restaurant
la nazione		nation
la piazza		square
la persona		person
il tè		tea
l'albergo		hotel
il fungo		mushroom
il lago		lake
il bar		café
il computer		computer
lo sport		sport
il film		movie, film
il monumento		monument
la regione		region
il paese		village
il dottore		physician
l'insegnante		teacher
la lezione		lesson
la penna		pen
l'università		university
la bottiglia		bottle
il piatto		plate, dish
Edimburgo		Edinburgh
Londra		London
Mosca		Moscow
Atene		Athens
il duomo		cathedral
la cupola		dome
la cattedra		teacher's desk
la lavagna		blackboard
il banco		school desk
il cameriere		waiter
la sedia		chair
la fontana		fountain
la strada		road
il televisore		television
il ponte		bridge
il tavolo		table
avere		to have
la cucina		kitchen
il gatto		cat
l'ufficio		office
il quaderno		exercise book, workbook
il giardino		garden
il giornale		newspaper
la rivista		magazine
l'anello		ring
il vestito		piece of clothing, garment, dress
il cane		dog
la villa		villa
la segretaria		secretary (f.)
la carta di credito		credit card
la finestra		window
l'abito		dress
la mappa		map, plan
la porta		door
la calza		stocking
il guanto		glove
l'ombrello		umbrella
la camicia		shirt
gli occhiali da sole		sunglasses
l'appartamento		apartment
il calendario		calendar
l'elenco telefonico		telephone book
la libreria		bookcase; bookstore
la fame		hunger
bisogno		need
il taxi		taxi
la sete		thirst
il sonno		sleep, sleepy
la paura		fear
caldo		hot
freddo		cold
americano		American
la matita		pencil
zero		zero
uno		one
due		two
tre		three
quattro		four
cinque		five
sei		six
sette		seven
otto		eight
nove		nine
dieci		ten
undici		eleven
dodici		12
tredici		13
quattordici		14
quindici		15
sedici		16
diciassette		17
diciotto		18
diciannove		19
venti		20
ventuno		21
ventidue		22
ventitré		23
ventiquattro		24
venticinque		25
ventisei		26
ventisette		27
ventotto		28
ventinove		29
trenta		30
trentuno		31
trentotto		38
trentadue		32
quaranta		40
cinquanta		50
sessanta		60
settanta		70
ottanta		80
novanta		90
cento		100
quanti anni hai?		how old are you?
il padre		father
la madre		mother
la busta		envelope
la lettera		letter
il francobollo		stamp
il passaporto		passport
il cibo		food
la verità		truth
la nazionalità		nationality
il papà		dad
Berlino		Berlin
giapponese		Japanese
l'ospedale		hospital
la banca		bank
il mercato		marketplace
lo stadio		stadium
la tabaccheria		tabacconist's
l'ufficio postale		post office
l'armadio		wardrobe
il tovagliolo		napkin
la lampada		lamp
lo zaino		backpack
la forchetta		fork
il coltello		knife
il telefono		telephone
lo zio		uncle
lo zoo		zoo
lo gnomo		gnome
lo psicologo		psychologist
l'agenda		planner
l'aula		classroom
la donna		woman
lo studio		study, office
il gioco		toy
l'infermiere		nurse
nero		black
l'albero		tree
l'albero di mele		apple tree
la mamma		mum, mom
rosso		red
la sigaretta		cigarette
il biglietto		ticket
l'autobus		bus
la scheda telefonica		phone card
il supermercato		supermarket
la frutta		fruit
il pane		bread
lo zucchero		sugar
l'arancia		orange (food)
il forno		oven
il frigorifero		fridge
la lavastoviglie		dishwasher
le posate		cutlery
comprare		to buy
il centro		center
lavorare		to work
abitare		to live (in a place)
viaggiare		to travel
guardare		to look at
telefonare		to telephone
parlare		to talk
cantare		to sing
ascoltare		to listen to
studiare		to study
pranzare		to have lunch, to dine
amare		to love
suonare		to play (an instrument)
arrivare		to arrive
preparare		to prepare
cucinare		to cook (to prepare food)
pensare		to think
apparecchiare		to prepare, to set
mangiare		to eat
fare		to do
stare		to stay, to remain, to be
dare		to give
andare		to go
insegnare		to teach
il medico		doctor
la biblioteca		library
imparare		to learn
cenare		to have dinner, supper
aspettare		to wait, to wait for
lasciare		to leave, to let go of
guidare		to drive
incontrare		to meet
passare		to pass by, to pass (time)
l'ambulanza		ambulance
il cinema		cinema
la farmacia		pharmacy
la fermata dell'autobus		bus stop
i soldi		money
la squadra sportiva		sports team
la stazione di servizio		gas station
la vacanza		holiday
la corsa		running, run, race, ride
il bancomat		cashpoint
malato		sick
la squadra		team
il nuoto		swimming
la caramella		sweet, candy
mandare		to send
scrivere		to write
giocare		to play (a game)
il coro		choir
celeste		sky blue
la chiesa		church
il cocomero		watermelon
la cintura		belt
la costa		coast
la pace		peace
la cena		dinner
ungherese		Hungarian
il dialogo		dialogue
il genitore		parent
Inghilterra		England
il gelo		frost
l'angelo		angel
largo		wide
il gufo		owl
il gelato		ice cream
la colla		glue
felice		happy
l'ago		needle
gente		people
il mago		wizard, magician
la cura		care
la giraffa		giraffe
la carta		map
la gomma		rubber
la chiave		key
la salumeria		delicatessen
la pasticceria		pastry shop
il fruttivendolo		fruit vendor (masc.)
la profumeria		perfumery
l'edicola		news stand
la pescheria		fish monger's
nuovo		new
bello		beautiful
buono		good
alto		high
divertente		funny
interessante		interesting
elegante		elegant
contento		glad, pleased
simpatico		likeable
affettuoso		affectionate
allegro		cheerful
grande		big
africano		African
Svezia		Sweden
svedese		Swedish
Giappone		Japan
gli Stati Uniti d'America		the United States of America
bere		to drink
rispondere		to answer, to reply
vivere		to live
leggere		to read
vendere		to sell
conoscere		to know (be familiar with)
credere		to believe
prendere		to take
cominciare		to begin, start
tornare		to come back
cercare		to look for, seek, search for
l'aranciata		orangeade
il ghiaccio		ice
la collana		necklace
il braccialetto		bracelet
la spilla		pin, brooch
la cartoleria		stationary shop
dire		to say
ballare		to dance
dormire		to sleep
la tigre		tiger, tigress
gli occhiali		glasses
lo spazzolino da denti		toothbrush
il pettine		comb
il portafoglio		wallet
il fazzoletto		handkerchief
il rossetto		lipstick
l'accendino		lighter
vincere		to win, to conquer
segnare		to mark
collaborare		to collaborate
l'artista		artist
l'architetto		architect
la barba		beard
il cappello		hat
la birra		beer
dipingere		to paint
il motorino		scooter
l'occhio		eye
lo sciatore		skier
il quadro		painting, picture
la palestra		gym
spolverare		to dust
nuotare		to swim
la piscina		swimming pool
mettere		to put, to place, to set
piccolo		small
fedele		loyal, faithful
l'esame		exam
visitare		to visit
biondo		blonde
marrone		brown
carino		pretty
grasso		fat
magro		thin
il giro		tour
il cestino		basket
giallo		yellow
vicino		near
il giro		tour, trip
la gita		trip, journey
il carro		cart
il gusto		flavor
il castello		castle
l'impiegato		employee/ clerk
il ghiro		dormouse
il gallo		rooster
il conto		bill, account
belga		Belgian
verde		green
utile		useful
la moda		fashion
accendere		to light, turn switch on
il calcio		soccer
famoso		famous, well known
perdere		to miss; to lose
la cultura		culture
l'arte		art
la bellezza		beauty
il cuscino		cushion, pl. seats in carriage, car or train
la tenda		curtain
il nipote		grandchild/nephew/niece
la gamba		leg
il parente		relative
il cognato		brother-in-law
la zia		aunt
il cugino		cousin
il fratello		brother
il figlio		son
il suocero		father-in-law
la sorella		sister
la nonna		grandmother
partire		to leave
capire		to understand
finire		to stop, end, finish
l'anziano		old, elderly, aged
falso		false
l'avvocato		lawyer
aprire		to open
sentire		to hear, to feel
vero		true
il bacio		kiss
il bagno		bath, bathroom
la camera		room
la canzone		song
la doccia		shower
la famIglia		family
la foto		photograph
la fotografia		photograph, picture
l'inverno		winter
la musica		music
il negozio		shop
il fiore		flower
pulire		to clean
il pranzo		lunch
offrire		to offer
il panino		sandwich
l'acqua		water
la valigia		(suit)case, bag
il treno		train
la macchina		car
il latte		milk
la chitarra		guitar
la pala		shovel
la pelle		skin
la pila		battery, pile, stack
il palo		post, pole
il callo		callus, corn
allora		well then, then
l'alloro		bay (tree), laurel
il pelo		hair, fur
l'ala		wing
il collo		neck
il filo		wire
il polo		pole (geographic)
il pollo		chicken
il gelo		frost
meraviglioso		marvellous; wonderful
la foglia		leaf
l'aglio		garlic
la moglie		wife
preferire		to prefer
spedire		to send, mail, dispatch
ieri		yesterday
l'abbraccio		hug, embrace, cuddle
il quartiere		neighbourhood; area; quarter
la periferia		outskirts, suburbs
rimanere		to remain
salire		to climb, to go up, to come up
uscire		to go out
venire		to come
questo		this
quello		that
dietro		behind
accanto		near, next to
sinistra		left (direction)
destra		right (direction)
sopra		on
lontano		far
l'asilo		playschool, kindergarden
l'azienda		firm/company
il centro storico		historical centre
il concerto		concert
il golfo		the gulf
la medicina		medicine, drug
la metropolitana		underground
il parcheggio		parking
il parco		park
il salotto		living room
la scala		staircase
scendere		to go down
il sedile		seat, bench
il semaforo		traffic light
lo specchio		mirror
la stazione		train station
vestirsi		to dress oneself
divertirsi		to enjoy oneself, to have a good time
mettersi		to put on (cloths)
dovere		to have to
potere		to be able to
volere		to want
truccarsi		to put on make up
prepararsi		to get oneself ready
svegliarsi		to wake up
la barca		boat
sud		south
nord		north
la via		road, street
lavarsi		to wash oneself
fermarsi		to stop (oneself), stay
il cavallo		horse
il romanzo		novel
dimenticarsi		to forget oneself
addormentarsi		to fall asleep
sentirsi		to feel
preoccuparsi		to be worried, anxious
il tango		tango
pescare		to fish
passeggiare		to walk, to take a walk, to stroll
osservare		to observe
l'aeroporto		airport
l'assistente di volo		flight attendant
il cantante		singer
la commedia		comedy
il cornetto		croissant
il cuoco		cook
fumare		to smoke
il giornalista		journalist
il grembiule		apron
il pasticciere		pastry chef
pettinarsi		to comb one's hair
il prezzo		price
recitare		to recite, to act
redazione		editing, version, compilation
spegnere		to switch off
lo spettacolo		show
riposarsi		to rest
il regista		director (film, teater)
la stella		star
la tuta		tracksuit
il volo		flight
il gruppo		group
il collega		colleague
il paesaggio		landscape
la coppia		couple
il mare		sea
il costume		costume
l'orchestra		orchestra
il centro estetico		beauty shop
la nave		ship
chiudere		to close
cadere		to fall
sciare		to ski
il meccanico		mechanic
l'autoscatto		self-timer, automatic shutter release
la scena		stage
il samba		samba
gennaio		January
marzo		March
febbraio		February
aprile		April
maggio		May
giugno		June
luglio		July
agosto		August
settembre		September
ottobre		October
novembre		November
dicembre		December
l'agriturismo		farm holidays
l'autunno		fall, autumn
l'estate		summer
la primavera		spring
il balcone		balcony
la cabina		(beach) hut
il cammello		camel
il campeggio		camping
il cielo		sky
la crociera		cruise
il deserto		desert
il messaggio		message
la montagna		mountain
l'ombrellone		beach umbrella
il panorama		the view (not vista)
il pesce		fish
la pianta		plant
il pirata		pirate
ricevere		to receive, to get
il rifugio		shelter
il raffreddore		cold (infection)
la spiaggia		beach
il tuffo		diving
il basilico		basil
l'olio		oil
la farina		flour
il riso		rice
il sale		salt
il pane		bread
l'uovo		egg
morire		to die
scegliere		to choose
decidere		to decide
la bistecca		steak
il carciofo		artichoke
la carne		meat
la colazione		breakfast
il formaggio		cheese
l'insalata		salad
la lenticchia		lentil
l'oliva		olive
l'osteria		tavern
la patata		potato
la melanzana		aubergine, eggplant
la verdura		vegetables
la tovaglia		tablecloth
la torta		cake
il prosciutto		ham
il peperone		pepper
l'orata		gilthead bream
il tramezzino		sandwich
i pantaloni		trousers
caro		dear
l'asparago		asparagus
la gonna		skirt
la maglietta		t-shirt
la felpa		sweatshirt
il maglione		sweater
la camicia		shirt (male)
la giacca		jacket
la camicetta		blouse
regalare		to give a present
alcune		some, a few
blu		blue
azzurro		(sky) blue
grigio		grey
viola		violet
rosa		pink
arancione		orange (colour)
bianco		white
beige		beige
qualche		some, a few
la sciarpa		scarf
la manica		sleeve
stretto		narrow, tight
il cappotto		coat
le righe		stripes
lungo		long
la cravatta		tie
la cerniera		zipper
la seta		silk
il velluto		velvet
la lana		wool
il cotone		cotton
il nylon		nylon
la lycra		lycra
il lino		linen
la maglia		t-shirt, sweater
il calzino		sock
il costume da bagno		swimsuit
la pantofola		slipper
la pigiama		pyjamas
il sandalo		sandal
lo stivale		boot
il tacco		heel (shoe)
il trucco		make-up
sapere		to know, find out, be able to
la notte		night
la note		note
la vetrina		show window, display case
aderente		tight, close-fitting
la pioggia		rain
il vento		wind
la neve		snow
la nebbia		fog
ovest		west
est		east
il limone		lemon
il capoluogo		chief town (of a region)
la cittadina		town
la ciliegia		cherry
la collina		hill
la coltura		cultivation, growing, tilling
il grano		wheat
la guida turistica		tourist guide
il mandarino		tangerine
la nuvola		cloud
l'ortaggio		vegetable
la penisola		peninsula
il porto		harbor
la vegetazione		vegetation
il monolocale		studio apartment
la mansarda		attic apartment
il vulcano		volcano
il garage		garage
il palazzo		palace
il portone		large door
il citofono		buzzer, intercom
la portineria		porter’s lodge, caretaker’s lodge
spazzare		to sweep
striare		to iron
allacciare		to fasten, lace, buckle
smettere		to stop
entrare		to enter
cucire		to sew
la radiosveglia		clock radio
il comodino		bedside table
il cassettone		chest of drawers
il lenzuolo		bedsheet
la coperta		blanket
il lavandino		sink
il tappeto		carpet
la vasca da bagno		bath tub
il letto		bed
il bidet		bidet
il tappetino		rug
il corridoio		aisle
la dispensa		storeroom
il divano		sofa
la cantina		cellar, basement
il cancello		gate
la lavatrice		washing machine
il ripostiglio		storeroom
la poltrona		armchair
la parete		wall
l'ingresso		entrance
la scrivania		desk
il soggiorno		stay, holiday
il termosifone		radiat