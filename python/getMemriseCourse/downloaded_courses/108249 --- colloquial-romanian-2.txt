aici		here
ce		what
chiar		even
da		yes
dar		but
de		of, from
de la, din		from
din păcate		unfortunately
magazin		shop
în		in
într-adevăr		indeed
în general		generally
la		to, at
nu		no
doar, numai		only
pe curând		see you soon
și		and
întotdeauna		always
petrecere		party
englez		English (person, m)
englezoaică		English (person, f)
român		Romanian (person, m)
româncă		Romanian (person, f)
american		American (person, m)
americană		American (person, f)
neamț		German (person, m)
neamțoaică		German (person, f)
francez		French (person, m)
franțuzoaică		French (person, f)
engleză		English (language)
franceză		French (language)
germană		German (language)
spaniolă		Spanish (language)
rusă		Russian (language)
chineză		Chinese (language)
Anglia		England
Marea Britanie		Great Britain
Statele Unite		The United States
Bună dimineața		Good morning
Bună ziua		Good day
Bună seara		Good evening
noapte bună		good night
Bună		Hi
Salut		Greetings
La revedere		Goodbye
Pa		Bye
Vârstă		Age
apoi		then, after
bineînțeles		naturally
Cât costă?		How much does it cost?
cumva		by any chance
de aici		from here
deja		already
doar, numai		only
drept înainte		straight ahead
după aceea		after that
e mai bine		it is better
înapoi		back
în fața		in front of
în jos		down(wards)
în dreapta		on the right
în stânga		on the left
la dreapta		to the right
la stânga		to the left
mai bine		rather, better
mai întâi		first of all
niște		some
pentru		for
unde		where
scop		purpose
pașaport		passport
bandă rulantă pentru bagaje		baggage reclaim conveyor belt
ședere		stay (noun)
secretar		secretary
dactilograf		typist
inginer		engineer
infirmier		nurse
profesor		teacher
vânzător		shop assistant
chelner		waiter
doctor		doctor
funcționar		civil servant
bugetar		public sector worker
ziarist		journalist
bancă		bank
fabrică		factory
uzină		factory, plant
divorțat		divorced
văduv		widower
căsătorit		married
însurat		married (men)
măritată		married (women)
bani		money
dolar		dollar
sterlină		sterling
casă de schimb valutar		bureau de change
coadă		queue
chitanță		receipt
birou de informații pentru turiști		Tourist information office
pentru puțin		You are welcome, think nothing of it
scări		stairs
cum ajung la...		How do I get to the...
gară		train station
aeroport		airport
metro		underground, tube
hotel		hotel
spital		hospital
poștă		post office
poștă centrală		main post office
ghișeu		counter
călătorie		journey
cam		around, about
distanță		distance
unde anume		whereabout
azi		today
închis		closed
deschis		open
circulație		traffic
deloc		at all
bani mărunți		small change
primul		first (m)
prima		first (f)
al doilea		second (m)
a doua		second (f)
al treilea		third (m)
a treia		third (f)
al patrulea		fourth (m)
a patra		fourth (f)
al cincilea		fifth (m)
a cincea		fifth (f)
am avut chef		I felt like it
a face baie		to take a bath
îmi pare rău		I am sorry
hai să vedem		let's see
în dreapta		on the right
ce păcat		what a pity
din păcate		unfortunately
în sfârșit		at last
se vede		it shows
surpriză		surprise
frumos		beautiful
munte		mountain
păcat		sin
plăcut		pleasant
a sta		to stay, to remain, to stand
concediu		holiday
ieri		yesterday
mare		sea
neplăcut		unpleasant
a ploua		to rain
soare		sun
am fost		I was
am venit		I came
am avut		I had
am dat		I gave
am scris		I wrote
am spus		I said
am stat		I stayed
am lucrat		I worked
am putut		I could
am mers		I went
am luat		I took
am vrut		I wanted
am făcut		I did
nu am stat mult		I did not stay long
nu am putut să fac baie		I could not go for a swim
nu am făcut		I did not do
nu am nimic		I have nothing
cameră		room
hartă		map
ieftin		cheap
duminică		Sunday
hotel		hotel
a încerca		to try
liber		free
ofertă		offer
rezervare		reservation
nicăieri		nowhere
nimic		nothing
a reveni		to return
vineri		Friday
tot timpul		all the time
oferte ieftine		cheap offers
săptămâna viitoare		next week
avere		fortune
bunic		grandfather
a divorța		to divorce
bărbat		man
coleg		colleague
doamnă		lady, woman, madam
frate		brother
părinte		parent
rochie		dress
soție		wife
vitreg		step-
nuntă		wedding
poză		photo
serviciu		workplace
unchi		uncle
în stânga lor		on their left
soția lui		his wife
rochia ei		her dress
soțul ei		her husband
a ajunge		to arrive
clasă		class
a costa		to cost
desigur		of course
întâi		first
jumătate		half
a pleca		to leave
a prefera		to prefer
repede		fast, quickly
târziu		late
tren accelerat, accelerat		medium-fast train
viteză		speed
bilet		ticket
copil		child
a depinde		to depend
geam		window
întreg		whole
loc		seat, place
posibilitate		possibility
a rezerva		to book
a trebui		to have to (must)
tren rapid		fast train
tren		train
fix		sharp, exactly (used for time)
a aranja		to arrange
după		after
încotro		to where
a merge		to go
noapte		night
a picta		to paint
spre		towards
viitor		future, next
cameră		room
excursie		trip
mănăstire		monastery
oraș		town
săptămână		week
a vizita		to visit
o să iau		I will get
o să coste		it will cost
unde o să mergi?		Where will you (singular) go?
o să stau		I will stay
unde o să stai?		Where will you (singular) stay?
o să plec		I will leave
o să fac		I shall do
nord		north
sud		south
est		east
vest		west
la nord		to the north
în nord		in the north
aproximativ		approximately
deal		hill
a forma		to form
a (se) învecina		to border, to neighbour
a așeza		to lay
fluviu		big river
graniță		border, customs
lecție		lesson
locuitor		inhabitant
pătrat		square (shape)
regiune		region
a (se) situa		to situate
lungime		length
populație		population
șes		plain (geographical feature)
suprafață		surface
Anglia		England
Marea Britanie		Great Britain
Statele Unite (ale Americii)		USA
Rusia		Russia
Franța		France
Italia		Italy
Serbia		Serbia
Uniunea Europeană		The EU
Organizația Națiunilor Unite		The UN
Orientul Apropiat		The Middle East
Dunărea		Danube river
Mureșul		Mureș river
Nistrul		Dniester river
Prutul		Prut river
București		Bucharest
Londra		London
Moscova		Moscow
Paris		Paris
Roma		Rome
sută		hundred
sute		hundreds
mie		thousand
mii		thousands
milion		million
milioane		millions
virgulă		decimal point
furculiță		fork
lingură		spoon
cuțit		knife
farfurie		plate
oală		pan
șervețel		napkin
coș de pâine		bread basket
față de masă		table cloth
pahar de vin		glass of wine
tigaie		frying pan
apă		water
aperitiv		starter
a bea		to drink
cafea		coffee
ciorbă		broth
cub		cube
desert		dessert
a dori		to wish
dulce		sweet
fel		course (of a meal)
gheață		ice
a începe		to start
în schimb		instead
a lua		to take
a mânca		to eat
meniu		menu
piersică		peach
porție		portion
portocală		orange (fruit)
sec		dry
sfârșit		end
soție		wife
suc		juice
vin		wine
cafenea		coffee shop
a ajunge		to arrive
ciocolată		chocolate
colț		corner
înghețată		ice cream
masă		table
a mulțumi		to thank
prăjitură		cake slice
vanilie		vanilla
Aș...		I would...
Ai...		You (singular) would...
Ar...		S/he would...
Am...		We would...
Ați...		You (plural) would...
Ar...		They would...
Avem și înghețată și prăjituri		We have both ice cream and cakes
Și pentru mine tot o înghețată		And an ice cream for me too
Ea mai doarme		She is still asleep
Mai vrei o cafea?		Do you want another cup of coffee?
El este tot la gară		He is still at the station
Ea tot vorbește		She is still talking
Și el stă acolo		He lives there too
a afla		to find out
baie		bathroom
bilet		ticket
călătorie		journey
cazare		accommodation
a costa		to cost
deltă		delta
duș		shower
încălzire		heating
hotel		hotel
mâine		tomorrow
a întrerupe		to interupt
munte		mountain
mare		sea
a ști		to know
pat		bed
a suna		to sound, to ring
țară		country
telecabină		cablecar
telescaun		chairlift
valabil		valid
vânzător		shop assistant
agent imobiliar		estate agent
în general		generally
între timp		in the meantime
o întâlnire de afaceri		a business meeting
seara		in the evening
sub formă de		in the form of
dimineața		in the morning
în cer		in heaven
în special		especially, in particular
la țară		in the countryside
prognoza vremii		the weather forecast
ascultător		listener
a cădea		to fall
cald		warm
cer		sky
drag		dear
frumos		beautiful
lapoviță		sleet
a menține		to maintain
ninsoare		snow
a oscila		to oscillate
precipitație		precipitation
timp probabil		weather forecast
următor		next
ușor		light, easy
vânt		wind
variat		varied
vreme		weather
zi		day
voi...		I will...
vei...		you (singular) will...
va...		s/he will...
vom...		we will...
veți...		you (plural) will...
vor...		they will...
vremea va rămâne frumoasă		The weather will stay fine
e soare		It is sunny
e rece		It is cold
e frig		It is cold
e cald		It is warm
bate vântul		the wind is blowing
plouă		it is raining
ninge		it is snowing
afacere		business
aglomerat		crowded
alaltăieri		the day before yesterday
azi		today
deseară		tonight
director		manager
după-amiază		in the afternoon
după-masă		in the afternoon
imprimantă		printer
întâlnire		meeting
obosit		tired
poimâine		the day after tomorrow
a rămâne		to remain
săptămână		week
ședință		meeting
a vedea		to see
lunea		on Mondays
marțea		on Tuesdays
miercurea		on Wednesdays
joia		on Thursdays
vinerea		on Fridays
sâmbăta		on Saturdays
duminica		on Sundays
joia viitoare		next Thursday
joia trecută		last Thursday
primăvară		spring
vară		summer
toamnă		autumn
iarnă		winter
așa		this way
cec de călătorie		traveller's cheque
la fel		the same
scrisoare recomandată		registered letter
ca de obicei		as usual
în funcție de		according to
lire sterline		pounds sterling
curs de schimb		exchange rate
stați puțin		wait a bit
a alege		to choose
a colecționa		to collect
a cumpăra		to buy
ieftin		cheap
prieten		friend
scrisoare		letter
tarif		tariff
a trimite		to send
carte		book
colet		parcel
fiu		son
pod		bridge
preț		price
rapid		quickly
sigur		safe (adjective)
timbru		stamp
vedere		post card
a afișa		to display
a crede		to think, to believe
a încasa		to collect (money)
a schimba		to change
bancnotă		bank note
chitanță		receipt
pașaport		passport
a semna		to sign
cacao		cocoa
ceartă		quarrel
fiert		boiled
mereu		always
ceai		tea
fără		without
geantă		bag
lapte		milk
mic dejun		breakfast
moft		whim
pâine		bread
sculare		getting up
unt		butter
zăpăcit		absent-minded
ou		egg
a pleca		to leave
a ști		to know
unui		of/for/to something (m)
unei		of/for/to something (f)
unor		of/for/to somethings
arată bine		it looks good
fabricat în România		made in Romania
maro închis		dark brown
merge bine cu ceva		goes well with something
cabină de probă		changing room
dacă se poate		if possible
în spate		behind
maro deschis		light brown
carte de credit		credit card
casă		cashier
a asorta		to match (clothes)
călduros		warm
etaj		floor (storey, level)
fustă		skirt
gamă		range (noun)
a împacheta		to pack, wrap
a însemna		to mean
magazin		shop
mărime		size
nasture		button (clothes)
a plăti		to pay
prost		stupid
strâmt		tight
a căuta		to look for
cumpărătură		shopping
fular		thick scarf
galben		yellow
haine		clothes
în străinătate		from abroad
a merge		to go, to suit
pălărie		hat
pantaloni		trousers
palton		overcoat
a proba		to try on
pulover		jumper
pungă		carrier bag
bucată		piece
ceapă		onion
grădină		garden
morcov		carrot
seră		greenhouse
struguri		grapes
zemos		juicy
cartof		potato
măr		apple
negru		black
usturoi		garlic
altcândva		some other time
altceva		something else
altcineva		someone else
altundeva		somewhere else
cu siguranță		certainly
de bună seamă		surely
de loc		not at all
desigur		of course
gata		ready, it's over
hai să vedem		let's see
într-adevăr		indeed
la fel de		equally
nu-i așa		isn't that so
mai în jos		further down
nicidecum		no way
a arăta		to show, to point, to look
asemănare		similarity
bucătar		cook, chef
ceartă		quarrel
deget		finger
diferență		difference
hartă		map
locuitor		inhabitant
popor		(a) people
renumit		famous
țară		countryside
a vedea		to see
a consuma		to consume
a dori		to wish
încet		slowly
îndeaproape		close (position)
litru		litre
nevoie		need (noun)
rapid		quickly
ultimul		the last, latest
a costa		to cost
a dura		to last
încoace		this way, over here
a întreține		to maintain
mașină		car
puțin		little
viteză		speed
mă		me (object)
te		you (object)
îl		him (object)
o		her (object)
ne		us (object)
vă		you (plural, object)
îi		them (masculine, object)
le		them (feminine, object)
a se întreba		to wonder
bunic		grandfather
bunică		grandmother
mătușă		aunt
unchi		uncle
mamă		mother
tată		father
văr		cousin (male)
verișoară		cousin (female)
nepot		nephew
nepoată		niece
soră		sister
frate		brother
nepoată		grand-daughter
nepot		grandson
a explica		to explain
geamăn		twin
nevastă		wife
părinte		parent
poză		photo
prieten		friend
relație		relationship
tânăr		young
a se uita		to look
vecin		neighbour
cadou de adio		farewell present
ce drăguț ești		you are so nice
ce vrei să-ti iau		what do you want me to give you?
de câte ori		every time
de obicei		usually
ca de obicei		as usual
a face cumpăraturi		to go shopping
fără discuții		without any doubt
a fi cu griji		to have worries
a fi de cuvânt		to be trustworthy
a-ți fi ușor		to be easy for you
am luat un cadou		I brought a present
mă doare să văd că...		it hurts me to see that...
îmi convine		it suits me
pantofii nu îți vin bine		the shoes do not suit you
mă roade gândul să...		I was thinking very seriously of...
ne-am luat câte ceva		we each bought ourselves something
a se ține de cuvânt		to keep one's word
vai!		oh!
a intra în panică		to panic
degeaba		in vain
a se acomoda		to adjust
a ajunge		to arrive
a aprinde		to light
cadou		(a) present
a cheltui		to spend
colorat		coloured
cumpătat		moderate
farfurie		plate
a găsi		to find
încălțăminte		shoes, footwear
întoarcere		return (noun)
lut		clay
a-și permite		to afford
a plăti		to pay
a reuși		to manage (to do something)
suvenir		souvenir
adio		farewell
album		album
a se bucura		to be happy
cartier		estate, area
cumpărătură		purchase (noun)
a descoperi		to discover
discuție		discussion
frumusețe		beauty
a încerca		to try
lumânare		candle
pantof		shoe
piele		leather
raion		store department
îmi		to me
îți		to you (singular)
îi		to him
îi		to her
ne		to us
vă		to you (plural)
le		to them
ascultător		obedient
comportament		behaviour
a deranja		to bother
a se descurca		to manage
dirigintă		form teacher
a durea		to hurt
fiică		daughter
grijă		worry (noun)
încredere		trust (noun)
a se îngrijora		to worry
obosit		tired
obraznic		rude
răbdător		patient (adj)
sfat		advice
a supăra		to upset
pe mine		me (empathetic form)
pe tine		you (singular, empathetic form)
pe el		him (empathetic form)
pe ea		her (empathetic form)
pe noi		us (empathetic form)
pe voi		you (plural, empathetic form)
pe ei		them (masculine, empathetic form)
pe ele		them (feminine, empathetic form)
pe sine		himself, herself (empathetic form)
ceas		clock, watch
a cicăli		to nag
a coborî		to go down, to descend
discotecă		disco
a găsi		to find
a (se) grabi		to hurry
a se îmbrăca		to get dressed
a întâlni		to meet
a întârzia		to be late
jumătate		half
a pleca		to leave
a propune		to propose
a ruga		to ask, to beg, to pray
sfert		quarter
a suna		to ring
singur		alone
treabă		thing to do
vină		fault
așa stau lucrurile		that's how it is
atâta timp cât		as long as
a cere parerea		to ask for an opinion
de obicei		usually
a critica		to criticise
în curs de desfașurare		in progress
iți convine		it suits you
a o lua la întrebăre		to question her
a merge la lucru/serviciu/munca		to go to work
ore peste program		overtime
să ne gândim la cei de lângă		let us spare a thought for those close to us
sper sa fie așa		I hope so
a sta pe scaun		to sit down
a sta de vorba		to chat
a apostrofa		to tell off
a se concentra		to concentrate
a conta		to count
a se duce		to go
a întârzia		to be late
măcar		at least
odată		once
a se scula		to wake up
a se spăla		to wash oneself
uneori		sometimes
avocat		solicitor
a se conforma		to conform
a se culca		to go to bed
a se gândi		to think
lucrare		work (noun)
mereu		regularly
a se odihni		to rest
serviciu		work place
stație		stop/station
aventură		adventure
bilet		ticket
conflict		conflict
excursie		trip
film		film
ocupat		busy
părere		opinion
poveste		story
a se pricepe		to know
prietenie		friendship
a (se) raci		to cool, to get a cold
a reacționa		to react
a rezerva		to book
a relata		to relate
a sugera		to suggest
a simți		to feel
tocmai		just
a telefona		to phone
datorită		because of
mulțumită		thanks to
mie		to me (indirect)
ție		to you (singular, indirect)
lui		to him  (indirect)
ei		to her (indirect)
nouă		to us  (indirect)
vouă		to you (plural, indirect)
lor		to them  (indirect)
cina		dinner
a claxona		to toot
departe		far
dezvoltare		development
echipă		team
flatat		flattered
gălăgie		noise
a înghesui		to crowd
a ierta		to forgive
a invita		to invite
județ		county
munca		work
nervos		nervous
plin		full
a prinde		to catch
a reduce		to reduce
rudă		relative (noun)
a sărbători		to celebrate
sat		village
scaun		chair
șef		chief
târziu		late
urmator		next
vecin		neighbour
am pus-o pe seama alcoolului		I blamed it on the alcohol
aștept câteva rânduri de la tine		drop me a line
aștept un copil		I am expecting a child
casetofon cu căști		walkman
îmi dau seama		I realise
în cele din urmă		at last
mi-e greață		I feel sick
mi se pare		it seems to me
nu aveți voie să		you are not allowed to (formal)
poftiți vă rog!		please come in!
ți-a priit căldura		did the heat do you good?
să-mi faci o vizită		pay me a visit
să-mi ții pumnii!		keep your fingers crossed for me!
te pup		love from
mi-am dat seama		I realised
și ce dacă		so what
taci din gură		shut up
analgezic		painkiller
cap		head
ceafă		nape of the neck
chef		party (loud, with alcohol)
destindere		relaxation
doctor		doctor
a durea		to hurt
examen radiologic		x-ray
frunte		forehead
greață		sickness
inot		swimming (noun)
investigație		investigation
a se înzdrăveni		to get better
mineral		mineral
mușchi		muscle
omoplat		shoulder blade
a prescrie		to prescribe
a ridica		to get up, to go up
spate		back (noun)
a suferi		to suffer
trimitere		referral
vitamină		vitamin
mâncărime		itching
mușcătură		bite (noun)
tăietură		cut, wound (noun)
înțepătură		sting (noun)
bubă		boil (noun) (child word)
bașică		boil (noun)
alergie		allergy
mătreață		dandruff
a sângera		to bleed
a se scărpina		to scratch
a ustura		to sting
a tuși		to cough
a strănuta		to sneeze
a vomita		to vomit
a se tăia		to cut oneself
a se înțepa		to get stung
a se lovi		to hurt oneself
a șchiopăta		to limp
a asculta		to listen
bagaj		baggage
cartelă		card
a comanda		to order
curat		clean (adj)
a deranja		to disturb
gură		mouth
a se îndepărta		to move away
întrebare		question (noun)
portbagaj		car boot
regulă		rule
a umbla		to walk
urgență		emergency
autocar		coach (bus)
caz		case
comportament		behaviour
glumă		joke
grup		group
imprejurime		surrounding
ordonat		tidy
prost		stupid
supraveghetor		supervisor
zi!		say!  (singular)
fă!		do! (singular)
ia!		take! (singular)
vino!		come! (singular)
dă!		give! (singular)
ajutor		help/aid
bronzat		tanned
bucurie		happiness
curând		soon
ieftin		cheap
mare		sea
mod		way/manner
brățitară		bracelet
căldură		heat
cazare		accommodation
frică		fear
a îmbunătăți		to improve, to recover, to get better
a începe		to start
margine		edge
munte		mountain
prietenă		girlfriend
a-i prii		to do someone good/to suit
pumn		fist
a pupa		to kiss
rând		line, row
săptămână		week
sat		village
scrisoare		letter
sfârșit		end
a studia		to study
a transmite		to transmit
vară		summer
mod		way, manner
du-te		go! (singular)
a avut dreptate		s/he was right
cum să fie		how could it be...
de-a lungul peretelui		along the wall
de lemn		of wood
de tot felul		all sorts
din afara curții		from outside the yard
dintr-o dată		suddenly
gris deschis		light grey
gris inchis		dark grey
îmi aduc aminte		I remember
îmi aduci aminti		You remind me
în fugă		running, quickly
în jur		around
în mijlocul		in the middle of
în spatele meu		behind me
întuneric beznă		pitch dark
îți vine să crezi?		can you believe it?
la un moment dat		at some point
le-am pierdut șirul		I lost track of them
mi s-a făcut frică		I got scared
nu va crește mare		s/he won't grow up
pe gratis		for free
pe rând		in turns
s-a făcut întuneric		it got dark
să-și facă bagajele		to pack her bags
să prindă sens		to make sense
se făcea		it happened
sunt convins		I am sure
te-am strigat pe nume		I called your name
trei sute de ani vechime		three hundred years old
țin minte		I remember
un joc de noroc		a gambling game
a asculta		to listen
ceas		clock, watch
bibliotecă		library
companie		company
a crește		to grow
eliberare		liberation
generozitate		generosity
genial		genial
grădiniță		nursery
grozav		great
a hotărî		to decide
a împărți		to divide
îngrozitor		terrible
iubită		girlfriend
laudă		praise (noun)
a locui		to live
obiect		object (noun)
păpușă		doll
porțelan		porcelain
a părăsi		to leave (permanently)
a purta		to wear
raft		shelf
rablă		wreck (noun)
relație		relationship
ratat		failed (adj)
replică		reply (noun)
scump		expensive
a sărbători		to celebrate
a termina		to finish
sindrofie		party, reunion
vechi		old
a umple		to fill
al meu		mine (singular, masculine)
a mea		mine (singular, feminine)
ai mei		mine (plural, masculine)
ale mele		mine (plural, feminine)
Rafturile de bibliotecă sunt ale mele		The bookshelves are mine
Cărțile astea sunt ale mele		These books are mine
Banii ăștia sunt ai mei		This money is mine
Acestea sunt ale lui		These things are his
Un bilet de-al meu		A ticket of mine
O rudă de-a mea		A relative of mine
cele zece porunci		the Ten Commandments
a asalta		to attack
bisericuță		little church
dornic		eager
grădină		garden
județ		county
mănăstire		monastery
masă		meal, table
mural		of a wall
memorie		memory
neapărat		by all means
om		person
pictură		painting
a pierde		to lose
plimbare		walk (noun)
primitor		hospitable
a renova		to renovate
sătean		villager
reținut		reserved (adj)
sătuc		hamlet
stare		state
șters		faded (adj)
străin		foreigner, stranger
tur		tour
vacanță		holiday
vorbăreț		talkative
zarzavat		greens, vegetable
zonă		zone, area
ai fi fost fericit		you would have been happy
ai fi fost prost să...		you would have been silly to...
ero mai bine dacă nu se ducea acolo		it would have been better if he had not gone there
ambulanță		ambulance
a se apropia		to come closer
a așeza		to lay
ascuțit		sharp
aur		gold
bătrân		old, elderly
beznă		pitch dark
bolnav		ill
cârlionți		hair curls
cizme de cauciuc		wellingtons
a conduce		to lead, to drive
copac		tree
copilărie		childhood
curte		yard
cutie		box
deal		hill
furtună		storm
gol		empty
a ămbrățișa		to hug
înger		angel
încercare		attempt (noun)
îngust		narrow
întunecos		dark (adj)
întuneric		darkness
joc		game
a se juca		to play
a lovi		to hit
medic		doctor
medicament		medicine
necunoscut		unknown
nenorocit		doomed
noroc		luck
oraș		town
pelerină de ploaie		raincoat
piatră		stone
a plânge		to cry
poezie		poem
a râde		to laugh
a recunoaște		to recognise
a rupe		to tear, to break
a striga		to call, to shout, to cry
strigăt		cry (noun)
surmenat		exhausted (adj)
uimire		amazement
a uita		to forget
a zăcea		to lie in bed
a zări		to catch sight
zbârcit		wrinkled
așa merge		is it ok like this?
cică		they say (very colloquial)
cu suflet		kind
cu cât dați ceapa?		How much do you sell the onions for?
a da		to give, to sell
ești de acord?		do you agree
douășcinci		twenty five (colloquial)
făcu ce făcu		he did what he could
fără îndoială		without doubt
în ultimul timp		recently
la mine		on me, at my place
lăsați din preț		lower the price
a lua		to take, to sell
mă descurc		I manage
n-avem ce să facem		there is nothing we can do
nu după mult timp		not long after
nu prea		not quite
preț redus		discount
sunt prea sigură de mine		I am too self confident
te pierzi imediat		you get flustered quickly
treacă de la mine		I will let it pass
uitați banii		here is the money
ultima dată		last time
bunicuță		dear grandma
du-te sănătos		go in peace
fugi cât îl ținură picioarele de repede		he ran as quickly as he could
gata să o înghită		ready to swallow her up
hai să o luăm ușor spre peron		let's start walking slowing to the platform
înainte de culcare		before going to bed
Ea s-a ajuns		She has arrived
nepoțică		dear granddaughter
să țipe după ajutor		to call for help
au scăpat cu viață		they remained alive
Scufița Roșie		Little Red Riding Hood
sentiment prevestitor de rău		ominous feeling
altădată		another time
cumplit		awful
fricos		faint-hearted
întârziere		delay
înzăpezit		snowbound
a se însura		to get married (males only)
lapte		milk
linguriță		teaspoon
modă		fashion
radical		square root
a recupera		to recuperate
peron		platform
ureche		ear
zahăr		sugar
adânc		deep
ajutor		help/aid
albastru		blue
a atinge		to touch
bolnav		sick (adj)
a bate		to beat, to knock
căscat		wide open
a ciocăni		to knock, to hammer
destul		enough
dinăuntru		from inside
gând		thought (noun)
gură		mouth
a îmbrățișa		to hug
a înghiți		to swallow
a întâmpina		to welcom
iubitor		loving (adj)
lăcomie		greed
lemn		piece of wood
lovitură		blow (noun)
lup		wolf
merinde		provisions, food
nevătămat		unharmed
ochi		eye
a păcăli		to fool
pădure		wood, forest
pătură		blanket
poveste		story, tale
sentiment		feeling, sentiment
prevestitor		foreboding
a spinteca		to tear, to rip
subțire		thin
tare		loud, hard
a ține		to hold, to last
teafăr		sound, safe, all right
topor		axe
a țipa		to shout
ardei		pepper
cămin		student's hall
cartof		potato
cârnaț		sausage
ceapă		onion
client		client, customer
a îndrăzni		to dare
provizie		supply (noun)
pungă		plastic bag
redus		reduced (adj)
uite!, uitați!		here!
usturoi		garlic
vânătă		aubergine
L-am văzut pe Bob		I saw Bob
are dreptate		he is right
aș face bine la examene		I would do well in the exams
au luat colțul		they turned the corner
dintr-o dată		suddenly
a face ochi dulci		to make eyes (at somebody)
i-ați pierdut din vedere		you lost sight of them
în jurul lumii		around the world
în legătură cu		regarding
minunile lumii		the wonders of the world
n-am ce zice		no doubt about it
nu fii așa de sigur		don't be so sure
nu știm ce va fi până la urmă		we don't know what is going to happen in the end
nu vă scapă nimic		you don't miss a trick
oricine ar face ca mine		anyone would do it like this
pe vremuri		in the old days
stație de poliție		police station
sunt de acord		I agree
te bagi ca musca-n lapte		you stick your nose in where it doesn't belong
tu ce ai de zis?		what do you have to say?
adineauri		earlier
ameliorare		improvement
anume		particular, certain
a audia		to hear someone
bej		beige
bluză		blouse
bursă		scholarship
cumsecade		kind-hearted
furt		theft
glugă		hood
hoț		thief
martor		witness
memorie		memory
perspicace		shrewd
polițist		policeman
portmoneu		wallet, purse
a prinde		to catch
pulover		jumper
a relua		to start again
vernil		light green
vârstă		age
viață		life
șic		chic, elegant
pe cine ați văzut exact?		whom did you see exactly?
pălăia pe care o ai este a mea		the hat that you have is mine
a propos		by the way
amic		friend, acquaintance
aniversare		anniversary
antipatic		unlikeable
aparență		appearance
bârfă		gossip
crem		cream, off white
a economisi		to save up
fiecare		each, every
gras		fat
gri		grey
a judeca		to judge
liceu		secondary school
a lungi		to extend
maro		brown
material		fabric
nasol		ugly
a-și permite		to allow, to afford
oaspete		guest (longer stay)
roz		pink
petrecere		party
șarmant		charming
vizavi		opposite
a alege		to choose
a angaja		to employ
a amâna		to postpone
antic		ancient, antique
a se apropia		to come closer
atroce		atrocious
căsătorie		marriage
civilizație		civilisation
datorie		debt
declatație		statement, declaration
a discuta		to talk, to discuss
gazdă		host
informatică		information technology
a se însănătoși		to get better
minune		wonder (noun)
a munci		to work
a obține		to obtain
a opta		to opt
plan		plan
a se preface		to pretend
a prefera		to prefer
secție		department, section
simț		sense
a spera		to hope
a stârni		to arouse
studenție		university years
subînțeles		inference
tramvai		tram
vacanță		holiday
ai grijă de tine		look after yourself
când ești mare		when you grow up
de altfel		by the way
înaintea celui de-al doilea război mondial		before the Second World War
handbal de performanță		professional handball
ieșită din comun		out of the ordinary
în nici un caz		no way, under no circumstances
le surâde ideea		they would like to...
mă pierdusem		I got flustered
mi-e foarte frică		I am very scared
niciunde altundeva		nowhere else
sportiv de performanță		professional sportsman
vă doresc succes		I wish you good luck
agil		agile
a alerga		to run
amândoi		both (m)
amândouă		both (f)
antrenament		training
antrenat		trained (adj)
baschet		basketball
campionat		championship
carieră		career
coș		basket
a deveni		to become
handbal		handball
a se îmbolnăvi		to fall ill
inot		swimming (noun)
îndrăgostit		in love (adj)
a întinde		to stretch
junior		junior
perspicace		canny
portar		goalkeeper
pregătit		prepared (adj)
rapiditate		rapidity
rezistent		lasting, enduring (adj)
renumit		renowned, famous
a sări		to jump
a surâde		to smile
succes		success
tenis		tennis
volei		volleyball
a ceda		to give up
celălalt		the other
a se comporta		to behave
a se culca		to go to bed
a culca		to put to bed
a (se) muta		to move
nepot		nephew, grandson
furtunos		stormy
nepoată		niece, grand-daughter
orice		whatever
oriunde		wherever
oricum		anyhow
opțiune		option
piesă		play (noun)
prostie		foolishness
șmecher		crafty person
teatru		theatre
a angaja		to employ
aritmetică		arithmetic
a aprofunda		to study in detail
cuminte		well-behaved
copilărie		childhood
a se despărți		a separate
doică		nanny
elev		schoolboy
fotbal		football
fragment		fragment, excerpt
frică		fear
greșeală		mistake
grădință		nursery
întoarcere		return (noun)
a învăța ceva		to learn something
a învăța pe cineva		to teach someone
învățătură		learning, study
istorie		history
mătușă		aunt
jucător		player
a memora		to memorise
meci		match (eg football)
a minți		to lie
minciună		lie (noun)
mondial		global, worldwide
morală		scorn
pace		peace
păcălit		fooled (adj)
performanță		performance
război		war
poezie		poem
a publica		to publish
a recita		to recite
recital		recital
roman		novel
sat		village
scenă		stage
silabă		syllable
spectacol		show (noun)
tehnică		technique
uriaș		vast, immense
bilete dus-întors		return tickets
carnet de student		student card
ciorbă de burtă		tripe soup
frânți de oboseală		totally exhausted
înainte de vreme		before time, earlier
la îndemână		handy
se bagă toți în față		they all push in front
a sta de vorba		to chat
zis și făcut		at the double
tren accelerat, accelerat		medium-fast train
căldură		heat (noun)
coadă		queue, tail
dezordine		untidyness
ghișeu		counter
grabă		hurry (noun)
a împinge		to push
răbdare		patience
reducere		discount
rezervare		reservation
a suporta		to bear, to stand
se vede că ea este bolnavă		it is obvious that she is ill
se spune că Roma este un oraș frumos		Rome is said so be a beautiful city
s-a deschis ușa		the door opened
s-a trimis faxul		the fax was sent
s-a spart gheața		the ice was broken
ne-am înâlnit la gară		we met each other at the station
s-au văzut la hotel		they saw each other at the hotel
primul		first (m)
prima		first (f)
al doilea		second (m)
a doua		second (f)
al treilea		third (m)
a treia		third (f)
coș		chimney
acoperiș		roof
fereastră		window
ușă		door
dormitor		bedroom
baie		bathroom
WC		toilet
hol		hall
parter		ground floor
etajul întâi		first floor
bucătărie		kitchen
sufragerie		dining room
camera de zi		living room
cadă		bathtub
cactus		cactus
canapea		sofa, couch
cămară		larder
a construi		to build
fier		iron
gard		fence
întâlnire		meeting
intrare		entrance
marmură		marble
mușafir		guest (shorter stay)
primitor		welcoming (adj)
schimbare		change (noun)
terasă		terrace
cort		tent
rucsac		rucksack
lanternă		torch
sac de dormit		sleeping bag
bocanci		boots
hartă		map
busolă		compass
a ataca		to attack
ciorbă		broth
cabană		mountain lodge
creastă		crest
foame		hunger
a încălzi		to warm up
întâmplare		event
a întuneca		to get dark
a leșina		to faint
oboseală		tiredness
a se panica		to panic
ploaie		rain
răsărit		sun rise
a risca		to risk
urs		bear (noun)
abia împlinise cincisprezece ani		she had just turned fifteen
în ciuda faptului că		despite the fact that
încântat de cunoștință		glad to meet you
în viață		alive
mor după o bere rece		I am dying for a cool beer
chef		party (loud, with alcohol)
curs		lecture
facultate		college
fumat		smoking
lucrare		work of art
pensulă		paintbrush
pictură		painting
șevalet		easel
vopsea		paint
s-ar putea să vină		S/he might come
s-ar putea să am banii		I may have the money
o fi venit cineva azi?		Did someone come today?
oare nu ne-am mai întâlnit undeva?		havențt we met somewhere before?
A plecat așa de repede încât și-a uitat portofelul acasă		He left so quickly that he forgot his wallet at home
atât de încât...		so much that...
apus		sunset
despărțire		separation
brusc		sudden
a se întrista		to get upset
împărat		emperor
a se înveseli		to cheer up
a se liniști		to calm down
necăjit		upset (adj)
a rapi		to kidnap
a retrage		to withdraw
cablu		cable
cartuș		cartridge
cerneală		ink
fotocopiator		photocopier
husă		cover
imprimantă		printer
încredere		trust (noun)
monitor		monitor
mașină de fax		fax machine
praf		dust
piesă de schimb		spare parts
programare		computer programming
a repara		to fix, to mend
șoricel		mouse (computer)
tastatură		keyboard
utilizare		computer operati