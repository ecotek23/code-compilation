переезжать		to move (house)
продолжать		to keep (doing); to still (do); to continue
уйти на пенсию		to retire
увольняться		to quit (job)
ехать кататься на лыжах		to go skiing
за границей		abroad
ехать за границу		to go abroad
до сих пор		still (to this day)
неплохой		not bad
примерно		about; approximately
вместе		together
обычно		usually
по		on; by; along
по понедельникам		on Mondays
через		after; through
через два часа		in two hours
расскажи мне о своей семье		tell me about your family
обычно мы ездим за границу зимой		we usually go abroad in the winter
у неё была прекрасная карьера пять лет назад		she had a fantastic career five years ago
мои родители уйдут на пенсию примерно через три года		my parents will retire in about three years
мои дедушка и бабушка до сих пор ездят кататься на лыжах		my grandpa and grandma still go skiing
мы продолжаем вместе играть в бадминтон по средам		we still play badminton together on Wednesdays
север		north
юг		south
восток		east
запад		west
Америка		America
Северная Америка		North America
Южная Америка		South America
Европа		Europe
Африка		Africa
Азия		Asia
Австралия		Australia
Антарктика		Antarctica
отпуск		holiday; vacation
место		place; site; space; seat
впечатление		experience (event); impression
фестиваль		festival
поездка на автобусе		bus trip
хостел		hostel
экскурсовод		tour guide
парашютный спорт		parachuting
ходить в поход		to hike
замечательный		remarkable
знаменитый		famous
самый		most
самый смешной		funniest
страшный		scary; ugly
самый страшный		scariest; ugliest
странный		strange
самый странный		strangest
плохой		bad
хуже		worse
худший		worst
лучше		better
лучший		best
яркий		bright; vivid
определённо		definitely; clearly; certainly
пройти		to pass
как прошёл твой отпуск?		how was your holiday?
почему ты поехал в Европу?		why did you go to Europe?
... потому что это очень интересное место		... because it's such an interesting place
какое впечатление у тебя было самым ярким?		what was your best experience?
нам очень понравился парашютный спорт		we really liked parachuting
индийский ресторан определённо был лучшим		the Indian restaurant was definitely the best one
президент		president
министр		minister
премьер-министр		prime minister
король		king
королева		queen
царь		tsar
царица		tsarina
политик		politician
правительство		government
лидер		leader
власть		power; authority
партия		party (political)
социалистический		socialist
социалист; социалистка		socialist (the person)
консервативный		conservative
республиканский		republican
республиканец; республиканка		republican (the person)
демократический		democratic
демократ; демократка		democrat
коммунистический		communist
коммунист; коммунистка		communist (the person)
Республиканская партия		the Republican Party
Демократическая партия		the Democratic party
Дума		Duma
великолепный		excellent
кто был первым президентом Соединённых Штатов?		who was the first president of the United States?
первым президентом Соединённых Штатов был ...		the first president of the United States was ...
есть ли в России царь?		is there a tsar in Russia?
в России нет царя		there is no tsar in Russia
у правительства нет большой власти		the government doesn't have a lot of power
Демократическая партия достаточно большая		the Democratic Party is pretty big
Президент - великолепный лидер		the President is an excellent leader
уверенный		certain; confident; sure
ты уверен?		are you certain?
окончательный		final
это моё окончательное предложение		this is my final offer
договорились!		it's a deal!
шаг		step
каким будет наш следующий шаг?		what’s the next step?
в каком смысле?		in what sense?
простой		simple; easy
это не так просто		it isn't that simple
сложный		hard; difficult; complicated
насколько		how; as far as
насколько сложно это может быть?		how hard can it be?
мнение		opinion
какое у тебя мнение?		what’s your opinion?
дело		affair; doing; concern
это не твоё дело		this is none of your concern
шутка		joke
велосипед		bike
уметь хорошо ...		to be good at ... (doing something)
способность к ...		ability to ... ; talent to ... (a discipline)
плохо уметь ...		to be bad at ...
ехать на велосипеде		to ride a bike
инструмент		instrument
гитара		guitar
играть на гитаре		to play the guitar
пианино		piano
играть на пианино		to play the piano
краска		paint
готовить		to cook; to prepare
нарисовать		to draw; to paint
слушать		to listen
иностранный		foreign
опасный		dangerous
особенный		special; particular
пример		example
например		for example
например, у них способности к математике		they're for example pretty good at maths
они хорошо умеют играть на пианино		they're good at playing the piano
она плохо умеет играть на гитаре		she's bad at playing the guitar
у неё особенные способности к пению		she's especially good at singing
я особенно плохо умею рассказывать анекдоты		I'm especially bad at telling jokes
я думаю, математика сложна		I think maths is difficult
фотография		photo
картина		picture; image
видео		video
сувенир		souvenir
деревня		village
остров		island
буфет		buffet
услышать		to hear
показывать		to show
не нужно, пожалуйста, ...		please don't ...
удалённый		remote (far away)
труднодоступный		remote (difficult to get to)
невероятный		incredible; unbelievable
опять		again
эти		these
те		those
ребята, хотите посмотреть наши фотографии?		guys, do you want to see our photos?
это моя фотография		this is a photo of me
мы ездили в удалённую деревню		we went to a remote village
я хочу опять услышать ту историю		I want to hear that story again
покажи нам опять то видео		show us that video again
не нужно, пожалуйста, опять показывать нам те фотографии		please don't show us those pictures again
война		war
дискуссия		discussion
закон		law
государство		state
экономика		economy
налоги		taxes
армия		army
военные власти		military
генерал		general
солдат		soldier
народ		people (of a nation)
обещание		promise
обещать		to promise; to make a promise
изменить		to change; to cheat
понизить		to lower
поднять		to raise
контролировать		to control
вечно		always; forever
война не нужна никогда		war is never necessary
народ хочет мира		the people want peace
политики вечно обещают изменить закон		politicians always promise to change the law
вообще-то политики ничего не контролируют		politicians actually control nothing
небо		sky
луна		moon
солнце		sun
солнечно		sunny
дождь		rain
дождь идёт		it rains
дождевик		raincoat
снег		snow
снег идёт		it snows
снеговик		snowman
слепить		to sculpt; to make a sculpture
градус		degree
купальник		swimsuit
одеть ...		to put on ...
снять ...		to take off ...
влажный		wet
сухой		dry
промокнуть		to get wet
холодно		(to be) cold
жарко		(to be) hot
на улице		outdoors; on the street
идти на улицу		to go outside
идти внутрь		to go inside
если		if
тогда		then (solution)
ты промокнешь		you'll get wet
если идёт дождь, ты промокнешь		if it rains you'll get wet
мне слишком жарко		I'm too hot
тогда сними своё пальто		then take off your coat
если идёт снег, мы можем слепить снеговика		if it snows we can make a snowman
дождь закончился, поэтому мы пошли на улицу		the rain stopped so we went outside
команда		team
игрок		player
менеджер		manager
стадион		stadium
век		century
матч		match
кубок мира		world cup
гол		goal (sports)
самый большой		biggest
игрок матча		man of the match
стать		to get; to become
талантливый		talented
развлекательный		entertaining
популярный		popular
баскетбол - самый популярный вид спорта		basketball is the most popular sport
это матч века		this is the match of the century
он очень талантливый игрок		he's a very talented player
он стал игроком матча		he was man of the match
он - игрок года		he's the player of the year
это самый большой стадион в Европе		this is the biggest stadium in Europe
гола не было!		that wasn't a goal!
теперь		now
момент		moment
в данный момент ...		at the moment ...
новичок		beginner
везти		to be lucky
новичкам везёт		beginner's luck
приметить		to notice
слона-то я и не приметил!		there is an elephant in the room
картинка		little picture
как с картинки		picture-perfect
капля		a drop
как две капли воды		the spitting image
в мгновение ока		in the blink of an eye
ни ... , ни ...		neither ... , nor ...
пух		down (on a chick)
перо		feather
ни пуха, ни пера!		break a leg!
заболевание		disease; illness
порез		cut
инфаркт		heart attack
грипп		(the) flu
лежать		to lay (oneself) down
лечь		to lie down
присесть		to sit (oneself) down
сесть		to sit
стоять		to stand
встать		to stand up
осмотреть		to examine
выздороветь		to recover
умереть		to die
редкий		rare
распространённый		common; widespread
беременный		pregnant
пострадавший		injured
повреждение		injury; damage
мёртвый		dead
пожалуйста, присядьте		please sit down
мне нужно осмотреть вашу грудь		I have to examine your chest
у вас редкое заболевание		you have a rare disease
ваша жена беременна		your wife is pregnant
вы не умрёте		you aren't going to die
бизнес		business
в молодости		in youth; when ... young
учить		to teach
успешный		successful
раньше		before
... а теперь нет		... but not anymore
мы раньше жили за границей		we used to live abroad
у нас был свой бизнес, а теперь нет		we used to have our own business, but not anymore
мои бабушка и дедушка ездили в Африку в молодости		my grandpa and grandma went to Africa when they were young
раньше мы жили в Америке, но переехали		we used to live in America, but we moved
природа		nature
лес		forest
тропический лес		rainforest; tropical forest
джунгли		jungle
вертолёт		helicopter
перерыв		break
рюкзак		backpack
спальный мешок		sleeping bag
зубная щётка		toothbrush
мороженое		ice cream
берег		coast; shore
уронить		to drop (something)
бояться		to be afraid; to be scared
смеяться		to laugh
плакать		to cry
принести		to bring
просыпаться		to wake (oneself) up
выбрать		to pick; to choose
ползать		to climb (upwards); to crawl
взбираться		to climb (a mountain)
чинить		to repair; to fix
спящий		asleep
удивлённый		surprised
разочарованный		disappointed
взволнованный		worried
испуганный		scared
восторженный		excited
нервничать		to be nervous
вдруг		suddenly
около		by; near
над		over
он был немного разочарован		he was a little disappointed
выбери своё любимое животное		pick your favourite animal
вдруг мне стало очень страшно		suddenly I got very scared
мне стало так страшно, что я уронил мороженое		I got so scared I dropped my ice cream
я не взял с собой свою зубную щётку		I didn't bring my toothbrush (said by a man)
мы ездили в милую гостиницу на берегу моря		we went to a nice hotel by the sea
выборы		elections
кандидат		candidate
дебаты		debate
опрос		poll; inquiry
результат		result
голос		vote; voice
голосовать		to vote
религия		religion
окружающая среда		(natural) environment
система		system
система здравоохранения		health care system
предлагать		to suggest; to offer
увеличить		to increase; to rise
заставлять		to make (someone do something); to force
положиться на ...		to rely (oneself) on ...
ненадёжен		unreliable
важный		important
часто		often
когда следующие выборы?		when is the next election?
следующие выборы через два года		the next election is in two years
ты считаешь, на президента можно положиться?		do you think one can rely on the President?
я считаю, опросы часто ненадёжны		I think polls are often unreliable
я предлагаю понизить налоги		I suggest lowering taxes
религия - важная тема		religion is an important topic
дебаты об экономике заставляют республиканцев нервничать		debates about the economy make the Republicans nervous
в этом году Демократическая партия покажет лучшие результаты		this year the Democratic Party will get better results
буря		storm
гроза		thunderstorm
ветер		wind
ветреный		windy
туман		fog
туманный		foggy
облако		cloud
облачный		cloudy
молния		lightning
ливень		shower (rain)
риск		risk
риск ...		(a) risk of ...
шанс		chance
шанс, что ...		(a) chance that ...
прогноз		prognosis; forecast
прогноз погоды		weather forecast
ясный		clear
проясняться		to clear (itself) up
возможность		opportunity; ability; chance
существовать		to exist
существует		(there) exists; there is
всегда читай прогноз погоды		always read the weather forecast
существует риск того, что будет гроза		there is a risk that there will be lightning
есть возможность того, что сегодня будет солнечно		there is a chance that it'll be sunny today
на западе будут ливни		there will be showers out west
возможно, на юге погода прояснится		the weather might clear up down south
возможно, сегодня вечером будет идти снег		it might snow this evening
если он не принесёт зонт, он промокнет		if he doesn't bring his umbrella he'll get wet
успех		success
абсолютный		total; absolute
это было абсолютным успехом!		this was a total success!
контроль		control
всё под контролем		everything is under control
выйти		to exit; to leave; to escape
из-под		from under
это вышло из-под контроля		it was out of control
нужда		need
нет нужды		(there's) no need
слово		word
у меня нет слов		I have no words
так оно и есть		that's just the way it is
кто ещё?		who else?
я люблю кого-то другого		I love someone else
пара		couple; pair
пустяк		triffle
пара пустяков		it's a piece of cake
тайм		half (in sports match)
радио		radio
судья		referee; judge
идиот; идиотка		idiot
интервью		interview (with the press)
тренер		coach; trainer
очко		point; score
выиграть		to win
терять		to lose
концентрироваться		to concentrate (oneself); to focus (oneself)
заработать		to gain; to earn
набирать очки		to score
пустой		empty
полный		full
неудачный		unfortunate
впереди		ahead
к сожалению		unfortunately; regrettably
полностью		completely; entirely
нет свободных туалетов		(there are) no toilets available
к сожалению, не было свободных мест		unfortunately there were no seats available
что за идиот!		what an idiot!
мы не заработали очков в первом тайме		we didn't score any points in the first half
я слышал результаты по радио		I heard the results over the radio (said by a man)
сейчас они на шесть очков впереди нас		now they're six points ahead
она выиграла, потому что была лучше		she won because she was better
хорошая игра		good game
хорошо сыграли!		well played!
ужасный		awful
разочаровывающий		disappointing
ошибка		mistake
совершить ошибку		to make a mistake
ругаться		to argue; to swear
простить		to forgive
кричать		to yell; to shout
пугать		to scare
расстроить		to upset
казаться		to seem
причинить ... боль		to hurt (someone; something)
мне кажется ...		it seems to me ... ; I think ...
несомненно		obviously
я совершил ошибку		I made a mistake (said by a man)
ты причинил ему боль		you hurt him
она несомненно совершила ошибку		she obviously made a mistake
он не имел этого в виду		he didn't mean it
она не хотела причинить тебе боль		she didn't mean to hurt you
прости меня		forgive me
я уверен, всё будет хорошо		I'm sure everything will be okay (said by a man)
член		member
фан-клуб		fan club
спортивный клуб		sports club
сезон		season
соревнование		tournament
лига		league
первенство		championship
начинаться		to begin (oneself); to start (oneself)
ничья		draw (equal points)
болеть за команду		to support a team
закончить		to finish; to complete; to end
сезон всегда начинается в августе		the season always begins in August
я хочу стать членом фан-клуба		I want to become a member of the fan club
если они выиграют этот матч, они выиграют и лигу		if they win this match they'll also win the league
за какую команду ты болеешь?		which team do you suppor