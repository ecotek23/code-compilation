el médico, the doctor
el enfermero, nurse (masc.)
el paramédico, paramedic
la fecha de nacimiento, date of birth
¿Cuánto pesa?, How much do you weigh? (formal)
las libras, pounds
¿Cuál es su altura?, How tall are you? (formal)
Acuéstese., Lie down. (formal)
Tome asiento., Take a seat. (formal)
¿Qué le trae por aquí hoy?, What brings you in today? (formal)
la alergia, allergy
la reacción, reaction
el medicamento, medicine; drug (not medicina; droga or farmaco)
¿Qué le duele?, What hurts? (formal)
¿Tiene algún dolor?, Do you have any pain? (formal)
la sala de emergencia, emergency room
la ambulancia, ambulance
la jeringa, syringe
¿Está embarazada?, Are you pregnant? (formal)
No se mueva., Don't move. (formal)
golpearse la cabeza, to hit one's head
perder la consciencia, to lose consciousness
inconsciente, unconscious
el cinturón de seguridad, seat belt
la conmoción cerebral, concussion
el accidente (automovilístico), car crash
la caída, fall (instance of falling)
caerse, to fall down
quemarse, to burn oneself
el trauma, trauma
la cabeza, head
el cerebro, brain
los ojos, eyes
la nariz, nose
la boca, mouth
los dientes, teeth
el cuello, neck; collar
el pecho, chest (on body)
el seno, breast
el estómago, stomach
el abdomen, abdomen
la espalda, back (anatomy)
el hombro, shoulder
el brazo, arm
el codo, elbow
la muñeca, wrist
los dedos, fingers
las piernas, legs
la rodilla, knee
el tobillo, ankle
los pies, feet
la cadera, hip
los dedos de pie, toes
el corazón, heart
los pulmones, lungs
el esófago, esophagus
el duodeno, duodenum (first part of small intestine)
el yeyuno, jejunum (second part of small intestine)
el íleon, ileum (third part of small intestine)
el intestino delgado, small intestine
el intestino grueso, large intestine
el páncreas, pancreas
el hígado, liver
la vesícula (biliar), gallbladder
los riñones, kidneys
la vejiga, bladder
la matriz, womb/uterus
la próstata, prostate
el pene, penis
el testículo, testicle
la vagina, vagina
el hueso, bone
las costillas, ribs
la espina dorsal, spine
la columna vertebral, vertebral column
el ataque al corazón, heart attack
la insuficiencia cardíaca, heart failure
la presión alta, high blood pressure / hypertension
el derrame cerebral, stroke
la epilepsia, epilepsy
la demencia, dementia
la diabetes, diabetes
el hipotiroidismo, hypothyroidism
la insuficiencia renal, kidney failure
la diálisis, dialysis
la cirrosis, cirrhosis
el cáncer, cancer
el colesterol, cholesterol
la tuberculosis, tuberculosis
el enfisema, emphysema
el asma, asthma
el SIDA, AIDS
el coágulo de sangre, blood clot
fumar, to smoke
beber alcohol, to drink alcohol
¿Toma drogas?, Do you take drugs? (formal)
el trabajo, job
la mascota, pet
viajar, to travel
¿Cuánto tiempo ha fumado?, How long have you smoked? (formal)
¿Cuántos cigarrillos fuma al día?, How many cigarettes do you smoke a day? (formal)
desempleado, unemployed
la heroína, heroin
la cocaína, cocaine
el alcohol, alcohol
la marihuana, marijuana
la cárcel, jail
sin techo, homeless
el asilo de ancianos, retirement home; nursing home
sexualmente activo, sexually active
la enfermedad de transmisión sexual (ETS), sexually transmitted disease (STD)
el anticonceptivo, contraceptive
el condón, condom
el dispositivo intrauterino (DIU), intrauterine device (IUD)
la vasectomía, vasectomy
la ligadura de trompas, tubal ligation
la píldora, the pill (contraceptive)
la adrenalina, adrenaline/epinephrine
el analgésico, painkiller
el antiácido, antacid
el antibiótico, antibiotic
el antidepresivo, antidepressant
el antihistamínico, antihistamine
la aspirina, aspirin
la cápsula, capsule
el antigripal, cold relief medicine
el jarabe para la tos, cough syrup
la crema, cream
el descongestionante, decongestant
el diurético, diuretic
las gotas, drops
el inhalador, inhaler (for asthma)
la inyección, injection
la insulina, insulin
intravenoso, intravenous
el líquido, liquid
el laxante, laxative
el ungüento, ointment
el parche, patch
la penicilina, penicillin
la pastilla, pill
el esteroide, steroid
el jarabe, syrup (e.g. cough mixture)
la tableta, tablet
el sedante, sedative
la fiebre, fever
el mareo, dizziness
marearse, to feel dizzy
bajar de peso, weight loss
los escalofríos, chills; shivers
sudar, to sweat
la alteración del sueño, sleep disturbance
la comezón, itch (irritation)
el apetito, appetite
el malestar, malaise/discomfort
el letargo, lethargy
la mialgia, myalgia
el dolor de cabeza, headache
lagrimear, to water/fill with tears (eyes)
los cambios visuales, visual changes
la hipersensibilidad a la luz, hypersensitivity to light
la congestión nasal, nasal congestion
el sangrado nasal, nosebleed (epistaxis)
el acúfeno, tinnitus
oír, to hear
el vértigo, vertigo
el dolor de garganta, sore throat
el dolor de pecho, chest pain
las palpitaciones, palpitations
la falta de aire, shortness of breath
la hinchazón, swelling
hinchado, swollen
la tos, a cough
la flema, phlegm
las sibilancias, wheezing
la tos productiva, productive cough
el esputo, sputum
el dolor abdominal, abdominal pain
la náusea, nausea
vomitar, to vomit
los cólicos abdominales, abdominal cramps
tragar, to swallow
la acidez, heartburn
la evacuación, bowel movement
defecar, to defecate
la diarrea, diarrhea
el estreñimiento, constipation
la distensión abdominal, abdominal distention
el mal aliento, bad breath
las heces, stool (feces)
orinar, to urinate
la orina, urine
el ardor, burning/heat (noun)
la menstruación, menstruation
el sangrado menstrual, menstrual bleeding
las relaciones sexuales, sexual relations
el dolor pélvico, pelvic pain
la secreción vaginal, vaginal discharge
el pezón, nipple (women)
el flujo, flow/discharge
la masa, dough; mass
las articulaciones, joints
la artritis, arthritis
el reuma, rheumatism
la rigidez, stiffness; rigidity
el habla confusa, garbled speech
la visión borrosa, blurry vision
la ceguera, blindness
el adormecimiento, numbness; drowsiness
la memoria, memory
la convulsión, seizure; convulsion
la debilidad, debility; weakness
el estado de ánimo, mood
el abatimiento, depression
la ansiedad, anxiety
la erupción, rash (usually generalized)
los granos, pimples/papules
la urticaria, hives
el sarpullido, diaper rash/dermatitis/heat rash
la piel, skin
la úlcera, ulcer
el enrojecimiento, redness/blotch
el lunar, mole
la verruga, wart
la herida, wound
las uñas, fingernails
el rasguño, scratch
la ampolla, blister (noun)
el absceso, abscess
la peca, freckle
la mancha, spot; stain
la cicatriz, scar
la crema protectora, sunblock
el pulso, pulse
la presión arterial, blood pressure
la temperatura, temperature
Abra los ojos., Open your eyes. (formal)
Abra la boca muy grande., Open your mouth wide. (formal)
Saque la lengua., Stick out your tongue. (formal)
Siga el movimiento de mi dedo., Follow the movement of my finger. (formal)
sonreír, to smile
Respire profundamente., Breathe deeply. (formal)
empujar, to push
Apriete mis dedos., Squeeze my fingers. (formal)
Voy a examinar..., I am going to examine...
¿Le duele aquí?, Does it hurt here? (formal)
Levante la pierna., Lift your leg. (formal)
Relájese., Relax.
la medicina interna, internal medicine
el internista, internist
la endocrinología, endocrinology
la radiología, radiology
la cirugía, surgery
la neurología, neurology
la otorrinolaringología, otolaryngology (ENT)
la pediatría, pediatrics
la ortopedia, orthopedics
la urología, urology
la neumología, pulmonology
la cardiología, cardiology
la gastroenterología, gastroenterology
la nefrología, nephrology
la psiquiatría, psychiatry
la hematología, hematology
la oncología, oncology
Ginecología y Obstetricia, gynecology and obstetrics (OB-Gy
