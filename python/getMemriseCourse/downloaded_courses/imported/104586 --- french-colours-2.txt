Argent, Silver
Merde, Poop brown
Sang, Blood Red
Feu, Fire Red
Bureau, Bureau
Café, Coffee
Noir, Black
Or, Gold
Rouge, Red
Blanc, White
Avocat, Avocado
Rose, Pink Rose
Bleu, Blue
Souris, Mouse
Neige, Snow
Vert, Green
Fer, Iron
Champagne, Champagne
Chair, Chair
Chocolat, Chocolate
Marine, Marine
Pêche, Peach
Jaune, Yellow
Miel, Honey
Sable, Sand
Crème, Cream
Puce, Chip
Orange, Orange
Beurre, Butter
Blé, Wheat
Franc, Pure
Tabac, Tobacco
Gris, Grey
Flamme, Flame-coloured
Plomb, Lead
Citron, Lemon
Tango, Tango orange
Maïs, Corn
Marron, Brown
Taupe, Taupe
Paille, Straw
Violet, Violet
Violet, Violet
Blond, Blonde
Bonbon, Candy-pink
Olive, Olive
Banane, Banana
Jade, Jade
Tomate, Tomato
Brun, Brown
Moutarde, Mustard
Raisin, Grape
Menthe, Mint
Saumon, Salmon
Brique, Brick Red
Fraise, Strawberry
Lin, Flax
Bronze, Bronze
Havane, Havana
Cerise, Cherry
Cuivre, Copper
Carotte, Carrot
Doré, Golden
Aurore, Yellow Grape
Melon, Melon
Rubis, Ruby
Roux, Redhead
Vanille, Vanilla
Caramel, Caramel
Citrouille, Pumpkin
Ivoire, Ivory
Mûre, Mulberry
Soufre, Sulfur
Argile, Clay
Ardoise, Slate
Azur, Azure
Rouille, Rust
Orchidée, Orchid
Cannelle, Cinnamon
Bordeaux, Bordeaux
Bordeaux, Claret
Tan, Tan or tawn
Pourpre, Purple
Platine, Platinum
Beige, Beige
Corail, Coral Red
Lavande, Lavender
Mauve, Purple
Sanguine, Blood
Lilas, Lilac
Fauve, Fawn
Cacao, Cocoa
Prune, Plum
Chaudron, Cauldron
Noisette, Hazel
Bourgogne, Burgundy
Bis, Greyish brown
Émeraude, Emerald
Glauque, Murky
Asperge, Asparagus
Réglisse, Licorice
Pistache, Pistachio
Framboise, Raspberry
Amande, Almond
Absinthe, Absinthe
Kaki, Khaki
Laque, Lake
Turquoise, Turquoise
Noiraud, Blackbird
Écarlate, Scarlet
Saphir, Sapphire
Mandarine, Mandarine
Nankin, Nankeen
Acajou, Mahogany
Safran, Saffron
Rougeur, Blush
Aubergine, Eggplant
Bitume, Bitumen
Châtain, Brown
Basané, Dark skinned
Bisque, Bisque
Abricot, Apricot
Magenta, Magenta
Papaye, Papaya
Alezan, Chestnut
Indigo, Indigo
Indigo, Indigo
Pervenche, Periwinkle
Ébène, Ebony
Parme, Parma Violet
Tourterelle, Dove
Albâtre, Alabaster
Tilleul, Lime
Grenadine, Grenadine
Cassis, Cassis
Auburn, Auburn
Pastel, Pastel
Géranium, Geranium
Vermillon, Vermillion
Amarante, Amaranth
Groseille, Gooseberry
Cramoisi, Crimson
Jonquille, Daffodil
Chartreuse, Chartreuse
Châtaigne, Chestnut
Fuchsia, Fuchsia
Jais, Black
Grenat, Garnet
Byzantin, Byzantine
Garance, Madder
Carmin, Carmine
Tangerine, Tangerine
Sarcelle, Teal
Denim, Denim
Améthyste, Amethyst
Sépia, Sepia
Topaze, Topaz
Écru, Ecru
Anthracite, Charcoal
Lavallière, Cravat
Violacé, Violaceous
Glycine, Glycine
Carné, Pink Flesh
Chenu, White
Cyan, Cyan
Cachou, Catechu
Coquelicot, Poppy
Incarnat, Crimson
Serin, Canary yellow
Charbonneux, Black Smoke
Baillet, Baillet
Grège, Raw
Cinabre, Cinnabar
Héliotrope, Heliotrope
Céruse, White Lead
Aile de corbeau, Raven's wing
Hoto, Black
Noir animal, Black animal
Noir d'encre, Black ink
Noir d'ivoire, Ivory Black
Noir de jais, Jet Black
Noir charbon, Black coal
Bleu primaire, Primary blue
Bleu Klein, Klein Blue
Bleu de minuit, Midnight Blue
Vert impérial, Imperial Green
Bleu des mers du sud, South Seas Blue
Vert secondaire, Secondary Green
Vert printemps, Spring Green
Cyan secondaire, Secondary Cyan
Vert pin, Pine Green
Bleu canard, Blue Duck
Bleu paon, Peacock blue
Vert sapin, Forest Green
Vert bouteille, Bottle green
Dium, Sodium
Bleu nuit, Night blue
Noir d'aniline, Aniline Black
Noir de carbone, Black carbon
Noir de fumée, Black smoke
Vert épinard, Hunter Green
Vert de chrome ou anglais, Chrome green or English
Bleu outremer, Ultramarine
Bleu pétrole, Oil Blue
Bleu de cobalt, Cobalt blue
Vert de vessie, Sap Green
Bleu de Prusse;  Berlin ou bleu hussard, Prussian blue; Berlin or blue hussar
Bleu céleste, Heavenly Blue
Bleu électrique, Electric Blue
Bleu roi ou de France, Blue King or France
Vert pomme, Apple Green
Vert mélèze, Larch Green
Bleu acier, Steel blue
Vert gazon ou herbe, Green grass or grass
Vert perroquet, Parrot Green
Brou de noix, Walnut
Bleu turquin, Blue Marble
Indigo du web, Indigo web
Vert poireau, Leek Green
Bleu barbeau ou bleuet, Barbel or cornflower blue
Menthe à l'eau, Mint water
Bleu guède, Woad blue
Vert prairie, Meadow Green
Vert militaire, Military green
Blet, Dark brown
Bleu Persan, Persian Blue
Gris de Payne, Payne's gray
Vert mousse, Moss green
Gris de maure, Moorish Gray
Vert sauge, Sage Green
Zinzolin, Zinzolin
Indigo électrique, Electric Indigo
Byzantium, Byzantium
Rouge vin, Wine
Violet d'évêque, Bishop Violet
Sang de bœuf, Oxblood
Azur clair, Clear azure
Bleu ciel, Blue sky
Café au lait, Coffee with milk
Indigo chaud, Hot indigo
Vert kaki, Khaki Green
Aigue-marine, Aquamarine
Gris fer, Gray iron
Vineux, Vinous
Magenta foncé, Dark Magenta
Bleu givré, Frosted blue
Vert céladon, Celadon green
Rouge sang, Blood Red
Bistre, Brownish yellow
Vert lichen, Lichen Green
Mordoré, Bronze
Terre de Sienne, Sienna
Bleu charrette, Blue cart
Passe-velours, Pass velvet
Queue-de-renard, Fox Tail
Terre d'ombre, Umber
Merise, Wild Cherry
Vert-de-gris, Greyish Green
Vert opaline, Opalescent Green
Feuille morte, Dead leaf
Rose Mountbatten, Mountbatten pink
Vert lime, Lime green
Vert anis, Anise
Fraise écrasée, Crushed strawberries
Rouge Bismarck, Bismarck Red
Vert tilleul, Lime green
Queue-de-vache foncé, Dark Tail Cow
Rouge turc, Turkish Red
Azurin, Azurin
Lie de vin, Wine
Ambre rouge, Red amber
Aquilain, Horse Tan
Rouge tomette, Red floor tile
Gris acier, Steel gray
Vert d'eau, Green water
Jaune primevère, Primrose
Poil de chameau, Camel
Rouge cardinal, Cardinal Red
Étain oxydé, Tin oxide
Rouge cerise, Cherry
Bleu fumée, Blue smoke
Rouge écrevisse, Crayfish Red
Ponceau, Poppy-red
Vert chartreuse, Chartreuse Green
Queue-de-vache clair, Light Tail Cow
Rose balais, Rose brushes
Rouge-violet, Violet Red
Orange brûlée, Burnt Orange
Brun clair, Light brown
Caca d'oie, Goose poop
Gris perle, Pearl gray
Gris de lin, Flax Grey
Pelure d'oignon, Onion skin
Magenta fuchsia, Fuchsia Magenta
Incarnat-e, Incarnate
Ocre rouge, Red ocher
Ocre jaune, Yellow ocher
Bleu dragée, Blue Bean
Jaune chartreuse, Yellow chartreuse
Rouge d'alizarine, Alizarin Red
Blond vénitien, Strawberry Blonde
Jaune canari, Canary
Terre de Sienne brûlée, Burnt Sienna
Ventre de biche, Fawn
Rouge d'aniline, Aniline Red
Papier bulle, Bubble wrap
Étain pur, Pure tin
Jaune chrome, Chrome yellow
Jaune de Mars, Mars Yellow
Jaune d'or, Yellow Gold
Ambre jaune, Amber
Azur brume, Blue haze
Opalin, Opal
Blanc lunaire, White Lunar
Beige clair, Clear Beige
Jaune bouton d’or, Yellow buttercup
Blanc de zinc, Zinc white
Rouge anglais, English Red
Rouge de Mars, Mars Red
Jaune poussin, Yellow chick
Rose bonbon, Candy pink
Blanc de lait, White milk
Orpin de Perse, Stonecrop Persia
Bouton d'or, Buttercup
Rose fuchsia, Fuchsia
Coquille d'œuf, Eggshell
Jaune auréolin, Cobalt Yellow
Rouge feu, Red Light
Incarnadin, Watermelon
Rose dragée, Pink Bean
Cuisse de nymphe, Nymph’s Thigh
Jaune mimosa, Yellow mimosa
Blanc d'Espagne, Spanish white
Blanc cassé, Off white
Blanc d'argent ou de plomb, Silver white or lead
Rouge primaire, Primary Red
Rose vif, Bright pink
Magenta secondaire, Secondary Magenta
Cuisse de nymphe émue, Nymph’s Thigh Touched
Rose thé, Tea Rose
Jaune d'oeuf, Egg Yolk
Jaune impérial, Imperial yellow
Jaune de Naples, Naples Yellow
Beurre frais, Fresh butter
Jaune primaire, Primary yell
