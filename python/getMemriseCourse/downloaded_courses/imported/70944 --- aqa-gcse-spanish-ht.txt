el novio		boyfriend
alcoholico/a		alcoholic
el barrio		neighbourhood
beber		to drink
barato/a		cheap
el bistec		steak
la ley		law
caro/a 		expensive
la reaccion		reaction
los cereales		cereal
el chorizo		sausage
comer		to eat
la comida		food
la comida basura		junk food
la comida rápida		fast food
la cosa		thing
delicioso/a		delicious
el desayuno		breakfast
la dieta		diet
la ensalada		salad
fresco/a 		fresh
la fruta		fruit
la galleta		cookie
la grasa		fat
la hamburguesa		hamburger
el helado		ice cream
el huevo		egg
la leche		milk
las legumbres		vegetables
los mariscos		seafood
el perrito caliente		hot dog
el pescado		fish (to eat)
picante		spicy
el plátano		banana
el plato		dish
el pollo		chicken
raro/a		strange
rico/a		tasty
saludable		healthy (sal...)
sano/a		healthy (4 letters)
la sopa		soup
las tapas		snacks
típico/a		typical
tomar		to take
la tortilla		omelette
la tostada		toast
vegetariano/a		vegetarian
las verduras		green vegetables
el pastel		cake (p...) 
el yogur		yoghurt
el zumo		juice
el pais		country
la tarta		cake (t...) 
acostarse		to go to bed, to lie down
adictivo/a		addictive
el adicto		addict
cambiar		to change
cansado/a		tired
el cuerpo		body
deportista		sporty
dormir		to sleep
el ejercicio		exercise
la energía		energy
la esfuerzo		effort
estar en forma		to be fit
evitar		to avoid
fumar		to smoke
joven		young
llevar una vida sana		to lead a healthy life
mantenerse en forma		to keep fit
Morir		to die
necesario/a		necessary
necesitar		to need
el problema		problem
relajarse		to relax
la respuesta		answer
la rutina		routine
la salud		health
el tabaco		smoking / tobacco
tener dolor de (cabeza)		to have a head ache
tener hambre		to be hungry
tener sed		to be thirsty
tener sueño		to be sleepy
despues		afterwards
la vida		life
el adolescente		teenager
afectar		to affect
asqueroso/a		disgusting/filfthy
causar		to cause
la chica		girl (c..)
el chico		boy (not niño or muchacho)
el cigarrillo		cigarette
el corazón		heart
el daño		danger / harm
dejar de (fumar)		to stop (smoking)
la enfermedad		illness / disease
no fumador		no smoking area
el/la fumador/a 		smoker
el fumar pasivo		passive smoking
la ley		law
el lugar		place
la mujer		woman
la muerte		death
el olor 		smell
el peligro		danger
la población		population
preocupante		worrying
la prohibición		ban
prohibido/a 		banned
los pulmónes		lungs
respiratorio/a		breathing / respiratory
se permite (fumar)		(smoking) is alowed
el sitio		place
el trabajo		work
el alcoholismo		Alcoholism
el alimento		food
la ayuda		help
borracho/a		drunk
el botellón		binge drinking
la calle		street
el canabis		cannabis
comprar		to buy
el crimen		crime
el dinero		money
distinto/a		different
la droga		drug
las drogas blandas		soft drugs
las drogas duras		hard drugs
el/la drogadicto/a		drug addict
emborracharse		to get drunk
la inyección		injection
los jóvenes		young people
la manera		way
mejor		better/ best
obtener		to obtain
olvidar		to forget
peor		worse
preocupar		to worry
provocar		to provoke
la rehabilitación		rehabilitation
seropositivo/a		HIV positive
sin techo		homeless
la sociedad		society
tener miedo		to be afraid
el vino		wine
la violencia		violence
alegre		happy/cheerful
alto/a		tall
amable		nice/ pleasant
antipático/a		unpleasant
bajo/a		short (not tall)
la barba		beard
el bigote		mustache
calvo/a		bald
corto/a		short (in length)
delgado/a		thin
extrovertido/a		extroverted/outgoing
las gafas		glasses
gracioso		funny
guapo/a		good looking
hablador(a)		chatty/ talkative
honesto/a		honest
largo/a		long
liso/a		straight
moreno/a		(dark) brown
la nariz		nose
el novio		boyfriend
las orejas		ears
paciente		patient
las pecas		freckles
pelirrojo/a		red-haired
rizado/a		curly
rubio/a		blonde
sensible		sensitive
simpático/a		nice/ pleasant
tímido/a		shy
torpe		clumsy
travieso/a		naughty
cómico		funny (c...)
vago/a		lazy
castaño		brown
el amor		love
casado/a		married
casarse		to get married
cuidar		to look after
divorciado/a		divorced
la esposa		wife
el esposo		husband
el/ la gemelo/a		twin
los hermanastros		stepbrothers and sisters
la hija		daughter
el hijo		son
los hijos		children
jubilado/a		retired
loco/a		mad
la madre soltera		single mother
el marido		husband
el matrimonio		marriage
la niñera		nanny
el niño		child (boy)
la novia		girlfriend
el padastro		stepfather
en el paro		unemployed
separado/a		separated
solo/a		alone
soltero/a		single
el/ la vecino/a		neighbour
la viuda		widow
el viudo		widower
aguantar		to tolerate/ put up with
la barrera generacional		generation gap
cariñoso/a		affectionate
comprensivo/a		understanding
comprender		to understand
la confianza		trust
convencer		to convince
criticar		to criticize
la discusion		argument
egoísta		selfish
enfadarse		to get angry (enf)
feliz		happy (f..)
honrado		honest/ honourable
impaciente		impatient
la influencia		influence
llevarse bien con		to get on well with
maleducado/a		rude
la pareja		couple
pelearse		to fight
relacionarse		to relate to/ get to know
respetar		to respect
el/ la compañero/a		companion/ friend
independiente		independent
jubilarse		to retire
el/ la nieto/a 		grandchild
pesimista		pessimistic
la vida familiar		family life
la actitud		attitude
el comentario		commentary
cometer		to commit
los derechos		rights
la desigualdad		inequality
la diferencia		difference
la discriminación		discrimination
la educación		education
educar		to educate
la encuesta		survey
estar a favor		to be in favour
Estar de acuerdo		to agree
estar en contra		to be against
el/ la extranjero/a		foreigner
la igualdad		equality
el/ la inmigrante		immigrant
la integración		integration
maltratar		to abuse
matar		to kill
la mayoria		majority
el mundo		world
la reaccion		reaction
el sentimiento		feeling
el/ la trabajador(a)		worker
la víctima		victim
la violencia domestica		domestic violence
acoger		to welcome
el agua corriente		running water
la alcantarilla		drain
arruinar		to ruin
el barrio		neighbourhood
el beneficio		benefit
la carretera		road
el cartón		cardboard
el desarrollo		development
encontrar		to find
estable		stable
las expectativas		expectations
el/ la gerente		manager
el/ la habitante		inhabitant
injusto/a		unfair
la ONG		NGO (non-governmental organisation)
el pais		country
la piedra		stone
los pobres		poor people
la pobreza		poverty
el prejuicio		prejudice
la riqueza		wealth
el techo		roof
el tratamiento		treatment
la batería		drums
el disco compacto		CD
la comedia		comedy
el deporte		sport
los dibujos animados		cartoons
escuchar		to listen (to)
gran hermano		big brother
la guitarra		guitar
invitar		to invite
jugar		to play
leer		to read
el mensaje		message
la música		music
las noticias		news
el ordenador portátil		laptop computer
el portatil		laptop
el partido		game/ match
la pelicula		film
la pelicula de accion		action film/ thriller
la pelicula romantica		romantic film
el ping-pong		table tennis
el rato		(short) time, a little while
recibir		to receive
la serie policiaca		crime series
la telenovela		soap opera
el tiempo libre		free time
tocar		to play (a musical instrument)
ver		to see/ watch
el videojuego		video game
aburrido/a		boring
el ambiente		atmosphere
el atletismo		athletics
bailar		to dance
el baile		dance
el campeonato		championship
la carrera		proffesion
el ciclismo		cycling
la ciencia ficcion		science fiction
el cine		cinema
la copa		cup/ trophy
correr		to run
el descanso		break/ half-term/ interval
el día festivo		(bank) holiday
la entrada		ticket/ entry
el equipo de musica		music system
el espectáculo		show
el estadio		stadium
la fiesta		party/ festival
el fin de semana		weekend
el finde		weekend (colloquial)
hacer los deberes		to do homework
ir de compras		to go shopping
el/ la jugador(a) juvenil		youth team player
la montaña rusa		roller coaster
montar		to ride/ to go on a ride
la natación		swimming
el parque tematico		theme park
pasar		to spend (time)
pasarlo bien		to have a good time
la pelicula de horror		horror film
regalar		to give (as a present)
salir		to go out
el tenis		tennis
ahorrar		to save
la chaqueta		jacket
el cumpleaños		birthday
el dinero		money
disfrutar		to enjoy (d...)
encontrar		to find
gastar		to spend
los grandes almacenes		department stores
la marca, la ropa de marca		make, designer clothes
la mayoria		majority
mejorar		to improve
la moda		fashion
la Navidad		Christmas
el ocio		leisure
la paga		pocket money
pagar		to pay
publicidad		advertising
el regalo		gift
la revista		magazine
la ropa		clothes
el santo		saint's day
la tarjeta de credito		credit card
Las zapatillas de deporte		trainers
las botas		boots
los calcetines		socks
la calidad		quality
de (buena) calidad		(good) quality
la camisa		shirt
la camiseta		t-shirt
el cinturón		belt
el collar		necklace
la comodidad		comfort
la corbata		tie
costar		to cost
el cuero		leather
de segunda mano		second hand
el descuento		discount
estar de moda		to be fashionable
la falda		skirt
la gorra		cap/ hat
los guantes		gloves
la imagen		image
irse bien		to fit/ suit
la lana		wool
llevar		to wear
las medias		stockings/ tights
la oferta (especial)		(special) offer
el pañuelo		scarf
los pendientes		earrings
el precio		price
probarse		to try on
el supermercado		supermarket
la talla		size (clothing)
el tamaño		size (not talla)
la tienda		tent
la tienda de ropa		clothing shop
el tipo		type
los vaqueros		jeans
el vestido		dress
la zapatería		shoe shop
los zapatos		shoes
la banda ancha		broadband
la biblioteca		library
la camara digital		digital camera
chatear		to chat (online)
conectarse		to connect
el correo basura		junk mail
descargar		to download
la desventaja		disadvantage
devolver		to give back
entretenido/a		entertaining
el equipo de musica		music system
guardar		to keep/ save
el juego		game
la libreria		bookshop
la llamada		call
mandar		to send
el mensajero instantaneo		instant messenger
el móvil		mobile phone
navegar en la red		to surf the internet
la television plana		flatscreen television
la ventaja		advantage
la videoconsola		video console
el aire libre		open air
el albergue juvenil		youth hostel
Alemania		Germany
alojarse		to stay
alquilar		to hire/ rent
antiguo/a		old
el avión		aeroplane
bañarse		to swim
montar a caballo		to go horse riding
la cancha (de baloncesto)		(basketball) court
jugar a las cartas		to play cards
el castillo		castle
ir de camping		to go camping
cómodo/a		comfortable
conocer		to meet/ get to know
construir		to build
descansar		to rest
el equipaje		luggage
el espectáculo		show
los estados unidos		The United States
el estres		stress
ir de excursion		to go on a trip
el extranjero		abroad
la ficha		form
la hamburgueseria		hamburger joint
la heladería		ice cream parlour
la insolación		sunstroke
Irelanda		Ireland
el lago		lake
libre		free
el libero de guia		guidebook
la mochila		rucksack
mojarse		to get wet
montana		mountain
nadar		to swim
la naturaleza		nature
olvidarse		to forget
el parador		state-run hotel
el pasaporte		passport
pasarlo bien		to have a good time
la peluquería		hairdresser's
practicar la pesca		to go fishing
rellenar		to fill in
el retraso		delay
el saco de dormir		sleeping bag
la Semana Santa		Easter Holidays/ Holy Week
la tienda		tent
tomar el sol		to sunbathe
veranear		to spend your summer holidays
vuelo		flight
el ambiente		atmosphere
broncearse		to sunbathe
la costumbre		custom/ tradition
el deporte de invierno		winter sport
descansar		to rest
el equipaje		luggage
la estación de esquí		ski resort
Grecia		Greece (the country)
el inglés		English
la isla		island
la nieve		snow
el paraguas		umbrella
pasar		to spend (time)
pasearse		to walk/ to stroll
la pista		runway
quedarse		to stay
la sierra		mountain range
la vista		view
en vivo		live (music)
irlandes		Irish
el intercambio		exchange (visit)
aprender		to learn
el concierto		concert
la corrida de toros		bullfight
el edificio		building
el espacio		space
la especialidad		speciality
el espectáculo de flamenco		flamenco show
la fiesta		party/ festival
los fuegos artificiales		fireworks
el museo		museum
el plato		dish
quemar		to burn (q)
el traje		costume/ outfit/ suit
el aeropuerto		airport
el aire acondicionado		air conditioning
alquilar		to hire/ rent
el andén		platform
el asiento		seat
el autocar		coach
el barco		boat
cambiar		to change
el carné joven		student card
el carnet de conducir		driving license (not permiso de..)
el carnet/ documento de identidad 		ID
la carretera		road
el cheque de viaje		traveler's cheque
el coche		car
coger		to pick/ take
conducir		to drive
la consigna		left luggage
el cruce		junction/ crossroads
en efectivo		in cash
enseñar		to teach
la estación		station
estar ocupado/a		to be occupied
la gasolina		petrol
el IVA		VAT
la llegada		arrival
la maleta		suitcase
el mapa		map
la máquina		machine
el metro		underground train system
mostrar		to show
la moto(cicleta)		motorbike
pagar		to pay
la parada		stop
el permiso de conducir		driving licence (not carnet de...)
preguntar		to ask
primera clase		first class
el puerto		port
regresar		to return (not volver)
RENFE		spanish state railway
la sala de espera		waiting room
la salida		exit
el seguro (del coche)		car insurance
los semáforos		traffic lights
el suplemento		supplement
la taquilla		ticket office
tardar		to take (time)
la tarjeta de credito		credit card
tomar		to take
el tren		train
el tren de cercanías		local train
utilizar		to use
la vía		track
viajar		to travel
el billete		ticket
el alquiler		hire  
acabar		to finish
el baile		dance
el balcón		balcony
cada		every/ each
la cancion		song
el caramelo		sweet (not dulce)
cargar		to load
celebrar		to celebrate
el dulce		sweet (not caramelo)
empezar		to begin
especial		special
esperar		to wait
la fiesta		party/ festival
la flor		flower
la gente		people
el invitado		guest
el juguete		toy
la medianoche		midnight
el/ la mejor		the best
la Navidad		Christmas
la nochebuena		Christmas Eve
la nochevieja		New Year's Eve
Papa Noel		father christmas
el paquete		parcel/ package
pedir		to ask for
el pescado		fish (to eat)
el regalo		gift
repartir		to share/give out
el/ la santo/a 		name/ saint's day
la uva		grape
adosado/a		semi-detached
antes		before
el aparcamiento		parking
el árbol		tree
el ascensor		lift
aunque		although
la bañera		bathtub
el bosque		forest
la calefacción		heating
caliente		hot/ warm
el campo		field/ countryside
caro/a 		expensive
el césped		lawn
la chimenea		fireplace/ chimney
por cierto		certainly/ by the way
el comedor		dining room
compartir		to share
el cristal		glass
dar a		to have a view of
demasiado/a		too much
por dentro		inside
la ducha		shower
sin duda		without a doubt
el electrodomestico		household appliance
entrar		to come in/ enter
el espacio		space
la flor		flower
fresco/a		cool
funcionar		to work
la granja		farm
grueso/a		thick
la habitación		room (h)
el hogar		home (not casa)
igual		same
la lámpara		lamp
la luz		light
mientras que		while
la moqueta		fitted carpet
el mueble		furniture
el nuestro		ours
parecido/a		similar
la persiana		window blind
estar pintado		to be painted
la planta		floor/ storey
pobre		poor
pocos/as		few
seguro/a		safe/ secure
subir		to go up/ rise
(ni) tampoco		neither
tan		so
tanto/a		so much
tranquilo/a		calm/ quiet
valer		to be worth
el/ la vecino/a		neighbour
la ventana		window
viejo/a		old
ahora		now
algunos/as		some
animado/a		lively
antes		before
la autopista		motorway
el barrio		neighbourhood
bastante		quite
la basura		rubbish
bonito/a		pretty
lo bueno		the good thing
estar cerrado		to be closed
construir		to build
la contaminación		pollution
la diversión		entertainment
estrecho/a  		narrow
hacer falta		to be needed
feo/a		ugly
la industria		industry
la instalación		facility
limpio/a		clean
el lugar		place
lo malo		the bad thing
por el medio		through the middle
necesitar		to need
el paro		unemployment
el parque infantil		playground
peligroso/a		dangerous
un poco		a little
por lo tanto		therefore
el puerto		port
el ruido		noise
también		also
el acceso		access
ademas		also/ besides
agradable		pleasant
alrededor		around
bajar		to go down
caluroso/a		hot
casi		almost
el cielo		sky
el clima		climate
desde... a...		from... to...
despejado/a		clear
la distancia		distance
durante		during
encontrar		to find
la gente		people
el grado		degree
hasta		until
hay que		you have to
la inundación		flood
llegar		to arrive/ get to
lleno/a		full
más de		more than
en medio de		in the middle of
lo mejor		the best
la mezquita		mosque
nunca		never
precioso/a		beautiful
saber		to know (facts)
según		according to
siempre		always
sin embargo		however
el teatro		theater
tener razón		to be right
tranquilo/a		calm/ quiet
el aeropuerto		airport
el accidente		accident
el aire		air
el anuncio		advert
apagar		to turn off
a pie		on foot
aprobar		to pass
la basura		rubbish
la bolsa de plastico		plastic bag
la calidad		quality
cambiar		to change
cercano/a		near
la contaminación		pollution
dañar		to damage/ spoil
la denuncia		complaint
el edificio		building
las emisiones		emissions
el envase		container
la fábrica		factory
fastidar		to annoy
me fastidia		it annoys me
Impedir		to impede/ prevent
la luz		light
matar		to kill
la medida		measurement
el medio ambiente		environment
la mitad		half
molestar		to annoy/ to bother
la multa		fine
la naturaleza		nature
el peligro		danger
peligroso/a		dangerous
la pesadilla		nightmare
la pista		runway
preocuparse		to worry
puro/a		pure/ clean
quejarse de		to complain about
reducir		to reduce
los residuos		waste
los residuos químicos		chemical waste
respirar		to breathe
el ruido		noise
según		according to
solucionar		to solve (not resolver)
el tráfico		traffic
tóxico/a		poisonous/ toxic
el/ la vecino/a		neighbour
la velocidad		speed
la encuesta		survey
el vertido		spillage
el alimento		food
aumentar  		to increase
el calentamiento global		global warming
caluroso/a		hot
el cambio climático		climate change
la catástrofe natural		natural disaster
crecer		to grow
la deforestación		deforestation
desaparecer		to disappear
demasiado/a		too much
destruir		to destroy
el efecto invernadero		greenhouse effect
el equilibrio		balance
la escasez		shortage
la extinción		extinction
faltar		to lack/ be absent
grave		serious
el hielo		ice
el huracán		hurricane
el incendio		fire (i.e. building burning)
la inundación		flood
irreversible		irreversible
el mundo		world
el nivel del mar		sea level
el oxígeno		oxygen
el petróleo		oil
el planeta		planet
la población		population
el polo norte		North Pole
el problema		problem
proteger		to protect
reciclable		recyclable
el recurso		resource
renovable		renewable
las selvas tropicales		tropical forests
la sequía		drought
la sobrepoblación		overpopulation
subir		to go up/ rise
la catastrofe		disaster
apagar		to turn off
apuntarse		to sign up
caliente		hot/ warm
el cartón		cardboard
el combustible		fuel
el consejo		advice
consumir		to consume
el consumo		consumption
el contenedor		container
la cortina		curtain
cuidar		to look after
ducharse		to have a shower
durar		to last
el edredón nórdico		quilt
la energía		energy
el/la friolero/a		person who feels the cold
El horno		oven
el lavaplatos		dishwasher
ligero/a		light
lleno/a		full
la nevera		fridge
la persiana		window blind
a pie		on foot
reciclar		to recycle
el residuo		waste
reutilizar		to reuse
separar		to separate
tirar		to throw/ throw away
el transporte público		public transport
usar		to use
el vidrio		glass (material)
el alemán		German
el/la alumno/a		pupil
la asignatura		subject
el aula (f)		classroom
el bachillerato		A-level equivalent
la biología		biology
el campo de deporte		sports field
las ciencias		sciences
las ciencias económicas		economics
el colegio		school (col...)
los conocimientos  		knowledge
el curso		course/ school year
los deberes		homework
difícil		difficult
el/la director(a)		head teacher
diseñar		to design
divertido/a		fun
la educación física		PE
el/la estudiante		student
estudiar		to study
la escuela		school (esc...)
fácil		easy
los estudios		studies
la física		physics
el francés		French
la geografía		geography
la gimnasia		gymnastics
el gimnasio		gym
hay		there is/ there are
la historia		history
el idioma		language
la informática		ICT
las instalaciones		facilities
el instutito		(secondary) school
el intercambio		exchange (visit)
interesante		interesting
el laboratorio		laboratory
maravilloso/a		marvellous
las matematicas		maths
mejor		better/ best
mixto/a		mixed
moderno/a		modern
odiar		to hate
el/la profesor(a)		teacher
la technologia		technology
tener		to have
terminar		to finish
los trabajos manuales		craft subjects
los apuntes		notes
anoche		last night
el chicle		chewing gum
comenzar		to begin (c...)
el detalle		detail
disfrutar		to enjoy (d...)
duro/a		hard
empezar		to begin
la enseñanza		education
el español		Spanish
estricto/a		strict
el examen		exam
fatal		awful
incluso		even
de momento		at the moment
parecer		to seem
pedir permiso		to ask for permission
el permiso		permission
ponerse a		to begin to
las prácticas laborales		work experience
la pregunta		question (not cuestión)
privado/a		private
la prueba		test
publico/a		public/ state school
el recreo		break
la semana		week
severo/a		strict
siempre		always
el silencio		silence
suspender		to fail
el trabajo		work
el trimestre		term
el/la tutor(a)		tutor
el vocabulario		vocabulary
el acoso escolar		bullying
a menudo		often
apoyar		to support
el asunto		matter/ topic
atacar		to attack
aunque		although
ausente		absent
la ayuda		help
el bolígrafo		pen
la calculadora		calculator
la carpeta		folder/ file
castigar		to punish
charlar		to chat
el comportamiento		behaviour
comprender		to understand
la conducta		behaviour
divertirse		to have fun
entender		to understand
el equilibrio		balance
estar a favor		to be in favour
estar en contra		to be against
estar harto de		to be fed up of
el estres		stress
el estuche		pencil case
la evaluación		assessment
el éxito		success
fisico/a		physical
el fracaso		failure
generalmente		usually
golpear		to hit
insolente		insolent/ cheeky
insultar		to insult
intimidar		to intimidate / threaten
libertad		freedom
pasar		to spend (time)
la presión		pressure
prometer		to promise
repasar		revise
la respuesta		answer
el sondeo		survey
la tarea		task
la víctima		victim
ya que		because
el tema		theme/ topic
respetar		to respect
alegrarse		to be pleased
aprobar		to pass
la cantidad		quantity
comportarse		to behave
conseguir  		to get/ achieve
decepcionante		disappointing
desobediente		disobedient
faltar		to lack/ be absent
fastidiar		to annoy
incómodo		uncomfortable
el inglés		English
mantener		to keep
la mayoria		majority
obligatorio/a		compulsory
olvidar		to forget
la opción		option
optar		to choose/ opt
optativo/a		optional
pensar		to think
preferir		to prefer
los vestuarios		changing rooms
la cantina		canteen
el uniforme		uniform
el/la abogado/a		lawyer
ahora		now
antiguo/a		old
calificado/a		qualified
la compañía		company
común		common
el consejo		advice
cuyo/a		whose
el día festivo		(bank) holiday
encargado/a de		in charge of
encargarse de		to be in charge of
la entrevista		interview
entusiasta		enthusiastic
ganar		to earn
hace un mes		a month ago
hacer practicas		to do work experience
hasta		until
el horario de trabajo		working hours
las horas de trabajo flexibles		flexible working hours
la intención		intention
al mismo tiempo		at the the same time
la participación		participation
por eso		therefore
por otro lado		on the other hand
probar		to have ago/ try
sin embargo		however
el taller		workshop
a tiempo completo		full time
a tiempo parcial		part time
trabajador(a)		hard-working
ya no		no longer
adjuntar		to attach/ enclose
el/la albanil		bricklayer
el amo de casa		house-husband
la ama de casa		housewife
la ambición		ambition
aprender		to learn
el/la camarero/a		waiter/waitress
el/la candidato/a		candidate
la carrera		proffesion
el/la cocinero/a		chef
competente		competent
las condiciones de trabajo		working conditions
contactar		to contact
contratar		to take on/ hire
el contrato		contract
la desventaja		disadvantage
el/la electricista		electrician
emocionante		exciting
la empresa		company/ firm
el entusiasmo		enthusiasm
la experiencia laboral		experience of work
la explicación		explanation
fascinante		fascinating
el idioma		language
llegar a ser		to become
máximo/a		maximum
mínimo/a		minimum
ofrecer		to offer
el/la panadero/a		baker
el/la peluquero/a		hairdresser
el propósito		aim
el/la recepcionista		receptionist
rellenar		to fill in
el salario		salary
sencillo/a		simple
solicitar		to apply
la solicitud		application
el sueldo		salary
el título		university degree
el/la trabajador(a)		worker
trabajar		to work
útil		useful
la ventaja		advantage
capaz		able, capable
el/la carnicero/a		butcher
el/la carpintero/a		joiner
la cita		appointment
la copia		copy
el correo		mail
el correo electrónico		e-mail
la correspondencia		mail/ post
el/la empleado/a		employee
el/la jefe/a		boss
la línea		line
la llamada		call
llamar (por teléfono)		to call
el mensaje		message
el objetivo		aim/ objective
la oficina		office
pronto		soon
el aprendizaje		apprenticeship/ learning
asi que		so
calificación		qualification
el/la contable		accountant
los derechos		rights
el empleo		job
encontrar		to find
el futuro		future
el/la mecanico/a		mechanic
obtener		to obtain
la preocupación		worry
tomar un ano libre		to have a gap year
la universidad		university
el/la aprendiz		apprentice
la azafata		flight attendent
el/la bombero/a		firefighter
el/la cajero/a		cashier
el/la camionero/a		lorry driver
la clínica		clinic
el club taurino		bullfighting club
el/la comerciante		trader/ shopkeeper
el comercio		commerce/ shop
el/la dentista		dentist
el/la dependiente/a		shop assistant
el/la enfermero/a		nurse
el/la escritor(a)		writer
estar estresado/a		to be stressed
estresante		stressful
explicar		to explain
el/la granjero/a		farmer
el hombre de negocios		businessman
el/la ejecutivo/a		executive
el/la ingeniero/a		engineer
el/la intérprete		interpreter
el/la jardinero/a		gardener
el/la medico		doctor
la mujer de negocios		businesswoman
el/la obrero/a		worker/ labourer
pagar		to pay
pagar bien		to pay well
el periodismo		journalism
el/la periodista		journalist
el/la pintor(a)		painter
el/la policia		police officer
al principio		at first
el/la soldado		soldier
soltar		to release
el/la traductor(a)		translator
el triunfo		triumph/ success
torear		to fight bulls
el traje de luces		bullfighter's suit
variado/a		varied
el/la veterinario/a		vet
la vida rural		country life
el ejército		ar