апелляция		appeal
басня		fable, fabrication
беззаконие		lawlessness
безотлагательно		urgently, immediately
бессонница		insomnia
боковой		lateral
бомж		tramp
буквально		literally
бушевать		to storm, rage
в пользу		for the use/benefit of
в том числе		including
в ходе		during
вводить/ввести		to introduce; to impose
вдвое		twice
ведение		authority, competence
вернее		or rather, to be more exact
вкалывать		to slave away
владелец		owner
внезапно		suddenly, all of a sudden
вниз		down, downwards
внушать/внушить		to insinuate, impress upon
вовсе		at all
возведение		construction
возмещение		compensation
возмутительный		outrageous
движение		movement, traffic
дескать		they say (indicates reported speech)
длительная командировка		lengthy business trip
возникло ощущение		the feeling arose
ворочаться		to fidget
воспользоваться		to make use of
восстановление социальной справедливости		restoration of social justice
впоследствии		subsequently, as a consequence
впрочем		however, though
вскричать		to cry, exclaim
вскрывать		to reveal
вступление		introduction, entry
выбросить мусор		to take the rubbish out
вывод		withdrawal, exclusion, conclusion
выделять/выделить		to highlight, emphasize
выносить/вынести		to carry out
выпуск		edition, output
выражение		expression
выставить		to expose
выступление		speech; presentation; performance
вышестоящая инстанция		higher authority
годиться		to be suited for
греть/согреть		to heat, warm up
грузовик		lorry, truck
грязь		mud
добавлять/добавить		to add
добираться/добраться		to reach
добиваться/добиться		to achieve/get
доставать/достать		to take out, fetch
жажда чудесного		hunger/thirst for something miraculous
жевать		to chew
желательно		it is advisable
жизненно		vitally
за счёт		at the expense of
забираться/забраться в/на		to climb in
заверенный		certified, witnessed
задорный		fervent, ardent
законная сила		legal force
замечать/заметить		to notice
замечательный		remarkable, wonderful
замок		lock (second stress) / castle (first stress)
заодно		at the same time
запинка		hesitation, delay
запоем		hard, heavily
запрещать/запретить		to ban, forbid
зачитаться		to become engrossed in reading
зевать/зевнуть		to yawn
зондирование		sounding
зреть/созреть		to mature, ripen
ибо		for
извращать/извратить		to distort
извращенный		distorted
издавать/издать		to publish, produce, emit
издаваться/издаться		to be published
излагать		to give an account
импозантный		extraordinary
информагентство		news agency
иск		action, suit
исправительный		correctional, disciplinary
исправление		repairing, reforming, correcting
испуганно		anxiously, fearfully
испуганный		frightened, startled
итог		total, result
как-то		somehow, somewhat
капюшон		hood
касаться		to touch, concern, apply to
квитанция		receipt
кодекс		code, lawbook
колесо		wheel
колония		settlement, camp
колония общего режима		labour camp
кража		theft
крестьянин		peasant
кусаться		to bite, sting, hurt
лишение		denial, deprivation
лопать/слопать		to eat up
место назначения		destination
многодневный		lasting several days
молиться		to pray
молчать		to be silent
мосгорсуд		Moscow city court
мчаться		to race, to speed
На всякий случай		in any case
на минувшей неделе		last week
наблюдать		to observe
надёжный		reliable
надирать		to roughen
наёмный		hired
наиболее		most
накопление		accumulation
наличие		presence
наповал		outright
нарушать/нарушить правило		to break a rule
наступать/наступить		to approach, occur, advance
начальник		boss
недвижимость		property
ненастный		gloomy, dismal
непристойный		indecent
неустойчивый		unstable, unsettled
неявка		absence
новостройка		tower block
нюхать		to sniff
ОАО (открытое акционерное общество)		public company
обеспечивать/обеспечить		to provide, ensure
обеспечиваться/обеспечиться		to provide oneself with
обеспеченный		well to do
обильный		generous
обитать		to inhabit
обойтись		to cost
оборудования		equipment
обращать/обратить внимание на		to pay attention to, take notice of
объявлять/объявить		to announce, declare
объясняться/объясниться (с)		to become clear (explain oneself to)
оглашать/огласить		to read out, announce
ограниченный		limited, restricted
озадачиваться		to set oneself a task, be concerned with
окружавший		surrounding
омрачаться/омрачиться		to darken
он одни кости		he's skin and bones
оповещать/оповестить		to notify, warn, inform
оправдание		justification, acquittal
определённый		specific
опушка		edge
основной		basic
ответчик		defendant
отвлекать/отвлечь внимание от		to divert attention from
отдел		department, division
отделываться/отделаться		to get rid of (+ gen)/to escape (+ins)
отдельный		separate, individual
отпускать/отпустить		to let go
отрасль		sector
отсутствие		absence
охрана		protection, guarding
очко		point
переносить/перенести		to carry, move, postpone
переплюнуть		to exceed, surpass
перешибить		to fracture, break
перрон		platform
по очереди		in turn
по поводу		about, concerning
побочный		side
повод		cause, reason
поворот		turning, bend
повреждение		damage, injury
поданный		submitted, filed
поддерживать хорошие отношения с		to stay on good terms with
поднимать/поднять		to lift up, raise, boost
подниматься/подняться		to ascend, rise
подозрение		suspicion
подсказывать/подсказать		to hint, suggest
позволять/позволить		to be allowed
позор		disgrace
покидать/покинуть		to leave, abandon
покойник		the deceased
полагать		to suppose, think
полагаться		to be supposed to
помещаться/поместиться		to find room, to fit
поражать/поразить		to astonish
посетитель		visitor
постановка		organisation
постановление		decision, resolution
потребительский		consumer
потребность		need
пошлина		duty, customs
появляться/появиться		to appear, emerge
представать/предстать перед		to appear before
предупреждать/предупредить		to warn
предупреждение		warning, prevention, notice
предыдущий		previous
преимущество		advantage, privilege
пренебрегать		to neglect
пререкание		argument
пригодиться		to prove useful
приём		hint, trick, method
приживаться/прижиться		to settle in
приказывать/приказать		to demand
принадлежать (+ dative)		to belong to
принижающий		belittling
приниматься/приняться		to begin to
пристрелить		to shoot
присутствие		presence; office; attendance; physical presence
производительность		productivity
промышленник		manufacturer
проникать		to intrude, break in
противодействие		opposition, resistance
противопоставлять/противопоставить		to oppose
противопоставление		opposition
проявлять/проявить		to show, display
равнодушный к		indifferent to
разгадывать/разгадать		to guess
раздавать/раздать		to distribute
рвануть с места		to dash on, move off
ровно		exactly, regularly
рознь		disagreement
рубить/срубить		to chop down
рубрика		column (newspaper)
с боку на бок		from side to side
сажать		to plant/ to put in prison
сбережения		savings, cash
сдвигаться/сдвинуться		to move off
село		village, settlement
сзади		from behind
следствие		investigation
слезать/слезть		to climb down
смена		change, shift
сносить/снести		to demolish
совещание		meeting, conference
содержаться		to keep, contain
сокращать/сократить		to curtail, reduce
соображать/сообразить		to imagine
соответствие		accordance, conformity
сопровождать		to accompany
составлять/составить		to compile, make up
состояние		state, condition; fortune
спадать/спасть		to fall
справка		certificate
справляться/справиться		to cope/to get to grips with/to manage/to inquire about
спуститься		to descend
средство		means, remedy
стеклянный		glass
стесняться/постесняться		to feel shy, ashamed, embarrassed
стихийно		spontaneously
стихийный		spontaneous
стремиться		to strive for, aspire to
ступеньки		stairs
сутки		24 hours
творческий		creative
тесть		father in law
тёща		mother in law
трогаться/тронуться		to set off
убеждаться/убедиться		to be convinced of
уберегать/уберечь		to protect, keep safe, guard
убитый		the killed/depressed
убытки		damages
увольнение		dismissal
угрюмый		gloomy, sullen, morose
удаваться/удаться		to succeed, turn out well, manage
управлять		to operate, control
уроженец		native
условность		convention
условный		conditional
установка		installation
устройство		means
утверждать/утвердить		to claim, assert, allege
хоть		even, at least
чередовать		to alternate, take turns
черты		boundaries
чесать/почесать		to comb, scratch
ЧП (чрезвычайное происшествие)		surprising incident
чужой		somebody else's, strange, forei