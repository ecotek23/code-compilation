la lima, lime
la ausencia, absence
el daño, damage
la estrategia, strategy
Marte, Mars
normalmente, normally
gobernar, to govern (not regir)
universitario, (from) university
garantizar, to guarantee
el capitán, captain
atraer, to attract
opinar, to opine (to believe; to think)
pasado, past
particular, particular
el planeta, planet
el piano, piano
la alegría, happiness; joy (a...)
la chica, girl (c...)
el delito, crime (not too serious)
figurar, to appear; to be listed (f...)
la culpa, guilt; blame
la arena, sand
el novio, boyfriend
la interpretación, interpretation
otorgar, to award (o...)
breve, brief; short (not corto)
la longitud, longitude; length (not "largo")
bailar, to dance
cuándo, when
el cubano, Cuban (person)
la explicación, explanation
el contenido, content
corto, short; brief (not breve)
el temor, fear (t...)
la agricultura, agriculture
la danza, dance
procedente, proceeding from
inmediatamente, immediately; at once (not enseguida)
el hueso, bone
incrementar, to increase (not aumentar)
modificar, to modify
la discusión, discussion; argument
la exportación, exportation
el imposible, impossible
despedir, to say goodbye; to fire/lay off (employee)
robar, to rob
registrar, to register
sesenta, sixty
la publicación, publication
médico, medical
experimentar, to experiment
administrativo, administrative
la academia, academy
el monte, mount; mountain (not "montaña")
evidente, evident
la encuesta, poll
característico, characteristic
el ingeniero, engineer
la nube, cloud
la perspectiva, perspective
inventar, to invent
la entrevista, interview
el dominicano, person from the Dominican Republic
la atmósfera, atmosphere (not "ambiente")
orientar, to orient; to guide; to direct
prometer, to promise
el coronel, colonel
constitucional, constitutional
genético, genetic
el cristal, crystal
auténtico, authentic
la moda, fashion
el jueves, Thursday
ingresar, to join; to enter; to deposit
la vieja, old woman
poner, to put (not colocar)
la columna, column
el ensayo, essay
absolutamente, absolutely
mezclar, to mix
contemporáneo, contemporary
la gestión, management
sumar, to sum
desempeñar, to carry out; to perform (d...)
el universo, universe
educativo, educational
doble, double
cultivar, to cultivate
el pájaro, bird (not "ave")
la guardia, guard (female)
impulsar, to boost; to propel
la tensión, tension
favorecer, to favor (not propiciar)
el arquitecto, architect
la riqueza, richness; wealth
el honor, honor
la manifestación, manifestation; demonstration (not "demostración")
acaso, perhaps (a...)
la dinastía, dynasty
la playa, beach
el lector, reader
el festival, festival
la galería, gallery
inmenso, immense
el invierno, winter
temprano, early
la leche, milk
el enfermo, sick person
arrancar, to tear off; to start (i.e. a computer)
el particular, private individual
arreglar, to mend; to fix (not reparar)
constante, constant
la ruta, route
volar, to fly; disappear/vanish; blow away; fly by
la derecha, right (direction)
la conclusión, conclusion
el auto, car (not "vehículo"; "coche"; "carro"; or "automóvil")
alzar, to lift; raise (a...) (not aumentar or asentar)
fiscal, fiscal
el secreto, secret
externo, external
la derrota, defeat
descender, to descend
supremo, supreme
la esquina, corner (not "rincón")
la sección, section
Washington, Washington
el retrato, portrait
habitual, habitual
la excepción, exception
ampliar, to amplify
lejano, far; distant (l...) (not lejos)
la suma, sum
nuevamente, newly
practicar, to practice
soñar (con), to dream (about)
lento, slow
la actuación, performance (of an actor)
hundir, to sink
fino, thin; fine (high quality)
el actor, actor
aclarar, to clarify
el productor, producer
gubernamental, governmental
el patrón, pattern
el nacimiento, birth
perfectamente, perfectly
interrumpir, to interrupt
la cola, tail; line; glue
derrotar, to defeat
invadir, to invade
la apertura, opening
la huelga, strike (work-stoppage; walk-out)
la inteligencia, intelligence
el pago, payment
el código, code (not "la clave")
tardar, to be late; to be slow; take (time)
la pasión, passion
inicial, initial
ceder, to yield
la distribución, distribution (not reparto)
la obligación, obligation
ligero, light; lightweight; slight (not leve)
diseñar, to design
brillante, bright; shiny; sparkling; brilliant
el alcalde, mayor
apartar, to move away; to separate (not separar)
creciente, growing
el socialista, socialist
peligroso, dangerous
el océano, ocean
la amistad, friendship
encabezar, to head
ajeno, alien; foreign
el laboratorio, laboratory
la soledad, loneliness
la sesión, session
lindo, cute
el símbolo, symbol
infantil, infantile
el costo, cost (not coste)
muchísimo, very much; a whole lot
percibir, to perceive
promover, to promote
gozar, to enjoy (g...)
denunciar, to denounce
la búsqueda, search (long word)
el techo, ceiling; roof (generic)
el virus, virus
esencial, essential (not "imprescindible")
soportar, to bear; to endure; to put up with (not aguantar)
la península, peninsula
el experto, expert
la conquista, conquest
carecer, to lack (not faltar)
amarillo, yellow
la profesión, profession
la lágrima, tear (water from eye)
conquistar, to conquer (not vencer)
el accidente, accident
perseguir, to pursue
regalar, to give (as a present)
Israel, Israel
afuera, outside
la amenaza, threat
desconocido, unknown
el mexicano, Mexican (person)
marino, marine
peor, worse
prever, to anticipate (not anticipar)
facilitar, to facilitate
el zapato, shoe
liberal, liberal
el margen, margin
encerrar, to enclose
efectivamente, really; indeed (e...)
el azúcar, sugar
laboral, laboring
reclamar, to claim
la conducta, conduct
la corona, crown
la expansión, expansion
la avenida, avenue
el humo, smoke
el polvo, dust; powder
cargar, to load
la busca, search
el gato, cat
el reloj, clock
el miércoles, Wednesday
la molécula, molecule
dulce, sweet
la emoción, emotion; excitement
húmedo, humid; damp; moist
el salón, living room (not sala)
encender, to light; arouse
Egipto, Egypt
centrar, to center
once, eleven
el regreso, regression
quemar, to burn; incinerate; scorch (q)
finalizar, to finalize (not ultimar)
el traje, suit
el incremento, increment; increase (not alza or aumento)
el borde, border; edge (not frontera)
el rasgo, feature
la caída, fall; falling; tumble
el cliente, client
el canto, singing
la protesta, protest
el vaso, glass; vessel
calificar, to qualify
la compra, purchase
la profundidad, depth; profundity
regional, regional
la humanidad, humanity
el aeropuerto, airport
el impuesto, tax
continuo, continuous
la rueda, wheel (r...)
poderoso, powerful
derivar, to derive
violento, violent
Venezuela, Venezuela
el salario, salary (not sueldo)
localizar, to locate (not ubicar)
elaborar, to make; manufacture; produce
el muro, wall (m.. ; not muralla)
organizado, organized
la catedral, cathedral
abarcar, to cover; to encompass; to span; to include (a...)
inclinar, to incline
el vacío, void
triste, sad
el caballero, gentleman
la cuerda, cord; rope
la extensión, extension (not ampliación)
el filósofo, philosopher
estrecho, narrow; tight
liberar, to liberate (not librar)
jurídico, legal (not "legal")
calcular, to calculate
el anciano, elderly person (not viejo)
frecuente, frequent
apreciar, to appreciate
Panamá, Panama
recientemente, recently (not recién)
fundamentalmente, fundamentally
cero, zero
condenar, to condemn
el castillo, castle
el sindicato, union (s...)
molestar, to bother; to upset
la observación, observation
la rosa, rose
previo, previous (not anterior)
originar, to originate
el reconocimiento, recognition
el italiano, Italian
verdaderamente, truly
el placer, pleasure (p...)
electrónico, electronic
el senador, senator
la pelota, ball (not bola or balón)
la hacienda, estate (h...)
la nariz, nose
la liberación, liberation
el toro, bull
confiar, to trust; to rely
saludar, to greet
desnudo, naked
deportivo, sportsmanlike
la corrupción, corruption
atreverse, to dare; to venture
la civilización, civilization
el testigo, witness
la vez, time; instance; turn
el equilibrio, balance (not balance)
el enemigo, enemy
el carro, car
el cáncer, cancer
la bomba, bomb
revisar, to revise; to review
la palma, palm
el judío, jew
la jornada, working day
concentrar, to concentrate
diario, daily (not cotidiano)
el hilo, thread (to sew with)
transportar, to transport
ajá, aha
resistir, to resist
autónomo, autonomous
el corredor, runner; corridor
el estadio, stadium
la partida, game; departure
inferior, lower
alimentar, to feed
adentro, inside (not dentro)
el cura, priest (not sacerdote)
elevado, elevated
ocultar, to hide (not esconder)
renunciar, to renounce; quit (not dimitir)
la doctrina, doctrine
el enfrentamiento, confrontation
confundir, to confound
la sierra, saw
lavar, to wash
vacío, empty; void
la nave, ship (n...)
precioso, precious
prolongar, to prolong
especializado, specialized
la medida, measurement
sobrevivir, to survive
soltar, to release; to let go of; to drop
correcto, correct
agarrar, to grab
la coalición, coalition
la torre, tower
callarse, to shut up; to be quiet
el automóvil, car (not carro; coche; or auto)
armado, armed
parlamentario, parliamentary
el loco, madman
ignorar, to be ignorant of; to not know (not desconocer)
el oído, ear (inner part); hearing (sense of)
designar, to designate
el fracaso, failure
el depósito, deposit
comparar, to compare
naturalmente, naturally
monetario, monetary
inspirar, to inspire
la resolución, resolution
el campeonato, championship
el argumento, argument; plot
terrestre, terrestrial
la fila, row; line (not línea or raya)
excelente, excellent
denominado, named; called
regular, to regulate
equivocarse, to be mistaken; to be wrong
la fábrica, factory
la fruta, fruit
crítico, critical
el tráfico, traffic
el tiro, throw; shot (not disparo)
la cumbre, summit (not cima)
el impacto, impact
el mando, command; rule; leadership
la marca, mark
intelectual, intellectual
convenir, to agree; to suit (not acceder or acordar)
el mandato, mandate
negativo, negative
correspondiente, corresponding
la dimensión, dimension
expulsar, to expel
arrojar, to throw; to hurl (not echar; lanzar; or tirar)
limitado, limited
la pobreza, poverty
la oscuridad, darkness
la fórmula, formula
marcado, marked
el huevo, egg
firme, firm
el personal, staff
suave, soft (not "blando")
japonés, Japanese
espacial, relating to space
futuro, future
caliente, hot
magnífico, magnificent
el sujeto, subject
la virgen, virgin
la tienda, shop
existente, existing
ejecutar, to execute; perform
suspender, to suspend
la instrucción, instruction
el incendio, fire (i.e. building burning)
el registro, registry
el ala, wing
el cabello, hair (not pelo)
el verso, verse
completar, to complete
empujar, to push
el príncipe, prince
el componente, component
evidentemente, evidently
la escalera, stairs
perdonar, to forgive; to pardon
el impulso, impulse
detectar, to detect
la dama, lady (d)
vincular, to link (not juntar)
la disciplina, discipline
la federación, federation
primitivo, primitive
pronunciar, to pronounce
divino, divine
fallecer, to die (not morir)
el señor, Mr.; master; the Lord
secundario, secondary
la aventura, adventure
poético, poetic
la orilla, shore
el crimen, crime (very serious)
Cristo, Christ
el concurso, contest; tendering process [business]
besar, to kiss
Grecia, Greece
el sacerdote, priest (not cura)
extenso, extensive
extraer, to extract
la denuncia, denunciation; complaint
el carbono, carbon
conformar, to conform
segundo, second
limpio, clean
maravilloso, wonderful; marvelous
el fruto, fruit
traducir, to translate
el pantalón, trousers; pants (singular)
relativamente, relatively
procurar, to seek; try (procure)
la concentración, concentration
conectar, to connect
abrazar, to hug
el exterior, exterior
poblar, to populate
rural, rural
la villa, villa
el vestido, dress
reproducir, to reproduce
setenta, seventy
falso, false
descansar, to rest
el ángel, angel
dudar, to doubt
efectivo, effective
el máximo, maximum
múltiple, multiple
adelantar, to advance (ad...)
indígena, indigenous
material, material
lentamente, slowly (l...)
la pista de baile, dance floor
colgar, to hang
el empleado, employee
el entorno, environment; surroundings (not ambiente)
la tela, fabric (not tejido)
formal, formal
confesar, to confess
el mapa, map
lucir, to shine (not brillar)
la oración, prayer; sentence
penetrar, to penetrate
la utilización, utilization; use (not uso or aplicación)
boliviano, Bolivian
el cantante, singer
Austria, Austria
la facilidad, facility; ease; aptitude
de repente, suddenly
la ilusión, illusion
el humor, mood
el víctor, victor
la mezcla, mixture; combination; blend
encantar, to enchant; like
inaugurar, to inaugurate
la tabla, board (not bordo)
Alejandro, Alexander
Canadá, Canada
pasear, to go for a walk (not caminar or andar)
ochenta, eighty
la intensidad, intensity
la computadora, computer (not ordenador)
la virtud, virtue
el oficio, trade; job
sólido, solid
la variación, variation
proclamar, to proclaim
ascender, to ascend
emprender, to undertake; to begin
el establecimiento, establishment
la concepción, conception
colombiano, Colombian
negociar, to negotiate
la misa, mass (religious)
habitar, to live in; to inhabit (not residir)
izquierda, left
la mancha, spot; stain
semejante, similar (not similar or parecido)
rendir, to perform well
respirar, to breathe
el alcance, reach; range
acumular, to accumulate
tremendo, tremendous
el acero, steel
asomar, to show up; stick out; poke out
el comandante, commander
la cárcel, jail
distribuir, to distribute (not repartir)
desplazar, to displace
el rincón, corner; nook (not esquina)
la imaginación, imagination
la reducción, reduction
el significado, meaning
pesado, heavy
orgánico, organic
global, global
la radiación, radiation
el desierto, desert
conversar, to converse; to talk (not dialogar; hablar; or platicar)
sagrado, sacred
limpiar, to clean
acceder, to agree; to access (not acordar or convenir)
el campeón, champion
la continuación, continuation
la llanura, prairie
metálico, metallic
la postura, posture
perdido, lost
eficaz, efficient (not eficiente)
apretar, to press (not presionar)
juzgar, to judge
Ana, Anna
Castilla, Castile
fumar, to smoke
sucio, dirty
el perdón, forgiveness
pintado, painted
la ceremonia, ceremony
disparar, to shoot; to fire (not tirar)
la tumba, grave
la llave, key (for a lock); tap; switch; wrench
dentro, inside (not adentro)
el especialista, specialist
criticar, to criticize
Isabel, Elizabeth
Jaime, James
Ernesto, Ernest
el combate, combat
el capítulo, chapter
circular, to circulate
grueso, thick (not espeso)
justificar, to justify
la consulta, consultation
el combustible, fuel
el aceite, oil
la combinación, combination
la clave, code; key (not código or llave)
la camisa, shirt
cansado, tired
la escritura, writing
la posesión, possession
la presentación, presentation
la identidad, identity
definitivamente, definitely
combatir, to combat; to fight (not luchar or pelear)
la botella, bottle
asesinar, to murder
increíble, incredible
la secretaría, secretary's office
la rodilla, knee
ancho, broad; wide; capacious (not amplio)
la cooperación, cooperation
el conservador, conservative
colonial, colonial
precisar, to specify
el seno, bosom; breast
alterar, to alter
el progreso, progress
el plato, dish; plate
medieval, medieval
vital, vital
invertir, to invest
la gloria, glory
Gonzalo, Gonzalo
la patria, homeland
solar, solar
el vapor, vapor
la preparación, preparation
la inflación, inflation
la antigüedad, seniority
considerable, considerable
la capitalización, capitalization
el esposo, husband (not marido)
consultar, to consult
el creador, creator
Díaz, Diaz
animar, to animate; to encourage
el obispo, bishop
la opción, option
la angustia, anguish
clasificar, to classify
dotar, to gift; to provide (not dar; proporcionar or regalar)
escoger, to choose (not elegir)
el palo, stick
Horacio, Horace
enamorar, to win the heart
delgado, slim; slender; lean
la cuenca, basin; socket
doler, to hurt
el paseo, walk (stroll)
proyectar, to project
el rival, rival
el narcotráfico, drug trafficking
mental, mental
el receptor, recipient
acostar, to put to bed
tropical, tropical
listo, clever; ready
la garantía, guarantee
alegre, happy (a...)
el cerro, hill (not colina)
el bolsillo, pocket
maestro, master; main
la grabación, recording
la curiosidad, curiosity
gastar, to spend
colaborar, to collaborate
afectado, affected
apagar, to turn off
situado, situated
la recuperación, recuperation; recovery (not recobro)
latinoamericano, Latin American
el escándalo, scandal
Diego, Diego
acariciar, to caress
la alternativa, alternative
el ballet, ballet
la proporción, proportion
la pata, paw
Colón, Columbus
el cadáver, cadaver
el vicepresidente, vice president
el cristiano, Christian
la creencia, belief
titular, to title; call
compuesto, composed
la presa, prey
el golfo, gulf
la colaboración, collaboration
cotidiano, daily (not diario)
evolucionar, to evolve
Nicolás, Nicholas
la clasificación, classification
el carbón, coal
el espectador, spectator
el defensor, defender
la reflexión, reflection (not reflejo)
la vela, candle
jurar, to swear
el hielo, ice
dictar, to dictate
el entusiasmo, enthusiasm
el asesinato, murder
abundante, abundant
delicado, delicate
respectivo, respective
recurrir, to turn to; appeal to
adaptar, to adapt
aislado, isolated
doméstico, domestic
el funcionamiento, operation (functioning)
judío, jewish
el montón, heap; pile
romántico, romantic
concebir, to conceive
cargado, loaded
la fortuna, fortune (not ventura)
eterno, eternal
el ejemplar, copy (e...)
la claridad, clarity
claramente, clearly
el salto, jump; bounce
el taller, workshop; garage
la infancia, childhood (i)
el testimonio, testimony
el turismo, tourism
repartir, to distribute (not distribuir)
el camión, truck
el culto, cult
el fiscal, prosecutor
probable, probable
el técnico, technician
estallar, to explode (not explotar)
el bronce, bronze
académico, academic
agradecer, to thank; be grateful for; appreciate
agudo, acute
el ayuntamiento, city hall
el prado, meadow
padecer, to suffer from
admirar, to admire
consumir, to consume
el coro, choir
la cita, appointment; date (with somebody)
la cinta, tape; ribbon
el cigarrillo, cigarette (not cigarro)
brindar, to drink a toast (to)
asiático, asian
la fibra, fiber
la tragedia, tragedy
transcurrir, to pass (time; not pasar)
el experimento, experiment
aguantar, to bear; to endure (not soporta
