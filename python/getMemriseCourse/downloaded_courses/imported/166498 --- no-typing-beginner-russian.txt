слегка, slightly
несколько, several; some
утрo/до полудня, morning/before noon
насчёт, as for
около, by; near
имя прилагательное, adjective
взрослый, adult
после того как, after (phrase)
после, after; afterwards
вторая половина дня, afternoon; second half of day
опять, again
аэропорт, airport
всё, everything
всегда, always
американский, American
и, and
ответ, answer; reply
отвечать, to answer
квартира, flat; apartment
апрель, april
прибывать, to arrive
прибыл/-а/-и, arrived (he/she/they)
картинная галерея, art gallery
просить, to ask
спрашивать, to ask a question
спрашивающий, asking
спящий, asleep; sleeping
в, in; at
август, August
плохой, bad
мешок/сумка, bag
балкон, balcony
бар, bar
плитка, bar; tablet
баскетбол, basketball
пляж, beach
прекрасный, beautiful; fine
стал/-а/-и, became
потому что, because
кровать, bed
до, to; up to; about; before
начало, beginning
велосипед, bicycle
большой, big; large; important
день рождения, birthday
черный, black
блузка, blouse
лодка, boat
скучный, boring
родившийся, born
оба/-е, both
бутылка, bottle
купил/-а/-и, bought (he/she/they)
рамка, frame
мальчик, boy
хлеб, bread
завтрак, breakfast
приносить, to bring
Британия, Britain
брат, brother
коричневый, brown
гамбургер, hamburger
автобус, bus
автобусная станция, bus station
занятой, busy
но, but
посредством, by means of; with the help of
пока (в знач. "до свидания"), bye; see you
кафе, cafe
торт/пирожное, cake
звонить по телефону, to make a phone call
называться, to call oneself
фотоаппарат, camera
отдых на природе в палатке, camping (in a tent)
территория университета/колледжа, campus
можно ли, is it possible
мочь/иметь возможность, to be able/to have the possibility
свеча, candle
автомобиль, car
Открытка, Postcard
Ковёр, Rug
повседневный, daily; casual
кошка/кот, cat
центр, center
стул, chair
дешёвый, cheap
дети, children
Китай, China
китайский, chinese
шоколад, chocolate
церковь, church
кинотеатр, movie theater
обводить кружком, to circle around smth.
классический, classical
уборщик/-ца, cleaner
закрывать, to close
закрытый/ая/-ые, closed
одежда, clothes; clothing
(ночной) клуб, (night) club
пальто, coat
кофе, coffee
кола, a cola
холодный, cold; cool
колледж, equivalent to community college
цвет, colour; color
приходить, to come
приходящий, coming
компания, company
компьютер, computer
концерт, concert
беседа, conversation
готовил/-а/-и, cooked; prepared (he/she/they)
приготовление пищи, cooking of food
прохладный, cool
хлопок, cotton
могли бы, could be
сельская местность, village; "countryside location"
супружеская пара, married couple
накрывать, to cover
крестик, cross
зачёркивать, to cross out
переполненный, overflowing
чашка, cup
шкаф для посуды, cupboard
ездить на велосипеде, to bike around
папа, dad; daddy
дочь, daughter
cутки, 24 hours
декабрь, december
пустыня, desert
письменный стол, desk; writing table
умер/-ла/-ли, died (he/she/they)
непохожий, different; dissimilar
ужин, supper
дискотека, dance club; disco
блюдо/кушанье, dish (of food)
делать, to make
врач, physician; doctor
собака, dog
делающий, doing
доллар(ы), dollar
не (делайте что-либо), don't (do something)
дверь, door
вдоль/по, downward; along
платье, dress
напиток, drink
пить, to drink
ехать/ездить на машине, to drive
сухой, dry
DVD/цифровой видеодиск, DVD
каждый, each; every
рано, early
есть (пищу), to eat (food)
египетский, Egyptian
восемь, eight
восемнадцать, eighteen
восемьдесят, eighty
одиннадцать, eleven
Англия, England
конверт, envelope
евро, euro
вечер, evening
каждый/все, each; all
везде/всюду, everywhere
Извините ..., Excuse me
дорогой (по цене), expensive; dear (in price)
семья, family
далеко, far; far off
быстрый, quick; fast
фаст фуд/еда быстрого приготовления, fast-food
отец, father
любимый, dear; loved
февраль, february
пятнадцать, fifteen
пятьдесят, fifty
фильм, film (movie)
хорошо себя чувствующий, feeling fine
заканчивать, to end; bring to an end
сперва/первым делом, in the first place
рыба, fish
пять, five
флаг, flag
пол, floor; sex; gender
еда, food
футбОл, soccer
для, for; to
на, on; it; at; to
официальный, official
сорок, forty
четыре, four
четырнадцать, fourteen
свободный, free; at liberty
пятница, Friday
друг, friend
из, from; of; in
фрукты, fruit
сад (при доме), garden (around the house)
Германия, Germany
добираться, to arrive; reach somewhere
становиться, to become
вставать, to get up
девочка/девушка, girl
стакан, glass; cup
очки, (eye)glasses
идти/ехать, to go
идущий, going
хороший, good; nice
до свидания, good bye
получил/-а/-и, got; received (he/she/they)
дедушка, grandfather
бабушка, grandmother
зелёный, green
вырос/-а/-и, grew (he/she/they)
серый, grey; dull
становиться взрослым, to grow up; become an adult
гитара, guitar
имел/-а/-и, had (he/she/they); possessed
парикмахер/парикмахерская, hairstylist
гамак, hammock
имеет (он/она/оно), has; possesses
шляпа, hat
иметь, to own
имеющий, having; owning
он, he
здравствуй / привет, hello
помогать, to help
её, her
здесь, here
Вот; пожалуйста., Here you are.
Привет, Hi (informal)
его, his
выходной день, holiday (day off)
каникулы/отпуск, holiday
жилище, home; dwelling place; abode
надеяться, to hope
больница, hospital
хостел/студенческое общежитие, hostel
горячий, hot (e.g. food; drink)
гостиница, hotel
час, hour
дом, house; home
работа по дому, housework
как, how; what; as; like
Как насчёт...?, what about...?
сто, hundred
муж, husband
я, I
мороженое, ice cream
идея, idea
больной, ill; sick
важный, important
в кровати, in bed
Индия, India
индийский, Indian
интересный, interesting
международный, international
приглашать, to invite
это, that; this; it
итальянский, Italian
Италия, Italy
куртка, light jacket
январь, january
Япония, Japan
японский, japanese
джинсы, jeans
работа, work
путешествие, trip; journey
июль, july
свитер, sweater
июнь, june
как раз, just; at once
только что, just; only
шашлык, shish kebab; _shashlik_
киоск, kiosk
кухня, kitchen
знать, to know
лампа, lamp
язык, language; tongue
прошлый, past; gone by
поздно, late
поздний, late
позднее, later
руководитель, head; leader
покидать, to leave; abandon
покинул/-а/-и, left (he/she/they); abandoned
лимонад, soft drink
меньше, less
давай(те), let's
буква, letter (of the alphabet)
нравиться, to be liked
слушать, to listen
слушал/-а/-и, heard (he/she/they)
жить, to live
жил/-а/-и, lived (he/she/they)
живёт (он/она/оно), lives (he/she/it)
долгий, long
далёкий, distant; far away
посмотреть (на), to look (at)
искать, to search
смотрел/-а/-и, watched (he/she/they)
терять, to lose
потерял/-а/-и, lost (he/she/they)
множество, multitude; mass
любить, to love
обед, dinner (lunch)
журнал, magazine
галерея магазинов, a commercial center; mall
мужчина, man; male
менеджер/руководитель, manager
много, many; much
карта, map
март, March
женатый/замужний, married (male/female))
жениться/выходить замуж, to get married (male/female)
Матч, Game; match (sports event)
май, May
может быть, maybe
я/меня, me
приём пищи, meal; taking food
означать, to mean
мясо, meat
Средний, Middle; central
встречать(ся), to meet
встреча, meeting; reception
мужчины, men
встретил/-а/-и, met (he/she/they)
метро, subway
мексиканский, Mexican
полдень, midday
минеральная вода, mineral water
мобильный телефон, mobile phone
понедельник, Monday
месяц, month
больше, more
утро, morning
мечеть, mosque
большинство, majority
мать, mother
переезжать, to drive across (impf)
переехал/-а/-и, moved away (he/she/they)
MP3 плеер, Mp3 player
Мама, mum
музей, museum
музыка, music
музыкант, musician
мой/моя/моё/мои, my
имя, name
Рядом, close-by
почти, almost
также не, neither
никогда, never
новый, new; modern
газета, newspaper
следующий, next; following
совсем рядом, immediately next to
приятный, pleasant
Приятно познакомиться., Nice to meet you.
ночь, night
девять, nine
девятнадцать, nineteen
девяносто, ninety
нет, no
нет проблема/запросто, no problem
шумный, noisy; loud
вермишель, noodles
нормальный/обычный, normal; habitual
не, not
имя существительное, noun
ноябрь, november
номер, number
по часам/на часах, o'clock
октябрь, october
конечно, of course
оффис, office
часто, often
в порядке, alright; in order
старый, old
один, one; some; alone
Открывать, to open
открытый, open
апельсиновый сок, orange juice
после полудня, afternoon
пачка, pack; package
страница, page
рисовать красками, to paint in color
рисовал/-а/-и, drew (he/she/they)
художник, painter; artist
пара, pair; couple
родители, parents
партнёр, a partner
вечеринка, party (get-together)
блюдо из макарон, a pasta dish
платить, to pay
ручка (для письма), a pen
люди, people
телефон, telephone
звонил/-а/-и по телефону, phoned (he/she/they)
пианино, piano
пикник, picnic
картина, picture
пицца, pizza
самолет, aircraft; aeroplane
тарелка, plate
играть, to play
играющий, playing
пожалуйста, please
поэзия, poetry
сосуд (для приготовления пищи/хранения жидкостей), pot (for preparing food/keeping liquids)
фунт(ы) (денежная единица), pound (currency unit)
молиться, to pray
подарок, gift
цена, price
вероятно, probably
ставить, to put; place; set
пирамида, pyramid
вопрос, question
тихий, quiet; low; silent
радио, radio
читать, to read
очень, very
красный, red
ресторан, restaurant
рис, rice
правильный, correct; right; proper
рок (музыка), Rock (music)
комната, room
Россия, Russia
салат, salad
продавец, grocer
такой же, same
бутерброд, sandwich
сел/-а/и, sat (he/she/they)
суббота, Saturday
видел/-а/-и, saw (he/she/they)
Говорить, to speak; to talk
школа, school
Увидимся., See you.
продавать, to sell
предложение, sentence; proposition
сентябрь, september
семь, seven
семнадцать, seventeen
семьдесят, seventy
она, she
полка, shelf; a bed in a train car
Рубашка, Men's shirt
туфли, shoes
магазин, store
покупка товаров, shopping
короткий, short
душ, shower
осмотр достопримечательностей, sightseeing
певец/-ца, singer
сестра, sister
сидеть, to sit
сидящий, sitting
шесть, six
шестнадцать, sixteen
шестьдесят, sixty
размер, size; dimensions
катание на лыжах, skiing
юбка, skirt
спать, to sleep
медленный, slow
маленький, small; little
итак/поэтому, so; that's why
продавал/-а/-и, sold (he/she/they)
несколько/некоторое количество, some/ a certain quantity
иногда, sometimes
сын, son
скоро, quickly; fast; soon
сожалеющий, sorry; regretting
особый, special
специальный, special
спорт, sport
марка, stamp
начинать, to begin
станция, station
оставаться, to stay
оставался/-ась/-ись, stayed
кусок мяса, a piece of meat
улица, street
уличный лоток, a street stall
крепкий, strong
учащийся/студент, student
изучал/-а/-и, studied (he/she/they)
изучать, to study
сахар, sugar
костюм, suit
лето, summer
воскресенье, Sunday
очки от солнца, sunglasses
солнечный, sunny
Супермаркет, Supermarket
уверенный, confident; sure
плавание, swimming
бассейн, swimming pool
занимать (по времени), to take (time)
говорящий, talking
такси, taxi
чай, tea
учитель, teacher
чайник (для заварки), teapot (kettle)
технология, technology
десять, ten
теннис, tennis
ужасный, awful
текст, text
сообщение с мобильного телефона, text message; mobile phone message
спасибо, thank you
тот/та/то, that
определённый артикль, definite article
США, USA
театр, theater
затем, then; after that
там/туда, there/to there
имеется, having
эти, these
они, they
думать, to think; believe
тринадцать, thirteen
тридцать, thirty
тридцать пять, thirty-five
этот/эта/это, this
три, three
Через, through; in (in time expressions)
четверг, Thursday
галочка/отметка, tick; checkmark
галстук, tie
время, time
сегодня, today
вместе, together
завтра, tomorrow
тоже, also; as well; too
слишком, too; too much
город, city
поезд, train
Трамвай, Streetcar; tram
дерево, tree
поездка, trip
брюки, trousers
футбОлка, T-Shirt; jersey
вторник, Tuesday
телевидение, TV (broadcast; programming)
двенадцать, twelve
двадцать, twenty
двадцать пять, twenty-five
два, two
подчёркивать, emphasize; underline
университет, university
использовать, to use; utilize; make use of
обычно, usually
овощ, vegetable
глагол, verb
деревня, village
посещать, to visit
посещал/-а/-и, visited (he/she/they)
официант, server
просыпаться/будить, to wake up
ходить/идти  пешком, to walk
прогулка, a stroll
стена, wall
вальс, waltz
теплый, warm
был/была, was (m/f)
Смотреть, to watch
смотрящий, watching
вода, water
мы, we
слабый, weak
носить (одежду), to wear (clothes)
одетый/-ая/-ые в, wearing
погода, weather
среда, environment
неделя, week
выходные (суббота и воскресенье), week-end (saturday and sunday)
шёл/шла/шли, went (m/f/pl)
были, they were
мокрый, wet
что/какой, what
Как?/На что похоже?, What... like?
когда, when; while; as
где, where
белый, white
кто, who; that; some
широкий, wide
жена, wife
окно, window
зима, winter
с, with; and; from; of
женщина, woman
женщины, women
замечательный, wonderful
слово, word
работать, to work
работающий, working
работает (он/она/оно), works (m/f/n)
очень хотел/-а бы, would love to
писать, to write
писатель, writer
писал/-а/-и, wrote
жёлтый, yellow
ты/вы, you
твой/ваш, you
