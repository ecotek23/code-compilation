anfangen, to start
bedienen, to wait on
sich beeilen, to hurry
berufen, to summon
besitzen, to own
biegen, to bend
binden, to bind
blühen, to bloom
einkaufen, to shop
einstellen, to put in
erwarten, to anticipate
fluchen, to swear
frieren, to freeze
frühstücken, to eat breakfast
graben, to dig
malen, to paint
sagen, to say
verbieten, to prohibit
verderben, to spoil
wagen, to risk
ausgehen, to go out
beweisen, to give evidence
brechen, to break
empfehlen, to recommend
sich ereignen, to take place
flechten, to braid
genießen, to enjoy
gucken, to look
herrschen, to reign
kündigen, to quit
ruhen, to rest
schießen, to shoot
schwingen, to swing
sein, to be
sitzen, to be sat
stoppen, to stop
teilen, to divide
tun, to do
sich verlieben, to fall in love
achten, to respect
antworten, to answer
speien, to spit
wägen, to consider
ändern, to change
bringen, to bring
überholen, to overtake
auffordern, to call upon
wechseln, to exchange
aufpassen, to pay attention
kämpfen, to battle
meiden, to avoid
versuchen, to try
sich heiraten, to get married
beruhigen, to quiet
unternehmen, to undertake
verteilen, to distribute
hauen, to chop down
beschäftigen, to occupy
mitteilen, to inform
zeichnen, to portray
beachten, to obey
entstehen, to originate
erhöhen, to increase
beobachten, to observe
erlauben, to permit
bedeuten, to mean
kochen, to cook
vorkommen, to occur
scheiden, to separate
verwenden, to use
begleiten, to accompany
befinden, to find
besuchen, to visit
gehen, to go
einladen, to invite
aufhören, to discontinue
reiben, to rub
reiten, to ride
aufregen, to upset
halten, to keep
entschuldigen, to excuse
heben, to raise
setzen, to set; place down
bestimmen, to determine
sieden, to boil
probieren, to sample
kommen, to come
überzeugen, to convince
saufen, to drink in excess
verhaften, to arrest
glauben, to believe
ausstellen, to exhibit
sinken, to sink
annehmen, to take on
greifen, to grasp
unterrichten, to teach
erfahren, to undergo
sich bemühen, to make an effort
werden, to become
befreien, to free
anbieten, to offer
beißen, to bite
regnen, to rain
aussprechen, to express
schlingen, to tie
sterben, to die
pfeifen, to whistle
einfallen, to collapse
existieren, to exist
laufen, to run
betonen, to stress
entlassen, to release
weisen, to refer; to reject
überwinden, to overcome
erklären, to explain
wissen, to know
schaffen, to create
konzentrieren, to concentrate
fallen, to fall
spalten, to split
bleiben, to stay
schelten, to moan
verbinden, to connect
sammeln, to collect
trösten, to comfort
zahlen, to pay for
erfüllen, to fulfill
schieben, to push
begrüßen, to greet
bestätigen, to verify
hören, to hear
gebrauchen, to employ
vergessen, to forget
unterbrechen, to interrupt
schnauben, to snort
festhalten, to hold tight
warnen, to warn
schmeißen, to fling
überlegen, to think over
baden, to bathe
holen, to fetch
kümmern, to worry
wiederholen, to repeat
haben, to have
anmachen, to turn on
schwören, to swear
nachdenken, to reflect
schmecken, to taste
pflegen, to care for
schinden, to mistreat
retten, to rescue
kleben, to stick
bleichen, to bleach
betragen, to amount to
treiben, to force
widersprechen, to contradict
ausschließen, to exclude
schweigen, to remain silent
Gelingen, To be successful
verändern, to modify
reservieren, to reserve
gefallen, to be pleasing
wählen, to choose
dauern, to last
empfangen, to receive; to greet
wühlen, to root
tanken, to fill up
ablehnen, to decline
nehmen, to take
heizen, to heat
erreichen, to attain
schimpfen, to grumble
zweifeln, to doubt
vorbereiten, to prepare
töten, to kill
machen, to make
spielen, to play
schwitzen, to sweat
ankommen, to arrive
unterhalten, to support; to entertain
arbeiten, to work
wirken, to have an effect
beten, to pray
aussehen, to appear
löschen, to extinguish
teilnehmen, to take part
wundern, to surprise
messen, to measure
verbrauchen, to consume
gründen, to found
wollen, to want
gelten, to be considered as; to be valid
informieren, to inform
drucken, to print
regieren, to rule
keimen, to germinate
begegnen, to encounter
backen, to bake
verletzen, to injure; to violate
schreien, to scream
liefern, to deliver; to provide
lachen, to laugh
überraschen, to surprise
gleiten, to glide
spazieren, to stroll
heißen, to be called
funktionieren, to function
schweben, to hover
gären, to ferment
planen, to plan
veröffentlichen, to publish
zwingen, to compel
mahlen, to grind
lohnen, to compensate
fordern, to ask for
sich erholen, to recuperate
wecken, to waken
bersten, to burst
anziehen, to take on
sich anziehen, to attract one another
eröffnen, to reveal
verabschieden, to discharge; to adopt
drehen, to turn
herkommen, to come here
versichern, to assure
behandeln, to deal with
ausgeben, to give out
leisten, to accomplish
besorgen, to attend to
folgen, to follow
bieten, to offer
verwirren, to entangle
erhalten, to preserve
treten, to step
erkennen, to recognize
aufheben, to balance out
trügen, to deceive
bekommen, to get
schrauben, to screw
zusammenarbeiten, to cooperate
essen, to eat
lernen, to learn
vermuten, to suspect
loben, to praise
kreischen, to screech
verlieren, to lose
schützen, to protect
beschließen, to decide
buchen, to book
bauen, to build
nutzen, to make use of
geben, to give
erleben, to experience
glimmen, to glimmer
ausziehen, to take off; to extract
Regeln, To regulate
blasen, to blow
wenden, to turn
korrigieren, to correct
kosten, to cost
fließen, to flow
schlafen, to sleep
schenken, to give
enthalten, to contain
sich enthalten, to refrain
versprechen, to promise
bitten, to request
zusammenfassen, to summarize
widmen, to dedicate
misslingen, to be unsuccessful
begründen, to establish
zerstören, to destroy
schleichen, to creep
drücken, to press
protestieren, to protest
beginnen, to begin
informieren, to inform
lesen, to read
trennen, to separate
nennen, to name
streichen, to stroke
verdienen, to deserve
lehnen, to lean
dienen, to be of use
kehren, to turn around; sweep
mögen, to like
feststellen, to state
verkaufen, to sell
bestehen, to endure; be composed of
anmelden, to register
streiten, to squabble
wandern, to wander
gießen, to pour
finden, to find
interpretieren, to interpret
freuen, to delight
springen, to jump
schließen, to close
beeinflussen, to influence
waschen, to wash
scheitern, to fail
feiern, to celebrate
brennen, to burn
sich bewerben, to apply; to compete
triefen, to drip
fehlen, to be absent
bestellen, to order (something)
fürchten, to fear
übersetzen, to translate
aufwachen, to awake
zählen, to count
schwellen, to swell
sprießen, to sprout; to spring up
verstehen, to understand
geschehen, to happen
strahlen, to shine; to beam
leben, to be living
sich entschließen, to make up one's mind
spinnen, to spin
rufen, to call
einschalten, to insert; to switch on
geraten, to get into; to land oneself into
rächen, to avenge
spülen, to rinse
trauen, to trust (in honesty)
treffen, to meet
starten, to start
entdecken, to discover
vertrauen, to trust (with confidence)
zuhören, to listen
aufräumen, to tidy up
passieren, to come to pass; to happen
tanzen, to dance
grüßen lassen, to send one's greetings
beleidigen, to offend
lächeln, to smile
melden, to report
wünschen, to wish
klingen, to sound
liegen, to lie
weinen, to cry
bezeichnen, to mark; to indicate
rechnen, to reckon; to estimate
träumen, to dream
erledigen, to complete; to deal with
merken, to notice
anrufen, to phone
klopfen, to beat
stürzen, to plunge; to tumble
enttäuschen, to disappoint
laden, to load
vergleichen, to compare
trinken, to drink
erfinden, to invent
benutzen, to utilize
verhandeln, to negotiate
wachsen, to grow
lügen, to lie; to fib
steigen, to climb
kennen, to be acquainted with
handeln, to trade
stinken, to stink
landen, to land
streben, to strive
fliegen, to fly
unterscheiden, to distinguish
erinnern, to remind
interessieren, to interest
knien, to kneel
feixen, to grin
schleifen, to sharpen
stoßen, to punch
unterstützen, to support
anschauen, to look at
öffnen, to open
reden, to chat
bellen, to bark
ordnen, to put in order
stecken, to stick
einsetzen, to insert
schwinden, to fade; to run out
scheinen, to shine
verhalten, to restrain
stechen, to sting
studieren, to study
sollen, to be supposed to
reißen, to tear
wohnen, to live
sehen, to see
suchen, to search for
gebären, to bear; to give birth to
lösen, to solve
saugen, to suck
sinnen, to ponder
können, to be able to
sprechen, to speak
ziehen, to pull
beschweren, to weigh down
schicken, to send
schreiten, to stride
verdrießen, to irritate
ringen, to wrestle
schätzen, to guess
bewegen, to move; to persuade
schreiben, to write
danken, to thank; to owe
lieben, to love
stattfinden, to take place
berücksichtigen, to bear in mind
führen, to lead
verbringen, to spend time
schauen, to watch
entscheiden, to determine
behalten, to retain
riechen, to smell
reichen, to be enough
helfen, to help
fangen, to catch
singen, to sing
entsprechen, to correspond; to be consistent
kriegen, to get
weichen, to budge; to yield
erschrecken, to be frightened
gewinnen, to win
stören, to disturb
wiegen, to weigh
erscheinen, to appear; to come out
reisen, to travel
bedingen, to imply
lehren, to teach
fühlen, to feel
schneien, to snow
einrichten, to set up; to arrange
ärgern, to annoy
heiraten, to get married
zunehmen, to gain weight
betrügen, to deceive; defraud
anfassen, to touch
leihen, to lend
denken, to think
gehören, to belong
prüfen, to test
abbiegen, To turn off
fahren, to go (vehicle)
stimmen, to be right
verlangen, to demand; to charge
räumen, to clear away; to vacate
vorstellen, to introduce
bereiten, to get ready
schallen, to ring out; to echo
klappen, to work well; to fold; to tilt
meinen, to mean; to be of the opinion
hinterlassen, to leave behind
kürzen, to shorten
einziehen, to draw in
rühren, to stir; to move
stützen, to support; to prop up
beschreiben, to describe
einsteigen, to board; to climb in
dringen, to penetrate; to spread
übernehmen, to take over
verzeihen, to forgive
aufstehen, to get up
beschädigen, to damage
anzünden, to ignite
stehlen, to steal
befehlen, to command
lassen, to let
sichern, to make secure
warten, to wait
hängen, to hang; to suspend
sparen, to save money
entwickeln, to develop
verbessern, to improve
stehen, to stand
abnehmen, to lose weight
preisen, to laud
diskutieren, to discuss
verhindern, to prevent
abholen, to pick up
zeigen, to show
klettern, to climb
fressen, to devour; eat (animals)
schrecken, to be startled
schlagen, to hit
bergen, to rescue; to shelter
gewöhnen, to accustom
überweisen, to transfer
werben, to advertise; to woo
beziehen, to relate; to subscribe to; to refer
fangen, to capture
tragen, to carry; to wear
kaufen, to buy
schwimmen, to swim
erkälten, to chill
schmelzen, to melt; to dissolve
fliehen, to flee
beraten, to advise
herstellen, to manufacture
mieten, to rent
zerstieben, to scatter; to disperse
hassen, to hate
beteiligen, to give a share
winden, to wind
ausmachen, to put out; to turn off
packen, to pack
stellen, to put
abfahren, to depart
schleißen, to strip; to split
abgeben, to hand over; to give off
erhalten, to obtain; to preserve
bezahlen, to pay
senden, to send; to broadcast
sorgen, to provide for; to cause
weben, to weave
legen, to place
wehren, to fight against
genesen, to get well
fragen, to ask
ehren, to honor
beschließen, to conclude; to resolve
müssen, to have to
leiten, to direct; to lead
besichtigen, to inspect; to go sightseeing
gedeihen, to thrive; to flourish
schaden, to harm
rennen, to run
werfen, to throw
atmen, to breathe
brauchen, to need
reagieren, to react
raten, to counsel
verlassen, to abandon
braten, to roast
ansehen, to look at
erzählen, to tell
behaupten, to maintain; to assert
dürfen, to be allowed to
behindern, to impede
erwähnen, to mention
schneiden, to cut
unterschreiben, to sign
leiden, to suffer
vorschlagen, to propose
klagen, to complain
ausschalten, to switch off; to eliminate
erlöschen, to go out; to expire
aufgeben, to give 
