bastante, quite
bien, well
demasiado, too
descripción, description
igual que, like
más que, more than
máximo, maximum
mayor, bigger
mayoría, majority
mejor, better
menor, smaller
menos que, less than
mínimo, minimum
mismo, same
muy, very
parecido a, similar to
peor, worse
poco, little
por ejemplo, for example
tan…como, as…as
tanto…como, as much…as
comparar, to compare
a pesar de, despite
así que, so
au si, even if
aunque, even though
como, as
cuando, whe
incluso, even
mientras, whereas
o, or
pero, but
por eso, that’s why
por lo tanto, therefore
porque, because
pues, since
si, if
sin embargo, however
tal vez, perhaps
también, also
y, and
ya que, since
además, also
aparte de, apart from
claro que, clearly
dado que, given that
es decir, that is to say
por un lado, on one hand
por otro lado, on the other hand
sin duda, without doubt
a, at
con, with
de, from
en, in
hacia, towards
hasta, until
para, for
por, by
según, according to
sin, without
jámas, never
ni…ni, neither…nor
nada, nothing
nadie, no one
ninguno, none
no, not
nunca, never
sino, except
tampoco, neither
ya no, no longer
acabar de, to finish
dar, to give
deber, to have to
estar, to be
hacer, to do
hacerse, to become
hay, there is
hay que, you must
ir, to go
ir a, to be going to
irse, to leave
ocurrir, to happen
oír, to hear
pasar, to happen
poder, to be able
poner, to put
querer, to want
quisiera, I would like
ser, to be
tener, to have
tener lugar, to take place
tener que, to have to
volverse, to become
Adónde, Where to
cómo, how
cuál, which
cuándo, when
cuánto, how much
Cuántos, How many
De dónde, Where from
De quién, Whose
dónde, where
Por dónde, Whereabouts
Por qué, Why
qué, what
quien, who
A qué hora, At what time
Cuánto cuesta, How much does it cost
Cuánto es, How much is it
Cuánto vale, How much is it worth
Cuántos años tienes, How old are you
De qué color, What colour
Dónde está, Where is it
Qué día, What day
Qué fecha, What date
basta, enough
bienvenido, welcome
buen viaje, have a good trip
buena suerte, good luck
claro, of course
cuidado, be careful
enhorabuena, congratulations
Felices Pascuas, Happy Easter
Felices vacaciones, Happy holidays
Felicidades, Congratulations
Feliz Año Nuevo, Happy New Year
Feliz cumpleaños, Happy birthday
Feliz Navidad, Happy Christmas
Ojo, Look out
Que aproveche, Enjoy your meal
Que lo pase bien, Have a good time
¡Qué asco!, Yuk!
¡Qué bien!, Great!
¡Qué horror!, How horrible!
¡Qué (+noun)!, What a….!
¡Qué lástima!, What a pity!
¡Qué pena!, What a shame!
¡Qué va!, Come on!
¿Cómo estas?, How are you?
¿Qué pasa?, What’s happening?
adiós, goodbye
Atentamente, Yours sincerely
Buenas noches, Goodnight
Buenas tardes, Good evening
Buenos días, Good morning
De nada, You’re welcome
Encantado, Pleased to meet you
Gracias, Thanks
Hasta el lunes, See you on Monday
Hasta mañana, See you tomorrow
Hasta pronto, See you soon
Hola, Hi
Lo siento, I’m sorry
Mucho gusto, Pleased to meet you
Perdón, Sorry
Por favor, Please
Saludos, Best wishes
sí, yes
Vale, OK
saludar, to greet
aburrido, boring
aceptable, acceptable
afortunado, lucky
agradable, pleasant
antiguo, old
apropriado, suitable
barato, cheap
bonito, pretty
bueno, good
calidad, quality
caro, expensive
decepcionado, disappointed
desepcionante, disappointing
desafortunadamente, unfortunately
desventaja, disadvantage
diferencia, difference
diferente, different
difícil, difficult
dificultad, difficulty
distinto, different
divertido, fun
duro, hard
económico, economical
emocionante, exciting
encantador, lovely
entretenido, entertaining
especial, special
espléndido, splendid
estupendo, marvellous
estúpido, stupid
excelente, excellent
extraordinario, extraordinary
fácil, easy
famoso, famous
fantástico, fantastic
fascinate, fascinating
fatal, terrible
favorable, favourable
favorito, favourite
fenomenal, great
feo, ugly
genial, great
hermoso, beautiful
horroroso, horrible
ideal, ideal
importante, important
imposible, impossible
impresionante, impressive
increíble, incredible
inseguro, insecure
interesante, interesting
inútil, useless
mal, badly
malo, bad
maravilloso, marvellous
moderno, modern
negativo, negative
nuevo, new
perfecto, perfect
posible, possible
positivo, positive
precioso, lovely
preferido, preferred
profundo, deep
raro, rare
regular, average
ridículo, ridiculous
seguro, safe
sencillo, simple
sorprendido, surprised
típico, typical
tonto, silly
tranquilo, calm
único, only
útil, useful
ventaja, advantage
viejo, old
aburrirse, to be bored
adorar, to adore
apreciar, to appreciate
aprovecharse, to make use of
alegrarse, to cheer up
creer, to believe
decepcionar, to disappoint
decir, to say
desear, to want
detestar, to hate
disfrutar, to enjoy
divertirse, to have a good time
dudar, to doubt
encantar, to delight
esperar, to hope
estar de acuerdo, to agree
estar a favor, to be in favour
estar en contra, to be against
estar harto de, to be fed up with
fascinar, to fascinate
fastidiar, to annoy
gustar, to please
interesarse, to be interested
justificar, to justify
odiar, to hate
opinar, to think
parecer, to seem
pasarlo bien, to have a good time
pensar, to think
ponerse de acuerdo, to agree
preferir, to prefer
quedar en, to agree to
reconocer, to recognise
sentirse, to feel
valer la pena, to be worthwhile
lunes, monday
martes, Tuesday
miércoles, wednesday
jueves, thursday
viernes, friday
sábado, Saturday
domingo, Sunday
primavera, spring
verano, summer
otoño, autumn
invierno, winter
enero, January
febrero, February
marzo, March
abril, April
mayo, May
junio, June
julio, July
agosto, August
septiembre, September
octubre, October
noviembre, November
diciembre, December
a eso de, around
a tiempo, on time
en punto, exactly
hora, hour
horario, timetable
media hora, half-hour
medianoche, midnight
mediodía, midday
minuto, minute
segundo, second
y cuarto, quarter past
y media, half past
menos cuarto, quarter to
a diario, daily
a fines de, at the ed of
a mediados de, in the middle of
a menudo, often
a partir de, since
aproximadamete, approximately
a veces, sometimes
ahora, now
al mismo tiempo, at the same time
algunas veces, sometimes
anoche, last night
año, year
antes, before
ayer, yesterday
breve, short
casi, nearly
de, from
a, to
de momento, at the momet
de nuevo, again
de repente, suddenly
de vez en cuando, from time to time
dentro de, within
desde, from
desde hace, since
después, after
día, day
día festivo, public holiday
día laborable, working day
diariamente, daily
durante, during
durar, to last
en ese momento, at that moment
en seguida, straightaway
entonces, the
estación, season
fecha, date
fin de semana, weekend
final, ed
finalmente, finally
frecuente, frequent
futuro, future
generalmente, generally
hace dos años, two years ago
hoy, today
immediatamete, immediately
los lunes, on Mondays
luego, then
mañana, tomorrow
mes, month
mientras tanto, meanwhile
momento, moment
mucho tiempo, a lot of time
noche, night
normalmente, usually
otra vez, again
en el pasado, in the past
permanente, permanent
pocas veces, seldom
por fin, finally
por lo general, in general
principio, beginning
pronto, soon
próximo, next
que viene, next
quince días, fortnight
rápido, quick
raramente, rarely
rato, time
recientemente, recently
semana, week
siempre, always
siglo, century
siguiente, followig
sobre, on top of
solamente, only
tardar, to be late
tarde, afteroon
temprano, early
tener prisa, to be in a hurry
tiempo, time
todas las semanas, every week
todavía, still
todos los días, every day
último, last
vez, time
a un paso, near
abajo, below
adelante, forward
afuera, outside
ahí, there
aislado, isolated
al final, at the ed
allá, over there
allí, there
alrededor, around
aquí, here
arriba, above
atrás, behid
centro, centre
cerca de, near
contra, against
debajo, under
delante, in front
dentro, inside
derecha, right
derecho, stright
detrás, behind
dirección, direction
distancia, distance
por todas partes, everywhere
en las afueras, on the outskirts
enfrente, opposite
entre, between
este, east
exterior, outside
fondo, bottom
fuera, outside
interior, interior
izquierda, left
kilómetro, kilometre
lado, side
lejos, far
lugar, place
medio, middle
metro, metre
norte, north
oeste, west
sitio, place
sur, south
todo recto, straight ahead
estar situado, to be situated
amarillo, yellow
azúl, blue
blanco, white
claro, light
color, colour
gris, grey
marrón, brown
morado, purple
naranja, orange
negro, black
oscuro, dark
pálido, pale
rojo, red
rosa, pink
verde, green
violeta, violet
vivo, vivid
alcanzar, to reach
alto, tall
altura, heigh
ancho, wide
bajo, short
bolsa, bag
bote, tin
botella, bottle
caja, box
cantidad, quantity
cartón, carton
centímetro, centimetre
completo, full
cuarto, quarter
delgado, thin
doble, double
estrecho, narrow
gordo, fat
gramo, gram
grande, big
grueso, thick
lata, can
litro, litre
lleno, full
mediano, medium
medida, measurement
medio, half
mucho, a lot of
paquete, packet
pedazo, piece
pequeño, small
peso, weight
poco, a little
ración, portion
suficiente, enough
talla, height
tamaño, size
trozo, piece
vacío, empty
bastar, to be enough
medir, to measure
pesar, to weight
cuadrado, square
forma, shape
redondo, round
buen tiempo, good weather
mal tiempo, bad weather
caliente, hot
calor, hot
chubasco, shower
cielo, sky
clima, climate
despejado, clear
estable, stable
fresco, fresh
frío, cold
grado, degree
hielo, ice
húmedo, damp
lluvia, rain
niebla, fog
nieve, snow
nube, cloud
nublado, cloudy
pronóstico, forecast
relámpago, lightning
seco, dry
sol, sun
sombra, shade
temperatura, temperature
templado, mild
tiempo, weather
tormenta, storm
tormentoso, stormy
trueno, thunder
viento, wind
abierto, open
abrir, to open
acceso, access
cerrado, closed
cerrar, to close
gratis, free (of charge)
libre, free
necesario, necessary
ocupado, engaged
permitir, to permit
prohibido, prohibited
cierto, certain
correcto, correct
equivocado, wrog
exacto, exact
falso, false
mentira, lie
mentiroso, lying
razón, reason
verdad, truth
verdadero, true
corregir, to correct
mentir, to lie
algodón, cotton
cerámica, ceramic
cristal, crystal
cuero, leather
lana, wool
madera, wood
oro, gold
papel, paper
piel, leather
plástico, plastic
plata, silver
seda, silk
tela, fabric
vidrio, gla
