apaciguar, to appease; pacify
libérrimo, very free
acopiar, to gather together; to stock up
poder tenerse en pie, to manage to stand
el candil, oil lamp
el congénere, the fellow; person of the same sort
otorgar, consent; concede
la jerarquía, hierarchy
pasar de la raya, to cross the line; overstep bounds; take undue liberties
el lazo, bow; knot; loop
brindar, to offer; to provide
colar, to strain (drain)
la indemnización, compensation; indemnification
reposar, to rest; to take a rest
arrebatar, to snatch
enfocar, to focus
reflejar, to reflect (light etc.) (NOT to think)
los barrios bajos, slums
el estrépito, crash; clamor
adiestrar, to train (to train somebody in)
el alguacil, sheriff; bailiff
descartar, to discard
pasar por alto, to skip; to overlook
caer en cama, to fall ill
eso corre prisa, that is urgent
el clavecín (el clave), harpsicord
la similitud, similarity
suscitar, to give rise to
arrancar, to tear off; to start (i.e. a computer)
el aliento, breath
pulido, polished
mofletudo, chubby-cheeked
el sapo, toad (but as an adjective it means sharp or noisy)
la viga, the rafter; beam
molarle, to think something is cool (slang)
meter el hocico en algo, to poke one's nose into something
chapotear, to splash
solear, to put in the sun
el ámbito, scope (área de conocimiento)
chirriar, to squeak
canalizar, to channel
la molicie, pampering; easy living
dormir a pierna suelta, to sleep soundly
empapado, soaked
antediluviano, very old ("before the flood")
la finca, farm
el felpudo, doormat
laico, secular
encarnar, to embody
el dispositivo, presence (of police; of the military)
tachar, to cross out
achacar, to attribute to; lay blame on
truculento, gruesome. "Excesivamente cruel o atroz"
lesivo, harmful; damaging;  - in a moral or legal way
desvanecer, to dispel; to blur
la sien, temple (of your head)
desquiciado, unhinged; mad; crazy
perjudicar, to damage (p)
acérrimo, muy firme y entusiasta (colocations: ~ defensor; ~enemigo)
vacilar, hesitate
soberbio, arrogant; haughty
disfrazar, to dress up (in costume)
el entierro (la inhumación), burial (not sepultura)
precintar, to seal
rodear, to circle; surround
pecaminoso, sinful (act)
la espesura, denseness; thick vegetation
increpar, to insult; to rebuke
abrir la boca de par en par, to open one's mouth wide
fugaz, fleeting; brief
el duermevela, light sleep
recurrir a, to turn to; appeal to
que conste que, let the records show that
el ideario, ideology
la orden, order; command
el rasguño, scratch
amansar, to tame; to domesticate
desencadenar, to unchain/to trigger
el disecador, taxidermist
la nuca, nape of the neck
dar el pésame (a alguien), to offer condolences (to someone)
cicatrizar, Curar por completo una herida física o psíquica. (to heal)
jadear, to pant
el baúl, roofbox for a car
arañar, to scratch (a)
humilde, humble
el estreno, premiere
alborotar, to stir up; unsettle
por despecho, out of spite
absorto, engrossed; absorbed ("in one's thoughts")
estar de duelo, to mourn; be in mourning
la peca, freckle
la expectación, sense of expectancy anticipation
el párpado, eyelid
apelar, to appeal
el terrateniente, landowner
nocivo, harmful
enfrentarse con, to confront; meet face to face
el sirimiri, drizzle
el albor, dawn (frml)
el esmero, care
el suceso, incident; event (s)
asir, to seize; to grasp; hold on to
el fulano, the guy; so and so
el trasero, buttocks; butt (t)
desgastar, Gastar poco a poco algo por el roce o el uso (wear out)
la cometa, kite
vigésimo, twentieth
el torbellino, whirlwind
la manguera, hose
engorroso, bothersome
el haz, beam (of light)
mal de muchos; consuelo de tontos, it's no help that we're all in the same boat
el accionista, shareholder
la cautela, caution
tosco, crude; coarse; rough
jorobar, to annoy; to ruin
proporcionar, to provide
tartamudear, to stutter
la nuera, daughter-in-law
apiñarse, to crowd together; to bunch
el palo, stick
el tirachinas, catapult; slingshot
meramente, merely
el mozo, young boy/girl (m..)
el gusano, maggot; worm
desplomar, to fall heavily; fall flat
y si no te gusta; te jorobas, and if you don't like it; that's tough
el guión, script
vigente, valid
el chirrido, squeeking; screech
el apodo, nickname
el garabato, scribble
el alud, avalanche
la trama, the weave
el regazo, lap (of one's legs)
las zarandajas, nonsense (trifles)
hechizar, to cast a spell on (to bewitch)
la mantilla, veil
trastornar, to disturb (mentally)
Sacar en claro (sacar en limpio), To deduce; conclude
posarse, to land gently
Más vale pájaro en mano que ciento volando, a bird in the hand is worth two in the bush
el pegamento, glue
recapacitar, to reconsider
improcedente, inappropriate; unfair
la exigencia, demand
el acrecentamiento, growth; increase
recetar, to prescribe
alzado, elevated
la sucursal, branch (of a company; bank)
el enredo, tangle
estremecedor, horrifying; spine-chilling
cuadragésimo, fortieth
la represalia, reprisal; retaliation
el umbral, threshold
la persiana, blind
cegador, blinding
morar, to dwell
la cerradura, lock (on a door)
tener mala fama, to have a bad reputation
el molino, mill; windmill
templado, lukewarm (food); temperate (climate)
el orden, order (as in "we need a bit of order in here")
suplantar, to replace; supplant
mimoso, affectionate
atronar, to thunder
el hidalgo, gentleman; nobleman
preñada, pregnant (literally: "fertilized")
el presupuesto, budget; estimate
fehaciente, reliable (irrefutable)
estancar, to block / dam
la camada, litter (of puppies; etc.)
el yodo, iodine
el gozo, joy
abatir, to knock down; overthrow; throw down (a)
pecar, to sin; err
el apartado, section
damas y caballeros, ladies and gentlemen
embeber, to soak in
el guardaespaldas, bodyguard
la rabieta, tantrum
el vertido, spillage
privar, to deprive
fomentar, to foment; to promote (not promover)
el chismorreo, gossip
el inciso, digression (not la divagación)
suspirar, to sigh
si bien, although; while (not aunque)
el inquilino, tenant
la expectativa, expectation
asequible, attainable
posar, to pose
el plomo, lead (element)
ser reacio a (algo), to be against (something)
el sastre, tailor
reluciente, shiny
sepultar, to bury (not enterrar)
enjuto, lean; gaunt
vicario, vicarious
paulatinamente, gradually; little by little
decir para su coleto (or capote), to say to one's self
tener hipo, to hiccup
compaginar, to combine
erguir, to erect
el as, ace
la pesadumbre, weightiness
el enano (same as adj), dwarf
sumamente, extremely (not extremadamente)
el cincel, chisel
de antemano, beforehand
anidar, to nest
la caridad, charity
el moretón (el cardenal), bruise
el payaso, clown
la madera, wood
el omoplato, shoulder blade
la habladuría, rumour
factible, feasible
tener nada que hacer, to have no business
ligero, light; slight; thin (not delgado)
destetar, to wean
octogésimo, eightieth
el trance, trance
elogiar, to praise
la muchedumbre (la multitud) (el gentío), crowd
pellizcar, to pinch
prescindir, to do without
atávico, atavistic (characterized by reversion to smth ancient)
la cadera, hip
cachear (a alguien), to frisk (someone)
vaquero, denim
el dramaturgo, playwright
jorobado, hunchbacked
bondadoso, kind
el hazmerreír, laughing stock
la leña, firewood
las migas, crumbs
la arroba, @ symbol
falsificar, to forge (a document; signature)
coger el timón, take the helm
desviar, to divert
raer, to scrape; to rub off; to erase; to wipe out
mohoso, mouldy
el derrotero, course (direction)
el resabio, bad aftertaste
echar un piropo, to pay a compliment
el hueco, hole; hollow (not agujero)
dar de baja (despedir), to fire
muelle, soft; delicate; easy
la sanidad pública, public healthcare
la divagación, digression (not el inciso)
el estafador, con man (swindler)
el aforo, capacity
desentrañar, unravel; get to the bottom of
El que mucho abarca poco aprieta, he who takes on too much gets little done
rodar, to roll
incívico, uncivil
el chapoteo, splash
el paludismo, malaria
la destreza, dexterity; skill
verter, to pour
el ombligo, navel
conllevar, to bring with it; to entail
solapado, sly; underhand
descarriar, to lead astray
a matacaballo, at breakneck speed
pecoso, freckly
absorber, to absorb
segar, to reap; to cut
fulminante, sudden and devastating
acarrear, to give rise to
el atisbo, a sign; a glimpse
solapar, to overlap; cover up
la lente, lens
empinar el codo, to drink a lot
coadyuvar, to assist; to contribute
en todas partes cuecen habas, it's the same the world over (beans are cooked everywhere)
el calambre, electric shock
el yerno, son-in-law
el enlace, link
sollozar, to sob
el dispositivo, device; mechanisim
el derribo, demolition
bullicioso, busy; noisy
la suela, sole
la bestialidad, act of cruelty / barbarism
arrastrar, to drag (something across the floor)
encajar, to fit (and match)
el tallo, stem; stalk
el musgo, moss
la telaraña, spider's web
roer, gnaw
cada poco, frequently
la aportación, contribution
el abatimiento, depression; dejection
el recelo, distrust; suspicion
la berenjena, aubergine; eggplant
postergar, to postpone; to put back
la flaqueza, weakness (of character)
en torno, around; regarding
atrasado, delayed
lozano, lush; full of life
colmar, to fill up; to fulfill
despejar, to clear; to unblock
azulejar, to tile
el gancho, hook; paperclip; hanger
el barrote, bar (metal)
el garbanzo, chickpea
heredar, to inherit
tener a raya, To keep in bounds; hold in check
la butaca, armchair (in cinema; theatre)
la lápida, gravestone
desperezarse, to stretch oneself; to stretch one's arms and legs
la horquilla, hairpin; pitchfork
entre la espada y la pared, between a rock and a hard place
rescatar, to rescue
fertilizar, to fertilize (a field)
apremiante, pressing; urgent
el plácet, blessing (as in "I give you my blessing\approval")
ávido de, eager for / hungry for
el tobillo, ankle
atropellar, to run over
el lodo, mud
Pagar al contado, To pay by cash
silbar, to whistle
la estafa, fraud; con
saberle mal, to taste bad to someone. Also "To leave a bad taste in one's mouth" (idiom)
la grieta, crack
Matar dos pájaros de un tiro, to kill two birds with one stone
el ocaso, the sunset; decline
el vicario, vicar
vaya a saber, una frasa para manifestar duda. "Who knows"
la traición, treason
el asueto, day off; vacation; break
el pavor (me da pavor), terror; dread (it terrifies me)
la contraventana (el postigo), shutter
la parcela, plot of land
cortar el hilo, to interrupt
someter, to submit
agarrar, to grab; to grip
volcar, to knock over
escudar, to shield
solemne, solemn
implantar, to implant; to introduce
ingenuo, naive; ingenuous
el hollín, soot
pegar, to stick; adhere; paste (including on computer)
la aguja, needle
recorrer, to travel across
ataviar, to attire; to dress up; place in order
a gatas, on all fours; crawling
ligar, to tie; to bind
el representante, representative
quincuagésimo, fiftieth
el baúl, trunk (box)
suprimir, to suppress
degollar, to slit the throat of
la calvicie, baldness
la pantorrilla, calf (anatomy)
reflejar, to reflect (show)
la impronta, stamp; mark; hallmark; imprint
ansiar, to long for
la brecha, breach; gap; gash; opening
el eje, axis
deslizar, to slip; to slide
disgustado, very annoyed; frustrated
agobiado, overwhelmed
el tacón, heel
adosado, terraced
forjar, to forge (a metal)
la pestaña, eyelash
el ingreso (la renta), income
bullir, to bubble (up); to boil
la avidez, eagerness
el leño, log
divagar, to digress; ramble
estremecer, to shudder
el pañuelo, handkerchief
suministrar, to supply
estar en celo, to be in heat
el desiderátum, greatest wish
la pulgada, inch
reflexionar, to reflect (think)
la agalla, gill (of a fish); -s "guts" (bravery)
la corneja, crow
la línea, line (in geomentry)
jactarse de (tener; ser etc.), boast about (having; being; etc.)
ensordecedor, deafening
padecer (de), Sentir un daño;dolor;enfermedad o pena
ni con mucho, not by far; not by a long shot
tenebroso, dark; gloomy; sinister
difuso, diffused; vague
el talón, Parte posterior del pie humano (heel)
la golosina, (piece of) candy
abrumar, to overwhelm
más vale malo por conocido que bueno por conocer, beter the devil you know
brindar (por), to drink a toast (to)
sobornar, to bribe
lúdico, playful; recreational
el acantilado, cliff
supuestamente, supposedly
acrecer, increase (not aumentar or subir)
arrimarse, to move or come closer
el gozne, hinge
enfurruñarse, to sulk; to go into a huff
el socorro, help
con cuentagotas, drop by drop (little by little)
avasallador, domineering
regir, to govern; to rule
el sobresalto, fright
el sendero, path
avasallador, resounding; overbearing
la gama, range; scale; spectrum
el pomo, knob
el riñón, kidney
el trance, difficult situation
tétrico, gloomy; dismal
la calle bullía de gente, the street was teeming ("bubbling") with people
en el acto, Right away; at once
el aperturismo, (policy of) openness
el aerodeslizador, hovercar
avasallar, to subjugate
adular, to adulate; to flatter excessively
la perturbación, disturbance; disruption
pudiente, wealthy "poderoso; rico"
el reo, the defendant
trocar, to barter
el mentón, chin
la mecedora, rocking chair
el cimiento (la fundación), foundation
el ocio, spare time; leisure time(not tiempo libre)
el suplicio, torture
amenazar (le a alguien con algo), to menace; threaten
el sucedáneo, que puede reemplazar a otro por tener propiedades similares.
el alambre, wire
tachar de, to write off as
incidir (en), to influence; affect
la misericordia, mercy; compassion
a regañadientes, reluctantly
a pedir de boca, exactly as desired
sosegarse, to quiten down; to become calm
el bocado, bite; mouthful
poco probable (inverosímil), unlikely
lechoso, milky; pale
aflojar el ritmo, to calm down; to slow down the rate of something
parpadear, to blink
ensayar, to rehearse
desalentar, Quitar el ánimo de hacer algo; (dishearten)
la inanición, starvation
enderezar, to straighten (out); to put upright
la pauta, guideline
el obispo, bishop
aparejar, to saddle / harness a horse
veraniego, summery
el cordel, string
pegajoso, sticky
el cardenal, cardinal
áspero, rough; rugged; harsh
demacrado, haggard; drawn
apresar, to seize; to arrest
dotar, to gift; to provide (not dar; proporcionar or regalar)
extirpar, to erradicate/stamp out
exigir, to demand; require
la chulería, cockiness (but in a likeable way)
vacilante, unsteady; shaky; wobbly
el hoyuelo, dimple
inconmensurable, unmeasurable; vast
la desidia (la negligencia), negligence
cazar, to hunt
estar colado por alguien, to have a crush on someone
el chasquido, snap; crack
comprimir, to compress
el visillo, net curtain
la codicia, greed
la hilera, row; line (not fila)
sustituir, to substitute
soleado, sunny
la trama, plot
proseguir, to pursue
el heredero, heir
experimentar, to experience/feel
lego, Falto de instrucción en una materia determinada (lay)
el cohete, rocket
el madero, (piece of) timber
la desventura, misfortune
subestimar (menospreciar), underestimate
buscarle tres pies al gato, to complicate things (unecessarily)
derretirse, to melt
apartado, isolated
la ascua, ember
la propensión, propensity; tendency
la mansedumbre, meekness
la banqueta, stool
delictivo, criminal
difundir, to spread
el aturdimiento, bewilderment
llamativo, Que llama la atención
difuso, dim; vague
el estallido, explosion
retrasar, to slow down something
cascar, to crack; to chip (a cup)
malvado, wicked
el atasco, traffic jam
descomponer, to decompose
súbito, sudden
exponer, to exhibit
el estruendo, noise; racket; din
imperante, prevailing (i)
el celo, zeal
tramitar, to deal with; to arrange
sexagésimo, sixtieth
el accésit, consolation prize
sacudir, to shake; beat; dust (s)
descifrar, decipher
alumbrar, to light; illuminate; enlighten
cerciorar (a alguien), to assure (someone)
potenciar, to promote
la directriz, guideline; principle
bullicioso, boisterous
el meollo, marrow; heart of the matter
el desquite, revenge
el escudo, shield
el cónyuge, spouse (frml)
contrarrestar, to counteract
frenar, to brake (to slow down; to stop)
el rendimiento, rendition; performance (not actuación)
el paladar, Inside and upper part of the mouth (palate)
graznar, to caw; to make a bird noise
lapidar, to stone
ofrecerse a (ayudar), to offer (to help) (always goes with infinitive)
el credo, creed
el descansillo, landing (room)
la ampolla, blister
la fila, row; line (not hilera)
el abordaje, boarding
aducir, to put forward; to adduce (frml)
as en la manga, ace up one's sleeve
balbucear, to stutter; stammer; babble
mascullar, to  mutter
matutino, De las horas de la mañana o relativo a ellas ("of the morning")
fructífero, fruitful
de suyo, naturally; by nature
docente, scolastic; educational
conducir a (algo), lead to (something)
desmayarse, to faint
el ateo, atheist
ensanchar, to widen
ir de la mano, to go hand in hand
la prole, offspring
chistar, make a  sound (a peep)
el blindaje, armor; plating
abultar, to bulge (to make a bulge)
en resumen, in summary
saber a rayos, to taste terrible
dar la lata, to annoy
la pesadumbre, grief
el lunar, mole (of the skin)
el verdugo, executioner
el retroceso, backward movement; retreat
acontecer, to happen; occur (frml)
el hocico, snout
entrometido, Nosy / meddling
cosa de, approximately
nonagésimo, ninetieth
indagar, to inquire into; to investigate
el balbuceo, babbling; stuttering
la tertulia, gathering
la fachada, facade
rastrero, despicable
mortecino, faint
soplar, to blow; to blow out
desabrochar, undo (buttons; etc.)
el hormigueo, pins and needles; tingling
la planicie, plain
el timón, rudder
acontecer, to take place; occur (frml)
el preso, prisoner
el ademán, gesture
rociar, to sprinkle
la colada, washing
la ronquera, hoarseness
eructar, burp
a chorros, abundantly
hacer la vista gorda, turn a blind eye
vilipendiar, to vilify
replantearse, to reconsider; rethink
alargado, elongated
cimbrar, to sway; to shake
el aullido, howl
clavar, to hammer
sumirse (en depresión; sueño), to sink (into depression; a dream)
el tropiezo, stumbling block
el anhelo, longing
la zancada, stride
el clímax, climax
terco (testarudo), stubborn
el luto, mourning
el albedrío, will
el lagarto, lizard
la brújula, compass
la demora, delay
someterse, to submit
la yema, fingertip
el pliegue, fold (crease)
la índole, nature; character
la mancha, spot; stain
la alabanza, praise (la..)
la cera, wax
la sandez, nonsense; stupid thing
jalado de los pelos, far-fetched
una paliza, a beating
estornudar, to sneeze
ligar, to "make out with" / pick up
el arbusto, bush
aburrirse como una ostra, to get bored like an oyster (really bored)
la angustia, anguish
la fruición, delight
indemnizar (or resarcir), to indemnify; to make up for; to compensate
septuagésimo, seventieth
acarrear, to carry; convey
acosar, to hound; to harass
rozar, to brush
el calambre, cramp
soslayar, to get around; to avoid
ocasionar, to provoke
el trance, critical moment
la yema, yolk (egg)
pecar de, to be too -
el rótulo, sign
soltar, to release; to let go of; to drop
negar, to deny; to refuse
diurno, Relacionado con el día o que ocurre durante el día. ("of the day")
el pasamanos (la barandilla), handrail
la fuga, escape
la estancia, stay (e.g.; enjoy your stay)
el escalofrío, shiver
yacer, to lie; to lie down
esbozar, to sketch; to outline; to give a hint of
centésimo, one hundreth
psíquico, mental
está basado en, it's based on
alardear de (alguna cosa), boast about (something)
previsible, foreseeable
la yema, bud
advertir, to warn
la pesadez (peste), drag (bore); heaviness
merodear, to prowl
plomizo, leaden; lead-coloured
sin empacho, uninhibitedly
la síntesis, synthesis
envolver, to wrap
derrumbar, to demolish
tañer, to play a string or precussion instrument
la coartada, alibi
alucinar, to hallucinate
trucar, to rig (a game; elections); to touch up (a photograph)
en grueso, In bulk; by wholesale
exiguo, meager
cotizar, to pay; quote; value
entrañar, to entail; to involve
el serrucho, saw
el escollo, pitfall; obstacle
aborrecer, to abhor; hate
ducentésimo, two hundreth
en añadidura, in addition
la cacería, hunting
la garra, claw
aterrizar, to land
hojear, to leaf through
el elogio, (piece of) praise
el picaporte, handle
descomponer, to break down; split up; break
rechazar, to reject
el bosquejo, outline; sketch
inmerso (en algo), submerged (in something)
el acicate, spur; incentive
el encaje, lace
gozar de, enjoy
constar (en algo), to be stated; to be recorded (in something)
el mendigo, beggar
la tentativa, attempt
sacudirse, to shake off
fulgurar, to glow; to shine brightly
Irse (or Andar) a la deriva, To drift; to be adrift
abrasar, to burn; parch; sear; scorch (a)
acérrimo, staunch; bitter; acrimonious
codicioso, greedy (literally: covetous)
el latón, brass
el cerrojo, bolt
la lámina, sheet (porción plana y fina)
azotar, to whip
teñir, to dye
el percance, mishap; accident
la bahía, bay (body of water)
tener acciones en una compañía, to hold shares in a company
el contingente, contingency; possibility
traer (llevar) aparejado, to entail
tomar partido, to take sides
escupir, to spit
el resorte, spring (coiled piece of metal)
el despropósito, absurdity
la tasa, rate; valuation
Forzar la entrada, to break into
el despido improcedente, unfair dismissal
la ronda, patrol
la banda sonora, soundtrack
estorbar, to obstruct
reacio, reluctant
concomitante, concomitant (frml)
carcomer, to eat away at
enconar, inflame (an injury); exasperate
trigésimo, thirtieth
fecundar, to fertilize (an egg)
el fango, mud (f...)
el discapacitado (el minusválido), person with a handicap or disability
el lecho, bed (of river/plants)
el pozo, well; shaft
el traspié, stumble
forzoso, forced; compulsory
barrer, to sweep
a más no poder, to the utmost
empotrado, built in; fitted
por no decir, not to mention
zurcir, to darn; to mend
el vientre, belly; stomach; abdomen
la articulación, Unión de un hueso con otro. / Enlace o unión entre dos partes de una máquina que permite y ordena su movimiento. (joint)
envolver, to envelop
el cirujano, surgeon
la contraseña, password
entroncar (con), to establish a relationship (between); to relate (to)
frenar, to stop & to brake
el sesgo, slant; bias
el balde, bucket
extirpar, Arrancar de cuajo o de raíz (to remove; to extirpate)
el provecho, benefit
rastrero, creeping
el país en (vias de) desarrollo, developing country
despectivo, contemptuous
flamante, resplendent; brand new
adoctrinar, to indoctrinate
el plató, set
la caducidad, expiration; expiry date
el matón, thug; bully
depende de lo que digas, it depends on what you say
cincelar, to chisel; to engrave
el puñado, handful
el manicomio, Hospital para enfermos mentales.
vacilar, stagger; totter
la retroalimentación, feedback
vacilante, hesistant
la tatarabuela, great-great-grandmother
el peregrinaje, pilgrimage
la lombarda, red cabbage
adscribir, to attribute; assign
el compromiso, compromise; commitment; pledge
el quejido, groan (of suffering)
el guatero, hot-water bottle
estar prevenido, to be well-prepared
el fulgor, glow
cavar, to dig
carmesí, crimson
anticuado, out-dated; old-fashioned
fruncir, to frown
entrever, to glimpse; to catch sight of
níveo, snow-like
diezmar, to decimate
detentar, unlawfully hold
aleatorio, random; uncertain (something which depends on luck)
ayunar, to fast
el guión, hyphen (-)
la vejiga, Depósito muscular y membranoso en forma de bolsa que recoge y almacena la orina que secretan los riñones. (bladder)
Negarse a (contestar), To refuse to (answer)
el charco, puddle
cálido, warm
la ronda, round
la soga, rope
el buitre, vulture
rezar, to pray
ronronear, to purr
el sonámbulo, sleepwalker
el teorema, theorem
el transeúnte, passer-by
el rehén, hostage
rechinar, to creek; to squeek
el OVNI, Objecto volador no identificado (UFO)
iracundo, irate
hacer hincapié, to stress; emphasise
transigir, to give in/compromise
acudir, to go/to come (indication of movement)
lamer, to lick
avalar, to guarantee
el aval, backing; support
el guarismo, numeral; figure (synonym of "cifra")
saciar, to satiate; to satisfy
el tiroteo, shootout; shooting; shooting rampage
la revancha, rematch
el mandamiento, commandment
el artilugio, gadget
remitir, to remit (to send)
restante, remaining
pasar desapercibido, to go unnoticed
la morada, dwelling; abode
prescindir, to do without something
el desafío, challenge (not reto)
enlentecerse, to get slower
desterrar, to exile
acceder, to agree
la paja, straw
el recinto, premises; enclosure; grounds
cuchichear, to whisper
velar, to watch over
desprender, to detach; dislodge
el escaño, seat; bench
la macana, pity; inconvenience
la macana, truncheon
dar paso a, to give way to
adentrarse, to go deep into; penetrate
en cuanto, as soon as
en cuanto, regarding
rebuscar, to rummage
rebuscado, affected; pretentious
rebuscado, over-elaborate; roundabout
ahondar, to deepen
ahondar, to go into (greater) detail
afanarse (en/por), to strive (to/for)
emanar, to emanate
contracorriente, against the flow
placentero, pleasant
primar, to give a bonus to
primar sobre (algo), to take precedence over (something)
el halago, flattery
halagar, to flatter
constatar, to affirm; to state
constatar, to confirm; to check
constatar, to verify
la verruga, wart
puntiagudo, pointy; pointed
la patilla, sideburn
lacio, limp; lank
el pómulo, cheekbone; cheek
el muslo, thigh
palpar, to touch; feel; palpate
manosear, to fondle
el atracador, the robber
la boina, beret
sustentar, to support
el aplazamiento, postponement
brindarse a, to offer to; to volunteer to (frml)
la viña, vineyard
ser un fresco, to be someone with a lot of nerve (in the negative sense)
el ludópata, compulsive gambler
la coqueta, flirt
ser listo como el hambre, to be very clever
ser apocado, to be spiritless; timid
estar apocado, to be depressed; down (colloq.)
risueño, cheerful; smiley
no estar católico, to be ill (colloq.)
ser más bueno que el pan, to be a really nice person
hablar hasta con las piedras, to be really talkative
presumido, conceited
la sosa, caustic soda; lye
el mortero, mortar
la ciruela, plum
la baldosa, floor tile
el carrete, spool; reel
el pícaro, rogue
pícaro, crafty; cunning; roguish
el rictus, sneer
el nácar, mother-of-pearl; nacre
pulcro, neat; tidy; meticulous
surcar, to plow through
el escote, cleavage; neckline (of clothes)
la mata, bush; shrub; tuft (of hair)
la trenza, braid; plait
el azabache, jet (a black; shiny mineral)
púber, adolescent
almidonado, starched
el pudor, modesty
el canto, singing
el soplo, blow/puff
el celaje, cloudscape
la claraboya, skylight
embetunar, to polish (shoes)
la manteca, animal fat; lard
la bata, dressing gown
las entradas, (receding) hairline
salirse con la suya, to have one's own way
berrear, to bawl; bellow
agredir, to attack; to assault
el rótulo, sign; label
involucrar, to involve
gatear, to crawl
el peón, laborer
la camisa de fuerza, straight jacket
de relieve, in relief (embossed); outstanding; prominent
augurar, to augur; predict
resquebrajar, to crack; split
convocar, to convoke; call together; summon
el descenso, decrease
aglutinar, to draw together; bring together
volitivo, voluntary; volitive
el matiz, nuance
abordar, to board
abordar, to tackle; approach
estar dotado de, to be equipped with
trazar, to trace
el integrante, member (of group; club; site) (i)
rentabilizar, to achieve a return on
patente, clear; evident
rebatir, to refute
taimado, cunning; sly
la arenga, the harangue
aleatoriamente, randomly
aparatoso, showy; exaggerated; pretentious
la vertiente, aspect
la casilla, box; capartment; mailbox; booth
el atajo, short cut
el estratega, strategist
abstraerse, to become lost in one's thoughts
abrumador, overwhelming
equiparar, to compare; to equate; to relate one thing to another considering them equal or equivalent
gimotear, to whine; whimper
el colofón, climax; culmination
compaginar, to harmonize
compaginar, combine
emprender, to embark on; undertake
la calabaza, pumpkin
el pasamontañas, ski mask
rendir, to perform well
el solfeo, music theory
pasar la noche en blanco, not to sleep a wink
no tener pérdida, to be easy to find
doblegar, to break; bend
doblegar, to crush
el amortiguador, shock absorber
el tronco, trunk; log
es un tronco, s/he's usless
frenético, frenzied
efímero, ephemeral
dilucidar, to clarify; elucidate
espulgar, to groom (an animal) to delouse
en especial, especially; in particular
el portador, carrier
el parentesco, kinship
la criba, sieve
ensalzar, to extol; praise
ameno, engaging; enjoyable
remontar, to overcome
redundar, to be of use to
manido, well worn
almacenar, to store
sondear, to sound out; to test
el yunque, anvil
el trinquete, pawl; ratchet
desatar, to untie
la vidriera, (stained) glass window
agachar, to bend; stoop; duck
el pergamino, parchment; scroll
untar, to smear; rub; spread
la escaramuza, skirmish
batir, to beat; whisk; whip
batirse, to duel
el mecenas de las artes, patron of the arts
el casco antiguo, old quarter
la cúspide, the peak
la superación, overcoming
el apartado, paragraph; section
la ordenación, regulation; organization
la ordenación, ordination
otorgar, to award
el afiche, poster
Hablar por los codos, To talk too much; chatter constantly
al pie de la letra, literally; to the letter
parecerse como dos gotas de agua, to be like two peas in a pod
ser uña y carne, to be thick as thieves
el casco, helmet
policromado, polychrome; multicolored
el apego, fondness; attachment
invertir, to invest
la contraportada, back page; back cover
la chuleta, chop; cutlet
el cerrazón, stubbornness
la abertura, opening; hole
la apertura, opening; commencement
la reprimenda, reprimand; telling off
feúcho, very ugly
el chucho, mutt; mongrel
el renacuajo, tadpole
de cara a, with a view to; in the face of
contraponer, to contrast; to oppose
el ensalzamiento, exaltation; praise
el convencimiento, conviction; certainty
resaltar, to stand out; to highlight (not destacar)
plasmar, to give expression to; capture (frml)
propiciar, to favor
el auge, peak
escatimar, to skimp on; be sparing with
salir del paso, to get out of a difficulty
ajeno, alien; foreign
ingente, enormous; huge; prodigous
la hazaña, feat; exploit; achievement
de andar por casa, for around the house
detenidamente, carefully; thoroughly
la profecía autocumplida, self-fullfilling prophecy
repentino, sudden
de lo contrario, if not; otherwise
por el contrario, on the contrary
la chabola, shack; shanty
con antelación, in advance
recopilar, to collect; gather
recabar, to request
sopesar, to weigh up (decision)
concienzudo, conscientious
promedio, average; average grade
el sindicalista, the trade unionist (sindicate)
nefasto, disastrous
el profano, layman (secular/profane)
el anclaje, anchorage
el ancla, anchor
la moda, mode; the value that appears most often in a set of data
el media, mean (mathematics / statistics)
la mediana, median;  the numerical value separating the higher half of a sample from the lower half
el alza, rise
a expensas de, at the expense of
propenso (a), prone to
enmarcar, to frame
fidedigno, reliable; trustworthy
disimular, to dissemble; to conceal
afectuoso, affectionate; caring
la investigación detenida, detailed investigation
la perspicacia, Insight
la coacción, coercion
paliar, to alleviate
alegar, to claim; to allege
entablar, to set up; enter into
la idoneidad, suitability
tajante, emphatic; strict
la tentación, temptation
holgazanear, to laze around
dadas las circunstancias, given the circumstances
quien tiene boca se equivoca, everyone makes mistakes
imprudente, reckless; careless; imprudent
perseverante, persevering
el helecho, fern
la huida, the flight (fleeing)
estrellarse contra, To smash into/against
la rueda de reconocimiento, police line-up
subsanar, to correct; rectify (s)
concerniente a (algo), concerning (something)
ostentar, to display; to flaunt
ostentar, to hold (e.g. a position)
en cuanto a, as for; with regard to
ceñirse a, to stick/adhere to
contrarrestar, to counteract; resist; oppose
recaudar fondos, to raise funds
airoso, graceful; elegant
la maniobra, maneuver
el empate, tie (game)
empaparse (de/en algo), to get soaking wet (to learn a lot about/be imbued with something)
el parador, high class tourist hotel run by the state
ir como la seda, to go smoothly
dar un vuelco el corazón, heart to skip a beat
tener la piel de gallina, to have goosebumps
el cabezazo, head-butt
la agujeta, muscle pain (Spain); shoelace (Mex)
la litera, bunk bed
esgrimir, to put forward
esgrimir, to wield/brandish
precaver, warn; give warning to
estribar (en), to rest on; stem from
el desabastecimiento, shortage; scarcity
atañer, to concern
la hucha, piggy bank
atenerse a, to stick to; abide by
el barullo, the din; racket (B)
perdurar, to endure; to last
el prójimo, fellow man; neighbor
desembocar, to flow into
el patinaje, skating
aferrar, seize; grasp; grab; take hold of
el afán de superación, desire for self improvement
el agravio, insult; offense
la medición, measurement
la gota que colma el vaso, the straw that broke the camel's back
en vísperas, shortly before
el desaire, snub; rebuff
el jinete, horseman
el desdén, disdain (not desprecio)
la óptica, view point (not punto de vista)
la pancarta, placard; banner
ensombrecer, to darken; cover in shadows
adiposo, adipose; fat
el sosiego, quiet; tranquility
a sabiendas de que, knowing full well that..
el rebote, the rebound
consensuar, to reach a consensus
arrojar, to throw; emit; cast out (not echar; emitir or lanzar)
arrojar, produce; generate; bring
la fuerza motriz, driving force; power
el pañal, nappy
la fritanga, fried food (Latin am.); greasy fried food (Spain)
pillar, to catch (not coger)
mermar, to diminish
cruento, bloody
el estante, bookcase; bookshelf
la pretensión, aim; intention
patente, clear; evident
perfilar, to outline; to define
dar calabazas, to jilt; to reject
hacerse cargo (de), to take charge; to be responsible for
el portazo, door slam
el historial, log; record
acuñar, to coin; mint
franquear, go through;or allow to go through
el escaño, seat; bench
diáfano, open-plan
los estragos, damage; havoc
la ingle, the groin
lúgubre, lugubrious; dismal; dreary
vislumbrar, to make out; discern; glimpse
el chaleco, vest; waist-coat
la sortija, ring (not anillo)
la solapa, lapel
macizo, solid; compact; robust
el rentista, Landlord who lives off rents paid them
irse al garete, to go down the drain/tubes
novedoso, novel; original
el avaro, miser
necio, stupid; foolish; fatuous
el decoro, decorum
rapaz, predatory
la tajada, slice (t)
la alambrera, wire netting
la melaza, molasses
atónito, astonished
atascar, to block; clog
atrincherar, to entrench; dig in
el parto, chidbirth
la sacudida, jolt (sudden movement or electric shock)
encrespar, to frizz; to irritate
el arrullo, soft sound
las tinieblas, darkness; (not oscuridad)
hacerse el sordo, To pretend not to hear; turn a deaf ear
fragoroso, noisy (not ruidoso; estruendoso)
zancudo, long legged (in America it means "mosquito")
el sopor, the torpor; drowsiness
la hez, faeces (Spain) lowest of the low (America)
el rapto, kidnapping (not secuestro)
el brebaje, the brew; unpleasent concoction
el asceta, hermit (not ermitaño)
darse ínfulas, to put on airs
los víveres, food supplies
la hazaña, feat; exploit; achievement
el casero, home owner
dar de alta, to register; sign up for
letrado, educated; learned
carcomer, to eat away at
escarpado, steep; craggy
lustrar, to polish
el estuche, case (of pencils; glasses)
el vaho, steam; breath
primoroso, exquisite
apacible, gentle; mild; peaceful
propicio, favourable
minucioso, detailed; meticulous
el dédalo, labrynth (not laberinto)
el rescoldo, ember
descarriado, wayword
en pelota, stark naked
el chaleco, vest; waist-coat
fétido, foul-smelling
de puntillas, on tiptoe
el candado, padlock (not the lock on a door)
la ráfaga, gust
el delantal, apron
el alfiler, pin
goloso, sweet toothed
disuasorio, dissuasive
la reticencia, reluctance; reticence
el recado, message; errand (not mensaje)
en vilo, on tenterhooks
la añoranza, notalgia; yearning
subsanar, to correct; rectify (s)
diestro, right-handed
zurdo, left-handed
la aleación, alloy
insonorizado, soundproof
la migaja, crumb; scraps
insólito, unusual
la piltrafa, scrap; wretch
la avería, damage; breakdown
el prestidigitador, conjurer
raudo, swift
destartalado, ramshackle
el aguacero, downpour; rain shower
óseo, bony; osseous; bone
cenagoso, muddy
desatinar, to act foolishly
desatinar, to perplex; bewilder
aciago, unlucky; ill-fated (fateful)
asolar, to devastate
amonestar, to admonish; to warn
el mameluco, fool; idiot (Latin America: overalls)
el somnífero, sleeping tablet
el remanso, quite place; haven
volver en sí, To come to; regain consciousness
el llanto, crying; sobbing
sobrecoger, to startle; to surprise
sobrecogerse, to be overwhelmed; overcome
yerto, rigid (especially if caused by the cold)
estallar, to explode; burst
seguir la corriente, to play along; to humor
salpicado, speckled
estragar, to ravage; to ruin
pueril, childish
el recluso, prisoner (r)
rapado, shaven headed (coloq.)
encinta, pregnant; expecting (no embarazada ni preñada)
los escombros, rubble
el blanco, target
el recodo, twist; bend (in the road)
la campiña, the countryside (not el campo)
el aparecido, ghost (not fantasma)
el anfitrión, host
encaramar, climb up to a high and dificult to reach place
insigne, distinguished; famous
despedazar, to tear or cut into pieces
apuñalar, to stab
azuzar a (perros), to set (dogs) on
acartonado, wooden (performance)
acartonarse, to become stiff
echar mano, to use; to seize (2 word phrase)
el bucle, ringlet; curl
romperse la crisma, to split one's head open
la vitrina, display cabinet
nauseabundo, nauseating
no quiero conocer los pormenores, I'm not interested in knowing the details
el arrebato, outburst; fit (of rage)
atesorar, to acumulate; to hoard
el arcén, hard shoulder; verge
la baratija, knick-knack; tirnket
el amanuense, scribe (someone who takes dictation)
de toda la vida, traditional; for ages
la fecundidad, fertility (not fertilidad)
la romería, pilgrimage (not peregrinación); local religious festival
el nombre de pila, christian name
el vástago, offspring  (also offshoot of a plant)
hacer hueco, to make room
el nombre hipocorístico, pet name (not apodo)
la aurora, aurora; dawn
estrambótico, outlandish
capicúa, palindromey (no real adjective for this in English)
la jaqueca, migraine
atiborrar, to stuff (full)
la metralla, shrapne
