д		d
а		a
да		yes
н		n
е		ye; e
т		t
нет		no
к		k; c
о		o
м		m
с		s
р		r
б		b
л		l
ж		zh
п		p
и		i; ee
в		v
привет		hi
пока		bye bye
ы		i
ь		soft sign
может быть		maybe
й		y
ё		yo
пойдём!		let's go!
ч		ch
я		ya
э		e
ш		sh
г		g
ф		f
з		z
х		h; kh
ц		ts
у		oo
щ		sch
ъ		hard sign
ю		yu
спасибо		thank you; thanks
пожалуйста		please; you are welcome
добро пожаловать!		welcome!
Россия		Russia
молодец!		well done!
доброе утро		good morning
спокойной ночи		good night
что нового?		what's up?
за твоё здоровье!		cheers!
извини		I'm sorry (I did that to you)
мне жаль		I'm sorry (to hear that)
увидимся		see you later
до свидания		goodbye
я		I
ты (тебя; тебе; тобой)		you (singular)
как		how; like; as
дела		things; affairs
как дела?		how are you?
очень		very
хорошо		well; okay; alright
меня (мне; мной)		me
у меня		at me; I have
у тебя		at you; you have (singular)
всё		all; everything
у меня всё очень хорошо		I'm very well
звать		to call
зовут		they call
как тебя зовут?		what's your name?
меня зовут ...		my name is ...
гений		genius
ты гений!		you're a genius!
больной		sick
болен		sick (short version)
счастливый		happy
счастлив		happy (short version)
грустный		sad
злой		angry
неправ		wrong; not right
правильный		right; correct
голодный		hungry
хотеть		to want
ты хочешь		you want
пить		to drink
устать		to be tired; to get tired
я счастлив		I'm happy (said by a man)
я счастлива		I'm happy (said by a woman)
ты счастлив		you're happy (said to a man)
ты счастлива		you're happy (said to a woman)
ты счастлив?		are you happy? (said to a man)
я неправ?		am I wrong? (said by a man)
я голоден		I'm hungry (said by a man)
ты хочешь пить?		do you want to drink (something)?
еда		food
хлеб		bread
макароны		pasta; macaroni
паста		pasta (dish)
рис		rice
картошка		potato
овощ		vegetable
фрукт		fruit
мясо		meat
салат		salad
яблоко		apple
банан		banana
апельсин		orange
лимон		lemon
суп		soup
яйцо		egg
сыр		cheese
курица		chicken
свинина		pork
говядина		beef
рыба		fish
вода		water
кофе		coffee
чай		tea
пиво		beer
вино		wine
молоко		milk
сок		juice
соус		sauce
масло		butter; oil
он		he; it
она		she; it
оно		it
вещь		thing; item; object
очень вкусный		delicious; very tasty
отвратительный		disgusting
потрясающий		awesome; fantastic
идеальный		perfect
не		not
и		and
мне нравится ...		I like ... ; I enjoy ...
мне не нравится ...		I don't like ... ; I don't enjoy ...
мне нравится хлеб		I like bread
мне не нравится паста		I don't like pasta
мне нравятся апельсины		I like oranges
мне не нравятся яблоки		I don't like apples
это очень вкусно		it's delicious
кофе очень вкусный		coffee is delicious
мне нравятся чай и кофе		I like tea and coffee
английский		English
англичанин; англичанка		Englishman; Englishwoman
русский		Russian (language)
русский; русская		Russian
американский		American
американец; американка		American (person)
я англичанин		I'm English (said by a man)
я англичанка		I'm English (said by a woman)
я американец		I'm American (said by a man)
я американка		I'm American (said by a woman)
а ты?		how about you?
ты русский?		are you Russian? (said to a man)
ты русская?		are you Russian? (said to a woman)
я не русский		I'm not Russian (said by a man)
я не русская		I'm not Russian (said by a woman)
номер		number
телефон		telephone; phone
номер телефона		phone number
один		one; 1; alone; single
два		two; 2
три		three; 3
четыре		four; 4
пять		five; 5
шесть		six; 6
семь		seven; 7
восемь		eight; 8
девять		nine; 9
десять		ten; 10
какой		what; which
мой (моя; моё; мои)		my; mine
твой (твоя; твоё; твои)		your; yours (singular)
какой у тебя номер телефона?		what's your phone number?
мой номер телефона ...		my phone number is ...
ресторан		restaurant
стол		table
столик		(little) table
меню		menu
счёт		bill; account; score
нож		knife
вилка		fork
ложка		spoon
мне можно		I can; I'm allowed
заказывать		to order
есть		to eat
готов		ready
навынос		take-away
на		on; at
мы		we
нас (нам; нами)		us
вы (вас; вам; вами)		you (plural; formal)
они		they
им (ими)		them
их (них)		their; theirs
столик на двоих, пожалуйста		a table for two, please
можно нам ... , пожалуйста?		can we have ... please?
можно нам меню, пожалуйста?		can we have the menu please?
вы готовы?		are you ready? (formal)
вы готовы заказывать?		are you ready to order?
да, пожалуйста		yes please
нет, спасибо		no thank you
понимать		to understand
я понимаю		I understand
ты понимаешь		you understand (singular informal)
он понимает		he understands
мы понимаем		we understand
вы понимаете		you understand (plural; formal)
они понимают		they understand
чудесный		wonderful
чудесно		wonderfully
красивый		beautiful
сильный		strong; heavy (rain)
слабый		weak
толстый		fat
тонкий		thin (object)
милый		nice; cute; pretty
некрасивый		ugly
классно		cool; awesome
большой		big; large
короткий		short
маленький		small; little
длинный		long
что		what; that
тоже		too; also
слишком		too; very
слишком много		too much; too many
так		so; such; that way
она чудесная		she's wonderful
он красивый		he's beautiful
это мило		it's nice
как чудесно!		how wonderful!
думать		to think
как ты думаешь?		what do you think?
я думаю ...		I think ...
я думаю, это классно		I think it's cool
я думаю, он слишком большой		I think it's too big
я тоже так думаю		I think so too
я так не думаю		I don't think 