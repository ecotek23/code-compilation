der Mensch, human
der Mann, man; husband
die Frau, woman; wife
das Kind, child
die Leute, people
der Junge, boy
das Mädchen, girl
der Freund, friend; boyfriend
die Freundin, friend (female); girlfriend
das Baby, baby
die Dame, lady
der Herr, gentleman; "Mr"
die Mutter, mother
der Vater, father
die Mama, mum
die Großmutter, grandmother
der Großvater, grandfather
der Sohn, son
die Tochter, daughter
der Bruder, brother
die Schwester, sister
der Onkel, uncle
die Tante, aunt
die Familie, family
der Cousin, cousin
die Cousine, cousin (female)
der Neffe, nephew
die Nichte, niece
der Verwandte, relative
die Eltern, parents
der Tag, day
die Woche, week
der Monat, month
das Jahr, year
das Jahrhundert, century
der Frühling, spring
der Sommer, summer
der Herbst, autumn
der Winter, Winter
die Saison, season
der Montag, Monday
der Dienstag, Tuesday
der Mittwoch, Wednesday
der Donnerstag, Thursday
der Freitag, Friday
der Samstag, Saturday
der Sonntag, Sunday
der Januar, January
der Februar, February
der März, March
der April, April
der Mai, May
der Juni, June
der Juli, July
der August, August
der September, September
der Oktober, October
der November, November
der Dezember, December
das Gesicht, face
das Auge, eye
der Mund, mouth
die Nase, nose
das Ohr, ear
das Haar, hair
das Bein, leg
der Körper, body
der Arm, arm
die Hand, hand
der Hals, neck
das Handgelenk, wrist
der Finger, finger
der Fuß, foot
die Taille, waist
die Handfläche, palm (hand)
der Nagel, nail
die Schulter, shoulder
der Ellenbogen, elbow
die Brust, breast
der Oberschenkel, thigh
der Rücken, back (of the body)
der Zahn, tooth
das Knie, knee
die Zunge, tongue
die Lippe, lip
das Herz, heart
das Gehirn, brain
die Lunge, lung
die Niere, kidney
der Magen, stomach
der Bauch, belly
der Darm, intestine
die Leber, liver
das Haus, house
der Garten, the garden / the yard
das Fenster, window
die Tür, door
das Zimmer, room
das Dach, roof
die Mauer, wall
der Zugang, access
der Flur, hallway
der Gang, corridor
das Treppenhaus, staircase
das Stockwerk, storey
die Küche, kitchen
das Badezimmer, bathroom
das Schlafzimmer, bedroom
das Wohnzimmer, living room
die Garage, garage
der Teppich, carpet
der Tisch, table
der Stuhl, chair
der Sessel, armchair
das Sofa, sofa
die Lampe, lamp
der Balkon, balcony
die Terrasse, terrace
das Regal, shelf
das Bücherregal, bookcase
das Fernsehen, television
das Bett, bed
das Bad, bath
die Toilette, toilet
das Waschbecken, sink
der Spiegel, mirror
das Gemälde, painting
der Vorhang, curtain
der Schrank, cupboard
der Kleiderschrank, wardrobe
der Gefrierschrank, freezer
der Herd, stove
der Kochtopf, the sauce pan
das Kissen, pillow
das Essen, food
das Getränk, drink
das Frühstück, breakfast
das Mittagessen, lunch
das Abendessen, dinner
das Abendbrot, supper
das Brot, bread
das Esszimmer, dining room
der Morgen, morning
die Nacht, night
der Mittag, noon
der Nachmittag, afternoon
das Wochenende, weekend
die Ferien, holidays
der Feiertag, public holiday
der Urlaub, vacation
das Weihnachten, Christmas
der Heiligabend, Christmas Eve
das Ostern, Easter
das Wasser, water
das Leitungswasser, tap water
das Fleisch, meat
die Haut, skin
der Fisch, fish
das Obst, fruit
die Apfelsine, orange (fruit)
der Saft, juice
der O-Saft, orange juice
der Apfel, apple
die Birne, pear
die Milch, milk
der Kaffee, coffee
der Tee, tea
der Salat, salad
das Gemüse, vegetable
die Schokolade, chocolate
die Milchschokolade, milk chocolate
die Tomate, tomato
die Kanne, pot
die Pampelmuse, grapefruit
die Beere, berry
die Erdbeere, strawberry
die Kirsche, cherry
die Melone, melon; bowler hat
die Wassermelone, watermelon
die Nuss, nut
die Seife, soap
die Zwiebel, onion
der Knoblauch, garlic
das Salz, salt
der Zucker, sugar
der Pfeffer, black pepper; chili/hot pepper
die Gurke, cucumber
die Bohne, bean
die Hülsenfrucht, legume
das Bier, beer
der Alkohol, alcohol
der Wein, wine
der Becher, mug
das Glas, glass
die Tasse, cup
das Messer, knife
der Löffel, spoon
die Gabel, fork
der Hahn, rooster; faucet
das Gewürz, spice
das Kraut, cabbage; herb
die Farbe, color
grün, green
blau, blue
weiß, white
schwarz, black
gelb, yellow
lila, lilac/purple
braun, brown
grau, grey
rosa, pink
dunkel, dark
klar, clear
das Licht, light
die Dunkelheit, darkness
der Schatten, shadow/shade
die Sonne, sun
der Mond, moon
der Stern, star
der Himmel, sky
die Wolke, cloud
die Erde, earth
der Boden, ground; soil
der Planet, planet
das All, universe
die Galaxie, galaxy
die See, sea
der Fluss, river
der Baum, tree
der Weihnachtsbaum, Christmas tree
der Wald, forest
das Feuer, fire
der Wind, wind
das Eis, ice; ice cream
der Sand, sand
die Wüste, desert
der Stein, stone
der Berg, mountain
das Tal, valley
der Strand, beach
die Küste, coast
der Hügel, hill
die Blume, flower
die Pflanze, plant
das Tier, animal
der Vogel, bird
das Säugetier, mammal
das Reptil, reptile
die Wanze, bug
das Insekt, insect
der Hund, dog
die Katze, cat
der Bart, beard
der Bär, bear
der Affe, ape
das Huhn, chicken
das Pferd, horse
der Esel, donkey
das Kaninchen, rabbit
der Hamster, hamster
die Maus, mouse
die Ratte, rat
der Igel, hedgehog
der Fuchs, fox
der Wolf, wolf
die Fledermaus, bat
die Robbe, seal (animal)
der Otter, otter
die Welpe, puppy
das Eichhörnchen, squirrel
der Waschbär, raccoon
der Delfin, dolphin
der Wal, whale
das Walross, walrus
der Seelöwe, sea lion
der Elefant, elephant
der Hirsch, deer
das Kamel, camel
die Mähne, mane
das Zebra, zebra
die Giraffe, giraffe
der Huf, hoof
der Tiger, tiger
der Löwe, lion
der Gorilla, gorilla
das Horn, horn (animal)
das Nashorn, rhinoceros
das Nilpferd, hippopotamus
der Koalabär, koala
der Eisbär, polar bear
das Känguru, kangaroo
der Pandabär, panda
der Spatz, sparrow
der Kanarienvogel, canary
die Schwalbe, swallow (bird)
der Kolibri, hummingbird
die Krähe, crow
die Eule, owl
die Möwe, seagull
der Adler, eagle
die Taube, dove
der Falke, falcon
der Storch, stork
der Strauß, ostrich; bouquet
der Pinguin, penguin
der Schwan, swan
die Gans, goose
der Pfau, peacock
der Truthahn, turkey (bird)
der Schnabel, beak
die Kralle, claw
der Flügel, wing
die Feder, feather
der Papagei, parrot
das Krokodil, crocodile
die Schnauze, snout
die Schlange, snake
die Wasserschildkröte, turtle (not 'S...')
die Schildkröte, tortoise
der Panzer, shell
die Eidechse, lizard
der Frosch, frog
die Kröte, toad
der Salamander, salamander
der Aal, eel
der Haifisch, shark
der Schwanz, tail
die Schuppe, scale (of fish; animal)
die Ameise, ant
die Biene, bee
die Wespe, wasp
der Käfer, beetle
der Schmetterling, butterfly
die Kakerlake, cockroach
die Grille, cricket (insect)
der Skorpion, scorpion
die Spinne, spider
die Fliege, fly; also Bow tie
der Marienkäfer, ladybug
die Schnecke, snail
die Wegschnecke, slug
der Wurm, worm
der Seestern, starfish
die Muschel, mussel
die Qualle, jellyfish
der Krebs, crab
der Seepolyp, octopus
der Tintenfisch, squid
der Hummer, lobster
die Rinde, rind
der Stamm, trunk; root; clan; tribe
das Blatt, leaf
der Ast, branch (of tree)
die Kiefer, pine tree
die Eiche, oak
die Palme, palm (tree)
die Ulme, elm
das Blütenblatt, petal
der Stängel, stalk
der Stiel, stem
das Gänseblümchen, daisy
die Sonnenblume, sunflower
der Klatschmohn, poppy
der Klee, clover; shamrock
die Nessel, nettle
die Umwelt; -en, the environment
die Wiese, pasture; grassland; meadow
das Grasland, grassland
der Regenwald, rainforest
der Wasserfall, waterfall
der Bach, stream
die Klippe, cliff
die Höhle, cave
das Ufer, shore; bank
der Gletscher, glacier
der Hang, slope
die Landschaft, countryside; landscape
das Land, country
der Vulkan, volcano
die Lava, lava
die Asche, ash
der Krater, crater
das Beben, tremor
das Erdbeben, earthquake
der Kontinent, continent
die Insel, island
der Ozean, ocean
der Pol, pole
die Halbinsel, peninsula
das Wetter, weather
das Polarlicht, aurora
der Sonnenschein, sunshine
der Schauer, shower (weather)
die Temperatur, temperature
die Atmosphäre, atmosphere
der Regen, rain
das Gewitter, storm
der Regenbogen, rainbow
der Schnee, snow
der Tornado, tornado
die Stadt, city
die Straße, street
der Laden, shop
das Gebäude, building
die Straßenlaterne, street light
die Bibliothek, library
das Kino, cinema
das Theater, theater
der Wolkenkratzer, Skyscraper
die Schule, school
die Universität, university
das Dorf, village
der Park, park
das Hotel, hotel
der Hof, courtyard
der Bauernhof, farm
die Allee, avenue
die Stadtmitte, city center
der Platz, space; place; town square
die Fabrik, factory
die Kirche, church
das Auto, car
der Bus, bus
die Bushaltestelle, bus stop
der Bahnhof, railway station
der Schaffner, train guard
die Bahn, train
das Flugzeug, airplane
der Flughafen, airport
das Schiff, ship
der Hafen, port; harbour
der Busbahnhof, bus station
die U-Bahn, subway; underground train system
die Straßenbahn, tram
der Lastwagen, truck
das Fahrzeug, vehicle
das Motorrad, motorcycle
das Fahrrad, bicycle
der Fahrradweg, bicycle path
das Steuer, steering wheel
das Rad, wheel
das Radio, radio
der Fahrer, driver
das Denkmal, monument
das Museum, museum
die Statue, statue
die Bank, bank; bench
das Gericht, court
das Schloss, castle; lock
der Turm, tower
der Tempel, temple
die Brücke, bridge
die Kathedrale, cathedral (not 'D...')
der Staudamm, dam
das Krankenhaus, hospital
das Taxi, taxi
das Einkaufszentrum, shopping mall
der Gemüseladen, greengrocer's
das Fischgeschäft, fishmonger's
das Geschäft, business
die Metzgerei, butcher's
der Friseurladen, hairdresser's
der Job, job
der Beruf, occupation
der Student, (college) student
der Lehrer, teacher
die Polizei, police
der Polizist, policeman
der Arzt, doctor
die Kunst, art
der Künstler, artist
das Bild, picture
der Maler, painter
die Malerin, female painter
der Schauspieler, actor
der König, king
die Königin, queen
der Fürst, prince
die Fürstin, princess (not 'P...')
der Präsident, president
das Rathaus, town hall
die Hauptstadt, capital city
das Gebiet, region
die Gegend, area
der Staat, state
Europa, Europe
der Zirkus, circus
das Stadion, stadium
der Clown, clown
der Zoo, Zoo
das Ereignis, event
die Party, party
die Partei, political party
das Parlament, parliament
die Wirtschaft, economy
die Wissenschaft, science
der Wissenschaftler, academic
der Wirtschaftswissenschaftler, economist (not V___)
das Geld, money
die Zeit, time
die Freizeit, spare time
die Zeitung, newspaper
das Buch, book
der Stift, pen
der Bleistift, pencil
das Wort, word
das Wörterbuch, dictionary
die Sprache, language
die Fremdsprache, foreign language
der Journalismus, journalism
der Journalist, journalist
der Brief, letter
der Buchstabe, letter (of the alphabet)
die Post, post
der Briefträger, postman
der Verkäufer, salesperson
der Kunde, customer
der Feuerwehrmann, fireman
das Lied, song
der Sänger, singer
der Schriftsteller, writer (not V__)
der Klempner, plumber
die Mine, mine
der Bergarbeiter, miner
die Arbeit, work
der Minister, minister
das Gold, gold
das Silber, silver
das Erdöl, crude oil
das Benzin, gasoline; petrol
der Brennstoff, fuel
die Rakete, rocket
das Metall, metal
das Holz, wood
der Zimmermann, carpenter
das Papier, paper
der Soldat, soldier
die Mannschaft, team (not T__)
die Bäckerei, bakery
der Bäcker, baker
der Astronaut, astronaut
der Politiker, politician
die Politik, politics
der Pfarrer, pastor
der Priester, priest
der Mechaniker, mechanic
der Motor, engine
die Maschine, machine
die Waschmaschine, washing machine
der Wäschetrockner, dryer
der Unterricht, lesson (not L__)
die Lektion, lesson (not U__)
die Apotheke, pharmacy
der Apotheker, pharmacist
der Sekretär, secretary (person)
das Sekretariat, secretariat
das Büro, office
der Mitarbeiter, employee
der Beschäftigte, employee
die Abteilung, department
der Partner, partner
der Teil, part
der Teilnehmer, participant
das Stück, piece
die Regierung, government
der Beamte, official; civil servant
die Bundesrepublik, Federal Republic
das Königreich, kingdom
das Reich, empire
der Kaiser, emperor
die Demokratie, democracy
die Diktatur, dictatorship
der Ingenieur, engineer
das Café, café
die Akademie, academy
die Gesellschaft, society
das Gesetz, law (in general)
der Rechtsanwalt, lawyer
die Firma, company; firm
die Hausfrau, housewife
der Angestellter, employee (...; Beschäftigter; Mitarbeiter; Arbeitnehmer)
der Elektriker, electrician
die Elektrizität, electricity
die Energie, energy
die Physik, physics
die Chemie, chemistry
der Volkswirt, economist (not W___)
der Krankenpfleger, nurse (male)
der Schneider, tailor
die Kleidung, clothes
das Kleid, dress
das Hemd, shirt
die Hose, trousers
der Schuh, shoe
die Mütze, cap; bonnet
das T-shirt, t-shirt
die Krawatte, necktie
der Hausschuh, slipper
der Bikini, bikini
der Musiker, musician
die Bratpfanne, frying pan
die Jacke, jacket
der Ärmel, sleeve
ärmellos, sleeveless
der Rock, skirt
die Bluse, blouse
der Büstenhalter, bra
das Unterhemd, vest
der Slip, underwear; panties; briefs
der Hut, hat
das Halstuch, neckerchief
der Gürtel, belt
der Griff, handle
der Handschuh, glove
das Handtuch, towel
der Regenschirm, umbrella
das Kinn, chin
die Jeans, pair of jeans
das Trikot, jersey
das Gut, estate
der Edelstein, gemstone
das Juwel, jewel
die Tasche, pocket
der Mantel, coat
der Manager, manager
der Chef, boss
der Direktor, director; headmaster; principal
der Regisseur, director (film; stage etc.)
der Knopf, button
der Bademantel, dressing gown
der Regenmantel, raincoat
die Unterwäsche, underwear
der Zug, train
der Trainingsanzug, tracksuit
das Ding, thing
das Leder, leather
die Lederschuhe, leather shoes
die Weste, vest
die Windjacke, windbreaker
das Sweatshirt, sweatshirt
der Pullover, sweater
die Shorts, shorts
die Socke, sock
der Schlaf, sleep
der Schlafanzug, pajamas
der Sonnenhut, sunhat
die Schürze, apron
die Sportschuhe, trainers
der Rucksack, rucksack
der Beutel, bag
der Sack, sack
die Tüte, bag
der Anhänger, trailer (auto); supporter; pendant
das Armband, bracelet
die Kette, chain
die Halskette, necklace
der Ring, ring
der Ohrring, earring
die Uhr, watch; clock
die Brosche, brooch
die Perle, pearl
die Perlenkette, pearl necklace
der Verschluss, fastener; lock; clasp
die Handtasche, handbag
die Aktion, action
die Aktivität, activity
die Aktentasche, briefcase
die Briefmarke, stamp
der Briefumschlag, envelope (not U__)
der Markt, market
die Börse, stock market
die Brieftasche, briefcase
das Portemonnaie, purse
das Gepäck, baggage
der Absatz, paragraph
der Slipper, slip-on
die Sandale, sandal
der Schutz, protection
der Schuh mit hohem Absatz, high heel shoe
die Schnur, cord ; string
die Schönheit, beauty
der Lippenstift, lipstick
der Puder, powder (not Pulver)
der Pinsel, paintbrush
die Schminke, makeup
das Parfüm, perfume
der Föhn, hairdryer
das Shampoo, shampoo
das Haargel, hair gel; gel
die Schere, scissors
der Kamm, comb
die Bürste, brush
die Haarbürste, hairbrush
das Band, strap
der Band, volume
die Perücke, wig
der Ball, ball
der Kreis, circle
das Quadrat, square (shape)
der Kubus, cube
das Feld, field
die Wirkung, effect
der Wirkungskreis, sphere of influence
die Kugel, sphere
das Dreieck, triangle
die Ecke, corner
das Fünfeck, pentagon
das Vieleck, polygon
die Pyramide, pyramid
der Raum, room
die Welt, world
der Weltraum, (outer) space
die Linie, line; route
der Vers, line; verse
die Kurve, curve
der Knoten, knot
das Gelenk, joint
der Knochen, bone
der Muskel, muscle
der Stoff, fabric
das Material, material
die Materie, matter
das Leben, life
der Tod, death
der Anfang, beginning
das Ende, end
der Schluss, end
die Situation, situation
die Stelle, place
die Position, position
der Ort, location
die Adresse, address
die Nummer, number
das Telefon, telephone
das Problem, problem
die Ahnung, suspicion; idea
die Meinung, opinion
die Ansicht, opinion
die Arbeitslosigkeit, unemployment
das Glück, luck; happiness
die Gefahr, danger
der Schreck, fright
der Frieden, peace
der Krieg, war
die Ruhe, rest
der Lärm, noise
die Musik, music
die Gitarre, guitar
das Klavier, piano
die Geige, violin; fiddle
das Orchester, orchestra
die Stimme, voice
das Publikum, audience
die Reihe, row
der Sitz, seat
der Lauf, course
das Konzert, concert
das Kostüm, suit (for women); costume
die Oper, opera
die Harfe, Harp
die Bratsche, viola
die Noten, musical score
die Note, note
die Notenlinien, staff (music - not __system)
die Sinfonie, symphony
das Saxophon, saxophone
das Instrument, instrument
die Flöte, flute
die Trompete, Trumpet
die Pause, pause
die Trommel, Drum
der Stress, stress
der Schlager, pop song
das Zeug, stuff
der Schlagzeuger, drummer
der Schlag, hit
das Mikrophon, microphone
die Tafel, blackboard
der Gitarrist, guitarist
der Bund, association
der Tanz, dance
die Tanzmusik, dance music
die Melodie, melody
der Text, text
die Diskothek, discotheque
die Literatur, literature
das Hobby, hobby
die Enzyklopädie, encyclopedia
das Studium, university studies
die Untersuchung, investigation
die Unterschrift, signature (autograph)
das Fach, subject
die Schublade, drawer
das Thema, theme
die Ausgabe, edition
das Studienfach, subject (study)
der Punkt, dot
der Test, test
die Mathematik, mathematics
die Angelegenheit, affair
die Beziehung, relationship
das Monster, monster
der Sinn, sense
der Begriff, concept
das Gefühl, feeling
die Empfindung, emotion
die Traurigkeit, sadness
der Kellner, waiter
die Aussage, assertion; statement; deposition
der Zustand, condition; state; way
die Geschichte, history
die Religion, religion
der Glauben, belief
das Schicksal, fate
der Verstand, reason
das Verständnis, understanding
der Geist, ghost
die Seele, soul
das Geschöpf, creature
das Geheimnis, secret
der Rätsel, puzzle
das Spiel, game
der Fußball, football
der Sport, sport
der Wettbewerb, competition
der Spieler, player
der Richter, judge
der Schiedsrichter, referee
der Bezug, reference
der Zweck, purpose
das Ziel, target
das Tor, goal
der Torwart, goalkeeper
die Olympiade, olympiad
das Ticket, ticket
die Karte, card
der Eintritt, admission
der Ausweg, escape
der Marathon, marathon
der Basketball, basketball
der Korb, basket
der Behälter, container
der Müll, rubbish
der Mülleimer, dustbin
das Tennis, tennis
der Schläger, racket; bat; club
der Rugby, rugby
der Stoß, push
der Schuss, gunshot
der Tritt, kick
das Schwimmen, swimming
die Leichtathletik, athletics
der Athlet, athlete
der Sieg, victory
das Lager, warehouse
die Niederlage, defeat
der Baseball, baseball
das Boot, boat
das Kanu, canoe
das Segeln, sailing
das Segelboot, sailboat
der Stiefel, boot
der Angriff, attack
die Verteidigung, defence
die Auszeit, time out
der Spielstand, score (in a game)
das Boxen, boxing
der Rivale, rival
der Gegner, enemy
die Fahne, flag
das Symbol, symbol
der Versuch, attempt
das Risiko, risk
die Wette, bet
die Medaille, medal
die Trophäe, trophy
das Foul, foul
die Halbzeit, halftime
der Ersatz, substitute
das Hockey, hockey
das Spielfeld, field (sports)
der Pass, passport
das Netz, net
der Sprung, jump
der Sturz, fall; plunge
der Wurf, throw
der Fang, catch
die Saite, string (racket; instrument...)
das Rennen, race (sport)
das Match, match (not 'S...')
der Gewinner, winner
der Verlierer, loser
der Volleyball, volleyball
das Tischtennis, table tennis
das Golf, golf
das Loch, hole
die Schaukel, swing
das Eisen, iron (element)
die Bronze, bronze
die Rennbahn, track (athletics)
die Startlinie, starting line
die Ziellinie, finishing line
der Speer, javelin
das  Speerwerfen, javelin (sport)
der Rekord, record
das Turnen, gymnastics
der Turner, gymnast
die Matte, mat
der Kampf, battle
der Kampfsport, combat sport
das Karate, karate
das Judo, judo
die Waffe, weapon
das Schwert, sword
der Säbel, sabre
der Schild, shield; sign
die Maske, mask
der Helm, helmet
das Heer, army (not Armee)
das Militär, military
die Strategie, strategy
der Plan, plan (not V...)
das Projekt, project; venture
das Fallen, fall; act of falling
der Block, block; pad
die Faust, fist
die Badehose, (swimming) trunks
die Drehung, rotation; turn
die Wende, turning point
die Kehrtwendung, u-turn; about-turn
der Anker, anchor
der Leuchtturm, lighthouse
das Ruder, rudder
der Reiter, rider
der Zaun, fence
das Fischen, fishing (not A__)
der Angler, angler
die Angelrute, fishing rod
der Ski, ski
der Skisport, skiing
der Schlittschuh, ice skate (shoe)
der Rollschuh, roller skate
der Fallschirm, parachute
das Fallschirmspringen, parachuting
das Skateboard, skateboard
der Bogen, curve; bow
der Pfeil, arrow
das Bogenschießen, archery
das Bowling, bowling
das Snooker, snooker
die Fitness, fitness
die Sauna, sauna
die Übung, exercise
das Beispiel, example
das Becken, basin; pelvis
das Schwimmbecken, swimming pool (Schwimmbad; Schwimmpool; ...)
der Enkel, grandson
die Enkelin, granddaughter
der Besen, broom
der Mopp, mop
der Eimer, bucket
die Glühlampe, light bulb
die Decke, cover
die Tischdecke, tablecloth
der Lumpen, rag
das Tuch, cloth
das Taschentuch, handkerchief
das Laken, the linen; the sheets
das Bettlaken, bedsheet
der Stab, staff; rod
der Stock, floor (of building); stick
der Faden, thread
die Nadel, needle
der Stich, sting
die Schraube, screw
die Stunde, hour
die Minute, minute (time)
die Sekunde, second (time)
das Jahrzehnt, decade
die Dauer, duration
die Ewigkeit, eternity
das Sein, existence
die Wirklichkeit, reality (not Realität)
der Traum, dream
der Alptraum, nightmare
die Wahrheit, truth
die Lüge, lie
die Einheit, unit; unity
das Bündel, bundle
die Gruppe, group
die Masse, crowd
die Menge, quantity
die Statistik, statistics
das Maß, beer mug (one liter)
die Größe, size
die Dimension, dimension
die Höhe, height
die Weite, width
die Distanz, distance (not 'S...')
die Anzahl, quantity amount
der Meter, meter
die Länge, length
der Zentimeter, centimeter
der Kilometer, kilometre
der Weg, way
die Route, route
das Gleis, (railway) track
die Autobahn, highway
der Streifen, stripe; streak
der Zebrastreifen, crosswalk
der Fußgänger, pedestrian
die Kreuzung, crossing
Der Verkehr, Traffic
die Ampel, traffic light
der Wächter, guard
der Stau, traffic (jam)
die Kapazität, capacity
der Liter, litre
der Milliliter, milliliter
das Gewicht, weight
die Kraft, force; power
die Oberfläche, surface (not 'F...')
die Luft, air
das System, system
die Struktur, structure
die Organisation, organization
das Gymnasium, grammar school
die Turnhalle, gym
die Ausbildung, education; training
das Gramm, gram
das Kilogramm, kilogram
das Geldstück, coin (not 'M..')
der Schein, banknote (1)
der Geldschein, banknote (2)
das Zertifikat, certificate
das Gehalt, salary
der Lohn, wage
das Einkaufen, shopping
die Mode, fashion
die Anzeige, announcement; advertisement; notice
die Ankündigung, announcement
die Kreditkarte, credit card
die Schuld, debt
die Steuer, tax
das Bußgeld, fine; penalty (motoring)
das Bargeld, cash
der Kassierer, cashier
der Supermarkt, supermarket
die Bezahlung, payment
die Rechnung, bill
der Ober, head waiter
die Kellnerin, waitress
das Restaurant, restaurant
die Quittung, receipt
das Paket, packet
die Telefonzelle, telephone box
der Briefkasten, mailbox
das Dokument, document
der Reisepass, passport
der Personalausweis, identity card
der Austausch, exchange
der Wechsel, change
das Eigentum, property; ownership
das Schaf, sheep
die Wolle, wool
die Ziege, goat
der Bulle, bull
die Kuh, cow
der Ochse, ox
der Käse, cheese
das Schwein, pig
das Wildschwein, wild boar
das Weibchen, hen
die Ente, duck
das Frettchen, ferret
der Geier, vulture
das Korn, grain
der Samen, seed
die Landwirtschaft, agriculture
das Rind, bovine
die Rinder, cattle
die Ernte, crop; harvest
der Stil, style
der Brauch, custom (G...; S...; ?)
die Gewohnheit, habit
die Reise, travel
die Neugier, curiosity
die Erkundung, exploration
die Entdeckung, discovery
die Erfindung, invention
das Design, design
das Modell, model
die Heilung, cure
die Medikation, medication
die Medizin, medicine
die Vakzine, vaccine
die Krankheit, illness
die Gesundheit, health
die Operation, operation
die Chirurgie, surgery
der Chirurg, surgeon
das Pflaster, band-aid
der Sirup, syrup
die Flüssigkeit, fluid; liquidity
der Festkörper, solid
das Gas, gas
der Rauch, smoke
die Tablette, tablet
die Pille, pill
die Droge, drug
das Niesen, sneeze
der Reflex, reflex
die Sorge, worry
der Husten, cough
die Grippe, flu
der Kopf, head
der Schmerz, pain
der Kopfschmerz, headache
die Wange, cheek
die Schläfe, temple (of head)
das Augenlid, eyelid
die Augenbraue, eyebrow
das Blut, blood
die Arterie, artery
die Ader, vein (not 'V...')
die Erkältung, cold (illness)
die Übelkeit, nausea
der Herzinfarkt, heart attack
der Druck, pressure
der Blutdruck, blood pressure
die Allergie, allergy
die Infektion, infection
der Durchfall, diarrhea
der Kot, feces
der Schlamm, mud
der Harn, urine
der Virus, virus
die Magenschmerzen, stomach ache
die Konsultation, consultation
die Spritze, syringe
die Injektion, injection
der Patient, patient
die Behandlung, treatment
der Prozess, process; lawsuit
die Methode, method
die Manier, manner
die Haltung, attitude
das Verhalten, behaviour
die Waage, scales (weighing machine)
die Balance, balance
das Gleichgewicht, equilibrium
der Nerv, nerve
das Neuron, neuron
die Geduld, patience
die Ungeduld, impatience
die Zelle, cell
das Mobiltelefon, mobile phone (not H...)
der Akt, act; deed
die Tat, act; deed
die Tatsache, fact
die Sache, thing; affair
die Ursache, cause
das Motiv, motive
die Konsequenz, consequence
die Reihenfolge, sequence
die Ordnung, order; tidiness
die Unordnung, disorder
das Chaos, chaos
das Desaster, disaster
die Tragödie, tragedy
das Drama, drama
das Datum, date
der Termin, appointment
das Treffen, meeting (arranged)
die Versammlung, gathering (not 'Z...')
der Warteraum, waiting room
die Impfung, vaccination; inoculation
das Thermometer, thermometer
die Verletzung, injury
die Wunde, wound
der Schaden, damage
die Fraktur, fracture
die Verstauchung, sprain
die Sicherheit, security
die Drohung, threat
die Vergeltung, retaliation
die Rache, revenge
die Eifersucht, jealousy (not 'N...')
der Eifer, eagerness
der Neid, envy
der Hass, hatred
die Liebe, love
der Wahnsinn, madness
der Schnitt, cut; section
der Kratzer, scratch
die Brandwunde, burn; burn wound
der Sonnenbrand, sunburn
der Splitter, splinter
der Biss, bite
der Unfall, accident
die Blutung, bleeding
der Notfall, emergency
das Gift, poison
die Vergiftung, poisoning
der Suizid, suicide
die Hilfe, help
die erste Hilfe, first aid
die Einrichtung, institution
die Institution, institution
die Hochschule, college
die Bandage, bandage
der Puls, pulse
die Atmung, breathing
die Wiederbelebung, revival
das Gewissen, conscience
das Bewusstsein, consciousness
der Operationssaal, operating theatre
die Blutuntersuchung, blood test
das Diagramm, diagram
das Schaubild, chart
der Fall, case
der Bericht, report
die Information, information
das Wissen, knowledge
der Rollstuhl, wheelchair
die Klinik, clinic
die Kardiologie, cardiology
die Orthopädie, orthopaedy
die Ginäkologie, gynaecology
die Radiologie, radiology
die Pädiatrie, paediatrics
die Dermatologie, dermatology
die Psychiatrie, psychiatry
die Physiotherapie, physiotherapy
die Therapie, therapy
die Theorie, theory
das Ergebnis, the result
der Zahnarzt, dentist
die Krone, crown
die Prothese, prosthesis
die Zahnprothese, dentures
der Augenoptiker, optician
die Brille, glasses
das Brillengestell, glasses frame
die Sonnenbrille, sunglasses
die Pupille, pupil (of eye)
die Iris, iris
die Linse, lens (of eye)
die Träne, tear; teardrop
die Sehkraft, vision; eyesight
die Schwangerschaft, pregnancy
der Nabel, navel
die Nabelschnur, umbilical cord
die Plazenta, placenta
die Gebärmutter, uterus
der Penis, penis
der Hoden, testicle
die Drüse, gland
die Scheide, vagina
das Fötus, fetus
die Geburt, birth
der Monitor, monitor
das Stillen, nursing
das Alternativ, alternative
die Wahl, choice; election
die Entscheidung, decision
die Massage, massage
die Meditation, meditation
das Yoga, Yoga
der Therapeut, therapist
die Psychologie, psychology
die Entspannung, relaxation
die Verschmutzung, pollution
die Ökologie, ecology
die Kohle, coal
der Kohlenstoff, carbon
die Magie, magic
der Kuss, kiss
das Lachen, laughter
das Lächeln, smile
der Witz, joke
die Stimmung, mood; atmosphere
der Humor, humor
die Komödie, comedy
der Gedanke, thought
die Idee, idea
das Gedächtnis, memory; recall
die Fähigkeit, ability
das Merkmal, feature
die Eigenschaft, capacity; characteristic
die Charakteristik, characteristic
die Erinnerung, memory; reminder
die Überraschung, surprise
das Geschenk, gift
das Präsent, present (gift)
der Geburtstag, birthday
der Namenstag, name day
der Jahrestag, anniversary
die Hochzeit, wedding
die Scheidung, divorce
das Begräbnis, funeral
die Taufe, baptism
die Hochzeitreise, honeymoon (not 'F...')
die Ehe, marriage
die Heirat, matrimony
das Festival, festival
die Feier, celebration
der Karneval, carnival
die Parade, parade
das Neujahr, New Year
der Ekel, revulsion
der Zorn, anger; rage
der Ärger, trouble; anger; irritation
das Vergnügen, pleasure
die Wut, anger; rage
die Täuschung, deception
die Enttäuschung, disappointment
die Reue, regret
die Glücklichheit, happiness
die Angst, fear; anxiety
die Furcht, fear; dread
die Weile, while (period of time)
die Langeweile, boredom
die Verwirrung, confusion
der Hochmut, arrogance
der Stolz, pride
die Qualität, quality
die Ehre, honor
das Lob, praise
der Preis, price; prize
der Titel, title
die Branche, branch (of bank etc.)
die Industrie, industry
das Laboratorium, laboratory
das Vertrauen, trust
die Zuversicht, confidence
die Scham, shame
das Seufzen, sigh
das Begehren, desire (not 'L...')
die Freiheit, freedom
die Sklaverei, slavery
der Schrei, shout
das Weinen, cry
das Gefängnis, prison
der Sklave, slave
das Verbrechen, crime
die Gerechtigkeit, justice
das Unrecht, injustice
die Gerade, straight line
das Segment, segment
die Zone, zone
die Diskussion, discussion
die Rüstung, armor
das Gewehr, rifle
die Peitsche, whip
die Pistole, pistol
die Feuerwaffe, firearm
der Dieb, thief
der Raub, robbery
die Gewalttätigkeit, violence
die Schlacht, combat
der Mord, murder
die Ermordung, assassination
der Mörder, murderer
die List, trickery
die Liste, list
der Trick, trick
die Falle, trap
die Zuneigung, affection
die Verhaftung, arrest
die Kaserne, barracks
die Entführung, kidnapping
der Gefangener, prisoner
die Strafe, punishment; penalty
der Gerichtssaal, court room
das Urteil, judgment
die Jury, jury
der Zeuge, witness
das Alibi, alibi
das Verhör, interrogation (trial)
die Todesstrafe, death penalty
die Haftstrafe, prison sentence
der Knast, jail (not G___)
die Anklage, accusation
das Rasiermesser, razor knife
die Rasur, shaving
das Unglück, misfortune
das Erdolchen, stabbing
der Spieß, spike (weapon)
die Lage, location
die Stichwaffe, bladed weapon
der Schnurrbart, moustache
der Fingerabdruck, finger print
der Täter, culprit
die DNS, DNA
die Haft, imprisonment
der Hinweis, evidence; clue; hint
der Anhaltspunkt, clue; lead
der Tatort, crime scene
die Leiche, corpse
der Name, name
der Nachname, surname
der Sarg, coffin
der Friedhof, graveyard
das Attentat, violent attack; assassination
das Komplott, conspiracy
der Beweis, proof
der Detektiv, detective
die Autopsie, autopsy
der Attentäter, assassin
der Agent, agent
das Opfer, victim
das Geburtsdatum, date of birth
der Nachbar, neighbor; neighbour
die Nachbarschaft, neighborhood; neighbourhood
der Bekannter, acquaintance
das Bonbon, candy
der Kuchen, cake
der Stollen, stollen (German Christmas cake); tunnel (mineshaft)
die Speise, dish; meal; food
das Dessert, dessert (not N__)
die Falte, wrinkle; fold
die Pore, pore
der Ringfinger, ring finger
der Mittelfinger, middle finger
das Zentrum, centre
der Zeigefinger, index finger
der Daumen, thumb
der kleine Finger, little finger
die Stirn, forehead
der Kiefer, jaw
der Nacken, nape
die Achselhöhle, armpit
die Wade, calf
die Ferse, heel
der Knöchel, ankle
der Schädel, skull
das Skelett, skeleton
das Wadenbein, fibula
das Schienbein, tibia
die Sehne, tendon; sinew; string (bow); chord (music)
der Knorpel, cartilage
der Brustmuskel, pectoral
der Bizeps, biceps
der Trizeps, triceps
die Bauchmuskeln, abdominals
die Backe, buttock
der Tabak, tobacco
der Raucher, smoker
die Zigarette, cigarette
der Abhängige, addicted person
der Missbrauch, abuse
der Drogenmissbrauch, drug abuse
die Sucht, addiction
der Konsum, consumption
die Dosis, dose
das Gähnen, yawn
die Trinksucht, alcoholism
das Nikotin, nicotine
der Sex, sex
das Geschlecht, gender
die Gattung, genus
die Art, sort; kind
die Spezies, species
die Rasse, race; breed
der Typ, type; guy
der Geschlechtsverkehr, sexual intercourse
der Klang, sound; clang
die Orientierung, orientation
die Tendenz, tendency (not 'N...')
die Neigung, tendency; slope
die sexuelle Orientierung, sexual orientation
die Sexualität, sexuality
die Homosexualität, homosexuality
die Heterosexualität, heterosexuality
die Vorliebe, preference
die Priorität, priority
der Geschmack, flavour
das Leck, leak
das Kokain, cocaine
der Joint, joint (drug)
der Thunfisch, tuna
der Lachs, salmon
die Mücke, mosquito; music (slang)
die Karotte, carrot (not M__)
der Teller, plate
der Hintern, bottom; behind
die Milz, spleen
der Transport, transport
der Hubschrauber, helicopter
der Bahnsteig, platform (train station)
der Stall, stable
die Bucht, bay; cove
der Gipfel, peak; summit
das Meer, sea; ocean
die Inselgruppe, archipelago
das Kap, cape
der Canyon, canyon
der Professor, professor
das Alter, age
die Epoche, epoch
der Zeitraum, period of time
die Lebenszeit, lifetime
die Achtung, attention
der Wille, will; willingness
die Bühne, stage
der Ziegel, brick (constr.)
der Dachziegel, roof tile
der Schornstein, chimney; smokestack
der Kamin, fireplace; chimney
das Vordach, porch
die Hausaufgabe, homework
die Haustür, front door
die Miete, rent
der Keller, basement
der Mieter, tenant
das Geländer, hand rail
die Wohnung, apartment; flat
der Wohnblock, apartment block; block of flats
das Schloss, lock; castle
der Fahrstuhl, elevator (not A__)
der Heizkörper, radiator
der Ventilator, electric fan
der Fächer, fan (for cooling)
der Heizofen, heater; heating furnace
die Leitung, management; cable; line
der Stecker, plug
der Schalter, counter; switch
das WC, water closet; WC
die Flasche, bottle
die Dose, can
die Vase, vase
die Kerze, candle
das Arbeitszimmer, office
der Gast, guest
der Hunger, hunger
die Notwendigkeit, necessity
der Durst, thirst
die Schüssel, bowl
das Kännchen, (small) jug
die Teekanne, teapot
die Serviette, napkin
die Mikrowelle, microwave oven
der Toaster, toaster
das Gerät, device
der Apparat, apparatus
der Mixer, blender
der Roboter, robot
der Schaum, foam
das Spülmittel, dish soap
die Spülmaschine, dish washer
die Intelligenz, intelligence
der Hammer, hammer
der Dosenöffner, can opener
die Reibe, grater
der Flaschenöffner, bottle opener
das Sieb, colander
der Schneebesen, whisk; eggbeater
die Tagesdecke, bedspread
die Matratze, mattress
das Bügeleisen, iron (clothes)
der Kissenbezug, pillowcase
der Abfluss, drain
die Dusche, shower
das Duschgel, shower gel
die Zahnbürste, tooth brush
die Zahnpasta, toothpaste
das Deo, deodorant
das Badetuch, bath towel
das Kinderbett, cot
die Puppe, doll
das Puppenhaus, doll's house
das Spielzeug, toy
die Windel, diaper
die Kapuze, hood
die Wäsche, laundry
der Wäschekorb, laundry basket
die Klammer, clamp
das Bügelbrett, ironing board
die Schaufel, shovel
die Müllschaufel, dust pan
das Waschmittel, detergent; washing powder
die Säge, saw (tool)
die Batterie, battery
der Bohrer, drill
das Werkzeug, tool
der Schlüssel, key
die Feile, file (tool)
das Dekorieren, decoration
die Verzierung, adornment
das Element, element
der Faktor, factor
der Teich, pond
der Rasen, lawn
der Springbrunnen, fountain
der Kaktus, cactus
das Gras, grass
der Rechen, rake
der Blumentopf, flowerpot
die Rose, rose
die Gartenarbeit, gardening
die Hecke, hedge
der Elternteil, pare
