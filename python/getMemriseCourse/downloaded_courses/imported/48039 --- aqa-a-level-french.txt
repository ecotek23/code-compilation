putrescible		biodegradable
l'ère industriel (m)		industrial era
la circulation routière		road traffic
les ordures ménagères (f)		household waste
un égout		sewer
le navire		ship
le naufrage		shipwreck
rompre		to break, to upset
voire		indeed, even
grimper		to climb
le réchauffement		warming
la fonte des calottes glaciaires		melting of the ice caps
la sécheresse		drought
bouleverser		to upset
la balade		walk
déranger		to disturb
un adhérent		member
le défault		fault
viser à + inf		to aim at
lié à		linked to
primordial		very important, fundamental
sensible à		aware of
gourmand		greedy
sache		(subj. of savoir) know
confier à		to entrust
ferroviaire		using rail
fluvial		using rivers
bannir		to banish
nuisible		harmful
élaborer		to set out
puissant		powerful
atteigne		(subj. of atteindre) reach
avoir la cote		to be very popular
croitre		to grow
le pétrole		oil
l'épuisement (m.)		exhaustion, running out
épuiser		to use up
quotidien		daily
une flambée des prix		price hike
la raréfaction		scarcity
la politique énergétique		energy policy
la nocivité		noxiousness
inépuisable		inexhaustible
la vague		a wave
le fleuve		river
le panneau		panel
une éolienne		wind turbine
alimenter		to feed
la chute d'eau		waterfall
la marée		tide
efficace		efficient
brûlé		burnt
la mise en œuvre		construction
le barrage		dam
entraîner		to entail, to lead to
chasser		to drive out
la taille		size
diviser		to divide
souscrire à		to subscribe to
le gaspillage		waste
relever le défi		to rise to the challenge
étendu		spread out
le moindre déplacement		the least movement
lié à		linked to
le frein		brake
la croissance		growth
préconiser		to recommend
provenir: proviendra de		to come from: will come from 
une empreinte écologique		carbon footprint
s'exprimer		to be expressed
disposer de		to have
équitable		fair
or		well, but, now
inégalement		unequally
réparti		shared out
la durabilité		sustainability
l'inégalité		inequality
une espèce		species
disparaitre		disappear
les eaux douces		fresh water, lakes
la récolte		harvest
la chasse		hunting
envahissant		invading
quasiment		almost
le loup		wolf
le massif		mountain
le mammifère		mammal
un ours		bear
entamer		to open up
consacré à		dedicated to
la sidérurgie		steel
la veille		the night before
la réunion		the meeting
ratifier		to ratify, to endorse
prôner		to preach
viser à		to aim at
en proie à		victim of
la sécheresse		drought
le lieu		place
la durée		period of time
provenir		to come from
lointain		far away
une asile		asylum
la faiblesse de leurs revenus		their low income
le/la locataire		tenant
surpeuplé		overcrowded
davantage		more
un ouvrier		a worker
la décennie		decade
à peine		hardly
à l'inverse		on the contrary
désormais		from now on
la zone frontalière		border zone
le durcissement		hardening
insuffisant		insufficient
être soumis à		to be subject to
hors		except for, apart from
la prestation sociale		social security benefit
entrainer		to lead to
faire grand bruit		to cause an outcry
prévoir		to plan
la trêve		truce
le bidonville		shanty town
les tsiganes, les Roms		gypsies
au petit matin		early in the morning
les forces de l'ordre		the police
la fuite		escape
être prié de		to be requested to
d'une traite		without stopping on the way
réduire en morceaux		to smash to pieces
puiser		to draw from
le terrain vague		waste land
un ayant droit		a person entitled to something
faire bondir		to enrage
une ONG (organisation non gouvernementale)		NGO (humanitarian or non-gorvernmental organisation)
le/la beur		(verlan, tiré du mot "arabe") 2nd generation North African living in France
à la même enseigne		in the same boat
la grande distribution		(chains of) supermarkets
miser sur		to bank on, to count on
compte tenu de		considering, in view of
à rallonge		never-ending
s'égrener		to range from
la viande Halal		Halal meat
le/la responable		manager, person in charge
le BTS, brevet de technicien supérieur		2-year degree
le chef de rayon		department superviser
le CDD: contrat à durée déterminée		fixed-term contract
le CDI: contrat à durée indéterminée		permanent contract
la racine		root
la banlieue		suburb
le taux de chômage		the unemployment rate
se heurter à		to bump into, to come up against
une embauche		start, job
une émeute		riot
surgir		to arise
le quartier		area, neighbourhood
être déclenché		to be triggered off
la cible		target
la fonction publique		public service
s'en prendre à		to take it out on, to attack
l'acceuil		reception, welcome
la réussite		success
cependant		however
améliorer		to improve
reporter sur		to transfer to
un espoir		hope
s'éloigner		to move away
être contraint de		to be forced to
éviter		to avoid
le dérichement		tear, breakdown
le genre		kind
malgré tout		in spite of everything
avoir du mal à		to have difficulty in
l'inconnu		the unknown
la coutume		custom
être à l'abri de		to be safe from
mépriser		to despise
le défault		fault
lutter		to fight
triste		sad
se ressembler		to resemble each other, to be the same
le/la beur		(verlan, tiré du mot "arabe") 2nd generation North African living in France
la Cour d'appel		Court of appeal
le fabricant		manufacturer
coupable		guilty
une animatrice / un animateur		sales / promotions staff
la grande surface		hypermarket
une amende		a fine
prison avec sursis		suspended sentence
les dommages et intérêts		damages
les frais		expenses
une agence d'intérim		temping agency
l'inspection (f.) du travail		factory inspectorate
une équipe		team
le fichier		file
être le théâtre de...		to be the scene of...
le répite		respite, rest
être pris à parti		to be picked on
le gamin		kid
tenter		to attempt
le domicile		home
inscrire		to write
la croix gammée		swastika
faire l'objet de		to be the subject of
taguer		to graffiti
envahir		to invade
déménager		to move house
le/la SDF: sans domicile fixe		homeless person
le chômeur		unemployed person
le licenciement		redundancy
mendier		to beg
se ficher de (fam.)		not to give a damn about
scolariser		to educate
bosser (fam.)		to work
défavorisé		underpriveliged
actif		(of) working (age)
percevoir		to earn
traité de racaille		branded as scum
manifester		to protest
précaire		precarious
une embauche		start, job
la durée déterminée		fixed term
à temps partiel		part time
remonter à		to reach
atteindre		to reach
important		important
plus élevé		higher
la marche arrière		reverse (gear)
véritable		real
s'imposer		to be assertive / convincing
l'issue (à la dette)		the solution (to debt)
le révélateur		indicator
accorder		to grant
une association caritative		charity
aux prises avec		struggling against
s'efforcer d'apporter		to strive to provide
la délinquance		crime, criminality
il le vaut bien		he deserves it
fournir		to provide
parvenir		to succeed
entraîner		to entail, to lead to
soi		oneself
aboutir		to succeed / to end up
le délit		offense
une infraction		breach, infringement
enregistré		recorded
la voie publique		public highway
le taux d'élucidation		clear-up rate
le semestre		term (of six months)
une atteinte		attempt, attack
les biens		goods, property
le cambriolage		burglary
la récidive		reoffending
dissuader		to deter
à peine		hardly
engorgé		overcrowded
obsédé		obsessed
apaiser		to quell, to appease
une échelle		scale
le dispositif		measure, arrangement
mettre en place		to set up, to put in place
le milieu		environment, setting
périscolaire		outside school
le brevet		certificate, diploma
le rapport		relationship
l'immédiateté		immediacy
l'ubiquité		being everywhere
le lieu de travail		workplace
la gestion		management
ferroviaire		using rail
gérer		to manage
le point de repère		landmark
la répression		punishment
la voie de dépassement		overtaking lane
c'en est de même pour		the same applies to
disloqué		disembodied
émanant		emanating
déclencher		to trigger
se coincer		to get stuck
la conduite		driving
lutter		to struggle
le décryptage du génome		decoding of the human genome
la guérison		cure
le régime nutritionel		di