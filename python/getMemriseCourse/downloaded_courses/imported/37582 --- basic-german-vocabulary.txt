fleißig, hard working; diligent 
arm, poor; needy
dumm, silly; stupid
ehrlich, honest; sincere
faul, lazy; rotten
freundlich, friendly; kind
egoistisch, egotistical; selfish
klug, wise; clever
lustig, fun; funny
nett, nice; cute
reich, rich; abundant
ruhig, quiet; calm
spannend, exciting; fascinating
treu, true; loyal; faithful
hässlich, nasty; ugly
vorsichtig, careful
unvorsichtig, careless
zuverlässig, reliable
unzuverlässig, unreliable
die Ferien, holidays
das Wetter, weather (no plural)
der Norden; -s, north
der Osten; -s, east
der Süden; -s, south
der Westen; -s, west
gigantisch, gigantic
nichts los, nothing is happening
deprimiert, depressed
Da ist richtig was los, things went off
schrecklich, horrible
nervig, annoying
wunderbar, wonderful
übernachten, to overnight
wahrscheinlich, probable; probably
scheußlich, abominable
kalt, cold
warm, warm
der Regen, rain (plural: rare)
regnen, to rain
Es regnet, it's raining
scheinen, 1. to seem to be; 2. to shine
Es ist heiß, it's hot
der Schnee; -s, snow
schneien, to snow
Es schneit, it's snowing
Es ist kalt, it's cold
trocknen, to dry
das Glück, good luck
das Pech; -e, bad luck
das Klima; -s, climate
der Treffpunkt; -e, meeting point
pünktlich, on time
hinauf, up; upstairs
das Eis; -, ice
ankommen, 1. to arrive; 2. to depend on (takes 'auf')
fantastisch, fantastic
steigen, to climb; to rise
das Gasthaus;  ̈-er, guesthouse
trinken, to drink
begrüßen, to greet
alles, everything
danach, afterwards
aufbleiben, to stay up
einpacken, to pack something
losgehen, leave; start (a game; an event)
passieren, to happen (2)
hinfallen, to fall down
wehtun, to hurt
die Idee; -n, idea
weitergehen, to continue
der Wunsch;  ̈-e, wish
keine Ahnung, no idea (idiom; no plural)
verraten, to reveal; betray
das Tal; - ̈er, valley
der Campingplatz; - ̈e , campsite
die Jugendgruppe; -n, youth group
die Bergtour; -en, mountain tour
der Rhythmus; -, rhythm
aufpassen, to watch out
das Erdgeschoss;-e, ground floor
der Plan;  ̈ -e, plan
hoffen, to hope
der glücklicher Zufall, the happy (lucky) circumstance (idiom; no plural)
werden, to become (helping verb; "will")
weil, because
helfen, to help
reich werden, to get rich
berühmt, famous
das Ausland; -"er, foreign country
der Profisportler; -, athlete
der Schauspieler; -, actor
das Reisen; -, travel
die Fremdsprache; -n, foreign language
der Mond; -e, moon
der Popsänger; -, pop singer
reisen, to travel
die Lehrerin; -nen / der Lehrer; -, teacher (female/male)
der Journalist; -en, journalist
der Touristikkaufmann; -"er, tourist agent (male)
die Touristikkauffrau; -en, tourist agent (female)
der Gärtner; -, gardener
der Zahnarzt; "e, dentist
der Friseur; -en, hairdresser
der Sekretär; -e, secretary (masc.)
der Zahn; -  ̈e, tooth
erklären, to explain
die Operation; -en, surgery
drinnen, inside
die Technik; -en, electrician
die Arbeit; -en, work
leicht, easy; light
filmen, to film
die Hausfrau; -en, housewife
der Chef; -s, boss
die Chefin; -nen, boss
das Praktikum; -s, internship
herzlich, warm; friendly; sincere
Herzlich willkommen!, A warm welcome!
das Betrieb; -e, business
zuschauen, to watch
mitarbeiten, to collaborate
allein, alone
sauber machen, to clean up
der Schalter; -, counter; switch
legen, to place
die Lehre; -n, apprenticeship; learning
die Krankengymnastin; -nen, physiotherapist
praktisch, practically
die Klassenarbeit; -en, test
die Präsentation; -en, presentation
der Kilometer; -, kilometer
Rad fahren, to cycle
das Ziel; -e, aim
die Priorität; -en, priority
die Hilfe; -n, help
die Deutschnote; -n, german grade
das Beispiel; -e, example
der Platz; -  ̈ e, place
der Ratschlag; -  ̈ e, advice
kaputt, broken
die Freundschaft; -en, friendship
warum, why
verstehen, to understand
die Deutscharbeit; -en, German test
dass, that
das Kompliment; -e, compliment
der Skaterpark; -s, skate park
Ich mag dich sehr, I like you a lot
diskutieren, to discuss
die Note; -n, grade
lieben, to love
gleich, same; equal; right away (as adv.)
unbedingt, unconditional; certainly (as adv.)
als, as; while; than (comparative)
sportlich, casual; sporty; sporting (fair)
die Popmusik; -en, pop music
die Klassik; -en, classical music
das Motorrad;  ̈-er, motorbike
die Frisur; -en, haircut
reden, to talk
nervös, nervous
der Rock; - ̈e, rock music
lösen, to solve
der Quatsch, nonsense (no plural)
anders, different
ander, other
eine andere, someone else (fem.)
bestimmt, determined; certainly (as adv.)
recht haben, to be right
die Leute, people
unehrlich, insince
