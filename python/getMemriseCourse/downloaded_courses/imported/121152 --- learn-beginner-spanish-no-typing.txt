hola, hello
Buenos dias., Good morning.
adiós, goodbye
sí, yes
gracias, thank you
disculpe, excuse me (get attention/forgiveness)
la persona, person
la ayuda, aid; help
mi, my
este, this
éste, this one
eso, that (neutral)
ese, that
allá, there
No sé, I don't know
yo soy, I am (permanent)
Yo soy americano., I am American. (p)
estoy, I am (temporary)
Estoy perdido., I am lost. (t)
Estoy buscando..., I'm looking for... (t)
No estoy listo., I'm not ready. (t)
No estoy seguro., I'm not sure. (t)
usted es, you are (permanent/formal)
Usted es amable., You are kind. (p/f)
es, it is (permanent)
Es importante., It is important. (p)
está, it is (temporary)
Está allá., It is over there. (t)
el baño, bathroom
por favor, please
una cerveza, a beer
un café, a coffee
el agua, water
el frío, cold
caliente, hot
el tiempo, time; weather
mucho/a, much; many; a lot
poco/a, a little; few
voy a, I am going
Voy allá., I am going there.
quiero, I want
Quiero éste., I want this one.
necesito, I need
Necesito ayuda., I need help.
Llegar a, to arrive/ to get to a place
tengo que, I have to
el precio, price; fee; charge
comprar, to buy
grande, big; large; grand; great
pequeño/a, small; little
barato/a, cheap; cheaply; trashy
caro/a, expensive
el supermercado, supermarket
otro/a, another; other
No lo quiero., I don't want it.
el mesero; la mesera, waiter/waitress
delicioso, delicious
beber, to drink
comer, to eat
el bistec, steak
el camarón, shrimp
las patatas bravas, spicy potatoes
una orden de, an order of
el vaso de agua, glass of water
la copa de vino, a glass of wine
Me llamo..., My name is...
No entiendo., I don't understand.
ya, already; by now
bien, well; good; ok; fine
mal, bad; wrong
bueno, good
malo, bad
todo, all; everything
aquí, here
¿Está?, Is it?
Sí; está., Yes; it is.
No está., It's not.
por qué, why
porque, because
qué pasa, what's going on
me mola, that’s cool; I find it cool
Lo pasé guay., It was great.
hostia, wow; oh no
Es la hostia., It's very cool.
mogollón, a lot
¡Vale!, Okay! Sure!
terrible, terrible
el tío, uncle
¡Qué va tío!, You're having a laugh!
Ese tío es de abrigo., You've got to watch (out for) that guy.
el español, Spanish
¿Qué es eso?, What's that?
No tengo..., I don't have...
¿Cuánto cuesta?, How much is it?
Te amo., I love you.
Está bien., It's okay.
¿Puedes?, Can you?
¿Dónde está...?, Where is...?
¿Tienes...?, Do you have...?
¿Hablas inglés?, Do you speak English?
el café, coffee
de nada, you're welcome
el hotel, hotel
la habitación, room
una noche, one night
doble, double
sencillo, simple
¿Puedo reservar un cuarto?, Can I reserve a room?
¿Cuántas noches?, how many  nights?
¿Para cuántas personas?, for how many people?
dos personas, two people
¿Aceptan tarjeta?, Do you take cards?
¿Cuál es el total?, What's the total?
Buenas noches, Good night
¿Cómo se dice … en español?, How do you say.... in Spanish?
no soy, I am not
¿Entiendes?, Do you understand?
yo entiendo, I understand
eres, are you
soy, I am
quieres, do you want
no quiero, I don't want
te gusta, do you like
me gusta, I like
no me gusta, I don´t like
¿De dónde eres?, Where are you from?
soy americano, I am American
soy español, I am Spanish
no soy americano, I am not American
sabes, do you know
sé, I know (a fact)
no lo sé, I don't know
¿A dónde vas?, Where are you going?
¿Cómo te llamas?, What's your name?
puedo, can I
se puede, it can be done
¿qué?, what?
¿cómo?, how?
no es, it is not
¿Quién eres?, Who are you?
¿Qué pasa?, What's happening?
no hay problema, no problem
¿Lo puedes hacer así?, Can you do it like this?
¿Puedes hablar más lento?, Can you speak slowly?
he estado, I have been
está roto, it is broken
¿Puedes ayudarme?, Can you help me?
me he quedado, I have stayed
¿Dónde estás?, Where are you?
llegaremos a, we'll arrive at
No ha llegado, It has not arrived
Lo he perdido, I've lost it
Voy a llegar tarde, I am going to be late
Llegó tarde, He was late
vamos, shall we go
necesitas, do you need
yo, I
tú, you (informal)
él; ella, he; she
nosotros, we
ellos, they (m)
seguido, consecutive
muchos, very many
pocos, very few
a veces, sometimes (at times)
por qué/ porque, why / because
aunque, although
mi, my
tu, your
su, his; her; your (formal); their; its
nuestro, our
me siento mal, I feel bad; I feel ill
el médico, doctor
roto, broken
rápidamente, quickly
la calefacción, heating
la farmacia, pharmacy
descansar, to rest
dormir, to sleep
¿Ya está arreglado?, Is it fixed?
demasiado frío, too cold
demasiado caliente, too hot
el aire acondicionado, air conditioning
es un gran problema, it's a big problem
demasiado tarde, too late
es muy importante, it's very important
la hija, daughter
la hermana, sister
la familia, family
la mujer, woman
la compañía, company
el empleado, employee
trabajar, to work
viaje de negocios, business trip
las vacaciones, holidays; vacation
el papá, dad
la mamá, mom
el hijo, son
el hermano, brother
mayor, greater; older
menor, minor; lesser; younger
el niño, child (boy)
el oficio, trade; job
hacer negocios, to do business
el jefe, boss
el familiar, relative
el hombre, man
la escuela, school
el hospital, hospital
el camino, track; road
la calle, street
la mesa, table
la mitad, half; middle
dentro, inside
diferente, different
antiguo, old
nuevo, new
la fiesta, party
el maestro, teacher
el presidente, president
el país, country; nation
la silla, chair
el teléfono, phone
el móvil, mobile phone
colorado, red
azul, blue
verde, green
amarillo, yellow
negro, black
blanco, white
lado izquierdo, left side
lado derecho, right side
arriba; encima, above; on top
abajo; debajo, below; under
muy grande, very big
muy pequeño, very small
mil, thousand
cuatro, four
cinco, five
seis, six
siete, seven
ocho, eight
nueve, nine
diez, ten
once, eleven
doce, twelve
trece, thirteen
catorce, fourteen
quince, fifteen
diecisiete, seventeen
dieciocho, eighteen
diecinueve, nineteen
veinte, twenty
treinta, thirty
cuarenta, forty
cincuenta, fifty
sesenta, sixty
setenta, seventy
ochenta, eighty
noventa, ninety
cien, one hundred
cero, zero
dieciséis, sixteen
sabe mal, it tastes bad
dame un, give me (one; two...)
picoso, spicy; hot
sin chile, without chilli
el restaurante, restaurant
invitar, to invite; to treat
las enchiladas, typical mexican dish
la quesadilla, typical mexican snack
la tortilla, spanish omelet
el arroz, rice
¿Puedo ordenar?, Can I order?
la servilleta, napkin
el tenedor, fork
Tengo sed, I'm thirsty
Tengo hambre, I'm hungry
rico, rich; tasty; yummy
soy vegetariano, I'm vegetarian
la cuenta por favor, the bill please
tomar, to take; to have (something to eat or drink)
vamos por unos tragos, let's go for a few drinks
mesa para dos, table for two
la salsa, sauce; salsa
otro por favor, another please
el menú, menu
el plato, plate
Quiero comprar, I want to buy
el vidrio, piece of glass
es muy caro, it's too expensive
el tianguis, street market
el mercado, market
la tienda, shop
como éste, like this one
más grande, bigger
más pequeño, smaller
¿Me puedes decir…?, Can you tell me...?
¿Qué es esto?, What's this?
¿Cuánto es lo menos?, What's the lowest price?
Sólo estoy viendo., I'm just looking.
la tiendita, corner shop
su cambio, your change (formal)
¡Habla serio!, Be serious!
espera un momento, wait a minute
ahorita, right now
parece, he seems; she seems; you (formal) seem
hermoso, beautiful
olvídalo, forget it
divertido, entertaining
apúrate, hurry up
más o menos, more or less
¡Oye!, Hey!
creo; siento, I think; I feel
la buena idea, good idea
de hecho, actually; in fact
feo, ugly
en serio, seriously; really
¿Qué onda?, What's up?
que buena onda, way cool; that's great
chistoso, funny; comical
fresco, cool
¡Chale!, Oh no! (informal)
¡Órale!, Wow!  OK!
ni modo, oh well
ahí para la otra, next time
sale, alright then
Qué mala onda., That's terrible.
estúpido, stupid
¡Hostia!, oh no; damn it!
es la leche, that's great
¡me cago en la leche!, damn it!
gracioso, funny; graceful
gilipollas, dickhead; really stupid
¿Qué pasa tío?, What's up? (m)
¿a qué hora?, at what time?
antes, before
el carro, car
el autobús, bus
el metro, metro; subway
el avión, airplane
la estación, station
el vuelo, flight
¿Cuándo?, When?
salimos, we leave
llegamos, we arrive
a la 1, at 1 o'clock
tardío/a, later
hoy, today
después, after
una hora, one hour
todavía no ha llegado, still hasn't arrived
todo recto, straight ahead
dar vuelta a la izquierda, to turn left
dar vuelta a la derecha, to turn right
pare aquí, stop here
retrasado, delayed
está bueno, it is good
dame, give me
la comida, food
la bebida, drink
el platillo, the dish
el vaso, glass; vessel
los cubiertos, cutlery
la cuenta, count; account; bill
la cotización, price
¿Puedes decirme?, Can you tell me?
¿Qué es eso?, What's that?
¿Cuánto cuesta?, How much is it?
el efectivo, cash
la compra, purchase; shopping
¿Cuando?, When?
llego, I arrive
me voy, I go; I leave
la tarde, afternoon
¿__________ llegas?, A qué hora
Llego por ______________, la mañana
_____________ por la tarde, Me voy
Voy a tomar ____________, el autobús
____________ el metro, Voy a tomar
__________ al aeropuerto, Llego
¿___________ la estación?, Dónde está
¿Dónde está _____________?, el automóvil
¿Quieres...?, Do you want...?
¿Te gusta...?, Do you like...?
¿Conoces...?, Do you know...?
sí conozco, yes; I know (a person)
no conozco, I don't know (a person)
¿Cómo te llamas?, What is your name? [informal]
que, that (referring to subject not predicate)
cómo, how
la boleta, ticket
bonito/a, pretty
muy, very; highly
estoy, I am (location or state of health)
voy, I go
tengo, I have
Hasta luego., See you later.
¿Cómo estás?, How are you?
¿Qué tal?, How is it going?
Muy bien; gracias, Very good; thanks
Buenos días, Good morning
Buenas tardes, Good afternoon
Buenas noches, Good night
¿Cómo se llama usted?, What is your name? [Formal]
Me llamo (nombre)., My name is (name).
Yo soy (nombre)., I am (name).
Un placer conocerte., A pleasure to meet you.
¡Mucho gusto!, I'm pleased to meet you!
encantado, delighted
bienvenido, welcome (singular)
bienvenidos, welcome (plural)
los Estados Unidos, USA
China, China
España, Spain
¿De dónde eres?, Where are you from?
Yo soy de (país)., I am from (country).
Hablo un poco de español., I speak a little Spanish.
Perdón / Lo siento, Excuse me! Sorry!
ciertamente, certainly
¡Muchas gracias!, Thank you very much!
¡Salud!, Bless you! Cheers!
con permiso, excuse me (when walking past someone)
buen provecho, enjoy your food
No se preocupe., Don't worry about it.
usted, you (formal)
si es posible, if possible
muy sabroso, very tasty; delicious
un momento, one moment
¿En qué le puedo ayudar?, How can I help you?
por supuesto, of course
morado, purple
la naranja, orange
gris, gray
rosa, pink
marrón, brown
el otoño, autumn
el invierno, winter
la primavera, spring
el verano, summer
¿De qué color es...?, What color is...?
La grama es verde., The grass is green.
El cielo es azul., The sky is blue.
La nieve es blanca., The snow is white.
El sol es amarillo., The sun is yellow.
La manzana es roja., The apple is red.
Las flores son de color púrpura., The flowers are purple.
El monstruo es color naranja., The monster is orange.
El gato es gris., The cat is gray.
El carro es rosado., The car is pink.
El caballo es marrón., The horse is brown.
¿Cuántos años tienes?, How old are you? (years have you)
Tengo (edad) años., I am (age) years old.
veintidós, twenty-two
veintiuno, twenty-one
treinta y uno, thirty-one
cuarentidós (cuarenta y dos), forty-two
cincuentitrés (cincuenta y tres), fifty-three
doscientos, two hundred
trescientos, three hundred
cuatrocientos, four hundred
quinientos, five hundred
seiscientos, six hundred
setecientos, seven hundred
ochocientos, eight hundred
novecientos, nine hundred
el millón, million
¿Quién?, Who?
¿Cuál?, What? Which one?
¿Cuánto?, How much?
¿Dónde?, Where?
¿Cómo se llama él?, What's his name?
¿Cómo se llama ella?, What's her name?
Se llama..., His (her) name is...
¿De dónde es él?, Where is he from?
¿De dónde es ella?, Where is she from?
Él es de..., He is from...
Ella es de..., She is from...
¿A dónde vive?, Where does he (she) live?
Vive en..., He/She lives in...
¿Puede repetir? (formal), Can you please repeat?
¿Cómo se dice....en español?, How do you say.... in Spanish?
¿Verdad?, True?
¡Estupendo!, Great!
¿Estudias o trabajas?, Do you work or study?
¿Eres estudiante?, Are you a student?
Sí; soy estudiante., Yes; I am a student.
¿Hablas español?, Do you speak Spanish?
¿Vienes?, Are you coming?
¿Cómo se llama tu compañero?, What is your companion's name?
¿Cuál es tu código postal?, What is your zip code?
tal vez, maybe; perhaps
cerca, close (nearby)
lejos, far
arriba, up
abajo, down
afuera, outside
la carretera, road
la vereda, path
delante, ahead
enfrente, in front
detrás, behind
el norte, north
el sur, south
el este, east
el oeste, west
¿Dónde está el baño?, Where is the bathroom?
¿Dónde está el hotel?, Where is the hotel?
¿Estás listo para ir?, Are you ready to go?
la hora, hour
el minuto, minute
segundo, second
pronto/a, soon; quick; prompt
el año, year
el lunes, Monday
el martes, Tuesday
el miércoles, Wednesday
el jueves, Thursday
el viernes, Friday
el sábado, Saturday
el domingo, Sunday
enero, January
febrero, February
marzo, March
abril, April
mayo, May
junio, June
julio, July
agosto, August
septiembre, September
octubre, October
noviembre, November
diciembre, December
la fecha, date (calendar)
medianoche, midnight
A qué hora?, At what time?
¿Qué hora es?, What time is it?
Son las nueve de la mañana., It's nine in the morning.
Es mediodía., It's noon.
Es la una de la tarde., It's one in the afternoon.
Son las dos de la tarde., It's two in the afternoon.
Son las diez de la noche., It's ten in the evening.
Son las cinco y media., It's five thirty.
Son las ocho y cuarto., It is 8:15.
tengo dolor, I feel pain.
me siento enfermo(a), I feel sick.
me duele, it hurts (single part of your body)
la cabeza, head
el corazón, heart
el estómago, stomach
la garganta, throat
la espalda, back
la nariz, nose
el pie, foot
el cuello, neck; collar
la cintura, waist
la rodilla, knee
el pecho, chest (on body)
el codo, elbow
me duelen, it hurts (plural for arms; legs; etc.)
los pies, feet
las manos, hands
los ojos, eyes
las orejas, ears
los brazos, arms
las piernas, legs
los dedos, fingers
el dinero, money
el dólar, dollar
el euro, euro
los centavos, cents; pennies
la moneda, coin; currency
pagar, to pay
aquello/a, that
el helado, ice cream
el pan, bread
la carne, meat
la carne de res; carne de vaca, beef
el pollo, chicken
el cerdo, pork
los peces, fish
Tengo sed., I am thirsty.
el jugo; el zumo, juice
la leche, milk
el té, tea
la madre, mother
el padre, father
el tío, uncle
la tía, aunt
las damas, ladies
los caballeros, gentlemen
la señorita, Miss
la niña, female child
la esposa, wife
el esposo, husband
el propietario, owner
el muchacho, boy; young man
la muchacha, girl; young woman
el primo, cousin (male)
la prima, cousin (female)
la amiga, friend (female)
el amigo, friend (male)
la gente, people
la pareja, the couple
mi pareja, my partner
mi cónyuge, my spouse
el ahijado; la ahijada, godson; goddaughter
el sobrino; la sobrina, nephew; niece
la madrastra, stepmother
el padrastro, stepfather
el suegro; la suegra, father-in-law; mother-in-law
el nieto; la nieta, grandson; granddaugher
la nuera, daughter-in-law
el yerno, son-in-law
el hijastro; la hijastra, stepson; stepdaughter
un conocido; una conocida, an acquaintance
el cuñado; la cuñada, brother-in-law; sister-in-law
el abuelo, grandfather
la abuela, grandmother
el piloto, pilot
dependiente, shop assistant; sales clerk
bailarín, dancer
abogado, lawyer
profesor, professor
vendedor, seller
bombero, firefighter
contable, accountant
policía, police officer
recepcionista, receptionist
disenador/ora, designer
dentista, dentist
periodista, journalist
cantante, singer
arquitecto, architect
traductor, translator
pintor, painter
cartero, postman; letter carrier
taxista, taxi/cab driver
cocinero, cook
actor, actor
mago, magician
guitarrista, guitarist
ganadero, rancher
lingüista, linguist
administrativo, office worker; white collar worker
peluquero, hairdresser
psicólogo, psychologist
el tren, train
el camión, truck
la motocicleta, motorcycle
la bicicleta, bicycle
la embarcación, boat
el helicóptero, helicopter
la ambulancia, ambulance
la vela, candle; sail
el autobús escolar, school bus
tuyo, yours (informal masculine singular)
tuya, yours (informal feminine singular)
tuyos, yours (informal masculine plural)
tuyas, yours (informal feminine plural)
suyo, yours (formal masculine singular)
suya, yours (formal feminine singular)
suyos, yours (formal masculine plural)
suyas, yours (formal feminine plural)
nuestra, ours (feminine singular)
nuestros, ours (masculine plural)
nuestras, ours (feminine plural)
vuestros, you all's (masculine plural)
vuestro, you all's (masculine singular)
vuestra, you all's (feminine singular)
vuestras, you all's (feminine plural)
me, me (indirect object pronoun)
te, you  (indirect object pronoun; informal)
le, him; her (indirect object pronoun; formal)
nos, us  (indirect object pronoun)
os, you-all  (indirect object pronoun; informal)
les, them (informal); you-all (formal) (indirect object pronoun)
la clase, class
el papel, paper
el calor, heat
la palabra, word
la casa, house
el perro, dog
el gato, cat
universitario/a, university
el amor, love
la guitarra, guitar
el buque, ship
el pájaro, bird
la película, film; movie
la luz, light
el estudiante, student
una habitacion, a room
la tarea, task; chore
lleno, full; filled up (not pleno)
cerrado, closed
abierto, open
caminar, to walk
correr, to run
hablar, to speak; talk
tener, to have
el reclamo, call; claim
entender, to understand
bañar, to bathe
querer, to want
la danza, dance
estudiar, to study
jugar, to play
llegar, to arrive
gustar, to like
nadar, to swim
sentir, to feel
¡Vamos!, Let's go!
saber, to know (facts)
conocer, to know (a person; recognize)
despacio, slowly
siempre, always
mejor, better
peor, worse
algunas veces, sometimes
nunca, never
a menudo, often
todos, every
bastante, quite
demasiado, too
ampliamente, amply; widely;easily
silenciosamente, quietly
quizás, mayb
