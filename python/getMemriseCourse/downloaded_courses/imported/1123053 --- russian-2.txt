нужен		needed (have to ... ; must; should); necessary
помощь		help
мне нужна помощь		I need help
сказать		to say
как сказать ... по-русски?		how do you say ... in Russian?
я не понимаю		I don't understand
ты понимаешь?		do you understand?
знать		to know
я не знаю		I don't know
я тоже		me too
я тоже нет		me neither
познакомиться		to meet (each other for the first time)
приятно		pleasing; nice
приятно познакомиться		nice to meet you
отлично!		great!
вот		here; now; this
вот, пожалуйста!		here you go
здоровье		health
на здоровье		you're welcome
удача		luck
удачи!		good luck!
Англия		England
Соединённые Штаты		the United States
откуда		where (from)
из		from
откуда ты?		where are you from?
я из Англии		I'm from England
ты из Англии?		are you from England?
я из Соединённых Штатов		I'm from the United States
говорить		to speak; to talk
немного		a little
ты говоришь по-русски?		do you speak Russian?
я говорю по-английски		I speak English
я немного говорю по-русски		I speak a little Russian
конечно!		of course!
конечно, я говорю по-английски!		of course I speak English!
ты очень хорошо говоришь по-русски		you speak Russian very well
тарелка		plate
стакан		(a) glass
бокал		wine glass
чашка		cup
бутылка		bottle
кофе латте		latte
закуска		starter
основной		basic; main; major
блюдо		course; dish
основное блюдо		main course
десерт		dessert
мне хотелось ...		I wanted (for myself) ...
я бы		I would
мне хотелось бы ...		I would like (for myself) ...
что-нибудь		something; anything
ничего		nothing; anything
или		or
что вы хотите?		what would you like?
принесите мне ...		bring me ... (plural; formal)
принесите мне чашку чая, пожалуйста		I would like a cup of tea, please
что вы хотите есть?		what would you like to eat?
хотите бутылку?		would you like a bottle?
вы бы хотели бутылку или бокал?		would you like a bottle or a (wine) glass?
принесите мне молока, пожалуйста		I would like some milk, please
у меня нет		I don't have
у вас есть десерты?		do you have any desserts?
у меня ничего нет		I don't have anything
у меня нет воды		I don't have any water
конечно, у меня есть вода		of course I have some water
одиннадцать		eleven; 11
двенадцать		twelve; 12
тринадцать		thirteen; 13
четырнадцать		fourteen; 14
пятнадцать		fifteen; 15
шестнадцать		sixteen; 16
семнадцать		seventeen; 17
восемнадцать		eighteen; 18
девятнадцать		nineteen; 19
двадцать		twenty; 20
рынок		market
банкомат		ATM
аптека		pharmacy
киоск		kiosk
люди		people
магазин		shop; store
супермаркет		supermarket
машина		car; machine
такси		taxi
банк		bank
книга		book
книжный магазин		bookshop
клиент		client
покупатель; покупательница		customer
продать		to sell
купить		to buy
платить		to pay
покупка		purchase
ходить		to go; to walk
за		for; behind
ходить за покупками		to go shopping
там		there
тут		here
быть		to be
есть		(there) is; (there) are; am
много		many; much; a lot of ...
для меня		for me
для тебя		for you
ли		emphatic question marker
пойдём за покупками		let's go shopping
пойдём на рынок		let's go to the market
я хочу купить что-нибудь для тебя		I want to buy something for you
это для тебя		it's for you
это для меня?		is it for me?
тут большая аптека		there is a big pharmacy here
тут много людей		there are many people here
есть ли тут банкомат?		is there an ATM here?
тут нет банков		there are no banks here
тут много маленьких книжных магазинов		there are many small bookshops here
клиент всегда прав		the customer is always right
напиток		drink
любимый		favourite
этот (эта; это)		this (one); that (one); it
тот (та; то)		that; it
весь (вся; всё; все)		all
любой		all; every; anyone
вообще-то		actually
ему		him; to him
ему нравится кофе		he likes coffee
ему не нравится рыба		he doesn't like fish
ей не нравятся чай и кофе		she doesn't like tea or coffee
еды нет		there is no food
эти мои любимые		they're my favourite
кофе - мой любимый напиток		coffee is my favourite drink
вообще-то мне это нравится		I actually like it
что это?		what's this?
конечно, ему нравится салат		of course he likes salad
семья		family
мама		mum
папа		dad
родитель		parent
сестра		sister
брат		brother
дедушка		grandpa
бабушка		grandma
мальчик		boy
сын		son
девочка		girl
дочь		daughter
друг; подруга		friend (male; female)
молодой человек		boyfriend; young man
девушка		girlfriend; young lady
человек		man; human; person
ребёнок		child; kid
дети		children; kids
малыш; малышка		baby
взрослый; взрослая		adult
мужчина		man
муж		husband
женщина		woman
жена		wife
работа		job; work
школа		school
офис		office
работать		to work
кто		who (questions)
в		in; to (somewhere)
кто они?		who are they?
это моя мама		this is my mum
он мой брат		he's my brother
это мои мама и папа		this is my mum and dad
это не моя дочь		that isn't my daughter
это мой друг из Соединённых Штатов		this is my friend from the United States
у него есть девушка?		does he have a girlfriend?
у твоего молодого человека есть работа?		does your boyfriend have a job?
он работает в офисе		he works in an office
цвет		colour
красный		red
синий		blue
жёлтый		yellow
зелёный		green
чёрный		black
розовый		pink
фиолетовый		purple
белый		white
оранжевый		orange
коричневый		brown
серый		grey
тёмный		dark
тёмно-красный		dark red
светлый		light (colour)
светло-зелёный		light green
голубой		light blue
извините меня		excuse me; forgive me
где ... ?		where (is) ... ?
больница		hospital
где больница?		where is the hospital?
вот она		here it is
чуть-чуть		just a little
что ты делаешь?		what are you doing?
возможно		possible; probable; perhaps (might)
это возможно		it's possible
возможно ли это?		is it possible?
невозможно		impossible; improbable
это невозможно!		it's impossible!
жаль		pity; shame
как жаль		what a shame
одежда		clothes
брюки		trousers
рубашка		shirt
футболка		T-shirt
юбка		skirt
платье		dress
шорты		shorts
пальто		coat
пиджак		jacket
свитер		jumper
шарф		scarf
шапка		hat (to keep you warm)
шляпа		hat (for style)
костюм		suit
носки		socks
обувь		shoes
тапки		slippers
ботинки		boots
кроссовки		trainers
перчатки		gloves
трусы		pants (underwear)
зонт		umbrella
бумажник		wallet
сумка		bag
пописать (поПИсать)		to pee
писать (пиСАТЬ)		to write
новый		new
старый		old
закрытый		closed
открытый		open
ты хочешь новую рубашку?		do you want a new shirt?
он хочет синие брюки		he wants the blue trousers
что ты хочешь купить?		what do you want to buy?
я хочу купить одежду		I want to buy some clothes
банк открыт		the bank is open
мне нужно купить зонт		I have to buy an umbrella
мне нужно поПИсать		I have to pee
орехи		nuts
конфеты		sweets
шоколад		chocolate
торт		cake
пицца		pizza
сахар		sugar
соль		salt
перец		pepper
вегетарианец; вегетарианка		vegetarian (person)
вегетарианский		vegetarian (food)
острый		spicy; sharp
горячий		hot
холодный		cold
зависимый		addicted; dependant
аллергия		allergy
я вегетарианец		I'm a vegetarian (said by a man)
он вегетарианец?		is he a vegetarian?
он вегетарианский?		is it vegetarian?
тебе нравятся конфеты?		do you like sweets?
суп горячий?		is the soup hot?
у него аллергия на орехи		he's allergic to nuts
животное		animal
собака		dog
кошка		cat
птица		bird
корова		cow
свинья		pig
кролик		rabbit
змея		snake
лев		lion
слон		elephant
лошадь		horse (mare)
конь		horse (stallion)
овца		sheep
обезьяна		monkey; ape
щенок		puppy
котёнок		kitten
жираф		giraffe
осёл		donkey
мышь		mouse
преданный		loyal
смелый		brave
умный		clever; smart; intelligent
глупый		stupid; silly; foolish
свободный		free; available
высокий		tall; high
низкий		low
тихий		quiet
симпатичный		cute
предан как собака		loyal as a dog
свободен как птица		free as a bird
день		day; afternoon
неделя		week
месяц		month
год		year
лет		years
молодой		young
сколько		how many; how much; how long
это моя собака		this is my dog
сколько ей лет?		how old is she?
ей двенадцать лет		she's twelve years old
моя кошка очень старая		my cat is very old
ты очень молодой		you're very young
квартира		flat (property)
дом		house; home
Москва		Moscow
Санкт-Петербург		Saint Petersburg
Лондон		London
Нью-Йорк		New York
Вашингтон		Washington D.C.
жить		to live
всегда		always
никогда		never
почему		why
потому что ...		because ...
такой		so; such
они живут в Санкт-Петербурге		they live in Saint Petersburg
у неё дом в Москве		she has a house in Moscow
ты всегда счастлив		you're always happy
несчастный		unhappy
он всегда несчастен		he's always unhappy
почему ты такой грустный?		why are you so sad?
... потому что ты болен		... because you're sick
я злой, потому что голоден		I'm angry because I'm hungry (said by a man)
он немного грустный, потому что у него нет подруги		he's a little sad because he doesn't have a friend
тридцать		thirty; 30
сорок		forty; 40
пятьдесят		fifty; 50
шестьдесят		sixty;60
семьдесят		seventy; 70
восемьдесят		eighty;80
девяносто		ninety; 90
сто		one hundred; 100
двадцать четыре		twenty-four; 24
сорок шесть		forty-six; 46
триста		three hundred; 300
время		time
час		hour; o'clock
минута		minute
секунда		second
встреча		appointment; meeting
приём		appointment (with a specialist); reception; approach
свидание		date (romantic meeting)
который		which
когда		when
который час?		what time is it?
сейчас		now
без		without
сейчас два часа		it's two o'clock
сейчас восемь тридцать		it's eight thirty
сейчас двадцать минут восьмого		it's twenty minutes past seven
сейчас без десяти два		it's ten to two
сейчас шесть минут шестого		it's six minutes past five
когда у тебя свидание?		when is your date?
во сколько?		at what time?
без десяти четыре		at ten to four
в час		at one o'clock
в два часа		at two o'clock
береги себя		take care
иметь		to own; to possess (to have)
вид		view; sight; kind; type
иметь в виду		to mean
понимаешь, что я имею в виду?		do you know what I mean?
случиться		to happen
что случилось?		what happened?
проблема		problem
без проблем		no problem
волноваться		to worry (oneself); to get (oneself) worried
не волнуйся		don't worry; never mind
против		against
я не против		I don't mind
ой!		oops!
о, нет!		oh no!
именно!		indeed!
поздравляю!		congratulations!
ура!		ya