el saludo, greeting
hola, hello
bienvenido, welcome
buenas tardes, good evening
bien, well
qué gusto, how great
adiós, goodbye
toparse con, to run into
hasta pronto, see you soon
en orden, Okay
perdón, pardon
encantado, pleased to meet you (e)
Hasta Mañana, See you tomorrow.
presentar, to introduce
buenos días, good morning
Buenas noches, Goodnight
¿Cómo estás?, How are you?
el permiso, permission
no, no
poder, to be able to
por favor, please
gracias, thank you
la prohibición, prohibition
estricto, strict (e)
detener, to detain
dejar, to leave; abandon; quit
la sensación, sensation; feeling
el placer, pleasure (p...)
el hambre, hunger
la sed, thirst
el dolor, pain; sorrow
la sorpresa, surprise
nervioso, nervous
cansado, tired
el miedo, fear (m...)
aburrirse, get bored
la bebida, drink
el agua, water
el agua mineral, mineral water
el zumo, juice
el té, tea
el café, coffee
el vino, wine
la cerveza, beer
beber, to drink
la leche, milk
la limonada, lemonade
el número, number
cero, zero
uno, one
dos, two
tres, three
cuatro, four
cinco, five
seis, six
siete, seven
ocho, eight
nueve, nine
diez, ten
once, eleven
doce, twelve
la docena, dozen
contar, to count; to tell
catorce, fourteen
quince, fifteen
dieciséis, sixteen
diecisiete, seventeen
dieciocho, eighteen
diecinueve, nineteen
veinte, twenty
el reloj, clock
la hora, hour
el minuto, minute
el segundo, (the) second
la media hora, half an hour
ahora, now
luego, later
pronto, soon
esperar, to expect; to wait; to hope
la cantidad, quantity; amount
cuánto, how much
grande, big; large; grand; great
pequeño, small
más, more
menos, minus
el litro, litre
la botella, bottle
el pedazo, piece; chunk; morsel (p...)
medir, to measure
el color, colour; color
azul, blue
rojo, red
verde, green
amarillo, yellow
marrón, brown
negro, black
blanco, white
claro, light (colour); clear
oscuro, dark
el calvo, bald
el pelo, hair (body)
rizado, curly
la barba, beard
la planta baja, the ground floor
el primer piso, the first floor
el ascensor, elevator
la escalera, stairs
la pared, wall
totalmente, totally
totalmente equipado, totally (fully) equiped
vender, to sell
compartir, to share
compartir piso, to share a flat
mudarse, to move house
la cabeza, head
la piel, skin
Los ojos, eyes
las orejas, ears
la boca, mouth
los dientes, teeth
el cuello, neck; collar
la espalda, back [anatomy]
el hombro, shoulder
la mano, hand
la uña, fingernail; toenail
el codo, elbow
el estómago, stomach
la pierna, leg
la rodilla, knee
barrer el suelo, to sweep the floor
fregar el suelo, to mop the floor
limpiar, to clean
limpiar el polvo, to dust (to clean the dust)
el trapo, cloth
pasar la aspiradora, to vacuum clean
hacer, to do
hacer la cama, to make the bed
hacer la colada, to do the washing
el detergente, detergent
el suavizante, the softener
tender, to hang out; to spread
tender la ropa, to hang the washing
planchar la ropa, To iron the clothes
hacer la compra, to do the shopping
hacer la comida, to make the food
poner, to put
poner la mesa, to set the table
fregar los platos, to wash the dishes
el lavavajillas, the dishwasher
poner el lavavajillas, to load the dishwasher
el arroz, rice
el pollo, chicken
¿cuánto cuesta todo?, how much does it all cost?
la cebolla, onion
el tomate, tomato
la papa/patata, potatoes
la carne, meat
tomar, to take; to have (something to eat or to drink)
la cuenta, count; account; bill
el plato, dish; plate
la ensalada, the salad
la comida, food
el billete, bill; ticket; fare
bien también, also well
el amigo, friend
la dirección de correo electrónico, the email address
el número de teléfono, telephone number
la parada, stop
la aceituna, olive
¿dónde vives?, where do you live? (informal)
estoy libre, I am free
un momento, One moment
el pepino, cucumber
la lechuga, lettuce
la vía, route; way
la espinaca, spinach
la frambuesa, raspberry
el rábano, radish
la estación, station
la siguiente estación, the next station
la derecha, right
la izquierda, left
recto; derecho, straight
la esquina, corner (not rincón)
el nieto, grandson
el pasajero, passenger
ir de pie, to stand
buen viaje, have a good trip (bon voyage)
mejor, better
la calabaza, pumpkin
masticar, to chew
la zanahoria, carrot
el nabo, turnip
nutritivo, nutritious
el pimiento, pepper; capsicum
fresco, cool
la manzana, apple
el limón, lemon
la fruta, fruit
el plátano, banana (p...)
el abuelo, grandfather
el primo, cousin
El andén, the platform
una copa de vino, a glass of wine
ácido, sour
nada más?, anything else?
el esposo, husband
la esposa, wife
el hermano, brother
la hermana, sister
la familia, family
aquí, here
con, with
el cantante, singer
famoso, famous
correcto, correct
el fin de semana, weekend
dar, to give
el concierto, concert
la ciudad, city
la banda, band
el lobo, wolf
el tipo, type; kind; guy
todo, all
la tienda, shop; store
Qué más?, What else?
espectacular, spectacular
el abogado, lawyer
el periodista, journalist
lento, slow
divertido, entertaining (d)
aburrido, bored; boring
el hotel, hotel
el museo, museum
el banco, bank
internacional, international
el tren, train
el avión, airplane
la revista, magazine
hasta la próxima, until next time
Permiso, Excuse me (p)
el baño, bathroom
el baño público, public toilet/bathroom
la cuadra, block; in a neighborhood
lo siento, I am sorry
no entiendo, I don't understand
despacio, slowly (d)
la calle, street
allí, there
doblar, to double; to fold; to bend; to turn
al final, at the end
seguir, to follow; to continue (s...)
la avenida, avenue
tomar el sol, to sunbathe
la playa, beach
nadar, to swim
qué bien, great; how good!
tal vez, maybe (sometime)
chévere, cool
contigo, with you
ayudar, to help
el tour, tour
la actividad, activity
pescar, to fish
el pez, fish [living; not food]
la pecera, tank (fish tank)
la iglesia, church
visitar, to visit
listo, clever; ready
el casado, married person
el soltero, single person
acampar, to camp
bucear, to scuba-dive
¿estás listo?, are you ready?
el vuelo, flight
el mismo, the same
salir, to leave; to go out
temprano, early
el aeropuerto, airport
tarde, late
suficiente, sufficient; enough (s..)
todavía, still (yet)
disculpe, excuse me (d)
amable, kind
muy, very; highly
De nada, you are welcome
la ayuda, help
con qué?, with what?
algo, something
alguien, someone
para, to; for
el azúcar, sugar
ambos, both
ayúdame, help me
el gusto es mío, the pleasure is mine
sólo un poco, only a little
trabajar, to work
la compañía, company
la noche, night
ver, to see
leer, to read
el libro, book
el viernes, Friday
el sábado, Saturday
el lunes, Monday
el miércoles, Wednesday
el, the (masc)
el martes, Tuesday
la empresa, company; enterprise
por, by; for
el animal doméstico, the pet
el/la perro/a, dog
el gato, cat
el pájaro, bird (not ave)
bonito, pretty
hermoso, beautiful
guapo, good-looking
la voz, voice
las voces, voices
el mar, sea
el marisco, seafood
el cielo, sky
el río, river
el sitio, site; place
el lugar, place
el primero, first
el último, last one
el elefante, elephant
el balón, ball; balloon
jugar, to play
el juego, game; play
el campo, field; meadow
El deporte, The sport
el pasillo, hallway; corridor (p...)
estrecho, narrow; tight
alto, tall; high
bajo, short
enorme, huge; enormous
el vestido, dress
sobre, on; about
el suelo, ground; floor
el sueño, dream
orar, to pray
además, in addition; besides; furthermore
el camello, camel
la montaña, mountain (not monte)
el traje, suit
la cocina, kitchen
el comedor, dining room
la habitación, room (h)
el jardín, garden
el  árbol, tree
la flor, flower
el garaje, the garage
otro, other
después, after
antes, before
inmediatamente, immediately
porque, because
el insecto, insect
venir, to come
lavar, to wash
treinta, thirty
trece, thirteen
cuarenta, forty
sesenta, sixty
cincuenta, fifty
setenta, seventy
ochenta, eighty
noventa, ninety
cien, one hundred
treinta y uno, thirty-one
la bicicleta, bicycle
la casa, house
la clínica, clinic
la taza, cup; mug
el cantante famoso, the famous singer
la jirafa, the giraffe
la silla, chair
enfadado(a), angry
la agenda, agenda
avergonzado(a), ashamed
al final de calle, at the end of the street
A qué hora?, At what time?
la tarta, the cake
el baloncesto, basketball
rubio, blonde
el barco, boat; ship
la caja, box
dentro, inside
brillante, bright; shiny; sparkling;brilliant
preocupado, worried
ocupado, busy
pero, but
chao, bye
nadie, nobody
el coche, car
la ropa, clothes
el abrigo, coat
seguro, safe; certain
confundido, confused
enhorabuena, congratulations
la corona, crown
sordo, deaf
el edificio, building
decepcionado, disappointed
punto, dot; full stop
octavo, eighth
el ingeniero, engineer
Inglaterra, England
inglés, English
excelente, excellent
excepto, except
emocionado, excited
gordo, fat
harto, fed up
quinto, fifth
cuarto, fourth
francés, French
lleno, full; filled up (not pleno)
alemán, German
contento, happy (c..)
dorado, gold; golden (color)
buena idea, good idea
gris, gray/grey
la media naranja, other half (as in partner)
feliz, happy (f..)
él, he
pesado, heavy
aquí están, here they are
acá, here; over here (not aquí)
caliente, hot
cien mil, one hundred thousand (100;000)
lo, it (l)
qué tal?, how are you? (informal)
en qué le puedo ayudar?, how can I help you?
cuántos?, how many? how much?
el estudiante, student
estoy Aquí, I am here
perdido, lost
el desempleo, unemployment
desempleado, unemployed
enfermo, sick
inteligente, intelligent
intenso, intense
interesar, to interest
interesante, interesting
Eso es todo?, Is that all?
enero, January
veamos, Let's see
lógico, logical
mira, look
casado, married
el metro, metro; subway
el bigote, mustache
el azul marino, navy blue
noveno, ninth
ruidoso, noisy
tú no?, not you?
por supuesto, of course
el anciano, elderly person
en punto, on the dot; exactly; sharp
un millón, one million
mil, thousand
el zumo de naranja, orange juice
la naranja, orange
rosa, pink
el poder, power
orgulloso, proud
el transporte público, public transportation
pelirrojo, red-haired
el bocadillo, sandwich
asustado, frightened
hasta luego, see you later
séptimo, seventh
corto, short
plateado, silver
sexto, sixth
más o menos, so and so
los calcetines, socks
algún, any; some
liso, straight
largo, long
diez mil, ten thousand
el diez, the tenth
agosto, August
décimo, tenth
qué lástima, that's a pity
el tobillo, the ankle
el arquitecto, architect
el brazo, arm
la tía, aunt
la maleta, suitcase
la bolsa, bag
la cama, bed
el chico, boy
el novio, boyfriend
el albañil, the builder
el pastel, cake
el funcionario, functionary
el director, director
el médico, doctor
la puerta, door
el conductor, driver
el huevo, egg
el empresario, businessman
la cara, face
el agricultor, the farmer
el padre, father
la madre, mother
el bombero, fireman
el pescador, fisherman
el auxiliar de vuelo, flight attendant
el auxilio, help; assistance
el pie, foot
la chica, girl (c..)
la novia, girlfriend
las gafas, glasses; spectacles
la abuela, grandmother
el grupo, group
el sombrero, hat
el hospital, hospital
el ama de casa, the housewife
el juez, judge
el idioma, language
el ordenador, computer (not computadora)
el hombre, man
la mujer, woman
el mecánico, the mechanic
el teléfono, phone
el teléfono móvil, the mobile phone
la mañana, morning; tomorrow
la música, music
el vecino, neighbor
el sobrino, nephew
la sobrina, niece
el periódico, newspaper
la próxima semana, the next week
el próximo, next; near (p)
la nariz, nose
el enfermero, nurse
la oficina, office
los opciones, the options
el piloto, pilot
el policía, policeman
la comisaría, the police station
el profesor, professor; teacher (not maestro)
la escuela, school
la secretaria, secretary (female)
el dependiente de comercio, the shop assistant
el estadio, stadium
la camisa, shirt
la mesa, table
el taxista, taxi driver
tercera, third
tercero, third
la garganta, throat (not neck)
el boleto, the ticket
el camino, track; road
el camión, truck
el tío, uncle
la universidad, university
hay, there is; there are
delgado, slim; thin
este, this
ese, that one (masc)
esta, this
esa, that
estos, these
esos, those
estas, these (f)
esas, those (f)
llegar, to arrive
comer, to eat
tener, to have
oír, to hear
la salida, exit
dormir, to sleep
sacar una foto, to take a picture
hoy, today
junto, together; next to
también, also; too
veinticinco, twenty-five
veintiocho, twenty-eight
veinticuatro, twenty four
veintinueve, twenty nine
veintiuno, twenty one
veintisiete, twenty-seven
veintiséis, twenty six
veintitrés, twenty three
veintidos, twenty two
feo, ugly
encantar, to like
vivir, to live
hablar, to speak
dulce, sweet
nosotros, we
vosotros, you (plural informal)
bueno, good; well
qué vas a hacer?, What are you going to do?
Qué haces?, What do you do?
qué quieres?, what do you want?
qué suerte, what luck
qué tomas?, what would you like to eat/drink?
cuándo vas?, when are you going?
¿dónde están?, where are they?
¿De dónde eres?, Where are you from?
Quién?, Who?
Por qué?, Why?
sí, yes
ustedes, you (plural; formal)
usted, you (formal)
joven, young
el interés, interest
la tableta, tablet
el caballo, horse
el caballo de fuerza, horsepower
la novela, novel
la biografía, biography
los libros de negocios, business books
como, like; as; such as
viajar, to travel
la experiencia, experience
agradable, pleasant
único, unique
conocer, to know (be familiar with)
las personas, people
diferente, different
la cultura, culture
la tradición, tradition
el país, country
la nación, nation
la nacionalidad, nationality
los cientos, the hundreds
el equipo, team; equipment
el seguidor, follower
la liga, league
el campeón, champion
el campeonato, championship
el extranjero, foreigner
la temporada, season
el ingreso, income
modesto, modest
la ambición, ambition
el objetivo, objective
el objeto, object
el gol, goal (target/score in sports)
la estrella, star
crecer, to grow
la tabla, board
sin embargo, however; never the less
la inyección, injection
la corporación, corporation
el salario, salary
la cifra, number; digit; figure (not número)
el mundo, world
mundial, worldwide; world
la llegada, arrival
fácil, easy
fácilmente, easily
el deseo, desire
desear, to desire (not querer)
la reservación, reservation
el desayuno, breakfast
incluido, included
el aparcamiento, parking
la habitación individual, single room
la habitación doble, double room
doble, double
relajante, relaxing
buscar, to search; to seek
en qué le puedo ayudar?, how can I help you?
la vacación, holiday; vacation
las vacaciones, holidays; vacation
vacío, empty; void
caro, expensive
dibujar, to draw
caminar, to walk
preguntar, to ask
describir, to describe
estudiar, to study
escribir, to write
sentir, to feel; regret; be sorry
decir, to say
entretener, to entertain
descansar, to rest
la prueba, test; proof
el cine, movie theatre
las carreras de caballos, horse races
la biblioteca, library
la piscina, swimming pool
alrededor, around; round
sin, without
cerca, close (nearby)
lejos, far
entre, between; among
delante, in front; ahead
fuera de, outside of; beyond
encima, above; over
debajo, underneath
detrás, behind
Frente a, In front of
la llave, key
el bolso, bag; handbag; purse
el bolsillo, pocket
cuál, which
el tiempo, time; the weather
mal, bad; wrong
hace mal tiempo, it's bad weather
el calor, heat
hace calor, it's hot (weather)
hace sol, it's sunny
el sol, sun
el viento, wind
hace viento, it's windy
llueve, it rains
cómo está el tiempo?, how is the weather?
sin pausa, without stopping
nevar, to snow
aunque, although;though; even if; even though
el examen, exam
cierto, true; certain
alguno, some [any]
la collección, collection
coleccionar, to collect (objects)
la cosa, thing
el sello, stamp
practicar, to practice
el voleibol, volleyball
gastar, to spend
las compras, shopping
En el extranjero, Abroad; out of the country
la gente, people
el agente, agent
la etapa, stage
el puesto, post; position
eso no importa, tha't isn't important
cualquiera, whichever
las verduras, vegetables
el zapato, shoe
la mantequilla, butter
probar, to test; to try (on clothes; food)
la chaqueta, jacket
el pantalón, pants
el probador, fitting room
claro que se puede, of course you can
la talla, size (clothing)
adecuado, adequate
costar, to cost
valer, to be worth
haber, to have (auxiliary verb)
la página, page
elegante, elegant
pagar, to pay (for)
acostarse, to go to bed
rápido, fast
sentirse, to feel (emotionally or physically; reflexive)
llamarse, to be called; to be named
despertar, to wake up
despertarse, to wake up oneself
quedar, to remain; to meet; to fit(clothes)
quedarse, to stay
dormirse, to fall asleep
vestir, to dress
vestirse, to dress oneself; to get dressed
levantar, to lift; pick up; to raise
levantarse, to get up; to rise (oneself)
duchar, to shower
ducharse, to take a shower; to shower oneself
el tiempo libre, free time
usualmente, usually
normalmente, normally
a menudo, often
siempre, always
a veces, sometimes; at times
nunca, never
casi, almost
casi nunca, almost never
el ejercicio, exercise
el parque, park
la tarea, task
el mueble, piece of furniture
deprisa; rápido, fast; quickly
la basura, garbage
el sofá, sofa
el cojín, cushion
el sillón, armchair (not silla or butaca)
la alfombra, carpet
el cuadro, picture; square; painting
la lámpara, lamp
la ventana, window
la cortina, curtain
la persiana, the window blind
la decoración, decoration
el dormitorio, bedroom
la sábana, sheet (on bed)
el edredón, duvet
la funda, cover
la almohada, pillow
la mesita de noche, bedside table
el armario, closet
la estantería, shelving
el póster, poster
el queso, cheese
el líquido, liquid
el tanque, tank
la química, chemistry
el accidente, the accident
el saxofón, saxophone
la traducción, translation
el taxista, taxi driver
el acceso, access
el exceso, excess
la dirección, address; direction
el accesorio, accessory
la corrección, correction
el éxito, result; outcome; success; hit
últimamente, lately
encontrar, to meet; to find
la lista, list
La lista de contactos, Contact list
el helado, ice cream
el mes, month
la carta, letter; menu
el quiosco, kiosk
la página web, web page
el emperador, emperor
¿Cuándo?, When?
¿Dónde?, Where?
¿Adónde?, To where?
¿De dónde?, From where?
cuánto?, how much?
cuántos, how many
¿cuántas?, how many? (fem)
qué, what
¿por qué?, why?
cómo?, how?
¿Cuáles?, Which ones?
quiénes, who? (plural)
bien hecho, well done
¿cómo te va?, how's it going? (informal)
adelante, forward
en, in; at; on
el lado, side
al lado (de), at one's side; next to
el lápiz, pencil
la salud, health
saludable, healthy
la medicina, medicine
la astronomía, astronomy
la matemática, mathematics
la historia, history
la época, epoch; period
la época dorada, the golden age
la tarjeta, card
regalar, to give [as a present]
la torta, cake
el regalo, gift; present
preparar, to prepare
prepararse, to prepare oneself
el aniversario, anniversary
el cuaderno, exercise book
alegre, happy (not felices)
trabajador/a, hard working
enojado, angry
Lo más pronto posible, As soon as possible
simpático, nice (s)
gracioso, funny; graceful
peor, worse
mayor, old; great [in magnitude]
menor, minor; lesser; younger
el águila, eagle
tan...como, as...as
la harina, flour
los lentes, glasses
flaco, skinny
mediano, medium
no lo se, i dont know
la escoba, broom
el recogedor, dustpan
la fregona, mop
la película, movie; film
la verdad, truth
el frigorífico, refrigerator (Sp)
el congelador, freezer
la encimera, worktop; kitchen counter
el fogón, stove; the bonfire
el horno, oven
el microondas, microwave
el fregadero, sink
el grifo, tap
el escurreplatos, dish rack
el sartén; la sartén, frying pan; skillet
la olla, pot
la cafetera, coffeemaker
la tostadora, toaster
la batidora, mixer; blender
el exprimidor, juicer
la báscula, weighing scale
el abrelatas, tin opener (can opener)
el sacacorchos, corkscrew
la frecuencia, frequency
infeliz, unhappy
el atletismo, athletics
El senderismo, Hiking
el ciclismo, cycling
el balonmano, handball
el tenis, tennis
el beisbol, baseball
la natación, swimming
practicar la vela, to sail
el esquí, skiing
el patinaje, skating
el gimnasio, gym
la competición, competition
el pueblo, town; nation; people
el resultado, result; outcome
desde, from; since
el partido, game (match); party (political)
el primer ministro, prime minister
la palabra, word
la legislación, legislation
guiar, to guide
el código, code
la ofensiva, offensive
el predicador, preacher
dominar, to dominate
el papel, paper; role
activo, active
el problema, problem
urgente, urgent
la milicia, militia
privado, private
la batalla, battle
religioso, religious
la posibilidad, possibility
tras, behind; after (position)
la cumbre, summit; peak point (not pico)
la derrota, defeat
el respaldo, back (of a chair; e.g.)
el gobierno, government
la concesión, concession
el enfoque, approach
la forma, shape; form
el ahorro, saving
el presupuesto, budget; estimate
responsable, responsible
terco, stubborn
el argumento, argument; plot
común, common
oriental, eastern
occidental, western
el/la líder, leader
el liderazgo, leadership
liderar, to lead
la integración, integration
original, original
el tratado, treaty
la suerte, luck
amado, beloved
el traslado, transfer
la raíz, root
el aliado, ally
R, "air-ay"
cauto, cautious
anterior, previous; former
sobrevivir, to survive
el verano, summer
la primavera, spring (season)
el otoño, autumn
el invierno, winter
las estaciones del año, the seasons of the year
¿Qué tiempo hace?, What's the weather like?
hace buen tiempo, it is nice weather
hace mal tiempo, the weather is bad
hace frío, it's cold (weather)
esta soleado, it's sunny
la nube, cloud
Está nublado., It is cloudy.
la lluvia, rain (not chaparrón)
llover, to rain
la niebla, fog; mist
la tormenta, storm
el rayo, ray; lightning
el arco iris, rainbow
la nieve, snow
el hielo, ice
por otro lado, on the other hand
el agradecimiento, gratitude
la bendición, Blessing
el pobre, poor
la lista de la compra, shopping list
la galleta, cookie
el paquete, package
el yogur, yoghurt
la sal, salt
el vinagre, vinegar
el aceite de oliva, olive oil
la ternera, veal; calf
el atún, tuna
el papel higiénico, toilet paper
el jabón, soap
el champú, shampoo
el champiñón, mushroom
la lata, can (tin)
el monedero, wallet; purse
tengo que irme, I have to go
el pronóstico del tiempo, weather report
el impermeable, raincoat
el paraguas, umbrella
la toalla, towel
el traje de baño, bathing suit
el retrete, toilet
la bañera, bathtub
el bidé, bidet
el lavabo, sink (in bathroom)
la ducha, shower (take a ...)
el gel de ducha, shower gel
el grifo, faucet; tap
el espejo, mirror
el cepillo, brush; toothbrush; hairbrush; carpenter's plane
el cepillo de dientes, toothbrush
el peine, comb
El secador de pelo, hair dryer
la maquinilla de afeitar, razor
la espuma de afeitar, shaving foam
la cisterna, cistern
el jefe, boss
¡enhorabuena!, congratulations!
¡Felicidades!, Best wishes!
¡feliz cumpleaños!, happy birthday!
¡Feliz aniversario!, Happy anniversary!
¡Te deseo lo mejor!, I wish you all the best!
¡Cuídate!, Take care!
¡Qué lo pases bien!, Have a good time!
¡Buen viaje!, Have a good journey!
¡Que tengas suerte!, Good luck!
¡Qué duermas bien!, Sleep well!
¡Que te mejores!, Get well soon!
¡qué aproveche!, Bon appétit
¡No me digas!, You don't say!
¡Qué sorpresa!, What a surprise!
¡Qué bien!, how nice; that's great!
¡Es fabuloso!, That's fab!
¡Qué contento estoy!, I am so happy!
¡Me da igual!, I don't mind!
¡qué pena!, what a pity!
¡qué asco!, how disgusting!
¡qué lástima!, what a shame!
¡No aguanto más!, I can't stand it anymore!
el pasaporte, passport
aquí está, here it is
el equipaje de mano, hand luggage
la mochila, backpack
la cartera, wallet
el boleto, ticket
no lo tengo, I don't have it
la conversación, conversation
el almuerzo, lunch
la fresa, strawberry
la cena, dinner
el pato, duck
la tortuga, turtle
la araña, spider
el oso, bear
el cangrejo, crab
el león, lion
el tigre, tiger
el mono, monkey
el zorro, fox
la vaca, cow
las hormigas, ants
la abeja, bee
el despertador, alarm clock
desayunar, to have breakfast
cepillar, to brush (c)
cepillarse, to brush oneself
coger, to catch (bus; plane etc); grasp; take
volver, to return (v...)
acostar, to put to bed
cada, each; every
el cacao, cocoa
el batido, milkshake
el batido de fresa, strawberry milkshake
el chocolate caliente, hot chocolate
la tostada, toast
la mermelada, jam
la bollería, cake; pastries
los huevos revueltos, scrambled eggs
los huevos cocidos, hard-boiled eggs
el familiar, relative
el familiar cercano, close relative
el pariente, relative
el pariente lejano, distant relative
La familia política, The in-laws
el cuñado, brother-in-law
la cuñada, sister in law
el suegro, father-in-law
la suegra, mother-in-law
el yerno, son-in-law
la nuera, daughter-in-law
el bisabuelo, great grandfather
la bisabuela, great grandmother
el tatarabuelo, the great-great-grandfather
la tatarabuela, great-great-grandmother
los mellizos, the twins
los trillizos, triplets
el matrimonio, marriage; married couple
la pareja, couple
la pareja de hecho, unmarried couple
la maternidad, maternity
la paternidad, paternity
la madre soltera, single mother
el huérfano, orphan
adoptar, to adopt
criar, to raise; to breed (animals)
ya era hora!, about time!
el apoyo, support
servicial, helpful; obliging
chico, small (c...)
fuerte, strong
débil, weak; faint; dim
mojado, wet
sucio, dirty
el luchador, fighter
caluroso, warm
helado, frozen; iced
ordenado, tidy; neat; orderly [describing objects]
desordenado, messy
triste, sad
adolorido, sore
besando, kissing
abrazando, hugging
tomando la mano, holding hands
relajar, to relax
el clima, climate
despejado, clear (weather)
nublado, cloudy
fresco, fresh
el asiento, seat
el centro comercial, shopping center; mall
yo busco, I look for
la falda, skirt
perfecto, perfect
el estilo, style
el vestidor, the dressing room
sólo, only; just
el crédito, credit
el cheque, check
el cheque de viaje, traveler's cheque/check
el efectivo, cash
Vale, Okay
la siesta, nap
Pasa!, Come in!
pues, well (not bien)
Mira, Look!
la terraza, terrace
la plaza, (town) square
la merienda, tea; snack
el cruasán, croissant
el viaje, journey
la línea, line
el convento, convent
el palacio, palace
enseguida, immediately; at once (not inmediatamente)
el apellido, surname
el nombre, noun; name
la ración, portion
la tapa, the snack
el servicio, service
el fondo, background; bottom; backyard
los pendientes, earrings
el collar, necklace
la pulsera, bracelet
el jueves, Thursday
el domingo, Sunday
el monumento, monument
el mediodía, midday
gratis, free (not libre)
billete de ida, one way ticket
el billete de ida y vuelta, round-trip ticket
la oficina de correos, post office
la fecha de nacimiento, date of birth
el sexo, sex; gender
la firma, signature
el este, east
el oeste, west
el norte, north
el sur, south
el ropero, wardrobe
el castillo, castle
las pilas, batteries
el cajero, cashier
estar enamorado, to be in love
estar prometido, to be engaged
el ex novio, ex-boyfriend
la amistad, friendship
el amigo íntimo, close frined
el viejo amigo, the long-time friend
el colega, colleague
la relación, relationship
la relación profesional, professional relationship
el empleado, employee
el socio, member; partner; associate (not miembro; integrante or colega)
el compañero, colleague; partner; mate (not colega)
el compañero de clase, classmate
el compañero de trabajo, work mate
el compañero de piso, rommate; flatmate
el conocido, acquiantance
el invitado, guest
el enemigo, enemy
mío/mía, mine
tuyo, yours
suyo, his
nuestro, our
vuestro, your (plural; informal)
suyos, theirs
la bufanda, scarf
la balda, shelf
barato, cheap; cheaply; trashy
el mercado, market
el recuerdo, memory (r..)
típico, typical; characteristic
el precio, price; fee; charge
gratis, free; free of charge
la calidad, quality
fijo, fixed
servir, to serve
claro que sí, of course; naturally
claro que no, of course not
el restaurante, restaurant
llenar, to fill
el formulario, form
el detalle, detail
los detalles, details
la tarjeta de crédito, credit card
el camping, campsite
la caravana, caravan
la tienda de campaña, tent
el martillo, hammer
el clavo, nail (metal)
la linterna, lantern; torch; flashlight
la brújula, compass
el termo, thermos
la nevera, fridge
la cuerda, rope; cord
la pensión, pension
la recepción, reception
reservar, to reserve
la temporada alta/baja, high/low season
alojar, to lodge; stay
el bufé, buffet (meal)
la pensión completa, full board
la excursión, excursion; tour
el guía turístico, tour guide
confirmar, to confirm
cancelar, to cancel
el itinerario, itinerary
el crucero, cruise ship
pedir, to ask for; to request
el chupito, shot (drink)
brindar (por), to drink a toast (to)
el cóctel, cocktail
el fuego, fire
el billar, billards
cenar, to have dinner
el camarero, waiter; barman
el menú, menu
el postre, dessert
el cajero automático, ATM
la tarjeta de débito, debit card
devolver, to refund; to return
abrir, to open
cerrar, to close
la ola, wave
la orilla, shore
bañar, to bathe
bañarse, to bathe oneself
el bañador, swimming costume
las chancletas, flip-flops
la arena, sand
la sombrilla, umbrella
detrás de, Behind; in back of
delante, ahead
enfrente, in front
al principio de, at the start of
al final de, at the end of
la acera, sidewalk; pavement
cruzar, to cross
perder, to lose
perderse, to get lost
el cuchillo, knife
la cuchara, spoon
la cucharilla, teaspoon
el tenedor, fork
la camarera, waitress
el orden, order (as in "we need a bit of order in here")
el bistec, steak
la carne de res (or carne de vaca), beef
la sopa, soup
la sopa del día, soup of the day
la ensalada, salad
la ensalada verde, green salad
los champiñones, edible mushrooms
las papas fritas, French fries (fried potatoes)
las papas al horno, baked potatoes
el pan, bread
la panadería, bakery
la carne molida, ground beef
¿Qué tal?, Hello!  How are you?
espero que sí, I hope so
espero que no, I hope not
la fiesta, party
la fiesta sorpresa, surprise party
el cumpleaños, birthday
la fiesta de cumpleaños, birthday party
cumplir, to fulfill; to comply
el año, year
el año nuevo, new year
el año pasado, last year
la semana, week
el día, day
parar, to stop
aterrizar, to land
la camiseta, t-shirt
la corbata, tie
la cita, a date (with somebody)
bailar, to dance
el autobús, bus
el taxi, taxi
el dinero, money
el hostal, hostel
el mapa, map
el producto, product
explorar, to explore
la cámara, camera
la foto, photograph
libre, free (at liberty; available)
disponible, available
entender, to understand
el departamento, department
el negocio, business
el proyecto, project
difícil, difficult
emplear, to employ
el escritorio, desk
la nomina, payroll
el recurso, resource
central, central
el nacional, national
el capital, capital
dirigir, to direct
el sector, sector
la economía, economy
la red, net; network
el campo, field
el carné, card; licence
el lugar de nacimiento, place of birth
la fecha, date
la edad, age
el estado civil, marital status
soltero, single
el piso, flat; floor
el código postal, zip code
el país, country; nation
el rascacielos, skyscraper
el apartamento, flat/apartment
el estudio, studio
la buhardilla, attic; loft
la casa de campo, country house
la mansión, mansion
reformar, to reform; renovate; do up
explicar, to explain
escuchar, to listen
el alumno, student (a)
responder, to respond; to reply
completar, to complete
el resumen, summary
la lección, lesson
memorizar, to memorize
repetir, to repeat
repasar, to revise
copiar, to copy
aprobar, to pass/approve
suspender, to suspend
el curso, course
la tema, subject
aprender, to learn
saber, to know (facts)
importante, important
extranjero, foreign
el centro de la ciudad, town centre (CC...)
las afueras, outskirts
el barrio, neighborhood
el teatro, theater
la farmacia, pharmacy
casi nunca, rarely
además de, in addition to; besides; furthermore
correr, to run
andar, to walk; to go (a...)
montar, to mount
la afición, hobby
me mola, that’s cool; I find it cool
por ejemplo, for example
la capital, capital city
tengo que, I have to
acelerar, to accelerate
la zona, area
¡Buena suerte!, Good luck!
el muerto, dead
falso, false
el secreto, secret
la canción, song
el ciudadano, citizen
el hombre de negocios, businessman
prisa, in a hurry
el frío, cold
Puede ser que, It may be that
de nada, you're welcome
el muchacho, guy; boy (m..)
extraño, strange
el desierto, desert
la boda, wedding
el pasado, past
si, if
la geografía, geography
el océano, ocean
el lago, lake
la catarata, waterfall (c)
la isla, island
el valle, valley
la colina, hill (not cerro)
la cueva, cave
el acantilado, cliff
el volcán, volcano
el bosque, forest
la selva, jungle
la costa, coast
el sabor, taste
el dolor de cabeza, headache
el ajo, garlic
el cocinero, chef; cook
la reserva, reservation; reserve
la servilleta, napkin
los cubiertos, cutlery
el primer plato, first course
el plato principal, main course (food)
casar, to marry
la paga extra, bonus
la contabilidad, accounting
la auditoría, audit
la impresora, printer
el comercial, salesman
la venta, sale
la compra, purchase
jubilarse, to retire
la jubilación, retirement
el jubilado, pensioner
el horario, schedule
el calendario, calendar
la carrera, career; race (speed)
la madera, wood
la construcción, construction
la planta, plant
la pintura, painting
la comunidad, community
construir, to construct; to build
la hipoteca, mortgage
Hipotecar, to mortgage
el salón, living room
el baño, bath; bathroom
la galería, gallery
el pincho, bar snack
el amor, the love (noun)
el encanto, charm
pillar, to catch
el club, club
la pista de baile, dance floor
nacer, to be born
odiar, to hate
encantar, to enchant; like
favorito, favourite
pesar, to weigh
el vaso, glass; vessel
limpio, clean
el cordero, lamb
el filete, filet
el cocido, stew
el aceite, oil
la crema, cream
el famoso, famous person
la noticia, news
el cotilleo, gossip
cotillear, to gossip
el texto, text
la televisión, television
declarar, to declare
el modelo, model
la respuesta, response; reply; answer
Es por eso que..., That's why...
la pregunta, question (not cuestión)
el rico, the rich
el silencio, silence
la prensa, press
publicar, to publish
el Dios, God
el rey, king
la reina, queen
el príncipe, prince
la presidencia, presidency
la república, republic
la democracia, democracy
la dictadura, dictatorship
la sociedad, society
la política, politics
el parlamento, parliament
parlamentario, parliamentary
el parlamentario, member of parliament
la mayoría, majority
la coalición, coalition
la corrupción, corruption
el senador, senator
el sindicato, union
la protesta, protest
gubernamental, governmental
estatal, state
la población, population
la autoridad, authority
la pobreza, poverty
la recesión, recession
la pista, track; trail (p)
la moda, fashion
poco, little; few
sentar, to sit
la realidad, reality
la idea, idea
aparecer, to appear
lograr, to achieve
caerse, to fall
establecer, to establish
terminar, to terminate
el movimiento, movement
la situación, situation
la muerte, death
distinto, distinct; different  [not "diferente"]
el nivel, level
la luz, light
aún, still; yet
alcanzar, to reach; to catch up with; to achieve
la región, region; area (r)
acabar, to end; to finish
incluso, even; including
varios, several
la razón, reason
ayer, yesterday
la década, decade
la gracia, grace
incluir, to include; to enclose
el sentido, sense (not sentimiento or sensación)
destacar, to emphasize; distinguish; stand out
obtener, to get (o...)
continuar, to continue
todo, all; everything
necesitar, to need
la paz, peace
la producción, production
el valor, value;   courage
la información, information
ofrecer, to offer
la base, base; foundation
el aire, air
la condición, condition
reconocer, to recognize
ocupar, to occupy
decidir, to decide
tanto, so much
la vista, view
acercar, to bring closer
acercarse, to approach
la investigación, research; investigation
iniciar, to initiate; start
dedicar, to dedicate
bastante, enough; rather
descubrir, to discover
traer, to bring
el medio, half
la imagen, image
el aspecto, aspect
el efecto, effect
ninguno, none; neither
el resto, rest
disponer, to arrange; to provide; to stipulate; to dispose
la educación, education
el supermercado, supermarket
el cliente, client
comprar, to buy
ir de compras, to go shopping
la sección, section
el congelados, frozen foods
durante, during
ya, already; by now
nada, nothing (negative - anything)
los deberes, homework
conmigo, with me
abierto, open
introvertido/a, introverted; shy
optimista, optimistic
pesimista, pessimistic
dinámico, dynamic
callado, quiet; silent
el paciente, patient
impaciente, impatient
sensato, sensible
irresponsable, irresponsible
curioso, curious
sincero, sincere
discreto, discrete/ subtle
tierno, tender
sensible, sensitive
atento, attentive
considerado, considerate
humilde, humble
altruista, altruistic
compasivo, compassionate
hospitalario, hospitable
apasionado, passionate
inseguro, insecure
arrogante, arrogant
ambicioso, ambitious
vago, lazy (v)
travieso, naughty
cobarde, cowardly
tacaño, selfish; stingy
resignado, resigned
grosero, rude; ill-mannered
insolente, insolent; cheeky
dolido, hurt; bereaved
avergonzado, embarrassed; ashamed
indignado, indignant
estresado, stressed
cariño, sweetie; hon
