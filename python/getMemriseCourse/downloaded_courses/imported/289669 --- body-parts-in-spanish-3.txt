la cabeza, the head
la cara, the face
la espalda, the back
los huesos, the bones
los músculos, the muscles
la piel , the skin
la sangre, the blood
el tronco , the trunk
las venas, the veins
la boca, the mouth
los dientes, the teeth
los labios, the lips
la lengua, the tongue
la nariz, the nose
las orejas, the ears
la frente, the forehead
el pelo/el cabello, the hair
las mejillas, the cheeks
el cuello , the neck
la nuca, the nape
el mentón, the chin
el bigote, the moustache
los ojos, the eyes
las cejas, the eyebrows
las pestañas, the eyelashes
la pupila, the pupil
la barba, the beard
el abdomen , the abdomen
los brazos, the arms
las manos, the hands
los dedos de las manos, the fingers
las muñecas, the wrists
las uñas, the nails
los codos, the elbows
los hombros, the shoulders
la cintura, the waist
la barriga, belly
el ombligo, belly button
el pecho, chest
los muslos, the thighs
las piernas, the legs (in persons)
las patas, legs (in animals and objects)
los pies, the feet
los dedos de los pies, the toes
las rodillas, the knees
el cerebro, the brain
la columna vertebral, the spinal column
el esófago, esophagus
el estómago, stomach
el hígado, liver
los intestinos, intestines
los pulmones, lungs
los riñones, kidneys
el páncreas, the pancre
