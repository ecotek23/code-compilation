le besoin		the need
avoir besoin de ...		to need (something)
l'aide		the help
j'ai besoin d'aide		I need help
dire		to say; to tell
comment est-ce que tu dis ... en français ?		how do you say ... in French?
savoir		to know (a fact)
je ne sais pas		I don't know
comprendre		to understand
je ne comprends pas		I don't understand
est-ce que tu comprends ?		do you understand?
moi aussi		me too
moi non plus		me neither
enchanté		nice to meet you (said by a man)
enchantée		nice to meet you (said by a woman)
super		great
c'est super !		that's great!
tiens		here you go
bienvenue		welcome
tout		all; everything
rien		nothing; anything
de		of; from
de rien		you're welcome
la chance		the chance; the luck
bonne chance !		good luck!
France		France
les Etats-Unis		the United States
Angleterre		England
parler		to speak; to talk
un peu		a little
du; des		of the; from the; some
où		where
venir		to come
je viens		I come
d'où est-ce que tu viens ?		where are you from?
je viens d'Angleterre		I'm from England
est-ce que tu viens d'Angleterre ?		are you from England?
je viens des Etats-Unis		I'm from the United States
je parle anglais		I speak English
est-ce que tu parles français ?		do you speak French?
je parle un peu français		I speak a little French
bien sûr !		of course!
bien sûr que je parle anglais		of course I speak English
tu parles très bien français		you speak French very well
l'assiette		the plate
le verre		the glass
la tasse		the cup
la bouteille		the bottle
le café au lait		the latte
l'entrée		the starter
le plat principal		the main course
le dessert		the dessert
j'aimerais		I would like
vous aimeriez		you would like (plural; formal)
la chose		the thing
quelque chose		something
qu'est-ce que vous aimeriez ?		what would you like?
j'aimerais une tasse de café s'il vous plaît		I would like a cup of coffee please
qu'est-ce que vous aimeriez manger ?		what would you like to eat?
est-ce que vous aimeriez une bouteille ou un verre ?		would you like a bottle or a glass?
j'aimerais du lait s'il vous plaît		I would like some milk please
est-ce que vous avez des desserts ?		do you have any desserts?
je n'ai rien		I don't have anything
je n'ai pas d'eau		I don't have any water
bien sûr que j'ai de l'eau		of course I have some water
onze		eleven; 11
douze		twelve; 12
treize		thirteen; 13
quatorze		fourteen; 14
quinze		fifteen; 15
seize		sixteen; 16
dix-sept		seventeen; 17
dix-huit		eighteen; 18
dix-neuf		nineteen; 19
vingt		twenty; 20
le marché		the market; the deal (business)
le distributeur automatique		the ATM
la pharmacie		the pharmacy
le kiosque		the kiosk
les gens		the people (persons)
le magasin		the shop; the store
le supermarché		the supermarket
la voiture		the car
le taxi		the taxi
la banque		the bank
le livre		the book
la librairie		the bookshop
le client; la cliente		the customer; the client
vouloir		to want; to mean (to do something)
je veux		I want
acheter		to buy
vendre		to sell; to deal
payer		to pay
faire du shopping		to go shopping (for leisure)
faire les courses		to go shopping (groceries)
là		there
il y a		there is; there are
allons faire du shopping		let's go shopping
allons au marché		let's go to the market
je veux acheter quelque chose pour toi		I want to buy something for you
il y a une grande pharmacie		there is a big pharmacy
il y a beaucoup de gens		there are many people
est-ce qu'il y a un distributeur automatique ?		is there an ATM?
il n'y a pas de banques		there are no banks
il y a beaucoup de petites librairies		there are many small bookshops
le client a toujours raison		the customer is always right
la boisson		the drink
préféré		favourite
ce (cette)		this; that
quoi		what (thing)
en fait		actually (in fact)
ou		or
il aime le café		he likes coffee
il n'aime pas le poisson		he doesn't like fish
elle n'aime pas le thé ou le café		she doesn't like tea or coffee
il n'y a pas de nourriture		there is no food
est-ce qu'il y a de la soupe ?		is there any soup?
c'est quoi ?		what's that?
ce sont mes préférés		they're my favourite
j'aime ça en fait		I actually like it
le café est ma boisson préférée		coffee is my favourite drink
la famille		the family
la mère		the mum
le père		the dad
les parents		the parents
la sœur		the sister
le frère		the brother
le grand-père		the grandpa
la grand-mère		the grandma
les grands-parents		the grandparents
le garçon		the boy
le fils		the son
la fille		the daughter; the girl
l'ami; l'amie		the friend
le petit ami		the boyfriend
la petite amie		the girlfriend
la personne		the person
l'homme		the man
le mari		the husband
la femme		the woman; the wife
le bébé		the baby
l'enfant		the kid; the child
l'adulte		the adult
le travail		the job; the work; the labour
l'école		the school
le bureau		the office; the desk
travailler		to work
connaître		to know (someone or somewhere); to be familiar with
qui		who; which
dans		in; into
c'est qui ?		who are they?
je ne connais pas cette personne		I don't know that person
c'est ma mère		this is my mum
c'est mon frère		he's my brother
c'est ma mère et mon père		this is my mum and dad
ce n'est pas ma fille		that isn't my daughter
c'est mon ami de France		this is my friend from France
est-ce qu'il a une petite amie ?		does he have a girlfriend?
je veux une femme		I want a wife
est-ce que ton petit ami a un travail ?		does your boyfriend have a job?
il travaille dans un bureau		he works in an office
la couleur		the colour
rouge		red
bleu		blue
jaune		yellow
vert		green
noir		black
rose		pink
violet		purple
blanc		white
orange		orange
marron		brown
gris		grey
clair		light (colour); clear
foncé		dark (colour)
bleu clair		light blue
rouge foncé		dark red
excusez-moi		excuse me
où est ... ?		where is ... ?
l'hôpital		the hospital
où est l'hôpital ?		where is the hospital?
ici		here
c'est ici		here it is
juste		just; right; fair
juste un peu		just a little
suffire		to be enough; to suffice
ça suffit !		that's enough!
qu'est-ce que tu fais ?		what are you doing?
possible		possible
c'est possible		it's possible
est-ce que c'est possible ?		is it possible?
impossible		impossible
c'est impossible !		it's impossible!
le dommage		the damage
c'est dommage !		what a shame!
les vêtements		the clothes
le pantalon		the trousers
la chemise		the shirt
le t-shirt		the T-shirt
la jupe		the skirt
la robe		the dress
le short		the shorts
le manteau		the coat
la veste		the jacket
le pull		the jumper
l'écharpe		the scarf
le chapeau		the hat
le costume		the suit
les chaussettes		the socks
les chaussures		the shoes
les chaussons		the slippers
les bottes		the boots
les tennis		the trainers; the tennis shoes
les gants		the gloves
la culotte		the pants
le parapluie		the umbrella
le sac		the bag
le portefeuille		the wallet
nouveau		new
vieux		old
faire pipi		to pee
devoir		to have (to do something)
je dois ...		I have to (do something); I must
fermé		closed
ouvert		open
est-ce que tu veux une nouvelle chemise ?		do you want a new shirt?
il veut le pantalon bleu		he wants the blue trousers
qu'est-ce que tu veux acheter ?		what do you want to buy?
je veux acheter des vêtements		I want to buy some clothes
je dois faire pipi		I have to pee
je dois acheter un parapluie		I have to buy an umbrella
la banque est ouverte		the bank is open
la noix		the nut
le bonbon		the sweet
le chocolat		the chocolate
le gâteau		the cake
la pizza		the pizza
le sucre		the sugar
le sel		the salt
le poivre		the pepper
végétarien		vegetarian
épicé		hot (spicy)
chaud		hot (temperature); warm
froid		cold
allergique		allergic
accro		addicted
est-ce que tu aimes les bonbons ?		do you like sweets?
c'est très bon		it's very good
je suis végétarien		I'm vegetarian
est-ce que la soupe est chaude ?		is the soup hot?
est-ce que c'est végétarien ?		is it vegetarian?
il est allergique aux noix		he's allergic to nuts
nous sommes accros au sucre		we're addicted to sugar
l'humain		the human
l'animal		the animal
le chien		the dog
le chat		the cat
l'oiseau		the bird
la vache		the cow
le cochon		the pig
le lapin		the rabbit
le serpent		the snake
le lion		the lion
l'éléphant		the elephant
le cheval		the horse
le mouton		the sheep
le singe		the monkey
le chiot		the puppy
le chaton		the kitten
la girafe		the giraffe
l'âne		the donkey
la souris		the mouse
fidèle		loyal
courageux		brave
intelligent		clever; intelligent
stupide		stupid
libre		free (at liberty)
calme		quiet
mignon		cute
l'air		the air
fidèle comme un chien		loyal as a dog
libre comme l'air		free as the air
le jour		the day
la semaine		the week
le mois		the month
l'an		the year
jeune		young
l'âge		the age
c'est mon chien		this is my dog
quel âge		how old
il a quel âge ?		how old is he?
il a douze ans		he's twelve years old
mon chat est très vieux		my cat is very old
tu es très jeune		you're very young
l'appartement		the flat; the apartment
la maison		the house
Londres		London
Liverpool		Liverpool
Washington D.C.		Washington D.C.
New York		New York
Paris		Paris
Lyon		Lyon
habiter		to live (in a place)
toujours		always; still
jamais		never
pourquoi		why
parce que		because
donc		so (giving reason)
ils habitent à		they live in
elle a une maison à Londres		she has a house in London
tu es toujours content		you're always happy
il n'est jamais content		he's never happy
pourquoi est-ce que tu es si triste ?		why are you so sad?
... parce que tu es malade		... because you're sick
je suis en colère parce que j'ai faim		I'm angry because I'm hungry
il est un peu triste parce qu'il n'a pas de petite amie		he's a little sad because he doesn't have a girlfriend
trente		thirty; 30
quarante		forty; 40
cinquante		fifty; 50
soixante		sixty; 60
soixante-dix		seventy; 70
quatre-vingts		eighty; 80
quatre-vingt-dix		ninety; 90
cent		one hundred; 100
vingt-quatre		twenty-four; 24
quarante-six		forty-six; 46
trois cents		three hundred; 300
le temps		the time; the weather
l'heure		the hour; o'clock
la minute		the minute
la seconde		the second
demi		half
moins		less; minus
plus		more; anymore
le rendez-vous		the date
la réunion		the meeting
quand		when
du soir		in the evening; p.m.
du matin		in the morning; a.m.
quelle heure est-il ?		what time is it?
il est deux heures		it's two o'clock
il est huit heures et demie		it's eight thirty
il est sept heures vingt		it's twenty past seven
il est deux heures moins dix		it's ten to two
il est cinq heures six		it's six past five
quand est ton rendez-vous ?		when is your date?
à quelle heure ?		at what time?
à quatre heures moins dix		at ten to four
à une heure		at one o'clock
prendre		to take
prendre soin		to take care
prends soin de toi		take care
voir		to see
je vois ...		I see ...
vouloir dire		to mean; to want to say
est-ce que tu vois ce que je veux dire ?		do you know what I mean?
qu'est-ce qu'il y a ?		what's the matter?
le problème		the problem
pas de problème		no problem
se		oneself; each other
s'inquiéter		to worry (oneself)
ne t'inquiète pas		don't worry
s'occuper de		to mind (oneself)
déranger		to bother
ça ne me dérange pas		I don't mind
oups !		oops!
oh non !		oh no!
effectivement !		indeed!
félicitations !		congratulations!
yahoo !		ya