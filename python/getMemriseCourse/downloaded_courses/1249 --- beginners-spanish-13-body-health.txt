el aliento		breath
arder		to burn, sting (a..)
la asistencia		assistance
aliviar		to relieve
la bacteria		bacteria, germ
el bienestar		well-being
la boca		mouth
el brazo		arm
el cáncer		cancer
la cirugía		surgery (the subject not place)
la clínica		clinic
consciente		conscious
la consulta		consultation
consultar		to consult
consumir		to consume
contraer		to contract
el corazón		heart
corporal		corporal
el cráneo		skull
el cuello		neck, collar
cuidar		to look after, to take care of
curar		to heal, to cure
dañar		to damage, harm, injure
débil		weak, faint, dim
la cura		cure
debilitar		to weaken
el dedo		finger, toe
delicado		delicate
la depresión		depression
el deterioro		deterioration
el diagnóstico		diagnosis
el diente		tooth
la dieta		diet
el médico		doctor
doler		to hurt
el dolor		pain, sorrow
la dosis		dose
doloroso		painful
el cuerpo		body
el adulto		adult
el hábito		habit
la energía		energy
la enfermedad		disease
el enfermo		sick person
el enfermero		nurse
la espalda		back [anatomy]
el especialista		specialist
el estómago		stomach
examinar		to examine
el fármaco		drug, medicine (f..)
fatal		fatal
la fiebre		fever
fumar		to smoke
la garganta		throat (not neck)
el hombro		shoulder
la hormona		hormone
el hospital		hospital
el hueso		bone
la infección		infection
el labio		lip
la lengua		tongue, language
la lesión		injury
leve		slight
el medicamento		medicine, drug (not medicina, droga or farmaco)
la medicina		medicine
mover		to move, to shift
la nariz		nose
negativo		negative
el nervio		nerve
el oído		ear (inner part), hearing(sense of)
notar		to notice
la operación		operation
oral		oral
la oreja		ear (exterior)
el pabellón		pavilion, ward
el paciente		patient
pálido		pale
padecer		to suffer from
paralizar		to paralyze
el pecho		chest (on body)
el pie		foot
la pierna		leg
la prueba		test, proof
el pulmón		lung
el remedio		remedy
respirar		to breathe
reventar		to burst
la rodilla		knee
roto		broken
la salud		health
la sangre		blood
sano		healthy
el seno		bosom, breast
el síntoma		symptom
temblar		to shiver
tener		to have
la terapia		therapy
el tratamiento		treatment
el tumor		tumor
la uña		fingernail, toenail
la vacuna		vaccine
la vena		vein
el virus		virus
vital		vital
bien		well
la condición		condition
mudo		mute
mal		bad, wrong
la alimentación		food, diet (not alimento, comida or diet