arrancar		to tear off, to start (i.e. a computer)
acordar		to agree
asociar		to associate
autorizar		to authorize
beneficiar		to benefit
comercial		commercial
consolidar		to consolidate
controlar		to control
convenir		to agree, to be advisable, to suit
distribuir		to distribute
el auge		peak
el comercio		commerce
el capital		capital
el beneficio		benefit
el asociado		associate
el asunto		matter, issue
el asesor		advisor
el agente		agent
el cliente		client
el control		control
el convenio		agreement
el coste		cost
el empresario		businessman
el despacho		office
el funcionamiento		operation (functioning)
el fundador		founder
el/la líder		leader
el liderazgo		leadership
el negocio		business
el objetivo		objective
el organismo		organism
el pacto		pact
el plan		plan
el recurso		resource
el riesgo		risk
el potencial		potential
el socio		member, partner, associate (not miembro, integrante or colega)
el propietario		owner
el suministro		supply
el término		term, terminus
el titular		title holder
el trato		treatment
el vínculo		link, tie (not corbata)
elaborar		to make, manufacture, produce
emprender		to embark on, undertake
empresarial		entrepreneurial
estratégico		strategic
exportar		to export
extender		to extend
firmar		to sign (a document)
fundar		to found, to establish
garantizar		to guarantee
generar		to generate
importar		to be important, to matter
industrial		industrial
integral		comprehensive
interesado		interested, selfish (not egoísta)
internacional		international
la agencia		agency
la ambición		ambition
la aprobación		approval
la asociación		association
la capitalización		capitalization
la compañía		company
la concesión		concession
la conferencia		conference
la distribución		distribution
la elaboración		making, production (not producción)
la estrategia		strategy
la estructura		structure
la evaluación		evaluation
la expansión		expansion
la exportación		exportation
la fábrica		factory
la fabricación		manufacture (not elaboración)
la firma		signature
la fusión		fusion
la importación		importation
la industria		industry
la infraestructura		infrastructure
la iniciativa		initiative
la integración		integration
la modalidad		modality
la negociación		negotiation
la organización		organization
la propuesta		proposal
la solución		solution
limitado		limited
negociar		to negotiate
nombrar		to name, mention, nominate, refer to (not denominar)
operar		to operate
operativo		operative
plantear		to pose (eg a question), raise, propose
privado		private
procurar		to seek, try (procure)
proponer		to propose
proyectar		to project
registrar		to register
suministrar		to supply
vincular		to link (not junta