aardig		pleasant; nice, kind
blauw		blue
de boer		farmer
de boot		boat
de bus		bus
de buur		the neighbour
de deur		door
het dier		animal
duur		expensive
fijn		fine, great
het huis		house
de leeuw		the lion
de maan		moon
de man		man, husband
mooi		pretty, beautiful
nieuw		new
nu		now
het paar		pair, couple
de paraplu		umbrella
de peen		the carrot
de peer		pear
de pen		pen
de poel		pool
de poot		paw
de sla		lettuce
de straat		street
taai		tough
de trein		train
vriendelijk		friendly
het water		water
wit		white
de zee		sea
zitten		to sit
allebei		both
dus		therefore
en		and
hebben		to have
de hoofdpijn		headache
de keelpijn		sore throat
koorts hebben		to have a temperature
hetzelfde		the same
misschien		perhaps
moe		tired
ook		also, too, as well
precies		precisely
wat vreemd!		how funny! how odd!
ziek		sick
gek		mad, silly
allemaal		altogether
de groei		the growth
ik		I
jij/je		you (singular)
u		you (formal)
hij		he
zij/ze		she
het		it
wij/we		we
jullie		you (plural)
zij/ze		they
ik ben		I am
jij bent		you are (singular)
u bent		you are (formal)
hij is		he is
zij is		she is
het is		it is
wij zijn		we are
jullie zijn		you are (plural)
zij zijn		they are
Ik heb		I have
jij hebt		you have (singular)
u heeft/hebt		you have (formal)
hij heeft		he has
zij heeft		she has
het heeft		it has
wij hebben		we have
jullie hebben		you have (plural)
zij hebben		they ha