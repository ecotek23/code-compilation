la pluma		pen
buena onda		friendly
flojo		lazy
¿Qué onda?		How is it going?
el celular		cell phone
el boleto		ticket
el chavo		young man
¡Bueno!		Hello! (on the phone)
la alberca		swimming pool
el güey		dude, buddy
¡Órale!		Right on! All right!
chambear		to work
el chabacano		apricot
¡Ajá!		Yes!
la bolsa		(hand) bag
¡Chale!		Give me a break!
¡Ándale!		Come on!
tomar un taxi		to take a taxi
el borrador		eraser
el carro		car
el elevador		elevator, lift
la laptop		laptop
el abanico de techo		ceiling fan
el comercial		advertisement
el maní		peanut
el puerco		pig
el papalote		kite
la recámara		bedroom
las carnes frías		cold cuts
el lavatrastes		dishwasher
la cuadra		block (distance)
un morral		backpack
la computadora		computer
la cruda		hangover
el jugo		juice
las bocinas		loudspeakers
las lentes		(eye) glasses
el gis		chalk
el plomero		plumber
manejar		to drive (a car)
la soya		soya bean
el tecolote		owl
los tenis		sneakers
el timbre		(postage) stamp
el refrigerador		refrigerator
el librero		bookcase
el locker		locker
la forma		form (document)
estacionar		to park
el elote		corn
los direccionales		turn signals
la banqueta		sidewalk
el vocero		spokesperson
los huaraches		(huarache) sandals
los jeans		jeans
el jitomate		tomato
la pachanga		party
el cabello		hair
la cajuela		car trunk
¡A huevo!		Of course!
chismear		to gossip
el cuernito		croissant
los útiles		school supplies
la barda		enclosing wall
No hay bronca.		No problem.
¡No manches! 		No wa