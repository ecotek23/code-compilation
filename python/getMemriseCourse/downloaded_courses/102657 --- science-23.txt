l'anno luce		light year
l'astronauta		astronaut
l'astronave		spacecraft
il buco nero		black hole
la cometa		comet
il cosmo		cosmos
il cratere		crater
l'eclissi		eclipse
l'eclissi lunare		lunar eclipse
l'eclissi solare		solar eclipse
la galassia		galaxy
la gravità		gravity
la gravitazione		gravitation
la luce infrarossa		infrared light
la luce solare		sunlight
la luce ultravioletta		ultraviolet light
la luna nuova		new moon
la luna piena		full moon
la meteora		meteor
il missile		missile
il modulo lunare		lunar module
la navetta spaziale		space shuttle
l'orbita		orbit
orbitare		to orbit
il pianeta		planet
il raggio lunare		moonbeam
il raggio solare		sunbeam
il sistema solare		solar system
lo spazio		space
lo spazio tridimensionale		three-dimensional space
la stella		star
il telescopio		telescope
il veicolo spaziale		space vehicle
Giove		Jupiter
Marte		Mars
Mercurio		Mercury
Nettuno		Neptune
Plutone		Pluto
Saturno		Saturn
Terra		Earth
Urano		Uranus
Venere		Venus
addizionare		to add
l'addizione		addition
aggiungere		to add on
algebrico		algebraic
aritmetico		arithmetical
calcolare		to calculate
il calcolo		calculation
la cifra		digit
contabile		countable
contare		to count
la costante		constant
decimale		decimal
la differenza		difference
dividere		to divide
la divisione		division
elevare alla potenza di		to raise to the power of
l'equazione a una incognita		equation in one unknown
l'equazione a due incognite		equation in two unknowns
l'esponente		exponent
essere maggiore di		to be greater than
essere minore di		to be less than
essere simile a		to be similar to
essere uguale a		to be equal to
estrarre la radice		to take/extract the root
il fattore		factor
la frazione		fraction
la funzione		function
l'incognita		unknown
l'insieme		set
logaritmico		logarithmic
moltiplicare		multiply
la moltiplicazione		multiplication
il multiplo		multiple
numerare		to number
il numerale		numeral
i numeri dispari		odd numbers
i numeri pari		even numbers
numerico		numerical
il numero cardinale		cardinal number
il numero intero		whole number
il numero irrazionale		irrational number
il numero negativo		negative number
il numero ordinale		ordinal number
il numero positivo		positive number
il numero primo		prime number
il numero quadrato		square number
il numero razionale		rational number
per cento		percent
la percentuale		percentage
il prodotto		product
la proporzione		ratio, proportion
il quoziente		quotient
la radice cubica		cube root
la radice quadrata		square root
il reciproco		reciprocal
il simbolo		symbol
la soluzione		solution
la somma		sum
sommare		to sum up
sottrarre		to subtract
la sottrazione		subtraction
statistico		statistical
la tavola pitagorica		multiplication table
l'uguaglianza		equality
la variabile		variable
l'angolo acuto		acute angle
l'angolo adiacente		adjacent angle
l'angolo concavo		concave angle
l'angolo convesso		convex angle
l'angolo ottuso		obtuse angle
l'angolo piatto		straight angle
l'angolo retto		right angle
la bisettrice		bisector
il cilindro		cylinder
la circonferenza		circumference
il compasso		compass
il cono		cone
il cubo		cube
il decagono		decagon
l'esagono		hexagon
l'ettagono		heptagon
la figura piana		plane figure
l'ipotenusa		hypotenuse
la linea curva		curved line
la linea orizzontale		horizontal line
la linea parallela		parallel line
la linea perpendicolare		perpendicular
la linea retta		straight line
la linea spezzata		broken line
la linea verticale		vertical line
l'ottaedro		octahedron
l'ottagono		octagon
il parallelogramma		parallelogram
il pentagono		pentagon
la piramide		pyramid
il poliedro		polyhedron
il poligono		polygon
il prisma		prism
il quadrato		square
il quadrilatero		quadrilateral
il raggio  		radius
il rettangolo		rectangle
la riga		ruler
il rombo		rhombus
la sagoma		template
la sfera		sphere
il solido		solid
il tangente		tangent
il teorema di Pitagora		Pythagorean theorem
il tetraedro		tetrahedron
il trapezio		trapezium
il triangolo		triangle
il triangolo acutangolo		acute-angled triangle
il triangolo equilatero		equilateral triangle
il triangolo isoscele		isosceles triangle
il triangolo ottusangolo		obtuse-angled triangle
il triangolo rettangolo		right-angled triangle
il triangolo scaleno		scalene triangle
il vertice		vertex
il vettore		vector
l'acciaio inossidabile		stainless steel
l'amianto		asbestos
l'ammoniaca		ammonia
l'ammonio		ammonium
l'argento		silver
l'atomo		atom
la benzina		petrol
la bilancia		scale
il bronzo		bronze
il butano		butane
il calcio		calcium
il campo magnetico		magnetic field
il carbone		carbon (solid), coal
il carbonio		carbone (element)
il carburante		fuel
centrifugo		centrifugal
centripeto		centripetal
il cloro		chlorine
il composto		compound
l'ecosistema		ecosystem
l'effetto serra		greenhouse effect
l'elettrone		electron
l'etere		ether
il ferro		iron
il filtro		filter
il fosfato		phosphate
la frizione		friction
l'idrogeno		hydrogen
l'inquinamento		pollution
inquinare		to pollute
lo iodio		iodine
il laser		laser
la lente d'ingrandimento		magnifying glass
il magnesio		magnesium
il mercurio		mercury
il metallo		metal
il metano		methane
la microonda		microwave
il microscopio		microscope
la molecola		molecule
il neutrone		neutron
il nitrato		nitrate
il nitrogeno		nitrogen
l'onda elettromagnetica		electromagnetic wave
l'onda sonora		sound wave
l'oro		gold
l'ossigeno		oxygen
l'ottano		octane
la particella		particle
il petrolio		petroleum
il piombo		lead (metal)
il platino		platinum
il potassio		potassium
la pressione		pressure
il propano		propane
il protone		proton
la provetta		test tube
il punto di congelamento		freezing point
il punto di ebollizione		boiling point
la radiazione		radiation
il raggio di luce		light beam, ray
il rame		copper
la resina		resin
il sale		salt
il sodio		sodium
la sostanza radioattiva		radioactive substance
lo spettro		spectrum
la teoria dei quanti		quantum theory
la teoria della relatività		theory of relativity
lo zolfo		sulphur
l'archivio		file
l'automa		automaton
il byte		byte
il circuito integrato		integrated circuit
compatibile		compatible
il dato		datum, data
di facile uso		user-friendly
il dischetto		floppy disc
l'elaboratore elettronico		computer
l'elaborazione dei dati		data processing
l'hardware		hardware
l'intelligenza artificiale		artificial intelligence
l'interfaccia		interface
il lettore ottico		optical reader
il linguaggio simbolico		programming language
la memoria ad accesso casuale		random access memory
il modem		modem
il programmatore/la programmatrice		programmer
il robot		robot
il sito web		website
il software		software
la stampante		printer
la tastiera		keyboard
il terminale		terminal
l'unità periferica		peripher