le basket		basketball
le billard		snooker
les cartes		cards
les échecs		chess
le football		football, soccer
le hockey sur glace		ice hockey
le rugby		rugby
le tennis		tennis
le tennis de table/le ping-pong		table tennis
le volley-ball		volleyball
Tu aimes?		Do you like?
J'adore		I love
J'aime		I like
Je n'aime pas		I don't like
Je déteste		I hate
C'est super		It's great
C'est bien		It's good
C'est nul		It's rubbish
Ça va		It's ok
Le foot, ça va		Football, it's ok
en été		in summer
en hiver		in winter
le lundi		on Monday
le mardi		on Tuesday
tous les jours		every day
Je joue au foot		I play football
Je joue au tennis		I play tennis
Je joue aux cartes		I play cards
Je joue aux échecs		I play chess
Je joue du saxophone		I play the saxophone
Je joue du piano		I play piano
Je joue du clavier		I play keyboard
Je joue du violon		I play the violin
Je joue de la guitare		I play the guitar
Je joue de la guitare éléctrique		I play electric guitar
Je joue de la trompette		I play the trumpet
Je joue de la batterie		I play the drums
Je ne joue pas d'instrument		I don't play an instrument
Je chante		I sing
une fois par semaine		Once a week
deux fois par semaine		twice a week
trois fois par semaine		three times a week
Il joue bien/mal		He plays well/badly
un groupe		a band
J'aime		I like
Je n'aime pas		I don't like
J'aime faire de la planche à voile		I like going windsurfing
J'aime faire du judo		I like doing judo
J'aime faire du skate		I like skateboarding
J'aime faire du ski		I like going skiing
J'aime faire du sport		I like doing sport
J'aime faire du vélo		I like cycling
J'aime faire la cuisine		I like doing the cooking
J'aime faire les magasins		I like going shopping
J'aime envoyer des textos		I like texting
J'aime jouer sur l'ordinateur		I like playing on the computer
J'aime regarder des DVD		I like watching DVDs
J'aime surfer sur l'Internet		I like surfing on the Internet
J'aime la lecture		I like reading
la console de jeu		game console
les loisirs		leisure activities
On peut		We can
On peut faire de la danse		We can go dancing
On peut faire de la gymnastique		We can do gymnastics
On peut faire de la natation		We can go swimming
On peut faire du bowling		We can go bowling
On peut faire du patin à glace		We can go ice-skating
On peut faire du skate		We can go skateboarding
On peut jouer au baby-foot		We can play table football
On peut acheter des articles de sport		We can buy sports things
On peut acheter plein de choses		We can buy loads of things
un bowling		a bowling alley
une cafétéria		a cafeteria
un toboggan géant		a giant slide
une patinoire		a skating rink
une grande salle (multisport)		a sports hall
des courts de tennis		tennis courts
Je vais		I am going to
Je vais aller à la pêche		I am going to go fishing
Je vais faire de la planche à voile		I am going to go windsurfing
Je vais faire de la voile		I am going to go sailing
Je vais faire du camping		I am going to go camping
Je vais faire du canoë		I am going to go canoeing
Je vais faire du VTT 		I am going to go mountain biking
Je vais faire de l'équitation		I am going to go horseriding
Je vais lire		I am going to read
Je vais rester		I am going to stay
Je vais visiter des châteaux et des musées		I am going to visit castles and museums
un camping		a campsite
la mer		the sea
la plage		the beach
le soleil		the sun
au bord de la mer		at the seaside
plein de bouquins		loads of books
ennuyeux		boring
passer des heures		to spend hours
passer une semaine		to spend a week
passer les vacances		to spend the holidays
le soir		the evening
car		becau