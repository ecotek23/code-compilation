avere bisogno di ...		to need ...
aiuto		help
ho bisogno di aiuto		I need help
dire		to say; to tell
come si dice ... in italiano?		how do you say ... in Italian?
capire		to understand
capisci?		do you understand?
non capisco		I don't understand
conoscere		to know (a person, a thing); to meet (for the first time)
sapere		to know (a fact)
so		I know (a fact)
non lo so		I don't know
felice di aiutarti		happy to help (you)
il piacere		the pleasure
piacere di conoscerti		pleasure to meet you
è grandioso!		that's great!
qui		here
eccoti qui		here you go
benvenuto		welcome
la fortuna		the luck; the fortune
buona fortuna!		good luck!
l'Italia		Italy
gli Stati Uniti		the United States
l'Inghilterra		England
il Galles		Wales
parlare		to speak; to talk
venire		to come
dove		where
un po'		a little; a bit
ovviamente		obviously
da dove vieni?		where are you from?
vengo dall'Inghilterra		I'm from England
vengo dal Galles		I'm from Wales
vieni dall'Inghilterra?		are you from England?
vieni dal Galles?		are you from Wales?
parlo inglese		I speak English
parlo gallese		I speak Welsh
parli italiano?		do you speak Italian?
parlo un po' di italiano		I speak a little Italian
ovviamente parlo inglese		of course I speak English
ovviamente parlo gallese		of course I speak Welsh
parli italiano molto bene		you speak Italian very well
il piatto		the plate; the dish
il bicchiere		the glass
la tazza		the cup
la bottiglia		the bottle
il latte macchiato		the latte
l'antipasto		the starter
il secondo		the main course; the second
il dolce		the dessert
vorrei		I would like
vorresti		you would like (singular)
desideri ... ?		would you like ... ? (singular informal)
del (dei; della; delle)		some; any; of
la cosa		the thing
qualcosa		something; anything
niente		nothing; anything
tutto (tutta; tutti; tutte)		everything; everyone; all
cosa desideri?		what would you like? (singular informal)
vorrei una tazza di tè per favore		I would like a cup of tea please
cosa desideri da mangiare?		what would you like to eat?
desideri una bottiglia o un bicchiere?		would you like a bottle or a glass?
vorrei del latte per favore		I would like some milk please
avete dei dolci?		do you have any desserts?
non ho niente		I don't have anything
non ho acqua		I don't have any water
ovviamente ho dell'acqua		of course I have some water
undici		eleven; 11
dodici		twelve; 12
tredici		thirteen; 13
quattordici		fourteen; 14
quindici		fifteen; 15
sedici		sixteen; 16
diciassette		seventeen; 17
diciotto		eighteen; 18
diciannove		nineteen; 19
venti		twenty; 20
il mercato		the market
il bancomat		the ATM
la farmacia		the pharmacy
l'edicola		the kiosk
la gente		(the) people
il supermercato		the supermarket
l'automobile		the car
il taxi		the taxi
la banca		the bank
il libro		the book
la libreria		the bookcase; the bookshop
il cliente; la cliente		the customer; the client
volere		to want
comprare		to buy
vendere		to sell
pagare		to pay
tirare		to pull
spingere		to push
ci		us; ourselves; there
c'è		there is
ci sono		there are
molti		many
pochi		few
sempre		always
mai		never; ever
fare		to do; to make
fare la spesa		to go shopping (for day-to-day need)
fare acquisti		to go shopping (general)
andiamo a fare acquisti		let's go shopping
andiamo al mercato		let's go to the market
voglio comprare qualcosa per te		I want to buy something for you
c'è una grande farmacia		there is a big pharmacy
ci sono molte persone		there are many people
c'è un bancomat?		is there an ATM?
non ci sono banche		there are no banks
ci sono molte piccole librerie		there are many small bookshops
il cliente ha sempre ragione		the customer is always right
la bevanda		the drink
preferito		favourite
questo (questa)		this; this one
quello (quella)		that; that one
in realtà		actually
o		or
gli piace il caffè		he likes coffee
non gli piace il pesce		he doesn't like fish
non le piacciono il tè o il caffè		she doesn't like tea or coffee
non c'è cibo		there is no food
c'è della zuppa?		is there any soup?
sono i miei preferiti		they're my favourite
il caffè è la mia bevanda preferita		coffee is my favourite drink
in realtà mi piace		I actually like it
questo è delizioso		this is delicious
cos'è quello?		what's that?
ovviamente gli piace l'insalata		of course he likes salad
la famiglia		the family
la mamma		the mum
il papà		the dad
il genitore		the parent
la sorella		the sister
il fratello		the brother
il nonno; la nonna		the grandpa; the grandma
i nonni		the grandparents; the grandpas
il figlio; la figlia		the son; the daughter
l'amico; l'amica		the friend
il fidanzato; la fidanzata		the boyfriend; the girlfriend
il marito		the husband
la moglie		the wife
il bambino; la bambina		the child; the kid
il ragazzo; la ragazza		the boy (the guy); the girl
l'uomo		the man
la donna		the woman
l'adulto; l'adulta		the adult
il neonato; la neonata		the baby
la persona		the person
la scuola		the school
l'ufficio		the office
il lavoro		the job; the work
lavorare		to work
chi		who (questions)
in		in
chi sono loro?		who are they?
non conosco quella persona		I don't know that person
questa è mia mamma		this is my mum
è mio fratello		he's my brother
questi sono mia mamma e mio papà		this is my mum and my dad
quella non è mia figlia		that isn't my daughter
questo è il mio amico italiano		this is my Italian friend
ha una fidanzata?		does he have a girlfriend?
il tuo fidanzato ha un lavoro?		does your boyfriend have a job?
lavora in ufficio		he works in an office
il colore		the colour
rosso		red; ginger
blu		blue
giallo		yellow
verde		green
nero		black
rosa		pink
viola		purple
bianco		white
arancione		orange
marrone		brown
grigio		grey
chiaro		light (colour)
scuro		dark
verde chiaro		light green
blu scuro		dark blue
scusi		excuse me (formal)
dov'è ... ?		where is ... ?
l'ospedale		the hospital
dov'è l'ospedale?		where is the hospital?
eccolo qui		here it is
solo un po'		just a little
abbastanza		enough; quite; pretty; fairly
ne ho abbastanza!		that's enough!
possibile		possible
è possibile		it's possible
è possibile?		is it possible?
impossibile		impossible
è impossibile!		it's impossible!
un peccato		a sin; a shame
che peccato!		what a shame!
gli abiti		the clothes
i pantaloni		the trousers
la camicia		the shirt
la maglietta		the T-shirt
la gonna		the skirt
il vestito		the dress
gli shorts		the shorts
il cappotto		the coat
la giacca		the jacket
la felpa		the jumper
la sciarpa		the scarf
il cappello		the hat
il completo		the suit
i calzini		the socks
le scarpe		the shoes
le pantofole		the slippers
gli stivali		the boots
le scarpe da ginnastica		the trainers
i guanti		the gloves
le mutande		the pants
l'ombrello		the umbrella
la borsa		the bag
il portafogli		the wallet
dovere		to have to ... ; must
devo		I have to; I must
devi		you have to ... ; you must  (singular)
fare pipì		to pee
nuovo		new
vecchio		old
chiuso		closed
aperto		open
vuoi una camicia nuova?		do you want a new shirt?
vuole i pantaloni blu		he wants the blue trousers
cosa vuoi comprare?		what do you want to buy?
voglio comprare dei vestiti		I want to buy some clothes
devo fare pipì		I have to pee
devo comprare un ombrello		I have to buy an umbrella
le nocciole		the nuts
i dolci		the sweets
il cioccolato		the chocolate
la torta		the cake
la pizza		the pizza
i pop corn		the popcorn
lo zucchero		the sugar
il sale		the salt
il pepe		the pepper
buono		good
cattivo		bad
vegetariano		vegetarian
piccante		hot (food); spicy
caldo		hot (temperature); heat
freddo		cold
allergico		allergic
dipendente		addicted; dependent
ti piacciono i dolci?		do you like sweets?
è molto buono		it's very good
sono vegetariano		I'm vegetarian
la zuppa è calda?		is the soup hot?
è vegetariano?		is it vegetarian?
è allergico alle nocciole		he's allergic to nuts
siamo dipendenti dallo zucchero		we're addicted to sugar
l'umano		the human
l'animale		the animal
il cane		the dog
il gatto		the cat
l'uccello		the bird
la mucca		the cow
il maiale		the pig
il coniglio		the rabbit
il serpente		the snake
il leone		the lion
l'elefante		the elephant
il cavallo		the horse
la pecora		the sheep
la scimmia		the monkey
il cagnolino		the puppy
il micino		the kitten
la giraffa		the giraffe
l'asino		the donkey
il topo		the mouse
fedele		loyal; faithful
coraggioso		brave
intelligente		clever; intelligent
stupido		stupid
alto		tall; high
basso		short; low
libero		free; spare
tranquillo		quiet
fedele come un cane		loyal as a dog
il giorno		the day
la settimana		the week
il mese		the month
l'anno		the year
giovane		young
quanti (quante)		how many
quanto (quanta)		how much
questo è il mio cane		this is my dog
quanti anni ha?		how old is he?
ha dodici anni		he's twelve years old
il mio gatto è molto vecchio		my cat is very old
tu sei molto giovane		you're very young
l'appartamento		the flat; the apartment
la casa		the house
la città		the city
Roma		Rome
Milano		Milan
Londra		London
Washington		Washington (D.C.)
New York		New York
vivere		to live
perché		why; because
così		so; thus; such
vivono a Milano		they live in Milan
ha una casa a Roma		she has a house in Rome
sei sempre felice		you're always happy
non è mai felice		he's never happy
perché sei così triste?		why are you so sad?
... perché sei ammalato		... because you're sick
sono arrabbiato perché ho fame		I'm angry because I'm hungry
è un po' triste perché non ha una fidanzata		he's a little sad because he doesn't have a girlfriend
trenta		thirty; 30
quaranta		forty; 40
cinquanta		fifty; 50
sessanta		sixty; 60
settanta		seventy; 70
ottanta		eighty; 80
novanta		ninety; 90
cento		one hundred; 100
ventiquattro		twenty-four; 24
quarantasei		forty-six; 46
trecento		three hundred; 300
il tempo		the time; the weather
l'ora		the hour
il minuto		the minute
l'appuntamento		the appointment; the date (romantic)
la riunione		the meeting
in punto		o'clock
la mattina		the morning
il pomeriggio		the afternoon
la sera		the evening
la notte		the night
del pomeriggio		in the afternoon (p.m.)
del mattino		in the morning (a.m.)
adesso		now
proprio		really; right; (my) own
proprio adesso		right now
che ora è?		what time is it?
sono le due in punto		it's two o'clock
sono le otto e trenta		it's eight thirty
sono le sette e venti		it's twenty past seven
sono dieci alle due		it's ten to two
sono le cinque e sei minuti		it's six minutes past five
quand'è il tuo appuntamento?		when is your date?
a che ora?		at what time?
a dieci alle quattro		at ten to four
all'una del pomeriggio		at one in the afternoon
avere cura		to take care
intendere		to intend; to mean (to say or do)
sai cosa intendo?		do you know what I mean?
il problema		the problem
qual è il problema?		what's the problem?
qualcuno (qualcuna)		someone; anyone
nessun (nessuno; nessuna)		no (none); no one; anyone
nessun problema		no problem
preoccuparsi		to worry (oneself)
non preoccuparti		don't worry
importare		to matter; to import
non mi importa		I don't care
ops!		oops!
oh no!		oh no!
davvero!		indeed!
congratulazioni!		congratulations!
evvai!		ya