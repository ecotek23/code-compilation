###
#
# The base code for the download is cloned from this repo (https://github.com/iiLaurens/getMemriseCourse), I added in the argument parsing to make it easier to use from the command line,
# and I made some small changes to the download code to account for the changes in the site over the last year
#
###

# Download all the accounts courses (MINE)

In the download_all_courses code, you can provide an account url where it displays your courses, and it will then download them all to the current working directory.

# Download Memrise Course
Grab a complete Memrise course by ID and save all words of every level into a text file

This works for Python 2.7 and probably also on Python 3 (try update the Print statement, untested) and requires the following packages:

Requests, lxml, BeautifulSoup

Simply use the get_course(id) function in the download_course.py file with a valid Memrise course ID.

example output (requires Korean alphabet):
```
오늘		Today
날씨		Weather
있다		To have / To exist (Plain)
없다		To not have / To not exist (Plain)
친구		Friend
시간		Time
재미		Fun / Interesting
```
