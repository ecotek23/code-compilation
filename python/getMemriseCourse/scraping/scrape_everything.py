##
##
# Deal with urls like https://www.memrise.com/join/?skip_onboarding=/course/574794/kanji-1er-ano-nivel-10-vocabulario/74/garden/learn/
# Use a regular expression to separate out the two sides and get the /course/NUMBER/ section
##
##

from download_course import *
import scrapy
import re

reg_exp = re.compile('/course/([0-9]+/.+?/)')

class MemriseSpider(scrapy.Spider):
	name = 'memrisespider'
	start_urls = ['http://www.memrise.com','http://www.memrise.com/courses/english/']
	allowed_domains = ['www.memrise.com']
	scraped = []

	def parse(self,response):
		self.scraped.append(response.url)
		print 'Currently on {}'.format(response.url)
		if '/course/' in response.url:
			url = re.findall(reg_exp,response.url)[0]
			get_course(url)
		for next_page in response.css('a ::attr(href)').extract():
			if next_page.startswith('http'):
				if next_page in self.scraped:
					continue
				yield scrapy.Request(next_page,callback=self.parse)
			else:
				if response.urljoin(next_page) in self.scraped:
					continue
				yield scrapy.Request(response.urljoin(next_page),callback=self.parse)