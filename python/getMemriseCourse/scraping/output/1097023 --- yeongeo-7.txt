a department		부서
a diamond		다이아몬드
to present		(사람에게) 주다; 증정하다
to report		보고하다
to represent		대표하다
to contain		포함하다
... the man who reported the problem		문제를 보고했던 그 남자 ~
... the box which contained the diamonds		다이아몬드들이 담겨 있는 상자 ~
... the letter which I wrote this morning		... 내가 오늘 아침에 쓴 그 편지
... the man whose sister works for the president		... 누나가 대통령을 위해 일하는 그 남자

electronics		전자공학; 전자제품
a store		가게
industry		산업
fashion		패션
service		서비스
credit		신용; 잔액
to rise		증가하다
to shake		흔들다
to nod		끄덕이다
to enter		들어가다
to be out of ...		~ 남아있지 않다
to protect		보호하다
to accept		받아들이다
to seek		찾다
to state		정식으로 말하다
to note		언급하다
to indicate		신호를 보내다; 나타내다
to claim		(~이 사실이라고) 주장하다
to mention		언급하다
only		단지
at noon		정오에
at midnight		한밤중
I told you I was out of credit on my phone		내 폰에 잔액이 남아있지 않았다고 네게 말했잖아
they claim they were only protecting the children		그들은 그들이 단지 아이들을 보호하는 중이었다고 주장해요
the king and queen stated that they were going to be parents		왕과 왕비는 그들이 부모가 될거라고 정식으로 말했어요
he noted that the electronics store was closed at noon		그는 전자제품 매장이 정오에 닫혀있었다고 언급했어요
the evidence indicates that you're guilty		증거는 네가 유죄임을 나타낸다

an administration		행정
an organisation		기관
an issue		문제
a rate		비율
a percent		퍼센트
a contract		계약
a company		회사
a CEO		최고경영자
a term		기간
terms and conditions		기간과 조건 (복수형)
to sign		사인하다
to develop		성장하다; 개발하다
to develop at a ... rate		~율로 성장하다
to decide		결정하다
to grow		자라다
to grow by ... percent		~퍼센트로 증가하다
governmental		정부의
private		개인의
public		공적인
the public		일반 국민
legal		합법의
illegal		불법의
commercial		상업적인
fast		빠른
at all		(부정문에서) 조금도 ~ 아니다

an engineer		엔지니어
a program		프로그램
a computer program		컴퓨터 프로그램
a website		장소; 사이트
a screen		화면
a model		모범; 모델
a version		버전
data		데이터
internet		인터넷
online		온라인
offline		오프라인

a list		목록
a shopping list		쇼핑 목록
a clock		시계
a process		과정
costs		경비
to treat		(특정 태도로) 대하다; 취급하다
to set		(특정 위치) 놓다; 설정하다
to add		첨가하다
to involve		참여시키다
to check		확인하다
to check if ...		~인지 확인하다
correctly		정확하게
yet		아직; 이미
since		그때부터 지금까지 쭉
by now		지금까지
have you set the clock correctly?		시계를 정확히 맞췄니?
have you added the butter yet?		너는 버터를 이미 넣었니?
have you considered getting glasses?		너는 안경 사는 것을 고려했었니?
did you check if we bought everything on the shopping list?		너는 우리가 쇼핑목록에 있는 것들을 모두 다 샀는지 확인했니?
we should have met her by now		우리는 지금쯤은 이미 그녀를 만났어요 해요
have you heard from her since yesterday?		너는 어제부터 지금까지 그녀에게 연락 받은거 있니?
he has always treated her well		그는 그녀에게 언제나 잘 해요

a response		대답
to respond		대답하다
an email		이메일
an article		(신문이나 잡지의) 글
an article on ...		~에 관한 글
to reveal		(비밀등이) 드러나다
a secret		비밀
a population		인구
social		사회의
media		미디어
social media		소셜 미디어
to post		발송하다; 게시하다
growth		증가
a fund		펀드
human		사람의
a right		권리
a human right		사람의 권리
to publish		출판하다
to set ... up		~ 세우다; ~ 제공하다
to establish		규명하다; 설립하다
to maintain		유지하다
to keep ... to oneself		~ 비밀을 지키다
personal		개인적인
powerful		영향력 있는

Earth		지구
a star		별
an astronaut		우주비행사
a planet		행성
a universe		우주
a spaceship		우주선
an alien		외계인
outer space		(대기권 외) 우주 공간
to reach		도착하다
to explore		탐사하다
to discover		발견하다
different		다른
unknown		잘 알려지지 않은
if I could fly, I could reach the stars		만일 내가 날수 있었다면 나는 별에 도착할 수 있었을 거예요
if I were an astronaut, I could go to outer space		만일 내가 우주비행사였다면 나는 우주에 갈수 있었을 거예요
if I had a spaceship, I would fly to the edge of the universe		만일 내가 우주선이 있었다면 나는 우주의 끝으로 날아갔을 거예요

an institution		기관
a cause		원인
an effect		효과
to seem		~처럼 보이다
to be in the way		방해가 되다
original		원래의
basic		기본의
not very ...		별로
he isn't very clever, is he?		그는 별로 영리하지 않아요, 그렇지 않아요?
he's always in the way, isn't he?		그는 항상 방해가 돼요, 그렇지 않아요?
the treatment doesn't seem to have an effect, does it?		치료는 효과기 있는거 같지 않아요, 그렇지 않아요?
he seems to be a clever politician, doesn't he?		그는 현명한 정치인인거 같아요, 그렇지 않아요?

cat got your tongue?		왜 입을 다물고 있니?
the cat is out of the bag		비밀이 새어 나가다
it's raining cats and dogs		비가 억수같이 내리네요
to bark		짖다
all bark and no bite		입만 살았어요
a doghouse		개집
he was in the doghouse		그는 미움을 샀었어요
Rome		로마
when in Rome ...		로마에서는 ~
to lead		(길이) 통하다
all roads lead to Rome		모든 길은 로마로 통한다
to build		건축하다
Rome wasn't built in a day		로마는 하루에 만들어지지 않았다
to fall		떨어져요
the apple never falls far from the tree		부전자전
the past		과거
the present		현재
the future		미래
to put		놓다
put your past behind you		지나간 과거는 잊어버려라

an authority		권한
the authorities		당국
a seat belt		안전벨트
steep		적시다
a price		가격
a steep price		부당한 가격
inflation		인플레이션
to prepare		준비하다
to hire		고용하다
to cause		~을 야기하다
to solve		해결하다
to produce		생산하다
to improve		향상하다
to require		요구하다
to create		창조하다
to identify		확인하다
to raise a child		아이를 키우다
cruel		잔혹한
severe		극심한
recently		최근에
sufficiently		충분히
the hospital was closed by the authorities		병원은 당국에 의해 문을 닫았어요
the case has already been solved by the police		사건은 경찰에 의해 이미 해결되었어요

a decision		결정
an agreement		합의
to reach an agreement		합의에 도달하다
a majority		다수
a minority		소수
a statement		성명
an approach		접근법
a speech		연설
a society		사회
a budget		예산
importance		중요성
trade		무역
land		땅
an owner		소유자
a land owner		땅 주인
an address		주소
to address		연설하다
a threat		위협
to threaten		협박하다
national		국가의
international		국제적인
global		세계적인
central		중심의
religious		종교적인
environmental		환경의
general		일반적인
mainly		중요하게
throughout		계속해서 내내

a test		테스트
an exam		시험
a report		리포트
a standard		기준
an opportunity		기회
missed		그리운
able		가능한
unable		불가능한
to pass		지나가다; 건네주다; 통과하다
to fail		실패하다

a role		역할
a character		캐릭터
a challenge		도전
a task		업무
a project		프로젝트
communication		의사소통
a skill		기술
an attitude		태도
a position		지위
problem solving		문제해결
confidence		자신감
responsibility		책임감
management		경영; 관리
an agent		에이전트
an agency		대행사
a client		의뢰인
a network		네트워크
a partner		파트너
an employee		고용인
friendly		친근하게
civil		시민
the civil service		대민 업무
to provide		제공하다
to manage		경영하다
to achieve		성취하다
to expect		기대하다
to offer		제공하다
a pleasure		즐거움
pleasure to meet you		만나서 반가워요

an army		부대
the forces		군대
senior		상위의
an officer		고위 공무원; 임원
an exercise		운동
an enemy		적군
a priority		우선사항
troops		병력
an area		지역
security		보안
training		훈련
to cover		덮다; 가리다
to serve		제공하다
to push ... over the edge		~ 벼랑 끝으로 밀다
to go through		~을 통하여 가다
physical		육체의

research		조사하다
an analysis		분석
a method		방법
a conference		학회
a scientist		과학자
a part		부분
a participant		참가자
an individual		개인
individual		개인의
a race		경주; 인종
a species		생물 분류의 기초단위인 종
this time		이번은
next time		다음에
a set		세트
to test		테스트하다
to base		~에 근거지를 두다
similar		비슷한
carefully		조심스럽게
once again		다시 한번
twice more		두번 더

a side		가장자리
a note		메모
to plan		계획하다
to join		참가하다
to focus		집중하다
to deal with ...		~을 처리하다
equally		동일하게
if you had been there, I would have heard your side of the story as well		만일 당신이 그곳에 있었다면 나는 당신의 입장 역시 들었을 거예요
if you had only written a note, I wouldn't have been so worried		만일 당신이 단지 메모만 했었다면 나는 그렇게 걱정하진 않았을 거예요
if he had dealt with his problems, he would have been alright		만일 그가 그의 문제들을 처리했었다면 그는 괜찮았을 거예요

a use		사용
a page		페이지
a level		단계
a priest		사제
a tyre		타이어
heat		열기
leftovers		남은 음식
a top		정상
a collection		수집품
fire		불
a fireman		소방관
a solicitor		사무 변호사
a change		변화
a machine		기계
a performance		공연
to end		끝나다

to belong		제자리에 있다; ~에 속하다
to wish		소망하다
to apply		지원하다
to burn		태우다
to head to ...		~을 향하여 가다
to head off		떠나다
to hang		걸다
to hang up		전화를 끊다
to pick up		전화를 받다
ridiculous		말도 안 되는
spare		남는
intense		극심한
even		~조차; 훨씬
ever		언제나 (부정문과 의문문)
perhaps		아마도
exactly		정확히
some day		언젠가
in time		제시간에
for fun		재미를 위해

violence		폭력
safety		안전
a trial		재판
a court		코트; 법정
in court		법정에서
on trial		재판 중에
until		~까지
a neighbourhood		이웃사람들; (도시) 지역
to attack		공격하다
to force		강요하다
to concern		걱정하다; 관련되다
to find out		생각해 내다

either you love it or you hate it		당신이 그것을 사랑하거나 그것을 몹시 싫어하거나
a situation		상황
it's a win-win situation		그것은 서로에게 좋은 상황이에요
same, but different		거의 비슷하지만 다른
that's life		사는게 다 그런거지 뭐
learn to love it		그것을 사랑하는 법을 배워요
to jinx		징크스가 있어요
don't jinx it		그것에 징크스 생기지 마세요
tough luck		거 참 운도 없군요
to smash		박살내다
smash it!		한 번 잘 해봐요!
onwards and upwards		계속 올라가고 있어요

production		생산
a product		제품
a unit		구성단위
an item		항목
an object		물건
a stock		재고
in stock		재고로
a style		스타일
a type		타입
a design		디자인
wide		넓은
narrow		좁은
a range		범위
plastic		플라스틱
a plant		식물
an end		끝
a turn		차례
an option		선택사항
to look for ...		~을 바라다
particular		특정한
specific		구체적인
massive		거대한
no longer		더 이상 ~ 아닌

a board		이사회
a board member		회사 임원
a firm		회사
an income		수입
a consumer		소비자
a benefit		혜택
a drawback		결점
room		방
a development		발달
an investment		투자
a loss		손실
an event		이벤트
a decade		(주로 기간을 뜻하는) 10년
to experience		경험하다
to stick to ...		~을 계속하다
to seem like ...		~처럼 보이다
to be concerned about ...		~에 대하여 걱정하다
annual		매년
financial		재정의
significant		중요한
cultural		문화의
recent		최근의

a sentence		문장
a full stop		마침표
a comma		쉼표
a caretaker		수위
to roll		구르다
to turn into ...		~으로 변하다
a disaster		재난
a stage		단계
to perform		수행하다
to affect		~에 영향을 미치다
to reflect		비추다
to exist		존재하다
to tend		~하는 경향이 있다
to remain		남아있다
to fill		채우다
to form		형성되다
to deal		나누어 주다; 거래하다
to go wrong		실수하다
for ages		오랫동안

the press		언론
a press conference		기자회견
a source		원천
information		정보
a movement		사람들이 조직적으로 벌이는 운동
a reason		이유
a campaign		캠페인
to run a campaign		캠페인을 운영하다
a generation		세대
political		정치적인

an olive		올리브
oil		오일
a rat		쥐
poison		독
an emotion		감정
to happen		발생하다
to notice		알아차리다
to relate		관련 시키다
to prevent		예방하다
to stare		응시하다
to refer to ...		~에 대해 언급하다; ~을 참조하다
to bake		굽다
to remove		제거하다
to name		이름 지어주다
to run out of ...		~을 다 써버리다
however		그러나
thus		이와같이

a reform		개혁
a community		커뮤니티
an impact		강력한 영향
a nation		국가
a union		조합
to strike		때리다; 파업하다
property		재산
a property owner		재산 소유인
transport		교통수단
social security		사회보장
local		지역의
educational		교육적인
economic		경제의
major		중요한
minor		중요하지 않은
on-site		현장의
a theory		이론
in theory		이론상으로는
in practice		실제로는

an activity		활동
a figure		몸매; 수치
a purpose		목적
to serve a purpose		도움이 되다
a series		시리즈
an account		계좌
a direction		방향
to organise		조직하다
dramatically		극적으로
directly		곧장
nearly		거의
entirely		전적으로
clearly		분명히

a course		강의
a course of action		행동 방침
a defence		방어
a strategy		전략
a resource		자원
a mission		임무
support		지원
to determine		결정하다
to act		행동하다
to admit		인정하다
to complete		완료하다
to run low on ...		~이 적어지다
medical		의학의
current		현재의

a size		사이즈
paint		페인트
a painting		그림
a behaviour		행동
reality		현실
the rest		나머지
to smile		웃다
to release		풀어 주다
to discuss		토론하다
to matter		문제되다
to make it		성공하다; 시간안에 해내다
random		랜덤; 무작위의
a relation		관련성
in relation to ...		~와 관련된
a surface		표면
on the surface		외견상으로는
a field		들판;  지역
in the field		현장에서; (스포츠) 경기에 출전하여

a concept		개념
a study		공부
a look		쳐다 봄
a factor		요인
knowledge		지식
to analyse		분석하다
to compare		비교하다
outdated		구식인
updated		최신의
effective		효과적인
critical		비판적인
in detail		상세하게

an amount		총액
a section		구획
an act		연극등의 막; 공연
a stain		얼룩
an engine		엔진
a factory		공장
a worker		근로자
quite		꽤
simply		단순하게
absolutely		전적으로
certainly		틀림없이
particularly		특별히
large		큰
beyond		저편에
to be beyond ...		~ 을 초월해요

a citizen		시민
freedom		자유
an opinion poll		여론조사
a region		지역
a policy		정책
northern		북쪽의
southern		남쪽의
eastern		동쪽의
western		서쪽의
convincing		설득력 있는
federal		연방제의
modern		현대의
traditional		전통적인

memory		기억
a memory		추억
useful		유용한
a base		근거지
a measure		측정
a structure		건축물
space		공간
soil		흙
a treasure		보물
to realise		알아차리다
to think highly of ...		~을 높이 평가하다
weight		몸무게
to lose weight		살이 빠지다
to gain weight		살이 찌다
eventually		결국

enormous		막대한
per		~ 마다
an audience		청중
a pair		한쌍의
main		중요한
various		다양한
generally		일반적으로; 보통
a charge		요금
to include		포함하다
to exclude		제외하다
to continue		계속하다
to point out		강조하다
a sender		발송자
to allow		허락하다
to be allowed		허락 받다; 허락되다
to hold		잡다
to place		(물건을) 두다
to cheat		속이다; 바람을 피우다

a needle		바늘
a haystack		건초더미
it's like looking for a needle in a haystack		그것은 건초더미에서 바늘 찾기와 같다
a bird in the hand ...		손안의 새 ~
curiosity		호기심
curiosity killed the cat		호기심이 지나치면 위험해요
to spill		쏟다
don't cry over spilt milk		이미 엎질어진 물이다
to judge		판단하다
a cover		겉
don't judge a book by its cover		겉을 보고 속을 판단하지 말라
a gift		선물
don't look a gift horse in the mouth		남의 호의를 트집잡지 말라
an omelette		오믈렛
you can't make an omelette without breaking eggs		중요한 일을 이루는 데는 사소한 문제들이 따르기 마련이에요
a lining		안감
every cloud has a silver lining		고생 끝에 낙이 온다
dawn		새벽
it's always darkest just before the dawn		동트기 직전이 가장 어둡다

material		재료
a pattern		패턴
a bar		술집
fear		무서움
a rock		바위
access		접근
an official		공무원
a voice		목소리
gas		가스
a director		임원
a group		그룹
to cut		자르다
to design		디자인 하다
to appear		나타나다
to step		움직이다
to show up		눈에 띄다; 나타나다
born		타고난
entire		전체의
key		핵심적인
special		특별한
neither ... nor		~도 ~도 아니다

well done!		잘 했어요!
you made it!		네가 해냈구나!
you smashed it!		(너) 참 잘했어요!
you nailed it!		너 해냈구나!
you rock!		너는 최고야!
let's celebrate!		축하합시