il sentimento		the feeling; the emotion
l'errore		the mistake; the error
commettere		to commit
commettere un errore		to make a mistake
discutere		to argue; to discuss; to fight
perdonare		to forgive
avere intenzione di ...		to mean to ... ; to intend to ...
urlare		to yell; to scream
spaventare		to scare
turbare		to upset
ferire		to hurt (someone)
ho commesso un errore		I made a mistake
l'hai ferito		you hurt him
ovviamente ha commesso un errore		she obviously made a mistake
non era sua intenzione		he didn't mean it
non aveva intenzione di ferirti		she didn't mean to hurt you
penso di aver ferito i suoi sentimenti		I think I hurt his feelings
perdonami		forgive me
sono sicuro che tutto andrà bene		I'm sure everything will be alright

il membro		the member
il fan club		the fan club
il club sportivo		the sports club
la stagione		the season
il torneo		the tournament
l'associazione		the association
il campionato		the championship; the league
iniziare		to start; to begin
finire		to finish; to end
tifare		to cheer; to support (a team)
diventare		to get; to become; to turn into ...
la stagione inizia sempre ad agosto		the season always begins in August
voglio diventare un membro del fan club		I want to become a member of the fan club
se vincono questa partita vinceranno il campionato		if they win this match they'll win the league
per quale squadra tifi?		which team do you support?

al momento		at the moment
stiamo cenando in giardino proprio adesso		we're having dinner in the garden right now
stanno parlando italiano al momento		they're speaking Italian at the moment
stava piovendo quando ci fermammo		it was raining when we stopped
stanno arrivando a Roma proprio adesso		they're arriving in Rome right now
si sta rasserenando mentre ne parliamo		it's clearing up as we speak
il dentista le sta guardando il dente proprio adesso		the dentist is looking at her tooth right now
mio papà stava parlando con mia mamma quando arrivai a casa		my dad was speaking to my mum when I came home
pioveva dove viviamo		it was raining where we live

di chi		whose (in questions)
di chi è questo cuscino?		whose pillow is this?
quali stivali sono i miei?		which boots are mine?
questa è la mia penna		this is my pen
questa penna è mia		this pen is mine
sono queste le mie mutande?		are these my pants?
queste mutande sono mie		these pants are mine
quelli sono i tuoi orecchini		those are your earrings
sono tuoi quegli orecchini?		are those earrings yours?
penso che questo braccialetto sia di mia mamma		I think this bracelet is my mum's
quella non è la collana di mia nonna		that isn't my grandma's necklace
questa è la casa di Marco adesso		this is Marco's house now

la scelta		the choice
saggio		wise
quella fu una scelta saggia		that was a wise choice
naturale		natural
era la scelta naturale		it was the natural choice
quella fu la prima scelta anche per me		that was my first choice as well
la qualità		the quality
la quantità		the amount
duro		hard; tough
questi sono tempi duri		these are tough times
supporre		to suppose
suppongo di sì		I suppose so
stai seguendo?		are you following?
beccato		got you
la questione		the matter; the issue
questa è una questione di opinione		this is a matter of opinion
la capacità		the ability
il meglio di ...		the best of ...
al meglio delle mie capacità		to the best of my ability
il costo		the cost
a ogni costo		at any cost
è in vantaggio		he's ahead of the game

per il bene comune		for the greater good
meno è meglio		less is more
chi la fa l'aspetti		what goes around comes around
l'ignoranza		(the) ignorance
beato		blessed
beata ignoranza		ignorance is bliss
la pratica		the practice
la pratica rende perfetti		practice makes perfect
arrischiarsi		to venture; to risk (oneself)
rosicare		to nibble
chi non risica non rosica		nothing ventured, nothing gained

la bugia		the lie
la verità		the truth
essere d'accordo		to agree
andare d'accordo		to get along
mentire		to lie
vero		true
falso		false
ingiusto		unfair
sciocco		silly
non vanno d'accordo		they don't get along
parla con me		talk to me
non siamo d'accordo		we don't agree
non sono d'accordo con te		I don't agree with you
non mi ascolta mai		she never listens to me
questo non è giusto		this isn't fair
non mentirmi		don't lie to me
questo è proprio ingiusto		this is really unfair
questa è semplicemente una discussione sciocca		this is just a silly argument
e va bene		fair enough

l'ordine		tidiness; the order
pubblico		public
ordine pubblico		law and order
riconoscere		to recognise
descrivere		to describe
indicare		to point; to indicate
in mezzo		in the middle
sulla sinistra		(positioned) on the left
sulla destra		(positioned) on the right
o ... o		either ... or
è un ordine!		it's an order!
penso sia stato il ragazzo sulla sinistra		I think it was the guy to the left
può fare un disegno di lui?		can you draw a picture of him?
o descriva o disegni		either describe or draw
può descrivere la donna o fare un disegno		you can either describe the woman or draw a picture

il consiglio		the (piece of) advice
il messaggio		the message
l'attenzione		the attention
prestare attenzione		to pay attention
lamentarsi		to complain (oneself)
dimenticare		to forget
diverso		different; diverse
stesso		same
io stesso (me stesso; io stessa; me stessa)		myself
tu stesso (te stesso; tu stessa; te stessa)		yourself
se stesso (se stessa)		himself (herself); oneself
dare		to give
lascia che ti dia ...		let me give you ...
lascia che ti dia un consiglio		let me give you some advice
dovresti ascoltare te stesso		you should listen to yourself
dovresti semplicemente dimenticarlo		you should just forget him
dovresti perdonarlo		you should forgive him
non deve raccontarlo a nessuno		she mustn't tell anyone
dovrei perdonare il mio fidanzato?		should I forgive my boyfriend?
smetti di preoccuparti		stop worrying

il fatto		the fact
affrontare		to face
affronta i fatti		face the facts
affrontiamolo ...		let's face it ...
chiedersi		to wonder (oneself)
mi chiedo ...		I wonder ...
mi è venuto in mente ...		it occurred to me ...
la fine		the end
in fin dei conti ...		all things considered ...
la cronaca		the news; the report
per la cronaca ...		for the record ...
oggi giorno ...		in this day and age ...
la condizione		the condition
a una condizione ...		on one condition ...
il volume		the volume
spegnere		to turn off
accendere		to turn on
alzare		to turn up; to raise
non avere possibilità		to not stand a chance
avere una possibilità di ...		to have a chance of ...
probabile		likely; probable
improbabile		unlikely; improbable
spegni la TV		turn the TV off
non hanno nessuna possibilità		they don't stand a chance
la squadra di mio fratello ha buone possibilità di vincere		my brother's team have a pretty good chance of winning
potrebbero vincere il campionato, ma è improbabile		they might win the league, but it's unlikely
penso che la mia squadra possa vincere domani		I think my team may win tomorrow

l'omicidio		the murder
l'assassino; l'assassina		the murderer
uccidere		to kill
la rapina		the robbery
il rapinatore; la rapinatrice		the robber
rapinare		to rob
il furto		the theft
il sospettato; la sospettata		the suspect
il testimone; la testimone		the witness
la pistola		the gun; the pistol
la maschera		the mask
la droga		the drug
l'arma		the weapon
l'arma del delitto		the murder weapon
la scena del crimine		the crime scene
la vittima		the victim
la tasca		the pocket
pugnalare		to stab
sparare		to shoot; to fire
cogliere		to catch
in flagrante		red-handed
riuscire		to manage (doing something)
in pieno giorno		in broad daylight
fu una rapina		it was a robbery
fu un omicidio		it was murder
il testimone vide il sospettato		the witness saw the suspect
rapinò la banca in pieno giorno		he robbed the bank in broad daylight
la polizia colse l'assassino in flagrante		the police caught the murderer red-handed
sfortunatamente la polizia non riuscì a trovare l'arma del delitto		unfortunately the police didn't manage to find the murder weapon
è un furto!		it's daylight robbery!

l'albero		the tree
la cascata		the waterfall
il suolo		the ground
la strada		the road
il cartello		the sign; the board
il cartello stradale		the road sign
il gruppo		the group
perdersi		to get (oneself) lost
non c'è più		is gone; is not there anymore
verso		towards
infine		finally
ci addentrammo nella giungla		we walked into the jungle
mio marito disse "speriamo di non perderci"		my husband said "I hope we don't get lost"
cercammo di andare verso le cascate, ma era troppo pericoloso		we tried to walk towards the waterfall, but it was too dangerous
improvvisamente la nostra guida turistica non c'era più		suddenly our tour guide was gone
infine trovammo un cartello stradale		finally we found a road sign
seguimmo il fiume così non ci perdemmo		we followed the river so we didn't get lost

la brezza		the breeze
la crema solare		the suntan lotion
la gomma		the rubber; the gum
gli stivali di gomma		the wellies
c'era il rischio di vento forte		there was a risk of strong wind
può darsi che ci sia il sole		there is a small chance of sunshine
... perciò abbiamo portato la crema solare		... so we brought suntan lotion
non devono dimenticare i loro stivali di gomma		they must not forget their wellies
pioverà domani perciò avranno bisogno degli impermeabili		it'll rain tomorrow so they'll need their raincoats
nevicò così tanto che non potevamo uscire		it snowed so much we couldn't go outside

il paziente; la paziente		the patient
la cura		the cure; the treatment
l'operazione		the operation
la pillola		the pill
gli antibiotici		the antibiotics
la crema		the cream
l'alcol		the alcohol
riposare		to rest
usare		to use
evitare		to avoid
rompere		to break
poco sano		unhealthy
sano		healthy
immediatamente		immediately
almeno		at least
come ti sei rotto il naso?		how did you break your nose?
questa cura funziona immediatamente		this treatment works immediately
il paziente deve riposare per almeno una settimana		the patient has to rest for at least a week
il paziente deve evitare il cibo poco sano		the patient has to avoid unhealthy food

la prova		the (piece of) evidence; the proof
il caso		the case; the fate
l'alibi		the alibi
rubare		to steal
arrestare		to arrest
ovunque		everywhere; throughout
da nessuna parte		nowhere; anywhere
senza		without
non c'è caso senza prove		there is no case without evidence
il sospettato non ha un alibi		the suspect has no alibi
c'erano testimoni ovunque		there were witnesses everywhere
qualcuno rubò la droga		someone stole the drug
la polizia non può arrestare il sospettato senza prove		the police can't arrest the suspect without evidence
questo non prova niente		this doesn't prove anything

il sogno		the dream
sognare		to dream
la pop star		the pop star
l'esperto; l'esperta		the expert
la barca		the boat
il musicista; la musicista		the musician
l'attore; l'attrice		the actor; the actress
il designer; la designer		the designer
girare		to turn; to go round
girare il mondo		to travel the world
aprire		to open
chiudere		to close
professionale		professional
povero		poor
ricco		rich
per tutta la vita		all my life
mia figlia vuole fare la pop star		my daughter wants to be a pop star
vuole girare il mondo		he wants to travel the world
la sua famiglia è povera perciò vuole diventare ricco		his family is poor so he wants to be rich
per tutta la vita ho desiderato diventare uno scrittore famoso		all my life I wanted to become a famous writer
diventare musicista è il mio sogno		becoming a musician is my dream
ho sempre sognato di diventare un giocatore professionista di golf		I always dreamed of becoming a professional golf player

la coppia		the couple
l'ex; la ex		the ex
avere un appuntamento		to date
mancare		to miss (a person)
fare la pace		to make up
disprezzare		to despise
mi manchi		I miss you
mi manca		I miss her; I miss him
non si parlano più		they don't speak to each other anymore
eravamo una coppia, ma non lo siamo più		we used to be a couple, but we aren't anymore
gli importa ancora della sua ex		he still cares about his ex
non vi parlate mai		you never speak to each other
dovremmo fare la pace		we should make up

purché ...		as long as ... ; provided that ...
purché tu sappia quello che stai facendo		as long as you know what you're doing
la scenata		the scene
non fare una scenata		don’t make a scene
azzeccarci		to guess right
ci hai azzeccato		you hit the nail on the head
costa un occhio della testa		it costs an arm and a leg
mordere		to bite
mi sono dovuto mordere la lingua		I had to bite my tongue
giudicare		to judge
sarai tu a giudicare		that's for you to judge

la regola		the rule
la differenza		the difference
la palla		the ball
la maratona		the marathon
la medaglia		the medal
d'oro		(of) gold; golden
d'argento		(of) silver
di bronzo		(of) bronze
il punteggio		the score
spiegare		to explain
calciare		to kick
gettare		to throw
imbrogliare		to cheat
allenare		to train
mettere		to put
mettercela tutta		to try hard
enorme		huge; enormous
minuscolo		tiny
orgoglioso		proud
essere orgoglioso di ...		to be proud of ...
se ce la metti tutta vincerai la medaglia		if you try hard, you will win a medal
se vince la medaglia d'oro sarò molto orgoglioso		if she wins a gold medal I'll be very proud

la pressione		the pressure
la pressione arteriosa		the blood pressure
gli anestetici		the anaesthetics
l'allergia		the allergy
la pelle		the skin
il sesso		the sex
le mestruazioni		the period (menstruation)
l'energia		the energy
fare esercizi		to do exercise
il dolore		the pain
provare dolore		to be in pain
soffrire		to suffer
depresso		depressed

il romanzo		the novel
la canzone		the song
il lavoro dei sogni		the dream job
sarebbe ...		that would be ...
piuttosto		rather
single		single
disoccupato		unemployed
immagina il lavoro dei tuoi sogni		imagine your dream job
non mi importa dei soldi		I don't care about money
non voglio essere single per sempre		I don't want to be single forever
sono sicuro che avrai molto successo		I'm sure you'll be very successful
chi vuole vivere per sempre?		who wants to live forever?
io voglio vivere per sempre		I want to live forever
quello sarebbe fantastico		that would be fantast