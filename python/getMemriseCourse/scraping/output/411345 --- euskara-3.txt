gorria		rojo
zuria		blanco
beltza		negro
horia		amarillo
larrosa		rosa
urdina		azul
laranja		naranja
berdea		verde
morea		morado
grisa		gris

ama		madre
aita		padre
amama		abuela
aitite		abuelo
osaba		tío
izeko		tía
lehengusua		primo
lehengusina		prima
neba		hermano
anaia		hermano
arreba		hermana
ahizpa		hermana
alaba		hija
semea		hijo
gurasoak		padres

eguzkia		sol
hodeiak		nubes
ilargia		luna
izarrak		estrellas
zerua		cielo
lurra		tierra
elurra		nieve
euria		lluvia
haizea		viento
hondartza		playa
ibaia		río
itsasoa		mar
mendia		monte

egunsentia		amanecer
goiza		mañana
eguerdia		mediodía
arratsaldea		tarde
gaua		noche
iluntzea		anochecer
udaberria		primavera
udazkena		otoño
uda		verano
negua		invierno
urtarrila		enero
otsaila		febrero
martxoa		marzo
apirila		abril
maiatza		mayo
ekaina		junio
uztaila		julio
abuztua		agosto
iraila		septiembre
urria		octubre
azaroa		noviembre
abendua		diciembre
astelehena		lunes
asteartea		martes
asteazkena		miercoles
osteguna		jueves
ostirala		viernes
larunbata		sábado
igandea		domingo
atzo		ayer
gaur		hoy
bihar		mañana
iaz		año pasado
aurten		este año
datorren urtea		año que viene

maisu		maestro
andereño		maestra
arbela		pizarra
hiztegia		diccionario
arkatza		lapiz
borragoma		goma de borrar
koadernoa		cuaderno
liburua		libro
erregela		regla
zorrozkiloa		sacapuntas
ikaslea		alumno, alumna
ordenagailua		ordenador
inprimagailua		impresora
kalkulagailua		calculadora
klariona		tiza

armiarma		araña
arraina		pez
barraskiloa		caracol
dordoka		tortuga
erlea		abeja
eulia		mosca
gamelua		camello
hartza		oso
igela		rana
lehoia		león
sagua		ratón
sugea		serpiente
tigrea		tigre
tximeleta		mariposa
txoria		pájaro

landarea		planta
belarra		hierba
lorea		flor
zuhaitza		arbol

etxea		casa
teilatua		tejado
garajea		garaje
leihoa		ventana
horma		pared
txirrina		timbre
ganbara		desván
sukaldea		cocina
logela		dormitorio
bainugela		baño
komuna		váter
egongela		salón
aulkia		silla
berogailua		estufa
mahaia		mesa
argia		luz
telefonoa		telefono
apalategia		estanteria

lagunak		amigos, amigas
neska-laguna		novia
mutil-laguna		novio
koadrilla		cuadrilla
musua		beso
besarkada		abrazo
zaplaztekoa		bofetada
laztana		caricia

kirola		deporte
kirolaria		deportista
saskibaloia		baloncesto
arrantza		pesca
igeriketa		natación
baloia		balón
txirrindularitza		ciclismo
tenisa		tenis
futbola		futbol
mendigoizalea		montañero

bizikleta		bicicleta
motorra		moto
kotxea		coche
autobusa		autobús
kamioia		camión
taxia		taxi
trena		tren
itsasontzia		barco
txalupa		barca
helikopteroa		helicopteroa
aireportua		aeropuerto
tren-geltokia		estación de tren
autobus-geltokia		estación de autobuses
bidea		camino
zubia		puente
trenbidea		vía de tren
portua		puerto

ogia		pan
hirina		harina
arrautza		huevo
eztia		miel
gatza		sal
olioa		aceite
arraina		pescado
haragia		carne
barazkiak		verduras
gozokiak		dulces
artoa		maiz
azukrea		azucar
ozpina		vinagre
gurina		mantequilla
oilaskoa		pollo
kafesnea		café con leche

prakak		pantalones
gona		falda
soinekoa		vestido
alkandora		camisa
eskularruak		guantes
barruko arropa		ropa interior
galtzontziloak		calzoncillos
kuleroak		bragas
gerrikoa		cinturon
zapatak		zapatos
jertsea		jersey
soinekoa		vestido
txalekoa		chaleco
txamarra		chamarra
txandala		chándal

belarritakoak		pendientes
betaurrekoak		gafas
botoia		botón
bufanda		bufanda
eraztuna		anillo
erlojua		reloj
eskularruak		guantes
euritakoa		paraguas
gerrikoa		cinturón
gorbata		corbata
kapela		sombrero
mahuka		manga
makila		bastón
musuzapia		pañuelo
poltsa		bolso
poltsikoa		bolsillo
txanoa		gorro

biltegia		almacén
bulegoa		oficina
lantegia		fábrica

abeltzaina		ganadero
abokatua		abogado
albaitaria		veterinario
argazkilaria		fotógrafo
arkitektoa		arquitecto
arotza		carpintero
arrain-saltzailea		pescadero
arrantzalea		pescador
botikaria		farmacéutico
bulegaria		oficinista
dekoratzailea		decorador
dendaria		tendero
erizaina		enfermero
garbitzailea		limpiador
harakina		carnicero
igeltseroa		albañil
ile-apaintzailea		peluquero
irakaslea		profesor
iturgina		fontanero
jostuna		modisto
kale-garbitzailea		barrendero
kazetaria		periodista
langilea		obrero
lorezaina		jardinero
mekanikaria		mecánico
nekazaria		agricultor
okina		panadero
pintorea		pintor
postaria		cartero
sukaldaria		cocinero
tabernaria		tabernero
udaltzaina		municipal
zerbitzaria		camarero

antzerkia		teatro
argazki-makina		cámara de fotos
bertso-saioa		recital de versos
dantza		baile
dominoa		dominó
karta-jokoa		juego de cartas
letra-zopa		sopa de letras
panpina		muñeca
partxisa		parchís
telebista		televisión
txotxongiloa		títere
xakea		ajedrez
zinema		cine
zirkua		circo

abestu		cantar
afaldu		cenar
agurtu		saludar
astindu		sacudir
atea jo		llamar a la puerta
atera		salir
bainatu 		bañarse
barre egin		reir
bazkaldu		comer
begiratu		mirar
bizarra moztu		afeitarse
bultzatu		empujar
dutxatu		ducharse
edan		beber
eman		dar
eseki		tender
eseri		sentarse
eskua eman		dar la mano
esnatu		despertarse
etorri		venir
euria egin		llover
garbitu		limpiar, lavar
gorde		guardar
gosaldu		desayunar
hegan egin		volar
hitz egin		hablar
ibili		caminar
idatzi		escribir
igo		subir
ikusi		ver
itzali		apagar
jaiki		levantarse
jaitsi		bajar
jan		comer
jantzi		vestirse
joan		ir
josi		coser
kaka egin		defecar
korrika egin		correr
landatu		plantar
lan egin 		trabajar
lisatu 		planchar
lo egin		dormir
marraztu		dibujar
min hartu		hacerse daño
musu eman		besar
nagiak atera 		desperezarse
negar egin		llorar

luzea		largo
laburra		corto
txikia		pequeño
handia		grande
ona		bueno
txarra		malo
itsusia		feo
polita		bonito
lodia		gordo
argala		delgado
berezia		especial
zoragarria		maravilloso
azkarra		rápido
geldoa		lento
altua		alto
baxua		bajo
arrunta		normal
atsegina		agradab