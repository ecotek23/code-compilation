cześć		hi; bye
chodźmy!		let's go!
na zdrowie!		cheers!; bless you!
proszę		please; here you are; you're welcome
tak		yes; so
tak, poproszę		yes, please
nie		no; not; don't
dziękuję		thank you
nie, dziękuję		no, thank you
przepraszam		I'm sorry; excuse me
dzień dobry		good morning; good afternoon
dobranoc		good night
dobrze		okay; alright; well
pewnie!		sure!
do zobaczenia później		see you later
do widzenia		goodbye

ty		you (singular informal)
u ciebie		at you
co		what
tam		there
co tam u ciebie?		how are you?
w porządku		(I'm) fine; alright
a co u ciebie?		and how are you?
co słychać?		what's up?
nic		nothing; anything
nowy		new
nic nowego		nothing new; all the same
imię		a name; a first name
mieć		to have
mam		I have
masz		you have
jak		how; like; as
na		on; to; for
jak masz na imię?		what's your name?
mam na imię ...		my name is ...
genialny		brilliant
jesteś		you're
jesteś genialny!		you're a genius!

szczęśliwy		happy; lucky
smutny		sad
zły		angry; wrong; bad
racja		right; that's right
zmęczony		tired
chory		sick; ill
głodny		hungry
trochę		a little; some
ja		I
mi		(to) me
jestem		I'm
jestem zmęczony		I'm tired (said by a man)
jestem zmęczona		I'm tired (said by a woman)
nie jestem zmęczony		I'm not tired
jesteś chory		you're sick (said to a man)
jesteś chora		you're sick (said to a woman)
jesteś głodny?		are you hungry? (said to a man)
jesteś głodna?		are you hungry? (said to a woman)
chcieć		to want
pić		to drink
się (siebie)		oneself (myself, yourself, etc.); each other
chce mi się pić		I'm thirsty
chce mi się trochę pić		I'm a little thirsty
nie mam		I don't have
nie mam racji?		am I wrong?
masz rację		you're right (singular informal)

jedzenie		food
posiłek		a meal
chleb		bread
makaron		pasta
ryż		rice
ziemniaki		potatoes
woda		water
kawa		coffee
herbata		tea
piwo		beer
wino		wine
wódka		vodka
mięso		meat
kurczak		chicken; a chicken
wieprzowina		pork
wołowina		beef
ryba		fish
kotlet schabowy		a pork chop
warzywo		a vegetable
owoc		fruit
jabłko		an apple
banan		a banana

to		it; this; that; then
jest		is; there is
to jest		it is; this is; that is
i		and
lubić		to like; to enjoy
pyszny		delicious
wstrętny		disgusting
fantastyczny		awesome; fantastic
lubię ...		I like ...
lubię chleb		I like bread
nie lubię ...		I don't like ...
nie lubię makaronu		I don't like pasta
lubię banany		I like bananas
nie lubię jabłek		I don't like apples
to jest pyszne		it's delicious
kawa jest pyszna		coffee is delicious
lubię herbatę i kawę		I like tea and coffee

świat		the world
kraj		a country
województwo		a voivodeship; a province
flaga		a flag
Ameryka		England
Stany Zjednoczone		Scotland
Kanada		Ireland
Australia		Northern Ireland
Nowa Zelandia		Wales
skąd		where ... from
z		from; with; of
skąd jesteś?		where are you from?
jestem z Anglii		I'm from England
jestem ze Szkocji		I'm from Scotland
jestem z Irlandii Północnej		I'm from Northern Ireland
jestem z Irlandii		I'm from Ireland
jesteś z Polski?		are you from Poland?
nie jestem z Polski		I'm not from Poland

on		he
ona		she
mu		him; to him
jej		her; to her; hers
taki		such; so
zawsze		always
nigdy		never
dlaczego; czemu		why
bo		because
że		that (followed by a sentence)
żeby		in order to; so that
taki smutny		so sad
zawsze jesteś szczęśliwy		you're always happy
on nigdy nie jest szczęśliwy		he's never happy
czemu jesteś taki smutny?		why are you so sad?
... bo jesteś chory		... because you're sick
jestem zły, bo jestem głodny		I'm angry because I'm hungry
trochę mu smutno, że jesteś zły		he's a little sad that you're angry

numer		a number
podać		to give; to serve
mój		my; mine
twój		your; yours (singular informal)
swój		one's (own); my (own); your (own) etc.
zero		zero
jeden		one
dwa		two
trzy		three
cztery		four
pięć		five
sześć		six
siedem		seven
osiem		eight
dziewięć		nine
dziesięć		ten
dasz mi swój numer?		would you give me your number?
mój numer to ...		my number is ...

pomarańcza		an orange
cytryna		a lemon
przekąska		a snack
zupa		soup
jajko		an egg
ser		cheese
frytki		fries
sałatka		salad
kanapka		a sandwich
mleko		milk
sok		juice
sos		sauce
olej		oil
masło		butter

restauracja		a restaurant
stół		a table (large; in the kitchen)
stolik		a table (small; at the restaurant)
osoba		a person; an individual
miejsce		a place; a seat; room
menu		a menu
nóż		a knife
widelec		a fork
łyżka		a spoon
pałeczki		chopsticks
państwo		you (plural formal)
móc		can; may; to be allowed to
mogę		I can; I'm allowed to
możemy		we can; we're allowed to
zamawiać		to order; to book
jeść		to eat
są		(they) are; there are; you are (formal)
prosić o ...		to ask for ...
poproszę		I'll have; I'd like; please
gotowy		ready
na wynos		take-out; to go
jeść na miejscu		to eat in
dla		for
do		to; into
o		about; at; for; oh
czy		if; or; whether
na miejscu czy na wynos?		eating in or taking out?
poproszę stolik dla dwóch osób		a table for two please
czy możemy prosić o ... ?		can we have ... please?
czy możemy prosić o menu?		can we have the menu please?
czy są państwo gotowi?		are you ready?
czy są państwo gotowi do zamówienia?		are you ready to order?

być		to be
myśleć		to think
wydawać się		to seem
myślę, że ...		I think (that) ...
on jest		he's
ona jest		she's
dobry		good; kind
cudowny		wonderful
przystojny		handsome; good-looking
piękny		beautiful
przepiękny		picture-perfect; gorgeous
śliczny		lovely; cute
silny		strong; powerful
słaby		weak
gruby		fat; thick
chudy		thin; skinny
ładny		nice; pretty
brzydki		ugly
fajny		fun; cool
duży		big; large
niski		short; low
mały		small; little
bardzo		very; really
za; zbyt		too
za duży		too big
też		too; also; as well
ona jest przepiękna		she's picture-perfect
on jest bardzo przystojny		he's very handsome
to jest ładne		it's nice
jakie śliczne!		how lovely!
jak myślisz?		what do you think?
myślę, że to jest fajne		I think it's cool
myślę, że to jest za duże		I think it's too big
też tak myślę		I think so too
nie wydaje mi się		I don't think so

potrzebować		to need
pomoc		help
potrzebuję pomocy		I need help
mówić		to speak; to say; to tell
po		after; past
mówić po polsku		to speak Polish
jak się mówi ... po polsku?		how do you say ... in Polish?
znaczyć		to mean
co znaczy ... ?		what does ... mean?
co to jest ... ?		what's a ... ?
co to jest?		what's that?
wiedzieć		to know
nie wiem		I don't know
rozumieć		to understand
nie rozumiem		I don't understand; I don't get it
rozumiesz?		do you understand?
ja też		me too
ja też nie		me neither
poznawać		to meet; to get to know
miło		nicely; pleasantly
miło mi cię poznać		nice to meet you
świetny		great; cool
to świetnie!		that's great!
sprawa		a matter; an issue; a case
nie ma		there isn't; there aren't
nie ma sprawy		don't mention it
przyjemność		pleasure
strona		a side; a page
cały		whole; all
cała przyjemność po mojej stronie		the pleasure is all mine
za		for; behind; in
nie ma za co		you're welcome; no problem
witamy!		welcome!
powodzenia!		good luc