anular		to cancel
convocar		to convoke, call together, summon
democrático		democratic
ejercer		to exercise, to practice
el candidato		candidate
el votante		voter
electoral		electoral
elegir		to elect, choose
la campaña		campaign
figurar		to appear
la candidatura		candidature, candidacy
la coalición		coalition
la convocatoria		notification
la contienda		conflict, contest, competition
la participación		participation

la sucesión		succession
la transición		transition
la votación		voting
votar		to vote
los comicios		elections
la propaganda		propaganda
el voto		vo