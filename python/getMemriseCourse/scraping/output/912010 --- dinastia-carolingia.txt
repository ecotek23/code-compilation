Carlos Martel		714 Abuelo de Carlomagno
Pipino III el Breve		747 Padre de Carlomagno
Carlomagno		768
Carlomán I		768 Hermano de Carlomagno
Carlomán		754 Hermano de Pipino
Pipino el Jorobado		810 Hijo de Carlomagno
Carlos el Joven		811 Hijo de Carlomagno
Pipino de Italia		781 Hijo de Carlomagno
Ludovico Pío		814 Hijo de Carlomagno
Lotario		780 Hijo de Carlomagno
Lotario I		840 Hijo de Ludovico

Lotario I		840 Hijo de Ludovico
Pipino de Aquitania		929 Hijo de Ludovico
Luis II el Germánico		843 Hijo de Ludovico
Carlos de Neustria		843 Hijo de Judith y Ludovico
Berengar		915 Hijo de Gisela y Eberhard
Tratado de Verdún		843
Ordinatio Imperii		817
Bernardo		810 Sobrino de Ludovico, hijo de Pipino de Italia
Gisela		Hija de Ludovico

Bernardo		Italia
Luis		Baviera
Pipino		Aquitania
Lotario		Emperador
Carlos		territorios en Retia, Alsacia, Alemania y Borgoña
Muerte de Ludovico		840

Rothilde		Hija de Lotario
Luis II Emperador		855 Hijo de Lotario
Carlos de Provenza		855 Hijo de Lotario
Lotario II de Lorena		855 Hijo de Lotario
Guido Emperador		891 Hijo de Guido de Spoleto y Rothilde
Lamberto Emperador		892 Hijo de Guido
Luis III Emperador		901 Nieto de Luis II

Luis el Germánico		Baviera, Sajonia, Alemania
Carlos		Neustria, Aquitania, Gascuña y Septimania
Lotario		Lotaringia

Luis II		Italia y título imperial
Carlos		La Provenza
Lotario II		La Loren