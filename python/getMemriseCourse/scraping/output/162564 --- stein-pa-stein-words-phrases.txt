miljø		environment, environmental
det kommer an på		it depends on
selvstendig		independent
fornøden		necessary
fornøyd med		pleased with
å tjene		to earn
slik		such, in this way
i en slik jobb		this job
å prate med		to talk
en idé		an idea
en sanger		a singer
å synge		to sing
et kor		a choir
fortsatt		still, even now, yet
offentlig		public
offentlig administrasjon		public administration
Utdanningsdirektorate		Norwegian directory for education and training
å tegne		to draw
en tegning		a drawing
teknisk tegning		technical drawing
innovativ		innovative
interaksjon		interaction
et smykke		jewelry
nyttig		helpful, useful
hos		at (a place)
en deltaker		participant
å bedre		to improve
å delta		to participate
godkjent		approved
forkjellige		different
et lager		a warehouse
å spare		to save
å jobbe for seg selv		to work for yourself
Toget kommer om fem minuter.		The train will come in five minutes.
nesten		almost
Jeg er nesten ferdig.		I am almost ready.
en feil		a mistake
en kirurg		a surgeon
å re sengen		to make the bed
en sykepleier		a nurse
å dytte		to push
å dra		pull, go, leave
å operere		to operate
å ta blodtrykket		to measure the blood pressure
en pasient		a patient
en rullerstol		a wheelchair
en krykke		a crutch
å plante		to plant
et hagarbeid		a garden work
en gartner		a gardener
en bakgår		a backyard
helsetning		clause
en leddsetning		a dependdent clause
når for tida		nowadays
å gjette		to guess
en kroppsdel		a body part
å puste in 		to breathe in
å puste ut		to breathe out
en byggning		a building
nordpolen		the north pole
sørpolen		the south pole
kjent		well-known, famous
å betegne		to charecterize, describe
å kjenne		to know, feel

forresten		by the way
Har du forresten snakket med bestefar?		By the way, have you spoken with your great grandfather?
synes		think, look, seem, find
ensom		lonely
å vokse opp		to grow up
et bryggeri		a brewery
en bygd		a district, countryside
et område		an area
en mangel		want, absence, defect, scarcity, shortage, lack
i utkanten av byene		Engelsk	on the outskirts of the town 
svær		great, very much
dessuten		moreover, besides, furthermore, in addition
en kamerat		a friend
å mene		to mean, intend, imagine
å skape		to create, make, produce
å ta seg av		take care of
mulig		possible
en mulighet		a possability
høflighet		politeness, civility, courteoussy
høflig		polite
å sette		to put, move
bortskjemt		spoilt, pampered
reiser seg		to rise (as for applause)
fremdeles		still
å tørste		to be thirsty
ulykkelig		miserable, unhappy, desolate
en ulykke		misery, disaster, accident, fatality
et uttrykk		an expression
en fremtidsplan		a future plan
Jeg kommer til å		I am going to
å tenke på noe		to think about something
å søke på		to apply for
en kunstner		an artist
en forsker		a researcher
likhet		similarity
kreativ		creative
mer eller mindre		more or less
virkelig		real, actual
regelmessig		regular, steady
en lønn		a wage, salary
en kunnskap		knowledge
et viskelær		an eraser
kjedelig		boring
en tålmodighet		patience
en måte		a manner, way
en tekst		lyrics
å samle folk		to unite
en oversetter		a translator
nårtid		present time
fortid		past
umulig		impossible
nokså		fairly, rather
nokså lett		very easy
å prate med		to talk
en komiker		a comedian
å pugge		to memorize
å vant til		to be used to 
å kjenne til		know (about), be familiar with
en lov		a law
å love		to promise
Det er lov.		It is leagal.
lovlig		legal
ulovlig		illegal

å kreve		to demand, require
foreløpig		at the moment, currently (something that will change)
komplisert		complicated
en osteøvel		a cheese knife
et fat		a side plate (dish)
en duk		a tablecloth
en leverpostei		pâté
en ensomhet		a loneliness, solitude
De		You (formal)
lykkelig		happy
ulykkelig		miserable, unhappy, desolate
heldig		lucky, fortunate, successful
uheldig		unlucky, unfortunate
en barndom		a childhood
en kulde		a cold
en vinterkulde		a winter coldness
streng kulde		severe cold
et nordlys		Northern light
å lege vekt på		to emphasize
å føle ansvar		to have a sense of responsibility
å være veloppdragen		to behave
å ha fantasi		to have an imagination
lydig		dutiful, obedient
en lydighet		obedience
sparsommelig		economical, thrifty
å være sparsommenlig		to be able to live with little
lat		lazy
en god arbeidsmoral		a good work morals
å samarbeide		to collaborate
å tenke selvstendig		to think independently
selvsikker		confident
å skille lag		to part ways, separate
en årsak		(a) cause, reason
annenhver		every other
tøff		tough (difficult)
begge		both
å svelge		to swallow
å svelge noen kameler		adjust one's expectations, to compromise
å ødelegge		to ruin, destroy
å tørre		to dare
Jeg tør ikke fortelle hva jeg føler for deg.		I wouldn't dare say how I feel about you.
å le		to laugh
en iver		eagerness, enthusiasm
vidunderlig		wonderful, fantastic
en sjasmin		jasmine
omkring		around
å oppfylle		to fulfil, come true
en oppfyllelse		a fullfilment
et kjærlighet		love
kjærlighet ved første blikk		love at first sight
et budskap		a message, announcement
fortjent		deserved, earned
en alder		an age
overalt		everywhere
et ekteskap		a marriage
ekte		real, authentic, genuine
en ekte Rembrandt		a genuine Rembrand (painting)
å ekte		to marry (formal)
en ektefelle		a spouse
en verdi		value, worth
en salgsverdi		a retail value
en verdiøkning		an increase in value
verdifull		valuable
å smelte		to melt
Bokmål eksempel	snøen smeltet bort		the snow melted away

en ytringfrihet		freedom of speech
å uttrykke		to express, manifest
en sensur		a censor
å risikere		to risk
en rettighet		a right
utsatt		vulnerable, exposed, postponed
utsatt for		subject to
å ha noe i fred		to keep something for yourself
et kjønn		sex, gender
en diskriminering		a discrimination
en tale		a speech
å omtale		to talk about
etisk		ethical
mektig		powerful, potent, mighty
et våpen		a weapon
å misbruke		to misuse, abuse
å slippe		leave hold of, let go, release, be excused from
slapp		left a hold of, released
har sluppet		have left a hold of, have released
Slipp meg!		Let me og!
Hun slapp å betale.		She didn't have to pay.
et hensyn		consideration, respect
å ta hensyn til		to take something into consideration
en konvensjon		a convention, agreement btw countries
et utvalg		a choice, selection
faglig		professional
å kollidere		to collide
et bosted		a place of residence
en samvittighet		a conscience
å omfatte		to comprise, consist, include
å skifte		to change
en tro		a faith, belief
en særstilling		unique position
en statskirke		a state church
å praktisere		to practice
et medlem av		a member of
en kvart		quarter
et trosamfunn		a religious body, group united by the same religion
å markere		to mark, indicate
en påvirkning		an influence
en kjenneskap		knowledge, awareness
et livssyn		outlook on life
å melde seg inn		to become a member
å medle seg frivillig		to volunteer
et personvern		a protection of privacy
å hevde		maintain, assert, claim, defend, uphold
et sår		a wound, cut
å såre		to be wounded, cut
såret		wounded, hurt
en jordmor		a nurse trained to assist women during birth
å avhenge		to depend
å lene		to lean
en lovgiving		legislation
en selvfølge		something self-explanatory
en realitet		a reality
reell		real, actual
å lene seg tilbake		to relax
en integrering		an integration
en tvang		a coercion, force, enforcement
en plikt		an obligation, duty
å gjennomføre		carry out, execute, realize
permanent		permanent
utmerket		excellent
et tiltak		initiative, drive, energy, measure
å gå ut fra		to assume, suppose
å følge		to accompany
en motivasjon		a motivation, drive
frivillig		voluntary
en skift		a work shift
lettet		relieved
framfor alt		above all, in the first place
først og fremst		first and foremost
skuffet		disappointed
i et nytt lys		in a different way
stivnet uttrykk		stiff expression
et uttrykk		an expression
engstelig		anxious
et oppgjør		an important fight, dispute, settlement
å avbryte		to interrupt
påfallende		remarkable, rare
å falle en inn		to begin to think about something
samfulle		whole, full, entire
idelig og alltid		constant
innvie deg i		to tell you about
en bekymring		a worry, trouble, concern
å komme til bunns i		to understand something completely
å øve urett		to mistreat
å skjule		to hide, conceal
uforstyrret		untroubled, calm, relaxed
å innrette		to arrange, plan
å late som		to pretend
fra hånd til munn		to be poor or have very little
å gjøre kunster		to perform, play an actor
å gjøre synd		to do something bad
urimelig		unreasonable
lystig		amusing, funny
liksom		like, as, as it were, so to speak
fornøyelig		amusing, delightful, pleasant

en historie		a history
Noen glimt fra Norges historie		A flash of Norway's History
en bosetning		a settlement
å kalle		to call
omtrent		about, aproximately, roughly, more or less
en innbygger		an inhabitant
rask		fast, quick
å frakte		to freight, carry, transport goods by ship, ect
langs		along, alongside
å seile		to sail
en skip		a ship
foruten		a part from, i tillegg til
å ro		to row, греба
en åre		an oar, гребло
en mast		a mast, мачта
et seil		a sail
et ror		a rudder, опашка на самолет, кораб
en kjøl		a keel
å skille		to differentiate
en tår		a tear, drop, sip
en grunn		ground, base, foundation, reason, cause
på grunn av		because of, as a result of, owing to
å plyndre		to plunder, плячкосвам
å stjele		to steal
et kloster		a monastery
å drepe		to kill
en fange		a prisoner
kristen		Christian
å tvinge		coerce, force
en befolkning		a population
imot		against, on the contrary
en makt		power, nation, state
å svekke		to weaken
etter hvert		gradually
en pave		Pope
en tro		belief, faith
å leve		to live, exist
videre		further
en feiring		a celebration
en skikk		a tradition, custom
å overleve		to survive
å klare		to manage
en fiende		an enemy
å finne ut		to find out
en handlemann		a trader
en rikdom		wealth, riches
å komme bort		to disappear, get lost
en hær		an army
en stilling		score, position, standing, status
ifølge		according to
ifølge alle beregninger		according to all estimates
fremmed		foreign, strange
å baksnakke		to slander, make false, damaging statements
farlig		dangerous
å merke		to mark, notice
uvennlig		unfriendly
kjølig		cold, chilly, reserved, unfriendly
å skjønne		to grasp, understand
en bestemelse		mission, destiny, rule, direction, regulation, condition
å forsvinne		to dissapear, vanish
et skriftspråke		writing language
å oppløse		to dissolve, close down, dismiss
å samle		to gather, collect, assemble, accumulate
et samfunn		society, association, community, academy
å sette i gang		to get started
et forhold		a relationship, circumstance, condition
meget		very, veldig
Kom inn		Come in
en begynnelse		beginning, start
å støtte		to support
derimot		on the other hand, however
en motstander		an opponent
å tape		to lose
en konferanse		a conferance
å skje		to take place, happen, occur
en domstol		a court of law
uavhengig		independent
en regjering		a government
å sørge		to mourn, lament
å sørge for		to provide, to look after
å dømme		to judge, pass sentence
å kaste		to discard, throw away, throw
å øke		to increase
å vare		to last
nokså		quite, fairly, rather
å putte		to put, thrust
å erobre		to conquer, capture, win
et bidrag		a contribution, benefit
jøde		jew
en bebyggelse		buildings, settlement
nøytralt		neutral
å sikre		to protect, secure
forsvarorganisasjon		defence organization
å forsvare		to defend
et samhold		solidarity
et styre		a government, rule
å styre		to govern, rule
en streik		a strike
enig		united, agreed, in agreement
strengt		strictly
på foten		on foot
en flåte		a fleet
en oppbygning		design, structure
å bygde opp		to build up
en arbeidsledighet		an unemployment
en ledighet		easiness of manner
kraftig		substantial
stadig		constand; changable
relativt		relative
en enighet		agreement, unity
å innføre		to introduce
trygg		secure, safe
å bryte		to break
en stang		a pole
naken		nacked
nettopp		just now
brøt		broke
har brutt		have broken
å bryte ut		to start, trigger
et angrep		an attack
å angripe		to attack
ødelagt		destroyed
å motarbeide		oppose, work against
en motstand		opposition, protest
skyldig		in debt, owing, guilty
dømt		condemned, sentenced
å henrette		to execute
en forbrytelse		a crime
et bidrag		benefit, help
en kamp		a fight, struggle
valg kamp		electoral campaign
en halvpart		50%
å bombe		to bomb
et militær		military
en sabotasjeaksjon		planned destruction
å spre(-dde)		to spread
å dele ut		to distribute
å sensurere		to censor
i alt		in all, altogether
et fengsel		a prison
et landssvik		a treason
bli finnet skyldig		were found guilty
en straff		penalty, fine
en bot		financial penalty
mild		mild
et amnesti		an amnesty
en optimisme		an optimism
sammensatt		compound, composite
et parti		a party
en klasseforskjell		class difference
å regulere		to regulate
en aktivitet		an activity
en utvikling		a development
en vekst		growth
en fattigdom		poverty
fattig		poor
en skipsfart		transport by sea
en velferdsstat		welfare state
en endring		a change
å falle utenfor		to fall outside
å unngå		to avoid
et forbruk		a consumption
å bli borte		to be gone, disappear
å forandre		to change
et selvstendighet		an independence
en innflytelse		an influence
innflytelsesrik		influential
å påvirke		to influence
å overfalle		to assault, attack
å døpe		to baptise
å rømme fra		to escape from
tålmodig		patient
en tålmodighet		a patience
å behandle		to treat

å se opp til		to look up to
å beundre		to admire
en beundrer		an admirer
å oppleve		to experience
å se ut		to seem, look
å bore		to drill
et selskap		a company
å opprette		to establish
på midten av 60-tallet		in the mid 60s
Det ser mørkt ut!		Things don't look bright
et hull		a hole
et felt		a field
en virksomhet		activity, trade, business, company
en aksje		a stock, share
en konsesjon		a concession, franchise
en tillatelse		a permission
å utgjøre		to constitute, compose, form
et fond		a fund
å plassere		to place
I Nordsjøen		in the North Sea
en oljeplattform		oil platform
Dette er en veldig hyggelig arbeidsplass.		This is a very nice workplace.
isolert		isolated
en brannstasjon		fire station
et utstyr		an equipment
avansert		advanced
opptatt		busy, accupied
en fagforening		trade union
et døgn		24 hours
en måte		a way
deretter		then, later, afterwards
å fungere		to function, work
en ulempe		a disadvantage
en fordel		an advantage
i orden		in order
tida går med til..		the time goes by in..
et jobbe skift		a job shift

et kamskell		a scallop
en hummer		a lobster
en kjøttdeig		ground meat
å tolle		to joke
å holde deg i form		to stay in shape
å løfte vekter		to lift weights
sunt		healthy
naturlig		natural
naturligvis		naturally
en forskning		a research
en kreft		cancer
å sprøyte		to spray
en insjeksjonsprøyte		an injection
å få en sprøyte		to have an injection
en sprøyte		a syringe
et sprøyt		nonsense, rubbish
sprøytemidler		fertilizers
en ond sirkel		an evil circle
i moderasjon		in moderation
å overdrive		to overdo, exaggerate
påvirket		influenced
imidlertid		however
imidlertid		however
midlertidig		temporarily
å påvirke		to influence
å påvirke		to influence
en påvirkning		an influence
påvirkelig		suggestible
en forbrenning		metabolism
en fiber		a fiber
tungmetaller		heavy metals
å lagre		to store
å fryse		to freeze, be cold
å sammenligne		to compare
oppdrettslaks		raised salmon
å slippe		to release
å slippe fri		to escape
nødvendigvis		necessarily
skummelt		scary, creepy
en aubergin		an eggplant
å bryte		to break
heletiden		all the time
jern		iron
å bevege seg		to move
å leve		to live
å øke		to increase
et hjerneslag		a stroke
en risiko		a risk
å ødelegge		to ruin, destroy
gjennomsnitt		average
en sykdomm		disease, sickness
farlig		dangerous
å forårsake		to cause
en nyresvikt		kidney failure
ei lommebok		a wallet
en honning		honey
tørket frukt		dry friut
lavkarbo		low carb diet
en diett		a diet
et kosthold		a diet
en heimkunnskap		home economics
å herme		to imitate, mimic
å lukte		to smell
et sollys		a sun light
en talva		a balck board
en saks		a pair of scissors
å surre		to fry
å surre		to fry
en talva		a black board
en oppfinnelse		an invention
en kikert		chickpea
en kikkert		binoculars
som regel		generally, as a rule
melkeprodukter		dairy products
solkrem		sun screen
latter		laughter, laugh
en mosjon		exercise

å glede seg til		to look forward to
å være glad for		to be happy for
å være glad i		to love someone or something
å ha lyst til å (gjøre noe)		to want to (do something)
en konsistens		a consistency
en tekstur		a texture
en rekke		a row, sequence
å rekke		to reach, to be enough
et kollektiv		a collective
å være lei av		to be sick of something
Jeg kjeder meg.		I am bored.
å kjede seg		to bore
en mening		a meaning
å sette pris av		to appreciate
en tvil		a doubt
Jeg har i tvil om		I doubt
Jeg er i tvil		I am in doubt
å bli kjent		to get to know
å slite med		to struggle with/to work hard with
å jukse		to cheat
å jukse med tallene		to manipulate the figures
et juks		cheating, fiddling
en vakt		a guard, watch
den nattevakten		the night watch
en barnevakt		a baby sitter
en livvakt		a bodyguard
å være på vakt		to be on guard
å tillate		to allow, permit

tydeligvis		obviously, evidently, apparently
gal		wrong, incorrect
å planlegge		to plan
å orke		to have the energy, feel like
unødvendig		unnecessary, needless
å abonnere på		to subscribe
et budsjett		a budget
forferdelig		terrible, horrifying
en forferdelse		horror, dismay
å dryppe		to drip, leaking
et verksted		workshop, shop
et bilverksted		car repair shop
en holdeplass		bus stop, tram stop
en rente		interest 
i tillegg		in addition
i tillegg til lånet		in addition to the loan
mørk		dark
å se mørk på noe		to be pesimistic
å pusse opp		to renovate, fix up
nærmest		nearest, closest, almost
på jakt etter		in pursuit of
å være på jakt etter noe		to be on the hunt for something
å sette ring rundt		to mark
egen		own
en egenkapital		one's own capital
en kausjonist		a guarantor
å greie		to manage
en utgift		expense, cost
å titte		to glimpse, peep
ikke titt		no peeping
tett		tight
tørr		dry
felles		mutual, common
fellesgjeld		common fee for renovation of residential buildings
fellesskap		fellowship
å lure		to fool
å lure på		to wonder
en egenskap		quality, property, characteristic
eneste		only, single
å starte en familie		to start a family
å misforstå		to misunderstand
uoppnåelig		unobtainable
oppnåelig		obtainable, achievable
en inntekt		an income
å levere		to de,liver
å gange		to multiply
delt på		divided by
2 ganger 5		two multiplied by 5
er lik		equal
rent miljø		clean environment
en avstand		distance
en heis		elevator
en megler		broker
å avtale		to agree, arrange, come to arrangement
en avtale		an agreement, contract, appointment, arrangement, deal
felleskostnader		common fee for cleaning
å ordne		to arrange, organize
en gjeld		debt, amount due
Ikke tale om		No way
et forbruk		consumption
en forbruksutgift		consumption expence
et brensel		fuel, materials used to produce power/heating
en strømkilde		electricity source

å stenge		to close, shut
et hærverk		a damage
en trygd		social security
en egenandel		part of the price one must pay him/herself
spinke og spare		to be very careful with spending money
å gå ut over		to affect negatively
en oppussing		a restoration, fixing
samtidig		contemporary
en skatt		a tax
en avgift		a fee, charge
uansett		irrespective of, regardless of
å trekke		to deduct, take away
trakk		deducted, took away
har trukket		have deducted, have taken away
ufør		disabled
å ha rett til		to have a right to
i forhold til		in relation to
rett som det er		often
et hovedtrekk		a main feature
en permisjon		the permition to have time off your job
en fødsel		a birth
født		born
å reservere		to reserve
å fordele		to divide
mesteparten		majority
iallfall		minority
å amme		to brest feed
en effekt		an effect, output
ikke i det hele tatt		absolutely nothing
å hindre		to prevent, restrain
en post		a part, item
kronisk		chronic
å være nødt til		to be forced to
den enkelte		every person
en byrde		a burden, load
å laste		to lead; to blame
å slite		to work hard; tear; wear, wear out
å falle over ende		to fall together
å sukke		to sigh
å nekte		to refuce, deny
slik gikk det til		it goes that w