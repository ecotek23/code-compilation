el hijo		son
el tío		uncle
el hermano		brother
el padre		father
único		unique
el crecimiento		growth
la mamá		mom
la visita		visit
el abuelo		grandfather
el papá		dad
visitar		to visit
integrar		to integrate
el marido		husband
familiar		of the family, familiar
respetar		to respect

mayor		old, great [in magnitude]
el esposo		husband
el nieto		grandson
la herencia		inheritance
la infancia		childhood (i)
el primo		cousin
juntar		to join, to put together, to collect
obedecer		to obey
acoger		to take in
el pariente		relative
el apellido		surname
el sobrino		nephew
el heredero		heir
heredar		to inherit
el lazo		bow, knot, loop

sanguíneo		bloody
el viudo		widower
responsable		responsible
la hija		daughter
la tía		aunt
la hermana		sister
el respeto		respect
la esposa		wife
el menor		minor
la nieta		granddaughter
la viuda		widow
la abuela		grandmother
la sobrina		niece
soportar		to put up with, to bear, to endu