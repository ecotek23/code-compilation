restaurant		饭店
San Francisco		旧金山
nationality		民族
which		哪个
that		那个
which person		哪位
that person		那位
lady		女士
this		这个
this person		这位
to live		住
question marker (expressing supposition of what answer will be)		吧
restaurant; hotel		大饭店
belonging (grammatical particle)		的
address		地址

fan4 dian4		饭店
jiu4 jin1 shan1		旧金山
min2 zu2		民族
na3 ge5		哪个
na4 ge5		那个
na3 wei4; nei3 wei4		哪位
nei4 wei4; na4 wei4		那位
nv3 shi4		女士
zhe4 ge5		这个
zhu4		住
ba5		吧
da4 fan4 dian4		大饭店
de5		的
di4 zhi3		地址

state guest; ambassador		国宾
number		号
home		家
street		街
road		路
where		哪里
there		那里
friend		朋友
major		少校
bank		银行
here		这里
first		第一
post office		邮政局
father		爸爸
younger brother		弟弟

guo2 bin1		国宾
hao4		号
jia1		家
jie1		街
lu4		路
na3 li3; na3 li5		哪里
na4 li5		那里
peng2 you5		朋友
shao4 xiao4		少校
yin2 hang2		银行
zhe4 li3; zhe4 li5		这里
di4 yi1		第一
you2 zheng4 ju2		邮政局
ba4 ba5		爸爸
di4 di5		弟弟

all		都
parents		父母
father		父亲
older brother		哥哥
with		跟
still		还
child		孩子
family		家里
older sister		姐姐
sisters		姐妹
how many; how much		几个
mother		妈妈
younger sister		妹妹
to not have		没有
plural		们

dou1, du1		都
fu4 mu3		父母
fu4 qin1		父亲
ge1 ge5		哥哥
gen1		跟
hai2		还
hai2 zi5		孩子
jia1 li3		家里
jie3 jie5		姐姐
jie3 mei4		姐妹
ji3 ge5		几个
ma1 ma5; ma1 ma1		妈妈
mei4 mei5		妹妹
mei2 you3		没有
men5		们

mother		母亲
male		男
boy		男孩子
woman		女
girl		女孩子
maternal grandfather		外祖父
maternal grandmother		外祖母
we		我们
brothers		兄弟
brothers and sisters		兄弟姐妹
to have		有
only		只
paternal grandmother		祖母
to arrive		到
son		儿子

mu3 qin1		母亲
nan2		男
nan2 hai2 zi3; nan2 hai2 zi5		男孩子
nv3		女
nv3 hai2 zi5		女孩子
wai4 zu3 fu4		外祖父
wai4 zu3 mu3		外祖母
wo3 men5		我们
xiong1 di4		兄弟
xiong1 di4 jie3 mei4		兄弟姐妹
you3		有
zhi3		只
zu3 mu3		祖母
dao4		到
er2 zi5		儿子

the day after tomorrow		后天
today		今天
to come		来
tomorrow		明天
which day; when		哪天
daughter		女儿
the day before yesterday		前天
time		时候
yes		是的
every day		天天
one person; alone		一个人
already		已经
to walk		走
yesterday		昨天
to marry		结婚

hou4 tian1		后天
jin1 tian1		今天
lai2		来
ming2 tian1		明天
nv3 er2		女儿
qian2 tian1		前天
shi2 hou5; shi2 hou4		时候
shi4 de5		是的
tian1 tian1		天天
yi1 ge5 ren2; yi2 ge5 ren2; yi2 ge4 ren2; yi1 ge4 ren2		一个人
yi3 jing1		已经
zou3		走
zuo2 tian1		昨天
jie2 hun1		结婚

but		可是
to think		想
how old		多大了
the year after next		后年
what date		几号
this year		今年
how old		几岁
which month		几月
next year		明年
which year		哪年
every year; annual		年年
the year before last		前年
last year		去年
to be born		生
years old		岁

ke3 shi4		可是
xiang3		想
duo1 da4 le5		多大了
hou4 nian2		后年
ji3 hao4		几号
jin1 nian2		今年
ji3 sui4		几岁
ji3 yue4		几月
ming2 nian2		明年
na3 nian2; nei3 nian2		哪年
qian2 nian2		前年
qu4 nian2		去年
sheng1		生
sui4		岁

what day of the week		星期几
Sunday		星期天
week		星期
moon, month		月
last month		上个月
next month		下个月
previously		从前
how long		多久
past tense		过
New York		纽约
to go		去
Hong Kong		香港
a pair		两
how much?		几
university		大学

xing1 qi1 ji3		星期几
xing1 qi1 tian1		星期天
xing1 qi1		星期
yue4		月
shang4 ge5 yue4		上个月
xia4 ge5 yue4		下个月
cong2 qian2		从前
duo1 jiu3		多久
guo4		过
niu3 yue1		纽约
qu4		去
xiang1 gang3		香港
liang3		两
ji3		几
da4 xue2		大学

speech		话
Washington		华盛顿
to know how to		会
economics		经济学
history		历史
State Council		国务院
difficult		难
to study		念书
Japanese		日文
easy		容易
to speak		说
literature		文学
to study		学
student		学生
to learn		学习

hua4		话
hua2 sheng4 dun4		华盛顿
hui4		会
jing1 ji5 xue2; jing1 ji4 xue2		经济学
li4 shi3		历史
guo2 wu4 yuan4		国务院
nan2		难
nian4 shu1		念书
ri4 wen2		日文
rong2 yi4		容易
shuo1		说
wen2 xue2		文学
xue2		学
xue2 sheng5; xue2 sheng1		学生
xue2 xi2		学习

a bit		一点
English		英文
political science		政治学
Chinese (language)		中文
to do		做
disease		病
German (language)		德文
French language		法文
navy		海军
officer		军官
serviceman		军人
lesson		课
air force		空军
army		陆军
soldier		士兵

yi1 dian3		一点
ying1 wen2		英文
zheng4 zhi5 xue2; zheng4 zhi4 xue2		政治学
zhong1 wen2		中文
zuo4		做
bing4		病
de2 wen2		德文
fa3 wen2		法文
hai3 jun1		海军
jun1 guan1		军官
jun1 ren2		军人
ke4		课
kong1 jun1		空军
lu4 jun1		陆军
shi4 bing1		士兵

to write		写
Chinese character		字
to do things		做事
only then		就
without		没
heaven		天
measure word		个
past tense		了
position		位
grandfather (fathers side)		祖父
they		他们
you (the plural form)		你们

xie3		写
zi4		字
zuo4 shi4		做事
jiu4		就
mei2		没
tian1		天
ge4		个
le5		了
wei4		位
zu3 fu4		祖父
ta1 men5		他们
ni3 men5		你