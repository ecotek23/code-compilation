por favor		please
un poco		some, a little
me gustaría		I would like
vino		wine
me gustaría un poco de vino por favor		I would like some wine please
hola		hi
adiós		goodbye
sí		yes
no		no
gracias		thank you
café		coffee
me gustaría un poco de café por favor		I would like some coffee please
hablo		I speak
español		Spanish
hablo español		I speak Spanish
¿hablas español?		Do you speak Spanish ?
hablo un poco de español		I can speak a little Spanish
inglés		English
¿hablas inglés?		Do you speak English?
muy bien		very well
hablas español muy bien		You speak Spanish very well
decir		to say
cómo		how
cómo se dice en español		how do you say ... in Spanish

soy		I am
tú eres		you are
¿eres inglés?		are you English?
soy inglés		I am English
americano		American
no eres americano		You are not American
no soy inglés		I am not English
feliz		happy
¿eres feliz?		are you happy?
cansado		tired
él es		he is
pero		but
yo soy inglés pero él es francés		I am English but he is French
yo estoy feliz pero él está cansado		I am happy but he is tired
me llamo		I am called
me llamo Miguel		I am called Michael
maravilloso		wonderful
esto es		this is
negro		black
azul		blue
verde		green
rojo		red
blanco		white
amarillo		yellow
ridículo		ridiculous
esto es ridículo		this is ridiculous
tú eres maravilloso		You are wonderful
tengo		I have
calor		hot
tengo calor		i am hot
frío		cold
tengo frío		I am cold
¿tienes calor?		Are you hot?
¿tienes frío?		Are you cold?
no tengo calor		I am not hot

té		tea
¿hay?		Is there?
¿hay té?		Is there tea?
¿hay vino?		Is there wine?
cerveza		beer
¿hay cerveza?		Is there beer?
sí, hay cerveza		yes, there is beer
¿tienes?		Do you have?
¿tienes té?		Do you have any tea?
¿hay taxis?		Are there any taxis?
gente		people
¿hay gente?		Are there any people?
muy		very
alguien		anyone
compro		I buy
cuesta		it costs
¿cuánto cuesta?		How much is it?
caro		expensive
barato		cheap
es caro		It is expensive
es muy caro		It is very expensive
es más caro		It is more expensive
es más barato		It is cheaper
gratis		free (of charge)
también		too, also
diferente		different
grande		big
es grande		It is big
pequeño		small
es pequeño		It is small
parece		It seems
parece más pequeño		It seems smaller
parece más grande		It seems bigger
verdad		true
fácil		easy
difícil		difficult
urgente		urgent
importante		important
posible		possible
es posible		it is possible
no		not
no es posible		It is not possible
imposible		impossible
¿qué es?		What is it?
yo sé		I know
no sé		I don’t know
cerdo		pork
res		beef
pollo		Chicken
¿es carne de res?		Is it beef?
de hecho		In fact, actually
me gusta		I like

la tienda		shop
¿hay tiendas?		Are there any shops?
taxi		taxi
¿hay taxis?		Are there any taxis?
muchos		many
hay muchos taxis		There are many taxis
hay muchos problemas		There are many problems
no hay taxis		There are no taxis
no hay problemas		There are no problems
porque		because
desafortunadamente		unfortunately
seguramente		certainly
especialmente		especially
por ejemplo		for example
duele		it hurts
ruidoso		noisy
un accidente		an accident
feo		ugly
enojado		angry
tiene hambre		He is hungry
tiene sed		He is thirsty
maestro		teacher
amigo		friend
mi		my
él es mi maestro		He is my teacher
ella es		she is
gracioso		funny
es hermosa		She is beautiful
es verdad		It is true
pienso		I think
pienso que es grande		I think it is big
pienso que es pequeño		I think it is small
ese		that (one)
ese es grande		that one is big

dónde		where
¿dónde está?		where is?
allá		over there
está allá		It is over there
aquí		here
está aquí		It is here
disculpa		excuse me
¿dónde está el hospital?		where is the hospital?
el hospital está allá		the hospital is over there
¿de dónde eres?		Where are you from?
soy de		I am from
soy de los Estados Unidos		I am from the United States
la fiesta		the party
¿dónde es la fiesta?		where is the party?
¿dónde está la fiesta?		where's the party at?
decir		to say
¿qué significa esto?		what does this mean?
dar vuelta		to turn
derecha		right
da vuelta a la derecha		turn right
izquierda		left
todo derecho		straight ahead
lejos		far
el hotel está lejos		The hotel is far
cerca		near
hacia		towards
norte		north
sur		south
este		east
oeste		west
ahora		now
uno		one
dos		two
tres		three
cuatro		four
cinco		five
seis		six
siete		seven
ocho		eight
nueve		nine
diez		ten
veinte		twenty
treinta		thirty
cuarenta y cinco		forty five
el viaje		The journey
¿cuánto tomará?		How long will it take?
una hora		one hour
¿qué hora es?		What time is it?
once		eleven
son las once		it is 11 o’clock
doce		twelve
son las doce		it is 12 o’clock
un segundo		one second
un día		one day
dos días		two days
al menos		at least

tengo que		I have to
tengo que beber vino		I have to drink wine
debo tener una idea		I must have an idea
debo ir a la Catedral		I must go to the Cathedral
aburrido		boring
pienso que es aburrido		I think it is boring
pienso que esto es hermoso		I think this is beautiful
podría		I might
podría beber cerveza		I might drink beer
podría ser americano		I might be American
satisfecho		satisfied
podrías estar satisfecho		you might be satisfied
decepcionante		disappointing
podría ser decepcionante		It might be disappointing
peligroso		dangerous
podría ser peligroso		It might be dangerous
ruidoso		noisy
maravilloso		wonderful
debería		I should
debería ir a la Ciudad de México		I should go to Mexico City
debería tomar agua		I should drink water
algo		something
el café está caliente		the coffee is hot
es el café de John		It is John's coffee
doy		I give
le doy café a John		I give John some coffee
le damos café		we give him coffee
es la cerveza de Jane		it is Jane's beer
le doy una cerveza a Jane		I give Jane a beer
le damos una cerveza		we give her a beer
el vino está delicioso		the wine is delicious
es el vino de John		it is John's wine
le doy vino a John		I give John wine
le damos vino		We give him wine
ella debe darme vino		She must give me wine
¿a quién debería dárselo?		Who should I give it to?
¿quién me lo dio?		Who gave it to me?
ella necesita que le den cerveza		she needs to be given some beer
debes darme el vino		You must give the wine to me
no le doy cerveza a Jane		I don't give beer to Jane

estoy estudiando		I am studying
estoy estudiando español		I am studying Spanish
estoy esperando		I am waiting
estoy esperando un taxi		I am waiting for a taxi
ella lo está esperando		she is waiting for him
iré		i will go
pagaré		I will pay
comeré		I will eat
él comerá		he will eat
cuándo		when
¿cuándo irás allá?		When will you go there?
¿cuándo irá allá?		When will she go there?
iré mañana		I will go tomorrow
más tarde		later
hoy		today
mañana		tomorrow
la tarde		afternoon
la próxima semana		next week
Iré a Cancún la próxima semana		I will go to Cancún next week
la estación de tren		train station
voy a		I am going
voy a ir a México		I am going to Mexico
¿vas a un restaurante?		Are you going to a restaurant?
sí, voy a un restaurante		Yes, I am going to a restaurant
come esto		eat this
bebe esto		drink this
espera aquí		wait here
detente aquí		stop here
¿llegas hoy?		Are you going to arrive today?
hablaré español		I will speak Spanish
pronto		soon
hablamos más tarde		We'll speak later
te veré más tarde		I will see you later
lo veré en el aeropuerto		I will see him at the airport
será divertido		It will be fun
será difícil		It will be difficult
increíble		incredible
será increíble		It will be incredible
lento		slowly
¿puedes hablar más lento por favor?		can you speak more slowly please?
repite		You repeat
¿puedes repetir eso por favor?		Can you repeat that please?

había		there was
había un problema		was there a problem?
hubo un problema		there was a problem
¿había vino?		was there any wine?
alguien		someone
había alguien ahí		there was someone there
fui		I went
fui a México		I went to México
fue a Acapulco la semana pasada		he went to Acapulco last week
tenía frío		I was cold
no había vino		there wasn’t any wine
no había taxis		there weren’t any taxis
bebí una cerveza		I had a beer
él bebió vino		he drank wine
bebiste agua		you drank water
comí carne de res		I ate beef
él comió vegetales		he ate vegetables
comiste cerdo		you ate pork
compraste café		you bought coffee
no bebí ni una cerveza		I didn’t have a beer
él no bebió vino		he didn’t have wine
no bebiste agua		you didn’t have water
no comí carne de res		I didn’t eat beef
él no comió vegetales		he didn’t eat vegetables
¿estaba feliz?		Was he happy?
¿estaba hambriento?		Was he hungry?
no estaba feliz		I wasn’t happy
no tenía frío		he wasn’t cold
no tenía hambre		she wasn’t hungry
no tenías hambre		you weren’t hungry
la culpa		fault
no fue mi culpa		it wasn't my fault
yo empiezo		I start
empecé a aprender español la semana pasada		I started learning Spanish last week
he estado aprendiendo español desde hace un mes		I have been learning Spanish for one month
hace		ago ( time )
fui a México hace dos meses		I went to México two months ago
vine a Los Cabos hace seis semanas		I came to Los Cabos six weeks ago
diversión		fun
divertirse		to have fun
me divertí en Oaxaca		I had fun in Oaxaca
me divertí contigo		I had fun with you
ayer por la noche		Yesterday night
me divertí anoche		I had fun last night
dije que llegaría hoy		I said that I would arrive tod