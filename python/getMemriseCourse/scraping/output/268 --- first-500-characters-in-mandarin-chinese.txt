belonging, possessive		的
one		一
to be		是
not		不
person		人
I (also: me)		我
located		在
to have		有
he		他
this		这
middle		中
big		大
to come		来
upon		上
kingdom		国
measure word		个
to arrive		到
to speak		说
plural		们
for the sake of		为
child		子
and		和
you		你
the earth		地
to go out		出


de5		的
yi1		一
shi4		是
bu4		不
ren2		人
wo3		我
zai4		在
you3		有
ta1		他
zhe4		这
zhong1		中
da4		大
lai2		来
shang4		上
guo2		国
ge4		个
dao4		到
shuo1		说
men5		们
wei4		为
zi3		子
he2		和
ni3		你
di4		地
chu1		出

the way		道
also		也
time		时
year		年
only then		就
that		那
to want		要
below		下
using		以
to be born		生
to know how to		会
self		自
to go		去
belonging		之
past tense		过
home		家
to study		学
against, correct		对
can		可
she		她
village (also: in, inside)		里
behind		后
small		小
what		么
heart		心

dao4		道
ye3		也
shi2		时
nian2		年
jiu4		就
na4		那
yao4		要
xia4		下
yi3		以
sheng1		生
hui4		会
zi4		自
qu4		去
zhi1		之
guo4		过
jia1		家
xue2		学
dui4		对
ke3		可
ta1		她
li3		里
hou4		后
xiao3		小
me5		么
xin1		心

many		多
heaven		天
and yet, beard (radical)		而
to be able		能
good		好
all		都
correct		然
sun		日
at, with		于
to rise		起
to perfect		成
business		事
only		只
to do		作
to work as		当
to think		想
script, pattern		文
to lack		无
to open		开
hand		手
ten		十
to use		用
lord		主
square		方
if		如

duo1		多
tian1		天
er2		而
neng2		能
hao3		好
dou1, du1		都
ran2		然
ri4		日
yu2		于
qi3		起
cheng2		成
shi4		事
zhi3		只
zuo4		作
dang1		当
xiang3		想
wen2		文
wu2		无
kai1		开
shou3		手
shi2		十
yong4		用
zhu3		主
fang1		方
ru2		如

front		前
which		所
root		本
to see		见
scripture		经
head		头
face		面
public		公
the same		同
three		三
already		已
old		老
following, from		从
to move		动
a pair		两
to know		知
people		民
appearance		样
now		现
to divide		分
to handle, will be		将
outside		外
but		但
body		身
a few		些

qian2		前
suo3		所
ben3		本
jian4		见
jing1		经
tou2		头
mian4		面
gong1		公
tong2		同
san1		三
yi3		已
lao3		老
cong2		从
dong4		动
liang3		两
zhi1		知
min2		民
yang4		样
xian4		现
fen1		分
jiang1		将
wai4		外
dan4		但
shen1		身
xie1		些

to give; with, and		与
tall		高
opinion (also: idea)		意
to enter		进
law		法
this		此
reality		实
to return		回
two		二
logic		理
beautiful		美
a bit		点
moon, month		月
bright		明
sound		声
complete		全
work		工
oneself		己
speech		话
son		儿
person who does		者
direction		向
emotion		情
department		部
straight		正

yu3		与
gao1		高
yi4		意
jin4		进
fa3		法
ci3		此
shi2		实
hui2		回
er4		二
li3		理
mei3		美
dian3		点
yue4		月
ming2		明
sheng1		声
quan2		全
gong1		工
ji3		己
hua4		话
er2		儿
zhe3		者
xiang4		向
qing2		情
bu4		部
zheng4		正

name		名
to decide		定
woman		女
to ask		问
strength		力
machine		机
to give		给
to wait for		等
small table		几
very		很
industry		业
most, superlative		最
space		间
new		新
what		什
to hit		打
convenient		便
position		位
because of		因
by (indicates passive-voice sentences or clauses)		被
to walk		走
electricity		电
four		四
ordinal number		第
gate		门

ming2		名
ding4		定
nv3		女
wen4		问
li4		力
ji1		机
gei3		给
deng3		等
ji1; ji3		几
hen3		很
ye4		业
zui4		最
jian1		间
xin1		新
shen2		什
da3		打
bian4		便
wei4		位
yin1		因
bei4		被
zou3		走
dian4		电
si4		四
di4		第
men2		门

to examine		相
next, measure word for actions		次
east		东
politics		政
sea		海
mouth		口
to cause		使
to teach		教
west		西
again		再
to weigh		平
real		真
to listen		听
world		世
to breathe		气
letter, mail		信
north		北
few		少
shut; to concern		关
to combine		并
inside		内
to increase		加
to change into (also: to spend)		化
from		由
but		却

xiang4		相
ci4		次
dong1		东
zheng4		政
hai3		海
kou3		口
shi3		使
jiao1		教
xi1		西
zai4		再
ping2		平
zhen1		真
ting1		听
shi4		世
qi4		气
xin4		信
bei3		北
shao3		少
guan1		关
bing4		并
nei4		内
jia1		加
hua4		化
you2		由
que4		却

generation		代
army		军
to produce		产
to enter		入
first, previously, to precede		先
mountain		山
five		五
extremely		太
water		水
ten thousand		万
market		市
eye		眼
body		体
to separate		别
place		处
always		总
only just		才
open space		场
teacher		师
book		书
to compare		比
to live		住
person		员
nine		九
to laugh		笑

dai4		代
jun1		军
chan3		产
ru4		入
xian1		先
shan1		山
wu3		五
tai4		太
shui3		水
wan4		万
shi4		市
yan3		眼
ti3		体
bie2		别
chu4		处
zong3		总
cai2		才
chang3		场
shi1		师
shu1		书
bi3		比
zhu4		住
yuan2		员
jiu3		九
xiao4		笑

gender; suffix -ness or -ity		性
through		通
eye		目
to announce		报
to stand		立
horse		马
an order (also: destiny, life)		命
to stretch		张
life		活
spirit		神
maths		数
correspondence		件
peace		安
watch; display		表
origin		原
car		车
white		白
to respond		应
road		路
a period of time		期
to call		叫
to die		死
often		常
to raise		提
to sense		感

xing4		性
tong1		通
mu4		目
bao4		报
li4		立
ma3		马
ming4		命
zhang1		张
huo2		活
shen2		神
shu4; shu3		数
jian4		件
an1		安
biao3		表
yuan2		原
che1		车
bai2		白
ying4		应
lu4		路
qi1		期
jiao4		叫
si3		死
chang2		常
ti2		提
gan3		感

gold		金
what		何
to rebel		反
to shut (also: to join)		合
to put (also: to release)		放
to do		做
to count		计
perhaps		或
to take charge of		司
to receive		受
lustre; light		光
king		王
fruit		果
relatives (also: parent)		亲
world		界
to attain		及
present		今
capital		京
affair		务
to control		制
to divide		解
each		各
to appoint		任
to reach (arrive)		至
clear		清

jin1		金
he2		何
fan3		反
he2		合
fang4		放
zuo4		做
ji4		计
huo4		或
si1		司
shou4		受
guang1		光
wang2		王
guo3		果
qin1		亲
jie4		界
ji2		及
jin1		今
jing1		京
wu4		务
zhi4		制
jie3; xie4; jie4		解
ge4		各
ren4		任
zhi4		至
qing1		清

substance		物
stage		台
elephant		象
to remember		记
side		边
collectively		共
wind		风
war		战
to receive		接
it		它
to allow; to promise		许
eight		八
special (also: unique, distinguished)		特
to hope		望
straight		直
to serve		服
fur		毛
forest		林
topic		题
to construct		建
south		南
degree		度
to unify		统
color		色
Chinese character		字

wu4		物
tai2		台
xiang4		象
ji4		记
bian1		边
gong4		共
feng1		风
zhan4		战
jie1		接
ta1		它
xu3		许
ba1		八
te4		特
wang4		望
zhi2		直
fu2		服
mao2		毛
lin2		林
ti2		题
jian4		建
nan2		南
du4		度
tong3		统
se4		色
zi4		字

please		请
to exchange		交
to love		爱
to allow		让
to recognize		认
to calculate, to figure, to count, to plan		算
to discuss		论
hundred		百
to eat		吃
justice		义
science		科
how		怎
leading, first, unit of money		元
society		社
method		术
knot		结
six		六
achievement		功
finger		指
to think, to ponder		思
wrong		非
to flow		流
every		每
green		青
to care about		管

qing3		请
jiao1		交
ai4		爱
rang4		让
ren4		认
suan4		算
lun4		论
bai3		百
chi1		吃
yi4		义
ke1		科
zen3		怎
yuan2		元
she4		社
shu4		术
jie2		结
liu4		六
gong1		功
zhi3		指
si1		思
fei1		非
liu2		流
mei3		每
qing1		青
guan3		管

husband		夫
to link (also: continuously)		连
to be far away		远
capital (in the sense of money)		资
team		队
with		跟
to bring		带
flower		花
fast		快
twig		条
courtyard		院
to change (to transform, to become)		变
to unite		联
word		言
authority		权
towards		往
to unfold, to open, to spread out		展
should		该
to lead		领
to spread		传
near		近
to leave (something behind)		留
red		红
to govern		治
to decide		决

fu1		夫
lian2		连
yuan3		远
zi1		资
dui4		队
gen1		跟
dai4		带
hua1		花
kuai4		快
tiao2		条
yuan4		院
bian4		变
lian2		联
yan2		言
quan2		权
wang3		往
zhan3		展
gai1		该
ling3		领
chuan2		传
jin4		近
liu2		留
hong2		红
zhi4		治
jue2		决

week		周
to protect		保
to express (also: to attain, reach)		达
to manage		办
to transport		运
martial		武
half		半
to wait		候
seven		七
certainly		必
city walls		城
father		父
strength / strong		强
a pace		步
end (also: finished, to run out)		完
cowhide		革
deep		深
region		区
immediately, even if		即
to look for		求
product		品
scholar		士
to revolve		转
quantity		量
very		甚

zhou1		周
bao3		保
da2		达
ban4		办
yun4		运
wu3		武
ban4		半
hou4		候
qi1		七
bi4		必
cheng2		城
fu4		父
qiang2		强
bu4		步
wan2		完
ge2		革
shen1		深
qu1		区
ji2		即
qiu2		求
pin3		品
shi4		士
zhuan4; zhuan3		转
liang4		量
shen4		甚

crowd		众
skill		技
lightweight		轻
rule (also: journey)		程
to tell		告
river		江
language		语
hero		英
foundation		基
to send		派
full		满
type		式
plum tree		李
to rest		息
to write		写
a particle (a grammatical particle, not a physical one)		呢
knowledge		识
summit; extremely		极
to command		令
yellow		黄
moral		德
harvest		收
face		脸
money		钱
(political) party		党

zhong4		众
ji4		技
qing1		轻
cheng2		程
gao4		告
jiang1		江
yu3		语
ying1		英
ji1		基
pai4		派
man3		满
shi4		式
li3		李
xi1		息
xie3		写
ne5		呢
shi2		识
ji2		极
ling4		令
huang2		黄
de2		德
shou1		收
lian3		脸
qian2		钱
dang3		党

to turn upside down		倒
not yet		未
to hold		持
to grab		取
to arrange		设
beginning		始
edition, printing plate		版
twin		双
calendar		历
to exceed		越
history		史
to trade		商
thousand		千
slice		片
to permit		容
to grind (also: study; research)		研
to look like		像
to look for		找
friend		友
child		孩
station		站
vast, lean-to (radical)		广
to change		改
to comment on		议
shape		形

dao4		倒
wei4		未
chi2		持
qu3		取
she4		设
shi3		始
ban3		版
shuang1		双
li4		历
yue4		越
shi3		史
shang1		商
qian1		千
pian4		片
rong2		容
yan2		研
xiang4		像
zhao3		找
you3		友
hai2		孩
zhan4		站
guang3		广
gai3		改
yi4		议
xing2		形

to entrust		委
early		早
building		房
tone		音
fire		火
border		际
standard		则
chief		首
according to		据
to guide		导
shadow		影
to fail		失
to hold		拿
net		网
fragrant		香
similar		似
this		斯
specialised		专
rock		石
to seem		若
soldier		兵
younger brother		弟
who		谁
to read		读
records		志

wei3		委
zao3		早
fang2		房
yin1		音
huo3		火
ji4		际
ze2		则
shou3		首
ju4		据
dao3		导
ying3		影
shi1		失
na2		拿
wang3		网
xiang1		香
si4; shi4		似
si1		斯
zhuan1		专
shi2		石
ruo4		若
bing1		兵
di4		弟
shei2		谁
du2		读
zhi4		志

to fly		飞
to watch		观
to compete		争
to investigate (also: to study carefully)		究
to wrap		包
to organize		组
to make		造
to fall		落
to look at		视
to benefit		济
to enjoy		喜
to depart		离
although		虽
to sit		坐
to gather, to collect		集
to organize		编
treasure		宝
to chat		谈
government office		府
to pull		拉
black		黑
moreover, a long time (radical)		且
to comply with		随
line		格
to exhaust		尽

fei1		飞
guan1		观
zheng1		争
jiu1		究
bao1		包
zu3		组
zao4		造
luo4		落
shi4		视
ji4		济
xi3		喜
li2		离
sui1		虽
zuo4		坐
ji2		集
bian1		编
bao3		宝
tan2		谈
fu3		府
la1		拉
hei1		黑
qie3		且
sui2		随
ge2		格
jin3; jin4		尽

sword		剑
to speak		讲
to declare		布
to kill		杀
micro		微
to fear		怕
female elder, mother		母
to exchange		调
office, chessboard (radical meaning)		局
root; unit for long thin objects		根
once (in the past)		曾
to allow		准
group		团
paragraph		段
finish, end		终
happiness		乐
to cut		切
grade		级
to restrain		克
refined		精
which		哪
mandarin (a government official)		官
spirit, sign, altar		示
to dash against		冲
to finish, eventually, whole		竟

jian4		剑
jiang3		讲
bu4		布
sha1		杀
wei1		微
pa4		怕
mu3		母
diao4		调
ju2		局
gen1		根
ceng2		曾
zhun3		准
tuan2		团
duan4		段
zhong1		终
le4		乐
qie1		切
ji2		级
ke4		克
jing1		精
na3		哪
guan1		官
shi4		示
chong1		冲
jing4		竟

to doubt		乎
past tense		了
must		得
ongoing		着
negative		没
still		还
to dispatch		发
to look		看
walk, go, travel, line, rank, okay		行
again, right hand (radical)		又
long		长
to grasp		把
species		种
heavy		重
such (refers to something preceding it)		其
magnificent (also: China)		华
difficult		难
even more		更
department		系
dry		干
to sleep		觉
empty		空
single		单
school		校
male		男

hu1		乎
le5		了
dei3, de2, de5		得
zhe5		着
mei2		没
hai2		还
fa1		发
kan4		看
xing2		行
you4		又
chang2; zhang3; zhang1		长
ba3		把
zhong3		种
zhong4		重
qi2		其
hua2		华
nan2		难
geng4		更
xi4		系
gan1		干
jiao4		觉
kong1		空
dan1		单
xiao4		校
nan2	