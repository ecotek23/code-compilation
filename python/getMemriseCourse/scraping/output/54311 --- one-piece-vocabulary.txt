とみ		wealth
めいせい		fame
ちから		power
かいぞく		pirate
かいぞくおう		pirate king
どろぼう		thief
たから		treasure
ゆめ		dream
ちず		map
らしんばん		compass

あくま		devil
あくまのみ		devils fruit
ふね		ship
なみ		wave
うみ		sea
あらし		storm
たすける		help
ぬすむ		steal
ちょうちょう		town mayor
こうかいし		navigator

むら		village
しま		island
ゴム		rubber
やった!		yay!
けんし		swordsman
かいぐん		marine
むぎ わら		wheat straw
しょうきん		bounty
おう		king
トナカイ		reindeer

しょうきん かせぎ		bounty hunter
のうりょく		ability
のうりょくしゃ		ability user
にく		meat
なかま		comrade
たから の ちず		treasure map
はらへった		hungry
しつじ		butler
うそ		lie
うそつき		lier

さいみんじゅつし		hypnotist
せんちょう		ship's captain
たのみ		favour
こうかい		voyage
やろう		rascal
あほ		fool
ばか		idiot
ばかやろう		idiot!
くそ		shit / damn
すごい		amazing

ぼうし		hat
まりも		seaweed
ぎょじん		fishman
にんぎょ		mermaid
きょじん		giant
ふなだいく		carpenter / shipwright
いしゃ		doctor
さばく		desert
そげきへい		sniper
さくせん		plan
たかのめ		hawk e