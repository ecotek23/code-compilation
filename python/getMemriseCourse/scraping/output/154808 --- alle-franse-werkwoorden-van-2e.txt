je fais: tu fais: il fait: nous faisons: vous faites: ils font		doen, maken
je viens: tu viens: il vient: nous venons: vous venez: ils viennent		komen
je peux: tu peux: il peut: nous pouvons: vous pouvez: ils peuvent		kunnen, mogen
je veux: tu veux: il veut: nous voulons: vous voulez: ils veulent		willen
j'écris: tu écris: il écrit: nous écrivons: vous écrivez: ils écrivent		schrijven
je vois: tu vois: il voit: nous voyons: vous voyez: ils voient		zien
j'offre: tu offres: il offre: nous offrons: vous offrez: ils offrent		aanbieden
je mets: tu mets: il met: nous mettons: vous mettez: ils mettent		zetten, leggen
je parle: tu parles: il parle: nous parlons: vous parlez: ils parlent		praten
je nage: tu nages: il nage: nous nageons: vous nagez: ils nagent		zwemmen
je change: tu changes: il change: nous changeons: vous changez: ils changent		veranderen
je mange: tu manges: il mange: nous mangeons: vous mangez: ils mangent		eten

j'achète: tu achètes: il achète: nous achetons: vous achetez: ils achètent		kopen
j'appelle: tu appelles: il appelle: nous appelons: vous appelez: ils appellent		roepen, noemen
je commence: tu commences: il commence: nous commençons: vous commencez: ils commencent		beginnen
je paie: tu paies: il paie: nous payons: vous payez: ils paient		betalen
je me lave: tu te laves: il se lave: nous nous lavons: vous vous lavez: ils se lavent		zich wassen
je finis: tu finis: il finit: nous finissons: vous finissez: ils finissent		eindigen
je suis: tu es: il est: nous sommes: vous êtes: ils sont		zijn
j'ai: tu as: il a: nous avons: vous avez: ils ont		hebben
je vais: tu vas: il va: nous allons: vous allez: ils vont		gaan
je prends: tu prends: il prend: nous prenons: vous prenez: ils prennent		nemen
je comprends: tu comprends: il comprend: nous comprenons: vous comprenez: ils comprennent		begrijpen
j'apprends: tu apprends: il apprend: nous apprenons: vous apprenez: ils apprennent		leren

houden van/leuk vinden		j'aime: tu aimes: il aime: nous aimons: vous aimez: ils aiment
kennen/weten		je connais: tu connais: il connaît: nous aonnaissons: vous connaissez: ils connaissent
weten/kunnen		je sais: tu sais: il sait: nous savons: vous savez: ils save