import fileinput

identifier = 'ERROR WITH COURSE: '

def is_error_line(line):
	return line.startswith(identifier);

for line in fileinput.input(inplace=True, backup='.bak'):
	if is_error_line(line):
		print(line.lstrip(identifier), end='')