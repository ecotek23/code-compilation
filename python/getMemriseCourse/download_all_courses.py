from BeautifulSoup import BeautifulSoup
import download_course
import requests
import argparse
import os

def get_course_hrefs(account_url):
	soup = BeautifulSoup(requests.get(account_url).content)
	course_urls = [i['href'].lstrip('/course/') for i in soup.findAll('a',{'class':'picture-wrapper'})]
	return course_urls	

if __name__=='__main__':
	# My input is 'http://www.memrise.com/user/ingl0ri0usandr0id/courses/learning/'

	# Parse the url provided on the command line
	parser = argparse.ArgumentParser(description='Retrieve necessary url for account')
	parser.add_argument('account_url', metavar='id', type=str,
                    help='URL of the course to download')
	args = parser.parse_args()

	# Get the course url's on the account page
	course_hrefs = get_course_hrefs(args.account_url)

	# Download each course
	for course_href in course_hrefs:
		if not os.path.isfile(course_href.split('/')[0]+'.txt'):
			try:
				print 'downloading course: {}'.format(course_href)
				download_course.get_course(course_href)
			except Exception as e:
				print 'ERROR WITH COURSE: {}'.format(course_href)
		else: # Already downloaded that course
			print 'Already have course: {}'.format(course_href)
			continue
