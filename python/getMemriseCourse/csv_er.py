# For each file provided on the command line, change the delimiter from a double tab to csv

import fileinput

for line in fileinput.input(inplace=True,backup='.bak'):
	line = line.replace(',',';')
	print(', '.join(line.split('\t\t')),end='')