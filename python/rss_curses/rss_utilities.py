import feedparser
import curses
import os

def screen_init():
        screen = curses.initscr()
        screen.clear()
        screen.border()
        return screen

def get_feeds(database_connection):
	cursor = database_connection.cursor()
	cursor.execute('SELECT * FROM Feeds')
	feeds = cursor.fetchall()
	return feeds

def to_urls(feeds_list):
	url_list = [i[0] for i in feeds_list]
	return url_list

###
# WRITE THIS SAVING ROUTINE - IS IT NECESSARY?
###
def save_data(filename,data): # Export data to text file
	"""
	Write the database out to the text file
	"""
	if os.path.isfile(filename): # Already exists so append to it
		pass
	else:
		pass

def validate_and_add(feed_url,conn):
	"""
	Return a boolean value if the feed was valid and was added
	"""
	cur = conn.cursor()
	d = feedparser.parse(feed_url)
	if d.get('bozo_exception',0): # Feed was not valid, document was empty ###### PROBABLY NOTIFY
		return False
	else:
		cur.execute('INSERT INTO Feeds VALUES ("{}")'.format(feed_url))
		conn.commit()
		return True
