############################################
############################################
#
# TO-DO
#       1	Add in error handling where appropriate
#       2	Deal with overflowing text
#       3	Prevent duplicates being added
#       4	Add in ability to launch webbrowser with feed url (PUT ON FEED LIST)
#	5	Delete removes all, including duplicates (refer back to 3)
#
############################################
############################################

from rss_utilities import *

import feedparser
import sqlite3
import curses
import time
import os

feeds_database = 'feeds.db'

options = ["1\tList all feeds",
           "2\tDisplay all feeds",
           "3\tAdd a feed",
           "4\tDelete a feed",
           "5\tDisplay a specific feed",
           "6\tQuit",]

def main_menu():
        screen = screen_init()
        screen.addstr(2,1,"Main Menu")
        
        row_counter = 3
        for option in options:
                screen.addstr(row_counter,3,options[row_counter-3])
                row_counter+=1
        screen.addstr(row_counter,2,"Please input your selection: ")

        try:
                selection = int(screen.getstr())
        except: # Invalid option selected
                return -1       
        return selection
                
def create_feeds_database(feeds_database):
        """
        Creates the database required, adding in the necessary tables
        Returns a connection to the newly created database
        """
        conn = sqlite3.connect(feeds_database)
        cur = conn.cursor()
        cur.execute('CREATE TABLE Feeds(Url TEXT)')
        #### TESTING VALUES
        cur.execute('INSERT INTO Feeds VALUES ("http://feeds.bbci.co.uk/news/world/rss.xml?edition=uk")')
        cur.execute('INSERT INTO Feeds VALUES ("http://feeds.bbci.co.uk/news/uk/rss.xml?edition=uk")')
        cur.execute('INSERT INTO Feeds VALUES ("http://feeds.bbci.co.uk/news/technology/rss.xml?edition=uk")')
        ####
        conn.commit()
        return conn

def display_feed(feed_url):
        row_counter = 2
        
        curses.endwin()
        screen = screen_init()
        
        feed_parsed = feedparser.parse(feed_url)
        
        screen.addstr(row_counter,1,feed_parsed.feed.title)
        row_counter+=1
        
        for entry in feed_parsed.entries:
                screen.addstr(row_counter,3,entry.title)
                screen.addstr(row_counter+1,6,entry.summary_detail.value)
                row_counter+=2
                break ##### prevent overflow for testing
                
        x = screen.getch()
        curses.endwin()

def main_feed_display(feeds_list):
        row_counter = 2 

        screen = screen_init()
        
        screen.addstr(row_counter,1,'Please select an option:')
        row_counter+=1
        
        for index,feed in enumerate(feeds_list):
                feed_parsed = feedparser.parse(feed[0])
                screen.addstr(row_counter,3,'{}\t{}'.format(index,feed_parsed.feed.title))
                row_counter+=1
        
        screen.addstr(row_counter,1,'Please input your selection: ')
        
        try:
                feed_index = int(screen.getstr())
        except ValueError: # Unable to convert to int
                return
        
        if (feed_index < 0) or (feed_index >= len(feeds_list)):
                return # Outside of acceptable range
        
        display_feed(feeds_list[feed_index][0])

def option_1(feeds_list):
        row_counter = 3

        screen = screen_init()
        screen.addstr(row_counter-1,1,'All saved feeds:')

        for index,feed in enumerate(feeds_list):
                feed_parsed = feedparser.parse(feed[0])
                screen.addstr(row_counter,3,"{}\t{}".format(index,feed_parsed.feed.title))
                screen.addstr(row_counter+1,6,feed_parsed.href)
                row_counter+=2

        screen.addstr(row_counter,1,'Press a key to return to the main menu')
        screen.getch()

def option_3(database_connection):
        row_counter = 3

        screen = screen_init()
        screen.addstr(row_counter-1,1,'Please input the feed you would like added: ')

        feed_to_be_added = screen.getstr()

        validate_and_add(feed_to_be_added,database_connection)

def option_4(database_connection,feeds_list):
        row_counter = 3

        screen = screen_init()
        screen.addstr(row_counter-1,1,'All saved feeds:')

        for index,feed in enumerate(feeds_list):
                feed_parsed = feedparser.parse(feed[0])
                screen.addstr(row_counter,3,'{}\t{}'.format(index,feed_parsed.feed.title))
                row_counter+=1
        screen.addstr(row_counter,1,'Please input the feed you would like to be deleted: ')

        feed_index = int(screen.getstr())

        if (feed_index < 0) or (feed_index >= len(feeds_list)): # Invalid index to delete
                return
        
        feed_to_delete = feeds_list[feed_index]

        database_cursor = database_connection.cursor()
        database_cursor.execute('DELETE FROM Feeds WHERE Url="{}"'.format(feed_to_delete[0]))
        database_connection.commit()
        

if __name__ == '__main__':
        if not os.path.isfile(feeds_database): # Database does not exist - either first run or deleted by user
                conn = create_feeds_database(feeds_database)
        else: # Database already exists so just open connection
                conn = sqlite3.connect(feeds_database)

        feeds = get_feeds(conn)

        selection = main_menu()
        while selection != len(options): # HACK FOR LAST OPTION BEING QUIT
                if selection==1: # List all the feeds
                        option_1(feeds)

                elif selection==2: # Display all feeds # SEPARATE OUT THIS SECTION
                        screen = screen_init()
                        row_counter = 2
                        screen.addstr(row_counter,1,"PYTHON RSS FEED DISPLAY")
                        row_counter+=2
                        for feed in feeds:
                                display_feed(feed[0])
                        screen.refresh()

                elif selection==3: # Add a feed
                        option_3(conn)
                        feeds = get_feeds(conn)

                elif selection==4: # Delete a feed
                        option_4(conn,feeds)
                        feeds = get_feeds(conn)

                elif selection==5: # Display a specific feed
                        main_feed_display(feeds)

                selection = main_menu()

        # Close curses, commit changes and close connection
        curses.endwin()
        conn.commit()
        conn.close()
