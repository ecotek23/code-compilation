This is a small python program that allows you to monitor RSS feeds. It's currently limited to outputting 1 story per feed since I haven't yet looked into dealing with
paging the stories into an aesthetically pleasing fashion.

TO-DO
	Dealing with page scrolling to prevent overflow
	Maybe look into ticker type information feed for currently tracked feeds