import shutil
import os

if __name__ == '__main__':
	basedir = '/home/vfv/Desktop/russianpodcast_scrape/'
	for root,dirnames,filenames in os.walk(basedir):
		for dirname in dirnames:
			if dirname.startswith('Podcast N') or dirname.startswith('Подкаст'):
				pieces = dirname.split(' ')[:2]
				pieces[0] = 'Podcast'
				pieces[1] = pieces[1][2:-1]
				newdir = os.path.join(root,' '.join(pieces[:2]))
				olddir = os.path.join(root,dirname)
				shutil.move(olddir,newdir)
				