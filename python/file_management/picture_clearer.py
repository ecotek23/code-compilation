import os

base = r"/home/vfv/Pictures"
repo = r"/home/vfv/.config/variety/Favorites"

repo_files = os.listdir(repo)
base_files = os.listdir(base)

dup_count = 0
del_count = 0

for current_image in repo_files:
	if current_image in base_files:
		dup_count += 1
		try:
			os.remove("{}/{}".format(base,current_image))
			del_count += 1
		except:
			pass

print "Duplicates found: {}".format(dup_count)
print "Duplicates deleted: {}".format(del_count)