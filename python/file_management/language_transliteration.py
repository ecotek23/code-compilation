'''
 python3 required for unicode considerations

 recursively walks through the supplied root directory transliterating
 all the names from the cyrillic alphabet into the latin alphabet.
 (I needed this because filezilla was playing up with unicode names)
'''
from transliterate import translit, get_available_language_codes
import os

def walker(root_dir,language_encoding='ru'):
	dirs_to_change = []
	for root,dirnames,filenames in os.walk(dir):
		for filename in filenames:
			specific_file_hit(os.path.join(root,filename),language_encoding)
		dirs_to_change.extend([os.path.join(root,i) for i in dirnames])
	map(specific_file_hit,dirs_to_change) # Change all directories
	
def specific_file_hit(filename,language_coding):
	dirname,basename = os.path.split(filename)
	basename_trans = translit(basename,language_coding,reversed=True)
	if basename!=basename_trans:
		new = os.path.join(dirname,basename_trans)
		print("{} --> {}".format(filename,new))
		os.rename(filename,new)

if __name__=='__main__':
	dir = "/home/vfv/Desktop/russianpodcast_scrape"
	print('running script')
	walker(dir)
	print('finished walk')