from hash_routines import hash
import hashlib
import os

if __name__=='__main__':
	keep_dir = raw_input("Enter the keep_dir: ")
	repo_dir = raw_input("Enter the repo_dir: ")
	keep_directory_listing = os.listdir(keep_dir)
	repo_directory_listing = os.listdir(repo_dir)
	keep_hashes = []
	del_count = 0
	
	for current_image in keep_directory_listing: # Set up hashes to find duplicates of
		with open("{}/{}".format(keep_dir,current_image)) as image_handle:
			image_hash = hash(image_handle,hashlib.sha256())
			keep_hashes.append(image_hash)
	
	for current_image in repo_directory_listing: # Go through image repo and remove any duplicates found
		with open("{}/{}".format(repo_dir,current_image)) as image_handle:
			image_hash = hash(image_handle,hashlib.sha256())
			if image_hash in keep_hashes:
				os.remove("{}/{}".format(repo_dir,current_image))
				del_count += 1

	print "Duplicates deleted: {}".format(del_count)