from hash_routines import hash
import hashlib
import os

if __name__ == '__main__':
#	base_dir = r"/root/Pictures/Mega background dump"
	base_dir = raw_input("Enter the base_dir: ")
	directory_listing = os.listdir(base_dir)
	hashes = []
	del_count = 0
	
	for current_image in directory_listing:
		with open("{}/{}".format(base_dir,current_image)) as image_handle:
			image_hash = hash(image_handle,hashlib.sha256())
			if image_hash in hashes:
				os.remove("{}/{}".format(base_dir,current_image))
				del_count += 1
			else:
				hashes.append(image_hash)
	
	print "Duplicates deleted: {}".format(del_count)