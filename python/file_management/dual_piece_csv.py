import fileinput

def pieces_test(number_of_pieces):
	for line in fileinput.input(inplace=True,backup='.bak'):
		if len(line.split(','))==number_of_pieces:
			print(line,end='')

if __name__=='__main__':
	pieces_test(2);