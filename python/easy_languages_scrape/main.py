import subprocess
import fileinput
import os

if __name__ == '__main__':
	for line in fileinput.input():
		line = line.strip()
		command = 'youtube-dl --ignore-errors --yes-playlist -o videos/%(playlist_title)s/%(title)s youtube.com{}'.format(line)
		subprocess.call(command.split(),shell=False)