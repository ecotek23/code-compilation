from pprint import pprint as p
from bs4 import BeautifulSoup

def get_questions(soup):
	return [i.text.strip() for i in soup.findAll('q')]

def get_answers(soup):
	return [i.text.strip() for i in soup.findAll('a')]

def get_questions_and_answers(soup):
	questions = get_questions(soup)
	answers = get_answers(soup)
	return list(zip(questions,answers))

if __name__ == '__main__':
	filename = '/home/vfv/fullrecalldb/spanish/spanish_nouns_1_2/elements.xml'
	soup = BeautifulSoup(open(filename,'r').read(),'lxml')
	p(get_questions_and_answers(soup))