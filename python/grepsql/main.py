#
# Look at implementing in C++ for an exercise
# ADD IN COLORING OF THE PRINT TO EMULATE GREP
#
import argparse
import sqlite3
import re
import os

def initialize_testing():
	'''
	Create two tables for testing
	'''
	conn = sqlite3.connect('testing.db')
	c = conn.cursor()
	
	c.execute('''CREATE TABLE stocks
				(date text, trans text, symbol text, qty real, price real)''')
	c.execute('''CREATE TABLE stocks_2
				(date text, trans text, symbol text, qty real, price real)''')
				
	c.execute('INSERT INTO stocks VALUES ("TEST","TEST","TEST",0.01,0.01)')
	
	c.execute('INSERT INTO stocks_2 VALUES ("BEST","BEST","BEST",0.01,0.01)')
	c.execute('INSERT INTO stocks_2 VALUES ("VEST","VEST","VEST",0.01,0.01)')
	c.execute('INSERT INTO stocks_2 VALUES ("JEST","JEST","JEST",0.01,0.01)')
	conn.commit()
	conn.close()

if __name__=='__main__':
	parser = argparse.ArgumentParser(description='Grep through a database')
	#	parser.add_argument('--db',type=str) # add this in when using mysql instead of the sqlite testing
	parser.add_argument('--val', type=str)
	args = parser.parse_args()
	
	if not os.path.isfile('testing.db'):
		initialize_testing()
	
	conn = sqlite3.connect('testing.db')
	c = conn.cursor()
	
	c.execute('SELECT name FROM sqlite_master WHERE type="table";')
	tables = c.fetchall()
	
	pattern = re.compile(args.string)
	
	for table in tables:
		c.execute('SELECT * FROM {}'.format(*table))
		rows = [list(map(str,i)) for i in c.fetchall()]
		for row in rows:
			for item in row:
				if re.findall(pattern,item):
					print(*table,end=":\t")
					print(row)
					break
