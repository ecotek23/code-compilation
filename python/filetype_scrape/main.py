'''
TO-DO
	ADD IN COMMAND LINE ARGUMENTS FOR URL AND FILETYPE
	DEAL WITH SPLIT/BASENAME IN DOWNLOAD SECTION
'''
from urllib.parse import urljoin
from pprint import pprint as p
from bs4 import BeautifulSoup
import requests
import os

def soupify(url):
	soup = BeautifulSoup(requests.get(url).content,'lxml')
	return soup

def find_links(url):
	soup = soupify(url)
	hrefs = [i['href'] for i in soup.find_all('a')]
	return hrefs

def filter_types(hrefs,type):
	new_hrefs = [i for i in hrefs if i.endswith(type)]
	return new_hrefs

def download_file(url):
	filename = os.path.basename(url)
	with open(filename,'wb') as output_file:
		content = requests.get(url).content
		output_file.write(content)

if __name__ == '__main__':
	page = 'http://www.gefix.net/sazov/book.html'
	download_base = 'http://www.gefix.net/sazov/'
	hrefs = find_links(page)
	types = filter_types(hrefs,'zip')
	for option in types:
		url = urljoin(download_base,option)
		download_file(url)