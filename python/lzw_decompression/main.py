from io import StringIO
import argparse
import bitio # Implementation of bit reading from http://rosettacode.org/wiki/Bitwise_IO#Python
import os

def init_dict():
	'''
	Set up the initial dictionary of codes as described in the algorithm specification
	'''
	initial_dict = {i:chr(i) for i in range(256)}
	return initial_dict

def decompress(filename,fixed_width=12):
	'''
	Decompress the file provided, both writing to stdout and saving a copy locally
	'''
	with open(filename,'rb') as f:
		reader = bitio.BitReader(f)
		result = StringIO()
		
		# Build the dictionary.
		dictionary = init_dict()
		dict_size = len(dictionary)

		# Deal with the first piece
		current = reader.readbits(fixed_width)
		result.write(dictionary[current])

		# Begin iteration over the remaining pieces
		while True:
			nxt = reader.readbits(fixed_width)

			# Code already exists in the dictionary
			if nxt in dictionary:
				entry = dictionary[nxt]
			# A new code
			elif nxt == dict_size:
				try:
					entry = dictionary[current]+dictionary[current][0]
				except:
					print(current)
					exit(0)
			result.write(entry)

			# Add to dictionary, increment size monitor and shift current along
			dictionary[dict_size] = dictionary[current]+entry[0]
			dict_size+=1
			current=nxt

			# The compressed file has been entirely read
			if reader.read==0:
					break
			
			# Reset dictionary once it overflows
			if dict_size == 4096:
				dictionary = init_dict()
				dict_size = len(dictionary)
				
	with open(os.path.splitext(filename)[0],'w') as output:
		output.write(result.getvalue())
	
	return result.getvalue()

if __name__ == '__main__':
	#####
	# Usage is 'python main.py --root_dir input_data/'
	#####

	# Set up an argument parser for getting the root directory
	parser = argparse.ArgumentParser()
	parser.add_argument('--root_dir', help='root directory where LZW compressed files reside', type=str)
	args = parser.parse_args()

	# Move to root_dir for processing
	os.chdir(args.root_dir)

	# Enumerate files, deal in alphabetical order
	for filename in sorted(os.listdir(os.getcwd())):
		# Only operate on appropriate files
		if filename.endswith('.z'):			
			# Decompress the file and save the result
			print('Decompressing: {}'.format(filename))
			output = decompress(filename,12)
		else:
			continue

