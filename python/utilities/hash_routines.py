def hash(file_handle,hasher,block_size=65536):
	'''
	Incrementally read a file in blocks and update the hash.
	More memory efficient than reading the whole file in at once
	'''
        buffer = file_handle.read(block_size)
        while len(buffer)>0:
                hasher.update(buffer)
                buffer = file_handle.read(block_size)
        return hasher.digest()