'''
TO-DO
	Make the input url a commandline argument
'''

from bs4 import BeautifulSoup
import requests
import os

def has_extension(filename,extension):
	return os.path.splitext(filename)[1][1:].lower()==extension.lower()

def download_pdfs(webpage,root_dir=os.getcwd()):
	"""
	Extract all links from the page that end in a pdf file extension and download them
	"""
	os.chdir(root_dir) # Set where the files will be saved
	content = requests.get(webpage).content
	soup = BeautifulSoup(content)
	pdf_links = [i['href'] for i in soup.find_all('a') if has_extension(i['href'],'pdf')] # Find all pdf specific links

	for link in pdf_links: # Iterate through all pdfs, download and save
		try:
			page_data = requests.get('{}/{}'.format(webpage,link)).content
			pdf = open(link,'wb')
			pdf.write(page_data)
			pdf.close()
		except Exception as e:	# Primarily in case of broken links
			print("There was a problem with file: {}".format(link))
			print(e)
		
if __name__ == '__main__':
	page = r'http://wwwf.imperial.ac.uk/~bin06/M3A22/2015/2014/' # Example, can change it
	download_pdfs(page)