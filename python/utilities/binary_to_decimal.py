#!/usr/bin/python3

def binaryToDecimal(inputString):
	'''
	Breaks the input binary number string into pieces, reverses it for convenience
	and then iteratively increases the number value based on index exponentiation
	'''
	pieces = list(inputString)[::-1]
	number = 0;
	for index,value in enumerate(pieces):
		number += (2**index)*int(value)
	return number
	
if __name__ == '__main__':
	print(binaryToDecimal('1101')) # 13