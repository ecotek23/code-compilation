'''
FURTHER IMPROVEMENTS:
	add in logging to make debug output more structured for each download
	change some of the for loops into map calls? Not really necessary but maybe more concise
'''

from pprint import pprint as p
from bs4 import BeautifulSoup
import requests
import os

def soupify(url):
	soup = BeautifulSoup(requests.get(url).content,'lxml')
	return soup

def get_podcast_pages(url):
	soup = soupify(url)
	pages = [i['href'] for i in soup.find_all('a') if 'podcasts/podcasts-' in i['href']] # Find all the urls that match the required format
	return pages

def get_grid_items(url):
	soup = soupify(url)
	grid_items = soup.find_all('div', {'class':'fap-grid-item'})
	return grid_items

def save_data(url,name='cover.jpg'):
	with open(name,'wb') as output_file:
		output_file.write(requests.get(url).content)

def download_grid_item(grid_item):
	title = grid_item.find('h3').text # Find the title of this podcast
	print('Dealing with {}'.format(title))
	
	if os.path.isdir(title.replace('/',' -- ')): # Have already downloaded this podcast (or at least tried to, maybe extend for partial completion)
		return
	else: # Create and descend into directory
		os.mkdir(title.replace('/',' -- ')) # Deal with any titles having a / in their name. 
		os.chdir(title)
	try:
		# Download the title picture
		picture_url = grid_item.find('img')['src']
		save_data(picture_url)
	
		# Download the supplied pdf
		try: # Sometimes there's an error on the index, this try block is just a hack around this, since total completion isn't strictly necessary
			pdf_url = [i['href'] for i in grid_item.find_all('a') if i['href'].endswith('pdf')][0]
			save_data(pdf_url,name=os.path.basename(pdf_url))
		except:
			pass
		# Download the supplied mp3 file
		mp3_url = grid_item.find('a',{'class':'fap-single-track'})['href']
		save_data(mp3_url,name=os.path.basename(mp3_url))
	
	except: # Wrapped the whole thing in a try-except since I don't need all the material and don't mind a few breaking
		pass
		
	os.chdir('..') # Return to original directory
	
if __name__ == '__main__':
	url = 'https://russianpodcast.eu/'
	pages = get_podcast_pages(url)

	for page in pages:
		page_dir = os.path.basename(page) # Get page directory name

		if os.path.isdir(page_dir): # Check if it already exists, else create it
			pass
		else: 
			os.mkdir(os.path.basename(page))
		os.chdir(os.path.basename(page))

		grid_items = get_grid_items(page)

		for grid_item in grid_items: # Download each grid item on the current page
			download_grid_item(grid_item)
			
		os.chdir('..')
