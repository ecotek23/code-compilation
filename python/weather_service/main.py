#!/usr/bin/python3
import configparser
import argparse
import requests
import json

def get_api_key(conf_file):
	'''
	Retrieves the api key from the configuration file
	'''
	config = configparser.ConfigParser()
	config.read(conf_file)
	key = config.get('api','key')
	return key

def key_retrieval(args):
	'''
	If the key is not set from the command line, retrieve from configuration file
	'''
	if not args.api_key: # Read from the conf file if no key supplied on command line
		conf_file = 'app.conf_secure' # Ignored from my github commits, you need your own
		args.api_key = get_api_key(conf_file)

def data_retrieval(args):
	'''
	Get the weather data from the appropriate endpoint based on provided arguments
	'''
	city_format = 'http://api.openweathermap.org/data/2.5/weather?q={}&appid={}'
	country_format = 'http://api.openweathermap.org/data/2.5/weather?q={},{}&appid={}'
	
	if args.country_code: # Uses the appropriate endpoint based on input
		data = requests.get(country_format.format(args.city,args.country_code,args.api_key))
	else:
		data = requests.get(city_format.format(args.city,args.api_key))
	return data

if __name__ == '__main__':
	# Set up argument parser
	parser = argparse.ArgumentParser(description='Retrieve weather information for location')
	parser.add_argument('--city',default='Wolverhampton',type=str)
	parser.add_argument('--country_code',default='UK',type=str)
	parser.add_argument('--api_key',default='',type=str)
	args = parser.parse_args()

	# Retrieve kay, data and parse it
	key_retrieval(args)
	data = data_retrieval(args)
	parsed_data = json.loads(data.content.decode('UTF-8'))	
	
	# Display data in readable format before saving to weather.dat file
	print(json.dumps(parsed_data, indent=4, sort_keys=True))
	
	with open('weather.dat','w') as result:
		json.dump(parsed_data,result)
