from bs4 import BeautifulSoup

def get_links(filename):
	'''
	Parse a html bookmark file and get all anchor href values
	'''
	data = open(filename,'r').read()
	soup = BeautifulSoup(data,'lxml')
	
	for link in soup.findAll('a'):
		print(link['href'])

if __name__=='__main__':
	filenames = ['bookmarks_30_11_2016.html',
	             'bookmarks_29_11_2016.html']
	for filename in filenames:
		get_links(filename)
