#!/usr/bin/python3
import requests
import json

def display(data):
	for key,value in data.items():
		print(key,'\n\t','\n\t'.join(value),sep='')

if __name__=='__main__':
	headers = {
		'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.100 Safari/537.36'
	}
	page = 'http://www.reddit.com/.json'
	collections = {}
	
	data = json.loads(requests.get(page,headers=headers).content.decode('UTF-8'))

	if data.get('error'): # There was a problem fetching from the endpoint
		print(data['message'])
		exit(0)

	for child in data['data']['children']:
		if child['data']['subreddit'] in collections:
			collections[child['data']['subreddit']].append(child['data']['title'])
		else:
			collections[child['data']['subreddit']] = [child['data']['title']]

	display(collections)
