'''
Reads in the options from a data file and randomly prints them forever
'''

import random

def display_counts(lines_count):
	print()
	for name,value in lines_count.items():
		print('{}\t{}'.format(name,value))

if __name__ == '__main__':
	print('Enter q to quit',end='\n\n')
	
	lines = open('data','r').read().splitlines()
	lines_count = {k:0 for k in lines}
	
	while True:
		name = random.choice(lines)
		lines_count[name]+=1 
		print(name,': ',end='')
		q = input() # Move to the next or quit
		if q == 'q':
			break
			
	print('quiting...')
	display_counts(lines_count)