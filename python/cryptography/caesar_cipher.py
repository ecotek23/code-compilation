from string import maketrans, lowercase

class CaesarCipher:
	''' 
	Instantiates an object with a fixed key length
	(ROT13 by default)
	'''
	def __init__(self,key=13):
		self.key = key;
		self.encodeTable = maketrans(lowercase,lowercase[key:]+lowercase[:key]);
		self.decodeTable = maketrans(lowercase[key:]+lowercase[:key],lowercase);
	def encode(self,inputString):
		return inputString.translate(self.encodeTable);
	def decode(self,inputString):
		return inputString.translate(self.decodeTable);

if __name__=='__main__':
	cipher = CaesarCipher()
	encoded = cipher.encode('hello, world!')
	print encoded
	decoded = cipher.decode(encoded)
	print decoded