class VernamCipher:
	def __init__(self,key='test'):
		self.key = key;

	def encodeDecode(self,inputString):
		longKey = self.key * (len(inputString)/len(self.key)+1)
		return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(inputString,longKey))

if __name__=='__main__':
	cipher = VernamCipher()
	encoded = cipher.encodeDecode('The meaning of life is the number 42')
	print encoded
	decoded = cipher.encodeDecode(encoded)
	print decoded