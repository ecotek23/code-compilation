from string import lowercase, maketrans
import itertools

class VigenereCipher:
	def __init__(self,keyWord='cake'):
		'''
		Instantiates a cipher object with a specified key or the default, creates a list of encode and decode tables from the key
		'''
		self.encodeTables = []
		self.decodeTables = []
		for letter in keyWord:
			shiftLength = ord(letter) % 97
			self.encodeTables.append(maketrans(lowercase,lowercase[shiftLength:]+lowercase[:shiftLength]))
			self.decodeTables.append(maketrans(lowercase[shiftLength:]+lowercase[:shiftLength],lowercase))

	def encode(self,inputString):
		'''
		Uses itertools to create a cycle object of encryption tables
		'''
		encodedString = '';
		encodeIterator = itertools.cycle(self.encodeTables)
		for value in inputString:
			encodedString+=value.translate(next(encodeIterator))
		return encodedString

	def decode(self,inputString):
		'''
		Same as above except for cyclic decryption tables
		'''
		decodedString = ''
		decodeIterator = itertools.cycle(self.decodeTables)
		for value in inputString:
			decodedString+=value.translate(next(decodeIterator))
		return decodedString

if __name__ == '__main__':
	cipher = VigenereCipher()
	encoded = cipher.encode('hello, world!');
	print encoded
	print cipher.decode(encoded)