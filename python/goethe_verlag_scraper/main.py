'''
TO-DO
	add in another function to scrape the homepages for every language
'''

from bs4 import BeautifulSoup
import requests
import os

from pprint import pprint as p

def get_page_title(url):
	soup = BeautifulSoup(requests.get(url).content,'lxml')
	return soup.title.text

def get_topic_links(url):
	links = {}
	soup = BeautifulSoup(requests.get(url).content,'lxml')
	blocks = soup.find_all('td')
	for block in blocks:
		try:
			name = '{} --- {}'.format(block.find('a').text.strip(),block.find('span',{'class':'Stil36'}).text.strip())
			name = name.replace('/','-')
			links[name] = block.find('a')['href']
		except: # Skip any errors
			pass
	return links

def scrape_topic_page(url):
	data = {}
	
	soup = BeautifulSoup(requests.get(url).content,'lxml')
	table_blocks = soup.find_all('td',{'width':'164'})
	title = get_page_title(url).split(' | ')[1]

	for block in table_blocks:
		foreign = block.find('span',{'class':'Stil46'}).text.strip()
		english = block.find('span',{'class':'Stil36'}).text.strip()
		data[english] = foreign

	with open(title,'w') as data_output:
		for english in data:
			output = '{}, {}\n'.format(english,data[english])
			data_output.write(output)
	
	
if __name__=='__main__':
	sites = ['http://www.goethe-verlag.com/book2/_VOCAB/EM/EMRU/EMRU.HTM']

	for site in sites:
		# Follow a sensible directory structure for each language and topic
		title = get_page_title(site).replace('/','-')
		if os.path.isdir(title):
			pass
		else:
			os.mkdir(title)
		os.chdir(title)

		basis = os.path.dirname(site)+'/'

		# Get the links for each section for later iteration
		links = get_topic_links(site)

		for link in links:
			# Scrape the page and save the data
			current_url = '{}{}'.format(basis,links[link])
			scrape_topic_page(current_url)

		# Return to root directory at end
		os.chdir('..')