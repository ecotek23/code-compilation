from bs4 import BeautifulSoup
import requests

def soupify(url):
	'''
	Utility function for getting the page soup
	'''
	soup = BeautifulSoup(requests.get(url).content,'lxml')
	return soup

def get_spans(url):
	'''
	Get all the necessary spans on the page
	'''
	soup = soupify(url) 
	spans = soup.find_all('span',{'style':'font-family: Courier New, Courier, monospace;'})
	return spans

def piecify(spans):
	'''
	Break up each span and save corresponding translations in a dictionary
	'''
	translations = {}
	for span in spans[1:]: # First one is empty
		pieces = span.text.split() # Split and save the required pieces in a dictionary
		translations[pieces[1]] = pieces[3]
	return translations

def save_data(translations):
	'''
	Save the data in a csv format for later importing to fullrecall
	'''
	with open('output.csv','w') as output_file:
		for key,value in translations.items():
			output_file.write('{}, {}\n'.format(key,value))

if __name__ == '__main__':
	url = 'http://frequencylists.blogspot.co.uk/2015/12/the-2000-most-frequently-used-russian.html'
	spans = get_spans(url)
	translations = piecify(spans)
	save_data(translations)