## To-do
#
#	Run all the web requests through tor using STEM
#
##
from bs4 import BeautifulSoup
import itertools
import requests
import logging
import time
import os


def query(url):
	"""
	Uses pycurl to fetch a site using the proxy on the SOCKS_PORT.
	"""
	output = io.BytesIO()
	SOCKS_PORT = 9050

	query = pycurl.Curl()
	query.setopt(pycurl.URL, url)
	query.setopt(pycurl.PROXY, 'localhost')
	query.setopt(pycurl.PROXYPORT, SOCKS_PORT)
	query.setopt(pycurl.PROXYTYPE, pycurl.PROXYTYPE_SOCKS5_HOSTNAME)
	query.setopt(pycurl.WRITEFUNCTION, output.write)

	try:
		query.perform()
		return output.getvalue()
	except pycurl.error as exc:
		return "Unable to reach %s (%s)" % (url, exc)

def make_lesson_url(lesson_format,code_combination):
	'''
	Combine the code combination and format together to get the
	language combinations page
	'''
	return lesson_format.format(*code_combination)

def get_language_codes(main_page):
	'''
	Get all the language options available from the front page
	based on locale code
	'''
	codes = []
	
	soup = BeautifulSoup(requests.get(main_page).content,'lxml')
	selection_bar = soup.find(id='wordLanguage')
	selection_bar_options = selection_bar.find_all('option')

	for option in selection_bar_options:
		codes.append(option['value'])

	return codes

def get_lessons_urls(page_url):
	'''
	Get the urls of every lesson for a specific language combination
	'''
	lessons_urls = set([])
	
	page_soup = BeautifulSoup(requests.get(page_url).content,'lxml')
	anchor_elements = page_soup.find_all('a')
	for anchor in anchor_elements:
		if anchor.get('href','').startswith('lesson-'):
			lessons_urls.add(anchor['href'])
	return lessons_urls

def lesson_rip(base_url,lesson_fragment):
	'''
	Get the data for a specific lesson
	'''
	lesson_data = {'title':'','data':[]}
	
	page = '{}{}'.format(base_url,lesson_fragment)
	page_soup = BeautifulSoup(requests.get(page).content,'lxml')
	
	lesson_data['title'] = page_soup.title.text
	
	table = page_soup.find('table', {'class':'table table-bordered table-striped table-condensed'})
	trs = table.find_all('tr')[1:] # Get all rows, dropping header row
	for tr in trs:
		appended = 0
		line = ''
		tds = tr.find_all('td')
		if len(tds) != 2: # Skip if row not of correct type
			continue
		for td in tds:
			text = td.find_all('span',{'class':'big-text'})
			if text:
				line += text[0].text
				appended += 1
				if appended == 1:
					line += '\t\t\t' # Delimiter between languages
				else:
					line += '\n'
		lesson_data['data'].append(line)
	return lesson_data
	
def save_lesson_data(data):
	'''
	Save the data for a specific lesson
	'''
	file_handle = open(data['title'],'w')
	file_handle.writelines(data['data'])
	file_handle.close()

if __name__=='__main__':

	logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)

	main_page = 'http://www.internetpolyglot.com/'
	lesson_format = 'http://www.internetpolyglot.com/lessons-{}-{}'	
	
	if not os.path.isdir('./data'): # Ensure storage location in place
		logging.info('Creating ./data')
		os.mkdir('./data')
	os.chdir('./data')
	
	codes = get_language_codes(main_page)

	for code_combo in itertools.product(codes,codes):
		if code_combo[0] == code_combo[1]: # Ignore same combinations
			continue
		
		else: # Do rip
			language_dir = './{}-{}'.format(code_combo[0],code_combo[1])
			
			if not os.path.isdir(language_dir):
				logging.info('Creating {}'.format(language_dir))
				os.mkdir(language_dir)
			os.chdir(language_dir)
			
			lessons_url = make_lesson_url(lesson_format,code_combo)
			lessons_urls = get_lessons_urls(lessons_url)
			
			for lesson in lessons_urls:
				logging.info('\tDealing with {}'.format(lesson))
				data = lesson_rip(main_page,lesson)
				save_lesson_data(data)
				
				time.sleep(10)

		os.chdir('..') # Return to data dir for next combination
