from bs4 import BeautifulSoup
import requests

def get_last_full_prep(rows,index):
	"""
	Account for the strange table formatting, need to find the last available preposition
	which encompasses the row under examination
	"""
	while len(rows[index].find_all('td'))!=3:
		index-=1
	return index;

if __name__ == '__main__':
	site = 'http://www.study-languages-online.com/grammar/tables/prepositions-cases'

	soup = BeautifulSoup(requests.get(site).content,'lxml')
	rows = soup.find_all('tr')

	with open('prepositions.csv','w') as output_file:
		# Ignore the first row since it's just table headers
		for index,row in enumerate(rows[1:]):
			index+=1
			output = ""

			# Initialize the last proper index for a preposition, then find out whether
			# there is actually one to be found on the current row, or else backtrack
			last_index = index
			if len(row.find_all('td'))!=3:
				last_index = get_last_full_prep(rows,index)

			# Add the preposition as the first part of output
			output += rows[last_index].find_all('td')[0].text

			# Deal with the rest of the row, depending upon whether it has length 2 or 3
			if last_index == index:
				output += ' ({}), "{}"'.format(row.find_all('td')[1].text,row.find_all('td')[2].text)
			else:
				output += ' ({}), "{}"'.format(row.find_all('td')[0].text,row.find_all('td')[1].text)

			output_file.write('{}\n'.format(output))