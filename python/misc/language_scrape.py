#!/usr/bin/python3
import requests
import bs4

site = "https://en.wikipedia.org/wiki/List_of_languages_in_the_Eurovision_Song_Contest"
soup = bs4.BeautifulSoup(requests.get(site).content,"lxml")

table_lines = soup.findAll('tr')
language_lines = [i.findAll('td') for i in table_lines if len(i.findAll('td'))==6]
for i in language_lines:
	if i[1].text:
		print(i[1].text)