#!/usr/bin/python3
def collatz(inputValue):
	print(inputValue)
	if inputValue==1:
		return
	elif inputValue%2==0:
		collatz(inputValue//2)
	else:
		collatz(3*inputValue+1)

if __name__=='__main__':
	inputValue = 100
	print('Running for value: {0}'.format(inputValue))
	collatz(inputValue)