#include <stdio.h>

void bubble_sort(int *array, int array_size) {
	int tmp;
	for (int upper=array_size; upper>0; upper--) {
		for (int lower=0; lower<upper; lower++) {
			if (array[lower]>array[lower+1]) {
				// Swap array values
				tmp = array[lower];
				array[lower] = array[lower+1];
				array[lower+1] = tmp;
			}
		}
	}
}

int main() {
	// Initialize and display array
	int test[] = {4,5,3,2,1};
	printf("Original:\n\t");
	for (int i=0;i<5;i++) {
		printf("%d ",test[i]);
	}
	printf("\n");

	// Sort and display sorted array
	bubble_sort(test,5);
	printf("Sorted:\n\t");
	for (int i=0;i<5;i++) {
		printf("%d ",test[i]);
	}
	printf("\n");

	return 0;
}