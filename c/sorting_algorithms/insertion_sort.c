#include <stdio.h>

void insertion_sort(int *array, int array_size) {
	int j,tmp;
	for (int i=1;i<array_size;i++) {
		j=i;
		while (j>0 && array[j-1]>array[j]) {
			// Swap the array values and decrement the tracking index j
			tmp = array[j-1];
			array[j-1] = array[j];
			array[j] = tmp;
			j -= 1;
		}
	}
}

int main() {
	// Initialize and display
	int test[] = {5,4,3,2,1};
	printf("Original:\n\t");
	for (int i=0;i<5;i++) {
		printf("%d ",test[i]);
	}
	printf("\n");

	// Sort and display sorted array
	insertion_sort(test,5);
	printf("Sorted:\n\t");
	for (int i=0;i<5;i++) {
		printf("%d ",test[i]);
	}
	printf("\n");

	return 0;
}