// Creates a segfault since memory not allocated, refactor

#include <stdio.h>

int main( void ) {
	int count;
	printf("Enter the number of people you want to document: ");
	scanf("%d",&count);

	char *names[count], *numbers[count];

	for (int i=0;i<count;i++) {
		printf("Enter a name: "); scanf("%s",names[i]);
		printf("Enter a phone number: "); scanf("%s",numbers[i]);
	}

	printf("%s\n",numbers[0]);
	
	return 0;
}