#include <stdio.h>

int min(int *array,int lower_index, int upper_index) {
	int current_index = lower_index;
	for (int i=lower_index+1;i<upper_index;i++) {
		if (array[current_index] > array[i]) {
			current_index = i;
		}
	}
	return current_index;
}

void selection_sort(int *array, int array_length) {
	int current_min_index, temp;
	for (int i=0;i<array_length;i++) {
		current_min_index = min(array,i,array_length);
		if (current_min_index==i) { // Already at min for this pass, skip swap
			continue;
		} else { // Swap the two values
			temp = array[i];
			array[i] = array[current_min_index];
			array[current_min_index] = temp;
		}
	}
}

int main( void ) {
	int num[] = {57,48,79,65,15,33,52};
	
	printf("Unsorted:\n");
	for (int i=0;i<sizeof(num)/sizeof(int);i++) {
		printf("%d\n",num[i]);
	}
	
	printf("\nSorted:\n");
	selection_sort(num,sizeof(num)/sizeof(int));
	for (int i=0;i<sizeof(num)/sizeof(int);i++) {
		printf("%d\n",num[i]);
	}
	
	return 0;
}