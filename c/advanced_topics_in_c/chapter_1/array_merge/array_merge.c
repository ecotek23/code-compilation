#include <stdio.h>

void array_merge(int *a, int a_size, int *b, int b_size, int *c) {
	int a_marker = 0;
	int b_marker = 0;

	while ((a_marker<a_size) && (b_marker<b_size)) {
		if (a[a_marker]<b[b_marker]) {
			c[a_marker+b_marker] = a[a_marker];
			a_marker += 1;
		} else {
			c[a_marker+b_marker] = b[b_marker];
			b_marker += 1;
		}
	}

	if (a_marker == a_size) { // Finished copying a over so copy the rest of b
		for (;b_marker<b_size;b_marker++) {
			c[a_marker+b_marker] = b[b_marker];
		}
	} else { // Finished copy b over so copy the rest of a
		for (;a_marker<a_size;b_marker++) {
			c[a_marker+b_marker] = a[a_marker];
		}
	}
}

int main( void ) {
	int a[] = {1,3,5};
	int b[] = {2,4,6};
	int a_size = 3;
	int b_size = 3;
	int c[a_size+b_size];

	array_merge(a,a_size,b,b_size,c);

	printf("Merged array:\n");
	for (int i=0;i<a_size+b_size;i++) {
		printf("%d\n",c[i]);
	}
	
	return 0;
}