#include <stdio.h>

void insertion_sort(int *array, int array_size) {
	int key,cur,temp;
	for(int i=1;i<array_size;i++) {
		key = array[i];
		cur = i-1;
		while ((key < array[cur]) && (cur >= 0)) {
			temp = array[cur];
			array[cur] = key;
			array[cur+1] = temp;
			cur -= 1;
		}
	}
}

int main( void ) {
	int num[] = {57,48,79,65,15,33,52};
	
	printf("Unsorted:\n");
	for (int i=0;i<sizeof(num)/sizeof(int);i++) {
		printf("%d\n",num[i]);
	}
	
	printf("\nSorted:\n");
	insertion_sort(num,sizeof(num)/sizeof(int));
	for (int i=0;i<sizeof(num)/sizeof(int);i++) {
		printf("%d\n",num[i]);
	}
	
	return 0;
}
