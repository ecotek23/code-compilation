#include <stdio.h>

int binary_search(int *array, int N, int val) {
	int lower=0,upper=N,av;
	while (lower < upper) {
		printf("lower: %d\nupper: %d\n\n",lower,upper);
		av = (lower+upper)/2;
		if (array[av]==val) {
			return av;
		} else {
			if (lower == av) { // No longer moving 
				break;
			} else if (array[av] < val) { // Cutting off lower half
				lower = av;
			} else { // Cutting off upper half
				upper = av;
			}
		}
	}
	return -1;
}

int main( void ) {
	int num[] = {15,30,45,60,75,90};

	printf("Value is at index: %d\n",binary_search(num,sizeof(num)/sizeof(int),75));
	
	return 0;
}
