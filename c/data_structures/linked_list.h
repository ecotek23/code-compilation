
// Node type declaration
typedef struct node {
	int data;
	struct node *next;
} Node;


// linked list type declaration
typedef struct linked_list {
	Node *head;
	Node *tail;
} LL;

// Necessary functions prototypes
int has_next(Node *current);
Node* get_next(Node *current);
void initialize_linked_list(LL *list, int val);
void append(LL *list, int val);
void insert(LL *list, int index, int val);
void prepend(LL *list, int val);
void alter_value(LL *list, int index, int val);
void delete(LL *list, int index);
int find(LL *list, int val);
int traverse(LL list);
