/* TO-DO
 * Factor out coding for creating a node
 * 		FUNCTIONS TO ADD:
 * 			DELETE A NODE
 * 			REVERSING THE LIST
 */
#include "linked_list.h"
#include <stdlib.h>
#include <stdio.h>

int has_next(Node *current) {
	return ( (current->next!=NULL) ? 1 : 0 );
}

Node* get_next(Node *current) {
	return current->next;
}

void initialize_linked_list(LL *list, int val) {
	// Create head element
	Node *head = (Node*)malloc(sizeof(Node));
	head->data = val;
	head->next = NULL;
	
	// Initially set head and tail to be the same, since only one element
	(*list).head = head;
	(*list).tail = head;
}

void append(LL *list, int val) {
	// Allocate space and assign values
	Node *new = (Node*)malloc(sizeof(Node));
	new->data = val;
	new->next = NULL;
	
	// Redirect current tail, and then set the new tail
	list->tail->next = new;
	list->tail = new;
}

void insert(LL *list, int index, int val) {
	if (index==0) { // Just change the head
		Node *new = (Node*)malloc(sizeof(Node));
		new->data = val;
		new->next = list->head;
		list->head = new;
	} else { // Cycle through and then insert the new value
		Node *current = list->head;
		int current_index = 0;
		while (1) {
			if (current_index == index-1) {
				Node *new = (Node*)malloc(sizeof(Node));
				new->data = val;
				new->next = current->next;
				current->next = new;
				break;
			}
			current_index++;
			current = current->next;
		}
	}
}

void prepend(LL *list, int val) { // Just insert a value at the beginning
	insert(list,0,val);
}

void alter_value(LL *list, int index, int val) {
	int current_index = 0;
	Node *current_node = list->head;
	while (current_index != index) {
		current_index++;
		current_node = current_node->next;
	}
	current_node->data = val;
}

void delete(LL *list, int index) {
	/*
	 * Account for deleting the head, then do the other cases
	 */
	int current_index = -1;
	Node *current_node = list->head;
	if (index==0) { // Delete the head
		Node *head_node = list->head;
		list->head = list->head->next;
		free(head_node);
	} else { // NOT DONE YET ADD SOME MORE HERE /////////////////////
		while (current_index != index-1) {
			current_index++;
			current_node = current_node->next;
		}
		current_node->next = current_node->next->next;
	
		// Free up the memory that is deleted from the list
		free(current_node->next);
	}
}

int find(LL *list, int val) {
	int index = 0;
	Node *current = list->head;
	while (current->data != val) {
		if (current->next == NULL) { // Reached end without finding the value
			index = -1;
			break;
		} else {
			index++;
			current = current->next;
		}
	}
	return index;
}	

int traverse(LL list) {
	printf("Traversing: ");
	// Set up current variable to examine each iteration
	Node *current = list.head;
	
	// Iterate through the list until there is no next value
	while (1) {
		printf("%d ",current->data);
		if (current->next == NULL) {
			break;
		} else {
			current = get_next(current);
		}
	}
	printf("\n");
}

int main() {
	// Declare and initialize the linked_list
	LL list;
	initialize_linked_list(&list,0);
	
	// Append new values
	for (int i=1;i<10;i++) {
		append(&list,i);
	}
	
	// Test traversing the list
	traverse(list);
	
	// Test finding a value
	printf("3 is located at index: %d\n",find(&list,3));
	printf("100 is located at index: %d\n",find(&list,100));
	
	// Prepend and traverse again
	prepend(&list,-1);
	traverse(list);
	
	// Insert and traverse again
	insert(&list,4,100);
	traverse(list);
	
	// Alter value and traverse again
	alter_value(&list,0,23);
	traverse(list);
	
	// Delete a node and traverse again
	delete(&list,0);
	traverse(list);
	
	return 0;
}
