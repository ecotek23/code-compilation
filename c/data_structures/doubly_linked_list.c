#include <stdlib.h>
#include <stdio.h>

typedef struct node {
	struct node *previous;
	int data;
	struct node *next;
} Node;

typedef struct doubly_linked_list {
	struct node *head;
	struct node *tail;
} DLL;

int has_previous(Node *input) {
	return ((input->previous!=NULL)?1:0);
}

int has_next(Node *input) {
	return ((input->next!=NULL)?1:0);
}

Node* get_previous(Node *input) {
	return input->previous;
}

Node* get_next(Node *input) {
	return input->next;
}

void initialize_list(DLL *list, int val) {
	Node *head = (Node*)malloc(sizeof(Node));
	head->data = val;
	head->previous = NULL;
	head->next = NULL;

	list->head = head;
	list->tail = head;
}

int main() {
	return 0;
}