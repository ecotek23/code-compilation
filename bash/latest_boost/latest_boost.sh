#!/bin/sh
# Downloads boost v1.63.0
wget -q https://sourceforge.net/projects/boost/files/boost/1.63.0/boost_1_63_0.tar.gz && \
tar -zxf boost_1_63_0.tar.gz && \
rm boost_1_63_0.tar.gz